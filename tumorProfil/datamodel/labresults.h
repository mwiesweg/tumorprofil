/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABRESULTS_H
#define LABRESULTS_H

#include <QDate>
#include <QDebug>
#include <QList>
#include <QString>
#include <QVariant>
#include <QVariantList>
#include <QVector>

// Primary storage structures

class LabValue
{
public:
    LabValue() {}
    LabValue(const QString& name, const QVariant& value) : name(name), value(value) {}
    QString  name;
    QVariant value; // Can be of Type Double (or Int on input, converted then to Double), String or ByteArray
};
QDebug operator<<(QDebug debug, const LabValue &s);

// Utility
class LabValueVector : public QVector<LabValue>
{
public:
    LabValueVector() {}
    LabValueVector(const QVector<LabValue>& v) : QVector<LabValue>(v) {}

    int find(const QString& name) const;
    QVariant valueForName(const QString& name) const;
    void edit(const LabValue& value);
    void edit(const QString& name, const QVariant& value);
    QSet<QString> names() const;
};

class LabSeries
{
public:
    LabSeries() {}
    LabSeries(const QDateTime& dateTime, const QString& type, const QVector<LabValue>& values = QVector<LabValue>())
        : dateTime(dateTime), type(type), values(values) {}
    QDateTime dateTime;
    QString   type;
    LabValueVector values;

    QString dateLabel() const;

    static QString clinicalType();
};
QDebug operator<<(QDebug debug, const LabSeries &s);

// Secondary return value structures

class LabDateValuePair
{
public:
    LabDateValuePair() {}
    LabDateValuePair(const QDateTime& dateTime, const QVariant& value) : dateTime(dateTime), value(value) {}
    QDateTime dateTime;
    QVariant  value;

    bool isNull() const { return dateTime.isNull(); }
};
QDebug operator<<(QDebug debug, const LabDateValuePair&s);

class LabSetForName : public QVector<LabDateValuePair>
{
public:
    QString name;
};

class LabSetForDate : public QVector<LabValue>
{
public:
    QDateTime         dateTime;
};

// Utility
class LabSeriesVector : public QVector<LabSeries>
{
public:
    LabSeriesVector() {}
    LabSeriesVector(const QVector<LabSeries>& v) : QVector<LabSeries>(v) {}

    LabSeriesVector& operator <<(const LabSeries& series) { append(series); return *this; }
    LabSeriesVector& operator +=(const LabSeries& series) { append(series); return *this; }
    void append(const LabSeries& series) { add(series); }
    /// Add series, or merges if same entry at same date present
    int add(const LabSeries& series);
    /// Simulates add(). Returns: 1) Index where series will be added or merged 2) if series will be added (true) or merged (false)
    QPair<int, bool> simulateAdd(const LabSeries& series);

    /// The entry for the given date, time and type. Returns -1 if not found.
    int find(const QDateTime& dateTime, const QString& type) const;
    /// The index of the first entry on given date (will be the midnight entry if present)
    /// If not found, returns -1.
    int findFirst(const QDate& date) const;
    /// The index if the first entry for the given date and time. Returns -1 if not found.
    int findFirst(const QDateTime& dateTime) const;
    /// The index such that dateTime <= all subsequent entries.
    /// Always returns 0 <= i <= size().
    /// Check for i < size() before accessing the element i.
    int indexNotGreater(const QDateTime& dateTime) const;

    QSet<QString> names() const;
};

// Main container structure

class LabResults
{
public:
    LabSeriesVector series;

    LabSetForDate valuesForDate(const QDate& date) const;
    LabSetForDate valuesForDateTime(const QDateTime& date) const;
    LabSetForName valuesForName(const QString& name) const;
    LabSetForName valuesForName(const QString& name, const QDate& from, const QDate& to) const;
    LabDateValuePair valueForNameAt(const QString& name, const QDate& date, const QDate&min, const QDate& max) const;
    LabDateValuePair valueForNameAt(const QString& name, const QString& dateSpec) const;
    QList<QDateTime> dateTimes() const;
    QDateTime earliest() const;
    QDateTime latest() const;
    void edit(const QDateTime&, const QString& type, const QString& name, const QVariant& value);
    void edit(const QDateTime&, const QString& type, const LabValue& value);

    static QMap<QString, QString> medicoAliases();
    static QPair<QStringList, QMap<QString, QStringList> > masks();
    static QMap<QString, QVariant::Type> types();

};
QDebug operator<<(QDebug debug, const LabResults &s);

#endif // LABRESULTS_H
