/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labresults.h"

#include <algorithm>

#include <QFile>
#include <QLocale>
#include <QTextStream>

QString LabSeries::dateLabel() const
{
    if (dateTime.time() == QTime(0,0,0,0))
    {
        return dateTime.toString(QObject::tr("dd.MM.yyyy"));
    }
    else
    {
        return dateTime.toString(QObject::tr("dd.MM.yyyy hh:mm"));
    }
}

QString LabSeries::clinicalType()
{
    return "clinical";
}

LabSetForDate LabResults::valuesForDate(const QDate &date) const
{
    return valuesForDateTime(date.startOfDay());
}

LabSetForDate LabResults::valuesForDateTime(const QDateTime &dateTime) const
{
    LabSetForDate set;
    set.dateTime = dateTime;
    bool validTime = dateTime.time() != QTime(0,0,0,0);
    foreach (const LabSeries& s, series)
    {
        if (validTime ? s.dateTime == dateTime : s.dateTime.date() == dateTime.date())
        {
            set += s.values;
        }
    }
    return set;
}

LabSetForName LabResults::valuesForName(const QString &name) const
{
    LabSetForName set;
    set.name = name;
    set.reserve(series.size());
    foreach (const LabSeries& s, series)
    {
        foreach (const LabValue& value, s.values)
        {
            if (value.name == name)
            {
                set << LabDateValuePair(s.dateTime, value.value);
            }
        }
    }
    return set;
}

LabSetForName LabResults::valuesForName(const QString &name, const QDate &from, const QDate &to) const
{
    if (name.isEmpty() || from.isNull() || to.isNull())
    {
        return LabSetForName();
    }

    LabSetForName set;
    set.name = name;
    QDateTime fromTime = from.startOfDay();
    QDateTime toTime = to.startOfDay().addDays(1); // midnight of next day, to allow "<="
    foreach (const LabSeries& s, series)
    {
        if (s.dateTime >= fromTime && s.dateTime < toTime)
        {
            foreach (const LabValue& value, s.values)
            {
                if (value.name == name)
                {
                    set << LabDateValuePair(s.dateTime, value.value);
                }
            }
        }
    }
    return set;
}

LabDateValuePair LabResults::valueForNameAt(const QString &name, const QDate &date, const QDate &min, const QDate &max) const
{
    // a very efficient implementation would optimize this intermediate step
    LabSetForName candidates = valuesForName(name, min, max);

    if (candidates.isEmpty())
    {
        return LabDateValuePair();
    }

    QDateTime target = QDateTime(date, QTime(8, 0, 0)); // take the morning of the target day
    LabDateValuePair& best = candidates.first();
    foreach (const LabDateValuePair& next, candidates)
    {
        if (qAbs(next.dateTime.secsTo(target)) < qAbs(best.dateTime.secsTo(target)))
        {
            best = next;
        }
    }
    return best;
}

LabDateValuePair LabResults::valueForNameAt(const QString &name, const QString &dateSpec) const
{
    if (name.isEmpty())
    {
        return LabDateValuePair();
    }

    if (dateSpec == "first")
    {
        for (LabSeriesVector::const_iterator it = series.constBegin(); it != series.end(); ++it)
        {
            foreach (const LabValue& value, it->values)
            {
                if (value.name == name)
                {
                    return LabDateValuePair(it->dateTime, value.value);
                }
            }
        }
    }
    else if (dateSpec == "last")
    {
        for (LabSeriesVector::const_reverse_iterator it = series.rbegin(); it != series.rend(); ++it)
        {
            foreach (const LabValue& value, it->values)
            {
                if (value.name == name)
                {
                    return LabDateValuePair(it->dateTime, value.value);
                }
            }
        }
    }
    return LabDateValuePair();
}

QList<QDateTime> LabResults::dateTimes() const
{
    QList<QDateTime> dateTimes;
    foreach (const LabSeries& s, series)
    {
        if (!dateTimes.isEmpty() && dateTimes.last() == s.dateTime)
        {
            continue;
        }
        dateTimes << s.dateTime;
    }
    return dateTimes;
}

QDateTime LabResults::earliest() const
{
    QDateTime earliest;
    foreach (const LabSeries& s, series)
    {
        if (!earliest.isValid() || s.dateTime < earliest)
        {
            earliest = s.dateTime;
        }
    }
    return earliest;
}

QDateTime LabResults::latest() const
{
    QDateTime latest;
    foreach (const LabSeries& s, series)
    {
        if (!latest.isValid() || s.dateTime > latest)
        {
            latest = s.dateTime;
        }
    }
    return latest;
}

void LabResults::edit(const QDateTime &dateTime, const QString &type, const QString &name, const QVariant &value)
{
    edit(dateTime, type, LabValue(name, value));
}

void LabResults::edit(const QDateTime &dateTime, const QString& type, const LabValue &value)
{
    int index = series.find(dateTime, type);
    if (index == -1)
    {
        index = series.add(LabSeries(dateTime, type, QVector<LabValue>() << value));
    }
    else
    {
        series[index].values.edit(value);
    }
}

QMap<QString, QString> LabResults::medicoAliases()
{
    QMap<QString, QString> aliases;
    QFile maskFile(":/medical/lab-medico-aliases");
    if (maskFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&maskFile);
        stream.setCodec("UTF-8");
        QString line;
        while ( !stream.atEnd() )
        {
            line = stream.readLine();
            QStringList names = line.split(' ', Qt::SkipEmptyParts);
            if (names.isEmpty())
            {
                continue;
            }
            for (QStringList::iterator it = names.begin(); it != names.end(); ++it)
            {
                it->replace('_', ' ');
            }
            QString name = names.takeFirst();
            foreach (const QString& alias, names)
            {
                aliases[alias] = name;
            }
        }
    }
    else
    {
        qDebug() << "Failed to open lab-medico-aliases resource file!";
    }
    return aliases;
}

QPair<QStringList, QMap<QString, QStringList> > LabResults::masks()
{
    QMap<QString, QStringList> masks;
    QStringList maskNames;
    QFile maskFile(":/medical/lab-masks");
    if (maskFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&maskFile);
        stream.setCodec("UTF-8");
        QString line;
        while ( !stream.atEnd() )
        {
            line = stream.readLine();
            QStringList names = line.split(' ', Qt::SkipEmptyParts);
            if (names.isEmpty())
            {
                continue;
            }
            for (QStringList::iterator it = names.begin(); it != names.end(); ++it)
            {
                it->replace('_', ' ');
            }
            QString name = names.takeFirst();
            maskNames << name;
            masks[name] = names;
        }
    }
    else
    {
        qDebug() << "Failed to open lab-masks resource file!";
    }
    return qMakePair(maskNames, masks);
}

QMap<QString, QVariant::Type> LabResults::types()
{
    QMap<QString, QVariant::Type> types;
    QFile typesFile(":/medical/lab-types");
    if (typesFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&typesFile);
        stream.setCodec("UTF-8");
        QString line;
        while ( !stream.atEnd() )
        {
            line = stream.readLine();
            QStringList names = line.split(' ', Qt::SkipEmptyParts);
            if (names.isEmpty())
            {
                continue;
            }
            for (QStringList::iterator it = names.begin(); it != names.end(); ++it)
            {
                it->replace('_', ' ');
            }
            QString typeName = names.takeFirst();
            QVariant::Type type;
            if (typeName == "float")
                type = QVariant::Double;
            else if (typeName == "int")
                type = QVariant::Int;
            else if (typeName == "string")
                type = QVariant::String;
            else if (typeName == "bool")
                type = QVariant::Bool;
            else
            {
                qDebug() << "Unknown type:" << typeName;
                continue;
            }
            foreach (const QString& name, names)
            {
                types[name] = type;
            }
        }
    }
    else
    {
        qDebug() << "Failed to open lab-masks resource file!";
    }
    return types;
}

int LabSeriesVector::add(const LabSeries &series)
{
    int index = indexNotGreater(series.dateTime);
    for (int i=index; i<size() && at(i).dateTime == series.dateTime; i++)
    {
        if (at(i).type == series.type)
        {
            // merge
            operator [](i).values += series.values;
            return i;
        }
    }
    insert(index, series);
    return index;
}

QPair<int, bool> LabSeriesVector::simulateAdd(const LabSeries &series)
{
    int index = indexNotGreater(series.dateTime);
    for (int i=index; i<size() && at(i).dateTime == series.dateTime; i++)
    {
        if (at(i).type == series.type)
        {
            // would merge
            return qMakePair(i, false);
        }
    }
    return qMakePair(index, true);
}

int LabSeriesVector::find(const QDateTime &dateTime, const QString &type) const
{
    int i = indexNotGreater(dateTime);
    for (; i<size() && at(i).dateTime == dateTime; i++)
    {
        if (at(i).type == type)
        {
            return i;
        }
    }
    return -1;
}

int LabSeriesVector::findFirst(const QDate &date) const
{
    int i = indexNotGreater(date.startOfDay());
    if (i < size() && at(i).dateTime.date() == date)
    {
        return i;
    }
    return -1;
}

int LabSeriesVector::findFirst(const QDateTime &dateTime) const
{
    int i = indexNotGreater(dateTime);
    if (i < size() && at(i).dateTime == dateTime)
    {
        return i;
    }
    return -1;
}

int LabSeriesVector::indexNotGreater(const QDateTime &dateTime) const
{
    int i=0;
    for (; i<size(); i++)
    {
        if (at(i).dateTime >= dateTime)
        {
            return i;
        }
    }
    return i;
}

QSet<QString> LabSeriesVector::names() const
{
    QSet<QString> names;
    foreach (const LabSeries& s, *this)
    {
        names += s.values.names();
    }
    return names;
}

int LabValueVector::find(const QString &name) const
{
    for (int i=0; i<size(); i++)
    {
        if (at(i).name == name)
        {
            return i;
        }
    }
    return -1;
}

QVariant LabValueVector::valueForName(const QString &name) const
{
    int i = find(name);
    if (i == -1)
    {
        return QVariant();
    }
    return at(i).value;
}

void LabValueVector::edit(const LabValue &value)
{
    int i = find(value.name);
    if (i == -1)
    {
        append(value);
    }
    else
    {
        operator[](i) = value;
    }
}

void LabValueVector::edit(const QString &name, const QVariant &value)
{
    edit(LabValue(name, value));
}

QSet<QString> LabValueVector::names() const
{
    QSet<QString> names;
    foreach (const LabValue& v, *this)
    {
        names += v.name;
    }
    return names;
}

QDebug operator<<(QDebug debug, const LabValue &s)
{
    debug.nospace().noquote() << "LabValue(" << s.name << ": " << s.value << ")";
    return debug.maybeSpace();
}

QDebug operator<<(QDebug debug, const LabSeries &s)
{
    debug.nospace().noquote() << "LabSeries (" << s.dateTime.toString(Qt::ISODate) << ", type " << s.type << "):" << Qt::endl;
    foreach (const LabValue& v, s.values)
    {
        debug.nospace().noquote() << "  " << v << Qt::endl;
    }

    return debug.maybeSpace();
}

QDebug operator<<(QDebug debug, const LabResults &r)
{
    debug.nospace().noquote() << "LabResults:" << Qt::endl;
    foreach (const LabSeries& s, r.series)
    {
        debug.nospace().noquote() << " " << s;
    }
    return debug.maybeSpace();
}


QDebug operator<<(QDebug debug, const LabDateValuePair &s)
{
    debug.nospace().noquote() << "LabDateValuePair (" << s.dateTime.toString(Qt::ISODate) << ": " << s.value << ")";
    return debug.maybeSpace();
}
