/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 21.02.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// Qt includes

#include <QObject>
#include <QRegExp>

// Local includes

#include "ihcscore.h"
#include "pathologypropertyinfo.h"
#include "property.h"


PathologyPropertyInfo::PathologyPropertyInfo()
    : property(InvalidProperty), valueType(InvalidCategory)
{
}

PathologyPropertyInfo::PathologyPropertyInfo(Property property, ValueTypeCategory valueType,
                                             const QString& id, const QString& label,
                                             const QString& detailLabel)
    : property(property), valueType(valueType), id(id), label(label), detailLabel(detailLabel)
{
}

PathologyPropertyInfo::PathologyPropertyInfo(Property property)
{
    *this = info(property);
}

enum ValueTypeCategory
{
    IHCClassical,
    IHCBoolean, // "<10%, niedrige Intensität"
    IHCBooleanPercentage,
    Fish,
    Mutation,
    StableUnstable
};

QString PathologyPropertyInfo::plainTextLabel() const
{
    if (label.contains('<'))
    {
        static QRegExp tagMatcher("<.+>");
        tagMatcher.setMinimal(true);
        QString plainLabel = label;
        plainLabel.replace(tagMatcher, "");
        return plainLabel;
    }
    return label;
}

bool PathologyPropertyInfo::isIHC() const
{
    return valueType == IHCClassical ||
           valueType == IHCClassicalPoints ||
           valueType == IHCBoolean ||
           valueType == IHCBooleanPercentage ||
           valueType == IHCTwoDim ||
           valueType == IHCHScore;
}

bool PathologyPropertyInfo::isCombined() const
{
    return valueType == BooleanCombination;
}

PathologyPropertyInfo PathologyPropertyInfo::info(Property property)
{
    switch (property)
    {
    case IHC_PTEN:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/pten", QObject::tr("PTEN"));
    case IHC_pAKT:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/p-akt?p=s473", QObject::tr("p-AKTₛ₄₇₃"));
    case IHC_pERK:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/p-erk", QObject::tr("p-ERK"));
    case IHC_ALK:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/alk", QObject::tr("ALK"));
    case IHC_HER2:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/her2?score=twodim", QObject::tr("HER2"));
    case IHC_HER2_DAKO:
        return PathologyPropertyInfo(property, IHCClassical, "ihc/her2", QObject::tr("HER2 DAKO"));
    case IHC_ER:
        return PathologyPropertyInfo(property, IHCClassical, "ihc/er", QObject::tr("ER"));
    case IHC_PR:
        return PathologyPropertyInfo(property, IHCClassical, "ihc/pr", QObject::tr("PR"));
    case IHC_cMET:
        return PathologyPropertyInfo(property, IHCHScore, "ihc/c-met", QObject::tr("cMet"));
    case IHC_ROS1:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/ros1", QObject::tr("ROS1"));
    case IHC_PD1:
        return PathologyPropertyInfo(property, IHCHScore, "ihc/pd1", QObject::tr("PD-1"));
    case IHC_PDL1:
        return PathologyPropertyInfo(property, IHCHScore, "ihc/pdl1", QObject::tr("PD-L1"));
    case IHC_PDL1_immunecell:
        return PathologyPropertyInfo(property, IHCBoolean, "ihc/pdl1-immunecell", QObject::tr("PD-L1 Immunzellen"));
    case IHC_panTRK:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/pantrk", QObject::tr("panTRK"));
    case Fish_ALK:
        return PathologyPropertyInfo(property, Fish, "fish/alk", QObject::tr("ALK-Translokation"), QObject::tr("Prozentsatz:"));
    case Fish_HER2:
        return PathologyPropertyInfo(property, Fish, "fish/her2", QObject::tr("HER2-Amplifikation"), QObject::tr("Ratio HER2/CEP7:"));
    case Mut_KRAS_2:
        return PathologyPropertyInfo(property, Mutation, "mut/kras?exon=2", QObject::tr("KRAS Exon 2"));
    case Mut_EGFR_19_21:
        return PathologyPropertyInfo(property, Mutation, "mut/egfr?exon=19,21", QObject::tr("EGFR Exon 19 & 21"));
    case Mut_PIK3CA:
        // UPDATE PathologyProperties SET property='mut/pik3ca' WHERE property='mut/pik3ca?exon=10,21';
        return PathologyPropertyInfo(property, Mutation, "mut/pik3ca", QObject::tr("PIK3CA"));
    case Mut_BRAF_15:
        return PathologyPropertyInfo(property, Mutation, "mut/braf?exon=15", QObject::tr("BRAF Exon 15"));
    case Mut_EGFR_18_20:
        return PathologyPropertyInfo(property, Mutation, "mut/egfr?exon=18,20", QObject::tr("EGFR Exon 18 & 20"));
    case Mut_KRAS_3:
        return PathologyPropertyInfo(property, Mutation, "mut/kras?exon=3", QObject::tr("KRAS Exon 3"));
    case Mut_KRAS_4:
        return PathologyPropertyInfo(property, Mutation, "mut/kras?exon=4", QObject::tr("KRAS Exon 4"));
    case Mut_NRAS_2_4:
        return PathologyPropertyInfo(property, Mutation, "mut/nras?exon=2-4", QObject::tr("NRAS Exon 2-4"));
    case Mut_BRAF_11:
        return PathologyPropertyInfo(property, Mutation, "mut/braf?exon=11", QObject::tr("BRAF Exon 11"));
    case Fish_FGFR1:
        return PathologyPropertyInfo(property, Fish, "fish/fgfr1", QObject::tr("FGFR1-Amplifikation"), QObject::tr("Ratio FGFR1/CEP8:"));
    case Fish_PIK3CA:
        return PathologyPropertyInfo(property, Fish, "fish/pik3ca", QObject::tr("PIK3ca-Amplifikation"));
    case Fish_cMET:
        return PathologyPropertyInfo(property, Fish, "fish/c-met", QObject::tr("cMET-Amplifikation"));
    case Fish_ROS1:
        return PathologyPropertyInfo(property, Fish, "fish/ros1", QObject::tr("ROS1-Rearrangement"));
    case Fish_RET:
        return PathologyPropertyInfo(property, Fish, "fish/ret", QObject::tr("RET-Rearrangement"));
    case Mut_DDR2:
        return PathologyPropertyInfo(property, Mutation, "mut/ddr2?exon=15-18", QObject::tr("DDR2 Exon 15-18"));
    case Mut_PTEN:
        return PathologyPropertyInfo(property, Mutation, "mut/pten", QObject::tr("PTEN"));
    case IHC_pP70S6K:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/p-p70S6k", QObject::tr("p-p70S6K"));
    case IHC_MLH1:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/mlh1", QObject::tr("MLH1"));
    case IHC_MSH2:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/msh2", QObject::tr("MSH2"));
    case IHC_MSH6:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/msh6", QObject::tr("MSH6"));
    case IHC_PMS2:
        return PathologyPropertyInfo(property, IHCTwoDim, "ihc/pms2", QObject::tr("PMS2"));
    case MSI_Panel:
        return PathologyPropertyInfo(property, MSI, "msi/panel", QObject::tr("MSI PCR"));
    case PCR_D5S346:
        return PathologyPropertyInfo(property, StableUnstable, "msi/d5s346", QObject::tr("MSI PCR: D5S346"));
    case PCR_BAT26:
        return PathologyPropertyInfo(property, StableUnstable, "msi/bat26", QObject::tr("MSI PCR: BAT26"));
    case PCR_BAT25:
        return PathologyPropertyInfo(property, StableUnstable, "msi/bat25", QObject::tr("MSI PCR: BAT25"));
    case PCR_D17S250:
        return PathologyPropertyInfo(property, StableUnstable, "msi/d17s250", QObject::tr("MSI PCR: D17S250"));
    case PCR_D2S123:
        return PathologyPropertyInfo(property, StableUnstable, "msi/d2s123", QObject::tr("MSI PCR: D2S123"));
    case Comb_HER2:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/her2", QObject::tr("HER2 positiv"));
    case Comb_HormoneReceptor:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/er_pr", QObject::tr("HR positiv"));
    case Comb_TripleNegative:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/triplenegative", QObject::tr("Triple-neg."));
    case Comb_cMetHighAmplification:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/met-amp-high", QObject::tr("High-level MET Amplifikation"));
    case Comb_cMetIHC3plusScore:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/c-met/ihc3+score", QObject::tr("cMet IHC 3+ Score"));
    case Comb_RASMutation:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/ras-mutation", QObject::tr("RAS Mutation"));
    case Comb_RASMutationRelaxed:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/ras-mutation-relaxed", QObject::tr("RAS Mutation"));
    case Comb_RASMutationStrict:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/ras-mutation-strict", QObject::tr("RAS Mutation"));
    case Comb_KRASMutation:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/kras-mutation", QObject::tr("KRAS Mutation"));
    case Comb_PDL1High:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/pdl1-high", QObject::tr("PD-L1 TPS≥50%"));
    case Comb_MMRDeficiency:
        return PathologyPropertyInfo(property, BooleanCombination, "combination/mmr-deficiency", QObject::tr("MMR Defizienz"));
    case  Mut_ERBB2:
        return PathologyPropertyInfo(property, Mutation, "mut/erbb2", QObject::tr("ERBB2"));
    case  Mut_FGFR1:
        return PathologyPropertyInfo(property, Mutation, "mut/fgfr1", QObject::tr("FGFR1"));
    case  Mut_FGFR2:
        return PathologyPropertyInfo(property, Mutation, "mut/fgfr2", QObject::tr("FGFR2"));
    case  Mut_FGFR3:
        return PathologyPropertyInfo(property, Mutation, "mut/fgfr3", QObject::tr("FGFR3"));
    case  Mut_FGFR4:
        return PathologyPropertyInfo(property, Mutation, "mut/fgfr4", QObject::tr("FGFR4"));
    case  Mut_HRAS_2_4:
        return PathologyPropertyInfo(property, Mutation, "mut/hras?exon=2-4", QObject::tr("HRAS Exon 2-4"));
    case  Mut_KIT:
        return PathologyPropertyInfo(property, Mutation, "mut/kit", QObject::tr("cKIT"));
    case  Mut_MET:
        return PathologyPropertyInfo(property, Mutation, "mut/met", QObject::tr("MET"));
    case  Mut_PDGFRa:
        return PathologyPropertyInfo(property, Mutation, "mut/pdgfra", QObject::tr("PDGFRa"));
    case  Mut_RET:
        return PathologyPropertyInfo(property, Mutation, "mut/ret", QObject::tr("RET"));
    case  Mut_TP53:
        return PathologyPropertyInfo(property, Mutation, "mut/tp53", QObject::tr("p53"));
    case  Mut_IDH1:
        return PathologyPropertyInfo(property, Mutation, "mut/idh1", QObject::tr("IDH1"));
    case  Mut_IDH2:
        return PathologyPropertyInfo(property, Mutation, "mut/idh2", QObject::tr("IDH2"));
    case  Mut_STK11:
        return PathologyPropertyInfo(property, Mutation, "mut/stk11", QObject::tr("STK11"));
    case  Mut_ALK:
        return PathologyPropertyInfo(property, Mutation, "mut/alk", QObject::tr("ALK"));
    case  Mut_CTNNB1:
        return PathologyPropertyInfo(property, Mutation, "mut/ctnnb1", QObject::tr("CTNNB1"));
    case  Mut_MAP2K1:
        return PathologyPropertyInfo(property, Mutation, "mut/map2k1", QObject::tr("MAP2K1"));
    case  Mut_ROS1:
        return PathologyPropertyInfo(property, Mutation, "mut/ros1", QObject::tr("ROS1"));
    case  Fusion_ALK:
        return PathologyPropertyInfo(property, Mutation, "fusion/alk", QObject::tr("ALK-Fusion"));
    case  Fusion_ROS1:
        return PathologyPropertyInfo(property, Mutation, "fusion/ros1", QObject::tr("ROS1-Fusion"));
    case  Fusion_RET:
        return PathologyPropertyInfo(property, Mutation, "fusion/ret", QObject::tr("RET-Fusion"));
    case  Fusion_MET:
        return PathologyPropertyInfo(property, Mutation, "fusion/met", QObject::tr("MET-Fusion"));
    case  Fusion_NTRK1:
        return PathologyPropertyInfo(property, Mutation, "fusion/ntrk1", QObject::tr("NTRK1-Fusion"));
    case  Fusion_NTRK2:
        return PathologyPropertyInfo(property, Mutation, "fusion/ntrk2", QObject::tr("NTRK2-Fusion"));
    case  Fusion_NTRK3:
        return PathologyPropertyInfo(property, Mutation, "fusion/ntrk3", QObject::tr("NTRK3-Fusion"));
    case  Fusion_AXL:
        return PathologyPropertyInfo(property, Mutation, "fusion/axl", QObject::tr("AXL-Fusion"));
    case  Fusion_BRAF:
        return PathologyPropertyInfo(property, Mutation, "fusion/braf", QObject::tr("BRAF-Fusion"));
    case  Fusion_CCND1:
        return PathologyPropertyInfo(property, Mutation, "fusion/ccnd1", QObject::tr("CCND1-Fusion"));
    case  Fusion_FGFR1:
        return PathologyPropertyInfo(property, Mutation, "fusion/fgfr1", QObject::tr("FGFR1-Fusion"));
    case  Fusion_FGFR2:
        return PathologyPropertyInfo(property, Mutation, "fusion/fgfr2", QObject::tr("FGFR2-Fusion"));
    case  Fusion_FGFR3:
        return PathologyPropertyInfo(property, Mutation, "fusion/fgfr3", QObject::tr("FGFR3-Fusion"));
    case  Fusion_NRG1:
        return PathologyPropertyInfo(property, Mutation, "fusion/nrg1", QObject::tr("NRG1-Fusion"));
    case  Fusion_PPARG:
        return PathologyPropertyInfo(property, Mutation, "fusion/pparg", QObject::tr("PPARG-Fusion"));
    case  Fusion_RAF1:
        return PathologyPropertyInfo(property, Mutation, "fusion/raf1", QObject::tr("RAF1-Fusion"));
    case  Fusion_THADA:
        return PathologyPropertyInfo(property, Mutation, "fusion/thada", QObject::tr("THADA-Fusion"));
    case  Mut_SF3B1:
        return PathologyPropertyInfo(property, Mutation, "mut/sf3b1", QObject::tr("SF3B1"));
    case  Mut_BCLAF1:
        return PathologyPropertyInfo(property, Mutation, "mut/bclaf1", QObject::tr("BCLAF1"));
    case  Mut_BRCA1:
        return PathologyPropertyInfo(property, Mutation, "mut/brca1", QObject::tr("BRCA1"));
    case  Mut_BRCA2:
        return PathologyPropertyInfo(property, Mutation, "mut/brca2", QObject::tr("BRCA2"));
    case  Mut_PALB2:
        return PathologyPropertyInfo(property, Mutation, "mut/palb2", QObject::tr("PALB2"));
    case  Mut_RPA1:
        return PathologyPropertyInfo(property, Mutation, "mut/rpa1", QObject::tr("RPA1"));
    case  Mut_ATM:
        return PathologyPropertyInfo(property, Mutation, "mut/atm", QObject::tr("ATM"));
    case  Mut_MLH1:
        return PathologyPropertyInfo(property, Mutation, "mut/mlh1", QObject::tr("MLH1"));
    case  Mut_MSH2:
        return PathologyPropertyInfo(property, Mutation, "mut/msh2", QObject::tr("MSH2"));
    case  Mut_BAP1:
        return PathologyPropertyInfo(property, Mutation, "mut/bap1", QObject::tr("BAP1"));
    case  Mut_ARID1A:
        return PathologyPropertyInfo(property, Mutation, "mut/arid1a", QObject::tr("ARID1A"));
    case  Mut_ARID1B:
        return PathologyPropertyInfo(property, Mutation, "mut/arid1b", QObject::tr("ARID1B"));
    case  Mut_SMARCA2:
        return PathologyPropertyInfo(property, Mutation, "mut/smarca2", QObject::tr("SMARCA2"));
    case  Mut_SMARCA4:
        return PathologyPropertyInfo(property, Mutation, "mut/smarca4", QObject::tr("SMARCA4"));
    case  Mut_SMARCB1:
        return PathologyPropertyInfo(property, Mutation, "mut/smarcb1", QObject::tr("SMARCB1"));
    case  Mut_KDM6A:
        return PathologyPropertyInfo(property, Mutation, "mut/kdm6a", QObject::tr("KDM6A"));
    case  Mut_PBRM1:
        return PathologyPropertyInfo(property, Mutation, "mut/pbrm1", QObject::tr("PBRM1"));
    case  Mut_SMAD4:
        return PathologyPropertyInfo(property, Mutation, "mut/smad4", QObject::tr("SMAD4"));
    case  Mut_MDM2:
        return PathologyPropertyInfo(property, Mutation, "mut/mdm2", QObject::tr("MDM2"));
    case  Mut_RNF43:
        return PathologyPropertyInfo(property, Mutation, "mut/rnf43", QObject::tr("RNF43"));
    case  Mut_GNAS:
        return PathologyPropertyInfo(property, Mutation, "mut/gnas", QObject::tr("GNAS"));
    case  Mut_TSC1:
        return PathologyPropertyInfo(property, Mutation, "mut/tsc1", QObject::tr("TSC1"));
    case  Mut_TSC2:
        return PathologyPropertyInfo(property, Mutation, "mut/tsc2", QObject::tr("TSC2"));
    case  Mut_NF1:
        return PathologyPropertyInfo(property, Mutation, "mut/nf1", QObject::tr("NF1"));
    case  Mut_MAP2K2:
        return PathologyPropertyInfo(property, Mutation, "mut/map2k2", QObject::tr("MAP2K2"));
    case  Mut_MAPK3:
        return PathologyPropertyInfo(property, Mutation, "mut/mapk3", QObject::tr("MAPK3"));
    case  Mut_MAPK1:
        return PathologyPropertyInfo(property, Mutation, "mut/mapk1", QObject::tr("MAPK1"));
    case  Mut_AKT1:
        return PathologyPropertyInfo(property, Mutation, "mut/akt1", QObject::tr("AKT1"));
    case  Mut_AKT2:
        return PathologyPropertyInfo(property, Mutation, "mut/akt2", QObject::tr("AKT2"));
    case  Mut_GNAQ:
        return PathologyPropertyInfo(property, Mutation, "mut/gnaq", QObject::tr("GNAQ"));
    case  Mut_GNA11:
        return PathologyPropertyInfo(property, Mutation, "mut/gna11", QObject::tr("GNA11"));
    case  Mut_RAF1:
        return PathologyPropertyInfo(property, Mutation, "mut/raf1", QObject::tr("RAF1"));
    case  Mut_KEAP1:
        return PathologyPropertyInfo(property, Mutation, "mut/keap1", QObject::tr("KEAP1"));
    case  Mut_NTRK1:
        return PathologyPropertyInfo(property, Mutation, "mut/ntrk1", QObject::tr("NTRK1"));
    case  Mut_NTRK2:
        return PathologyPropertyInfo(property, Mutation, "mut/ntrk2", QObject::tr("NTRK2"));
    case  Mut_NTRK3:
        return PathologyPropertyInfo(property, Mutation, "mut/ntrk3", QObject::tr("NTRK3"));
    case InvalidProperty:
        break;
    }
    return PathologyPropertyInfo();
}

PathologyPropertyInfo PathologyPropertyInfo::info(const QString& id)
{
    for (int i = FirstProperty; i<= LastProperty; i++)
    {
        PathologyPropertyInfo obj = info((Property)i);
        if (obj.id == id)
        {
            return obj;
        }
    }
    return PathologyPropertyInfo();
}

QList<PathologyPropertyInfo> PathologyPropertyInfo::allInfosWithType(ValueTypeCategory category)
{
    QList<PathologyPropertyInfo> infos;
    for (int i = FirstProperty; i<= LastProperty; i++)
    {
        PathologyPropertyInfo obj = info((Property)i);
        if (obj.valueType == category)
        {
            infos << obj;
        }
    }
    return infos;
}

QList<PathologyPropertyInfo> PathologyPropertyInfo::allIHC()
{
    QList<PathologyPropertyInfo> infos;
    for (int i = FirstProperty; i<= LastProperty; i++)
    {
        PathologyPropertyInfo obj = info((Property)i);
        if (obj.valueType >= IHCClassical && obj.valueType <= IHCHScore)
        {
            infos << obj;
        }
    }
    return infos;
}

ValueTypeCategoryInfo::ValueTypeCategoryInfo(PathologyPropertyInfo::ValueTypeCategory category)
    : category(category)
{
}

ValueTypeCategoryInfo::ValueTypeCategoryInfo(const PathologyPropertyInfo& info)
    : category(info.valueType)
{
}

QList<QVariant> ValueTypeCategoryInfo::optionsInUI() const
{
    QList<QVariant> values;
    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::IHCHScore:
        // the latter receives special treatment
        values << QVariant(QVariant::Bool)
               << 0 << 1 << 2 << 3;
        break;
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        values << QVariant(QVariant::Bool) << false << true;
        break;
    case PathologyPropertyInfo::MSI:
        values << QVariant(QVariant::Bool)
               << 0 << 1 << 2;
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return values;
}

QVariant ValueTypeCategoryInfo::negativeValue() const
{
    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::IHCHScore:
        return 0;
    case PathologyPropertyInfo::MSI:
        return PathologyPropertyInfo::MSS;
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        return false;
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QVariant();
}

QVariant ValueTypeCategoryInfo::notDoneValue() const
{
    // The null, valid variant of type Bool
    return QVariant(QVariant::Bool);
}

bool ValueTypeCategoryInfo::isScored() const
{
    return category == PathologyPropertyInfo::IHCClassical ||
           category == PathologyPropertyInfo::IHCTwoDim ||
           category == PathologyPropertyInfo::IHCClassicalPoints ||
           category == PathologyPropertyInfo::IHCHScore;
}

bool ValueTypeCategoryInfo::isTwoDimScored() const
{
    return category == PathologyPropertyInfo::IHCTwoDim;
}

bool ValueTypeCategoryInfo::isHScored() const
{
    return category == PathologyPropertyInfo::IHCHScore;
}

QString ValueTypeCategoryInfo::toUILabel(const QVariant& value) const
{
    if (category != PathologyPropertyInfo::InvalidCategory
            && value.isNull() && value.type() == QVariant::Bool)
    {
        return QObject::tr("n.d.");
    }

    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
        switch (value.toInt())
        {
        case 0:
            return QObject::tr("0");
        case 1:
            return QObject::tr("1+");
        case 2:
            return QObject::tr("2+");
        case 3:
            return QObject::tr("3+");
        }
        break;
    case PathologyPropertyInfo::IHCClassicalPoints:
        return QString::number(value.toInt()) + (value.toInt() == 0 ? QString() : "  ");
                // blank to have same length as IHC classical
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::IHCHScore:
        switch (value.toInt())
        {
        case 0:
            return QObject::tr("keine");
        case 1:
            return QObject::tr("schwache");
        case 2:
            return QObject::tr("mäßige");
        case 3:
            return QObject::tr("starke Färbung");
        }
        break;
    case PathologyPropertyInfo::IHCBoolean:
        if (value.toBool())
        {
            return QObject::tr("positiv");
        }
        else
        {
            return QObject::tr("negativ");
        }
        break;
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::BooleanCombination:
        if (value.toBool())
        {
            return QObject::tr("positiv");
        }
        else
        {
            if (category == PathologyPropertyInfo::Mutation)
            {
                return QObject::tr("Wildtyp");
            }
            else
            {
                return QObject::tr("negativ");
            }
        }
    case PathologyPropertyInfo::StableUnstable:
        if (value.toBool())
        {
            return QObject::tr("Unstable");
        }
        else
        {
            return QObject::tr("Stable");
        }
    case PathologyPropertyInfo::MSI:
        switch (value.toInt()) {
        case PathologyPropertyInfo::MSS:
            return QObject::tr("MSS");
        case PathologyPropertyInfo::MSI_L:
            return QObject::tr("MSI-L");
        case PathologyPropertyInfo::MSI_H:
            return QObject::tr("MSI-H");
        }
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QString();
}

IHCScore ValueTypeCategoryInfo::toIHCScore(const Property& prop) const
{
    if (category != PathologyPropertyInfo::IHCTwoDim)
    {
        return IHCScore();
    }
    return IHCScore(toValue(prop.value), prop.detail);
}

HScore ValueTypeCategoryInfo::toHScore(const Property& prop) const
{
    if (category != PathologyPropertyInfo::IHCHScore)
    {
        return HScore();
    }
    return HScore(toValue(prop.value));
}

void ValueTypeCategoryInfo::fillIHCScore(Property& prop, int intensity, const QString& percentage) const
{
    prop.value    = toPropertyValue(intensity);
    prop.detail   = percentage;
}

void ValueTypeCategoryInfo::fillHSCore(Property& prop, const HScore& score) const
{
    prop.value = toPropertyValue(score.binaryScore);
}

namespace
{

static QString toScoreString(int score)
{
    switch (score)
    {
    case 0:
        return QObject::tr("0");
    case 1:
        return QObject::tr("1+");
    case 2:
        return QObject::tr("2+");
    case 3:
        return QObject::tr("3+");
    }
    return QString();
}

static QString toPositiveNegativ(bool val)
{
    if (val)
    {
        return QObject::tr("Positiv");
    }
    else
    {
        return QObject::tr("Negativ");
    }
}

static QString toPlusMinus(bool val)
{
    if (val)
    {
        return "+";
    }
    else
    {
        return "-";
    }
}

}

// SELECT property FROM PathologyProperties WHERE pathologyid IN (SELECT id FROM Pathologies WHERE context='wtz/tumorprofil') AND property LIKE 'ihc/%';
QString ValueTypeCategoryInfo::toShortDisplayString(const Property& prop) const
{
    QVariant value = toValue(prop.value);

    if (category != PathologyPropertyInfo::InvalidCategory
            && value.isNull() && value.type() == QVariant::Bool)
    {
        return QString("");
    }

    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
        return toScoreString(value.toInt());
    case PathologyPropertyInfo::IHCClassicalPoints:
        return QString::number(value.toInt());
    case PathologyPropertyInfo::IHCTwoDim:
    {
        QVariant score = IHCScore(value, prop.detail).score();
        if (score.isNull())
        {
            return QString();
        }
        else if (score.type() == QVariant::Int)
        {
            return QString::number(score.toInt());
        }
        else
        {
            return toPlusMinus(score.toBool());
        }
    }
    case PathologyPropertyInfo::IHCHScore:
    {
        QVariant score = HScore(value).score();
        if (score.isNull())
        {
            return QString();
        }
        return QString::number(score.toInt());
    }
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        return toPlusMinus(value.toBool());
    case PathologyPropertyInfo::MSI:
        return toPlusMinus(value.toInt() == PathologyPropertyInfo::MSI_H);
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QString();
}

QString ValueTypeCategoryInfo::toLongDisplayString(const Property &prop) const
{
    QVariant value = toValue(prop.value);

    if (value.isNull())
    {
        return toUILabel(QVariant(QVariant::Bool)); // "n.d."
    }

    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
        return toScoreString(value.toInt());
    case PathologyPropertyInfo::IHCClassicalPoints:
        return QString::number(value.toInt());
    case PathologyPropertyInfo::IHCTwoDim:
    {
        IHCScore score(value, prop.detail);
        QString s;
        if (prop.property == "ihc/mlh1" || prop.property == "ihc/msh2" || prop.property == "ihc/msh6" || prop.property == "ihc/pms2")
        {
            switch (score.colorIntensity)
            {
            case IHCScore::InvalidIntensity:
                return toUILabel(QVariant(QVariant::Bool)); // "n.d."
            case IHCScore::NoIntensity:
                return QObject::tr("Expressionsverlust");
            case IHCScore::WeakIntensity:
            case IHCScore::MediumIntensity: // Medium is not used
                s = QObject::tr("Partieller Expressionsverlust in %1%");
                break;
            case IHCScore::StrongIntensity:
                return QObject::tr("Kein Expressionsverlust");
                break;
            }
        }
        else
        {
            switch (score.colorIntensity)
            {
            case IHCScore::InvalidIntensity:
                return toUILabel(QVariant(QVariant::Bool)); // "n.d."
            case IHCScore::NoIntensity:
                return QObject::tr("Negativ (keine Färbung)");
            case IHCScore::WeakIntensity:
                s = QObject::tr("Schwache Färbung in %1%");
                break;
            case IHCScore::MediumIntensity:
                s = QObject::tr("Mäßige Färbung in %1%");
                break;
            case IHCScore::StrongIntensity:
                s = QObject::tr("Starke Färbung in %1%");
                break;
            }
        }
        return s.arg(prop.detail);
    }
    case PathologyPropertyInfo::IHCHScore:
    {
        HScore hscore(value);
        QVector<int> percentages = hscore.percentages();
        if (percentages[3] == 100)
        {
            return QObject::tr("Negativ (keine Färbung)");
        }
        return QObject::tr("H-Score %1 (Stark %2%, mäßig %3%, schwach %4%)")
                .arg(hscore.score().toInt()).arg(percentages[0]).arg(percentages[1]).arg(percentages[2]);
    }
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::StableUnstable:
    {
        QString s = toUILabel(value);
        if (!prop.detail.isEmpty())
        {
            s += " (" + prop.detail + ")";
        }
        return s;
    }
    case PathologyPropertyInfo::MSI:
        return toUILabel(value);
    case PathologyPropertyInfo::Fish:
    {
        QString s = toPositiveNegativ(value.toBool());
        if (!prop.detail.isEmpty())
        {
            PathologyPropertyInfo info = PathologyPropertyInfo::info(prop.property);
            s += " (" + info.detailLabel + " " + prop.detail + ")";
        }
        return s;
    }
    case PathologyPropertyInfo::Mutation:
    {
        if (value.toBool())
        {
            if (prop.detail.isEmpty())
            {
                return QObject::tr("Mutationsnachweis");
            }
            return prop.detail;
        }
        else
        {
            return toUILabel(value);
        }
    }
    case PathologyPropertyInfo::BooleanCombination: // not applicable here
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QString();
}

QVariant ValueTypeCategoryInfo::toVariantData(const Property& prop) const
{
    QVariant value = toValue(prop.value);

    if (category != PathologyPropertyInfo::InvalidCategory
            && value.isNull() && value.type() == QVariant::Bool)
    {
        return QVariant();
    }

    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
        return value;
    case PathologyPropertyInfo::IHCTwoDim:
    {
        QVariant score = IHCScore(value, prop.detail).score();

        if (!score.isValid())
        {
            return QVariant();
        }
        else if (score.type() == QVariant::Int)
        {
            return score;
        }
        else
        {
            return score.toBool() ? 1 : 0;
        }
    }
    case PathologyPropertyInfo::IHCHScore:
    {
        QVariant score = HScore(value).score();
        if (score.isNull())
        {
            return QVariant();
        }
        return score;
    }
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        return value;
    case PathologyPropertyInfo::MSI:
        return static_cast<PathologyPropertyInfo::MSIStatus>(value.toInt());
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QVariant();
}


namespace
{

static QString boolToString(bool b)
{
    if (b)
    {
        return "1";
    }
    else
    {
        return "0";
    }
}

static QString boolToString(const QVariant& var)
{
    return boolToString(var.toBool());
}

static bool stringToBool(const QString& s)
{
    if (s == "1" || s == "true" || s == "pos" || s.startsWith("positiv") || s == "+")
    {
        return true;
    }
    if (s == "0" || s == "false" || s == "neg" || s.startsWith("negativ") || s == "-")
    {
        return false;
    }
    bool ok;
    int i = s.toInt(&ok);
    if (ok)
    {
        return i;
    }
    return false;
}
}

QString ValueTypeCategoryInfo::toPropertyValue(const QVariant& value) const
{
    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::IHCHScore:
    case PathologyPropertyInfo::MSI:
        return value.toString();
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        return boolToString(value);
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QString();
}

QVariant ValueTypeCategoryInfo::toValue(const QString& propertyValue) const
{
    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::IHCHScore:
    case PathologyPropertyInfo::MSI:
        return propertyValue.toInt();
        break;
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::BooleanCombination:
        return stringToBool(propertyValue);
        break;
    case PathologyPropertyInfo::InvalidCategory:
        break;
    }
    return QVariant();
}

QVariant ValueTypeCategoryInfo::toMedicalValue(const Property &prop) const
{
    if (isTwoDimScored())
    {
        return QVariant::fromValue(toIHCScore(prop));
    }
    else if (isHScored())
    {
        return QVariant::fromValue(toHScore(prop));
    }
    else if (category == PathologyPropertyInfo::MSI)
    {
        return toVariantData(prop);
    }
    return toValue(prop.value);
}

bool ValueTypeCategoryInfo::hasDetail() const
{
    switch (category)
    {
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::IHCTwoDim:
    case PathologyPropertyInfo::Fish:
    case PathologyPropertyInfo::Mutation:
        return true;
    default:
        return false;
    }
}

QPair<QString, QString> ValueTypeCategoryInfo::defaultDetailLabel() const
{
    switch (category)
    {
    case PathologyPropertyInfo::IHCClassical:
    case PathologyPropertyInfo::IHCClassicalPoints:
    case PathologyPropertyInfo::IHCBoolean:
    case PathologyPropertyInfo::StableUnstable:
    case PathologyPropertyInfo::InvalidCategory:
    case PathologyPropertyInfo::BooleanCombination:
    case PathologyPropertyInfo::IHCHScore:
    case PathologyPropertyInfo::MSI:
        break;
    case PathologyPropertyInfo::IHCBooleanPercentage:
    case PathologyPropertyInfo::IHCTwoDim:
        return qMakePair(QObject::tr("in"), QObject::tr("% Zellen"));
        break;
    case PathologyPropertyInfo::Fish:
        return qMakePair(QString(""), QString());
        break;
    case PathologyPropertyInfo::Mutation:
        return qMakePair(QObject::tr("Mutation:"), QString());
        break;
    }
    return QPair<QString, QString>();
}

// -----------------

PathologyContextInfo::PathologyContextInfo()
    : context(InvalidContext)
{
}

PathologyContextInfo::PathologyContextInfo(Context context,
                      const QString& id, const QString& label)
    : context(context), id(id), label(label)
{
}

PathologyContextInfo::PathologyContextInfo(Context context)
{
    *this = info(context);
}

PathologyContextInfo PathologyContextInfo::info(Context context)
{
    switch (context)
    {
    case InvalidContext:
        break;
    case Tumorprofil:
        return PathologyContextInfo(context, "wtz/tumorprofil", "Tumorprofil");
    case BestRx:
        return PathologyContextInfo(context, "novartis/bestrx", "BestRx");
    case ColonRetrospektiv:
        return PathologyContextInfo(context, "wtz/kasper/colonretrospektiv", "Colon retrospektiv");
    case ScreeningBGJ398:
        return PathologyContextInfo(context, "novartis/CBGJ398X2101", "BGJ398");
    case ScreeningBEZ235:
        return PathologyContextInfo(context, "novartis/CBEZ235A2101", "CBEZ235");
    case ScreeningBKM120:
        return PathologyContextInfo(context, "novartis/CBKM120D2201", "CBEZ235");
    }
    return PathologyContextInfo();
}

PathologyContextInfo PathologyContextInfo::info(const QString& id)
{
    for (int i = FirstContext; i<= LastContext; i++)
    {
        PathologyContextInfo obj = info((Context)i);
        if (obj.id == id)
        {
            return obj;
        }
    }
    return PathologyContextInfo();
}


// ------------

TrialContextInfo::TrialContextInfo()
    : trial(InvalidTrial)
{
}

TrialContextInfo::TrialContextInfo(Trial trial,
                      const QString& id, const QString& label)
    : trial(trial), id(id), label(label)
{
}

TrialContextInfo::TrialContextInfo(Trial trial)
{
    *this = info(trial);
}

TrialContextInfo TrialContextInfo::info(Trial trial)
{
    switch (trial)
    {
    case InvalidTrial:
        break;
    case AIO_TRK_0212:
        return TrialContextInfo(trial, "aio/TRK-0212", "PemSplitCisp Studie");
    case Biopredict:
        return TrialContextInfo(trial, "wtz/biopredict", "Biopredict-Projekt");
    case RadiomicsPDL1:
        return TrialContextInfo(trial, "wtz/radiomics/pdl1", "Radiomics PDL1 Kohorte");
    case LonsurfCUPKasper:
        return TrialContextInfo(trial, "wtz/crc/lonsurf-cup", "Lonsurf CUP");
    case NSCLCChemoimmuneRadiomics:
        return TrialContextInfo(trial, "wtz/radiomics/chemoimmune", "NSCLC Chemoimmun Radiomics");
    case PankreasCCCJW:
        return TrialContextInfo(trial, "wtz/gi/pancreasccc", "Pankreas / CCC");
    case NSCLC_LCNEC:
        return TrialContextInfo(trial, "wtz/nsclc/lcnec", "NSCLC LCNEC Projekt");
    }
    return TrialContextInfo();
}

TrialContextInfo TrialContextInfo::info(const QString& id)
{
    for (int i = FirstTrial; i<= LastTrial; i++)
    {
        TrialContextInfo obj = info((Trial)i);
        if (obj.id == id)
        {
            return obj;
        }
    }
    return TrialContextInfo();
}

