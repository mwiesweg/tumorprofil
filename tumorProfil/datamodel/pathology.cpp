/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 2012-01-22
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "pathology.h"

#include <QDebug>

Pathology::Pathology()
    : entity(UnknownEntity),
      sampleOrigin(UnknownOrigin),
      id(0)
{
}

bool Pathology::operator==(const Pathology& other) const
{
    if (   entity       == other.entity
        && sampleOrigin == other.sampleOrigin
        && context      == other.context
        && date         == other.date
           )
    {
        if (properties == other.properties)
        {
            return true;
        }
        if (properties.size() != other.properties.size())
        {
            //return false;
        }
        // Check if only order is different
        PropertyList leftSorted(properties);
        std::sort(leftSorted.begin(), leftSorted.end());
        PropertyList rightSorted(properties);
        std::sort(rightSorted.begin(), rightSorted.end());
        if (leftSorted == rightSorted)
        {
            return true;
        }
        qDebug() << "Left properties size" << properties.size();
        foreach (const Property& prop, properties)
        {
            qDebug() << prop.property << prop.value << prop.detail;
        }
        qDebug() << "Right properties size" << other.properties.size();
        foreach (const Property& prop, other.properties)
        {
            qDebug() << prop.property << prop.value << prop.detail;
        }
    }
    return false;
}

QString Pathology::entityLabel(Pathology::Entity entity)
{
    switch (entity)
    {
    case Pathology::PulmonaryAdeno:
    case Pathology::PulmonaryBronchoalveloar:
        return QObject::tr("NSCLC Adeno");
    case Pathology::PulmonaryNSCLCNOS:
        return QObject::tr("NSCLC NOS");
    case Pathology::PulmonarySquamous:
        return QObject::tr("NSCLC PEC");
    case Pathology::PulmonaryAdenosquamous:
        return QObject::tr("NSCLC Adenosquamös");
    case Pathology::PulmonaryOtherCarcinoma:
        return QObject::tr("NSCLC NOS");
    case Pathology::ColorectalAdeno:
        return QObject::tr("Kolorektales Karzinom");
    case Pathology::Cholangiocarcinoma:
        return QObject::tr("Cholangiozelluläres Karzinom");
    case Pathology::RenalCell:
        return QObject::tr("Nierenzellkarzinom");
    case Pathology::Esophageal:
        return QObject::tr("Ösophaguskarzinom");
    case Pathology::EsophagogastrealJunction:
        return QObject::tr("Magen/Ösophagus Übergangskarzinom");
    case Pathology::Gastric:
        return QObject::tr("Magenkarzinom");
    case Pathology::Breast:
        return QObject::tr("Mammakarzinom");
    case Pathology::TransitionalCell:
        return QObject::tr("Urothelkarzinom");
    case Pathology::Thyroid:
        return QObject::tr("Schilddrüsenkarzinom");
    case Pathology::Melanoma:
        return QObject::tr("Melanom");
    case Pathology::HeadNeckSCC:
        return QObject::tr("Kopf/Hals PEC");
    case Pathology::HeadNeckOther:
        return QObject::tr("Kopf/Hals (kein PEC)");
    case Pathology::PulmonarySCLC:
        return QObject::tr("SCLC");
    case Pathology::GIOtherCarcinoma:
        return QObject::tr("GI / andere");
    case Pathology::Sarcoma:
        return QObject::tr("Sarkom");
    case Pathology::Mesothelioma:
        return QObject::tr("Mesotheliom");
    case Pathology::PulmonaryLCNEC:
        return QObject::tr("LCNEC");
    case Pathology::PulmonarySarcomatoid:
        return QObject::tr("Lunge Sarkomatoid");
    case Pathology::PancreaticCarcinoma:
        return QObject::tr("Pankreaskarzinom");
    case Pathology::UnknownEntity:
        break;
    }
    return QString();
}

QString Pathology::shortEntityLabel(Pathology::Entity entity)
{
    switch (entity)
    {
    case Pathology::PulmonaryAdeno:
    case Pathology::PulmonaryBronchoalveloar:
        return "NSCLC/A";
    case Pathology::PulmonaryNSCLCNOS:
        return "NSCLC/NOS";
    case Pathology::PulmonarySquamous:
        return "NSCLC/Sq";
    case Pathology::PulmonaryAdenosquamous:
        return "NSCLC/AdS";
    case Pathology::PulmonaryOtherCarcinoma:
        return "NSCLC/?";
    case Pathology::ColorectalAdeno:
        return "CRC";
    case Pathology::Cholangiocarcinoma:
        return "CCC";
    case Pathology::RenalCell:
        return "RCC";
    case Pathology::Esophageal:
        return "Öso";
    case Pathology::EsophagogastrealJunction:
        return "AEG";
    case Pathology::Gastric:
        return "Magen";
    case Pathology::Breast:
        return "Mamma";
    case Pathology::TransitionalCell:
        return "Urothel";
    case Pathology::Thyroid:
        return QObject::tr("Schilddr.");
    case Pathology::Melanoma:
        return QObject::tr("Melanom");
    case Pathology::HeadNeckSCC:
        return QObject::tr("HNSCC");
    case Pathology::HeadNeckOther:
        return QObject::tr("HN other");
    case Pathology::PulmonarySCLC:
        return QObject::tr("SCLC");
    case Pathology::GIOtherCarcinoma:
        return QObject::tr("GI / andere");
    case Pathology::Sarcoma:
        return QObject::tr("Sarkom");
    case Pathology::Mesothelioma:
        return QObject::tr("Mesotheliom");
    case Pathology::PulmonaryLCNEC:
        return QObject::tr("LCNEC");
    case Pathology::PulmonarySarcomatoid:
        return QObject::tr("NSCLC/Sarc");
    case Pathology::PancreaticCarcinoma:
        return QObject::tr("Pankreas");
    case Pathology::UnknownEntity:
        return "?";
    }
    return QString();
}
