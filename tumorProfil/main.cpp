/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 2012-01-22
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// Qt includes

#include <QApplication>
#include <QCommandLineParser>
#include <QDir>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFuture>
#include <QFutureWatcher>
#include <QIcon>
#include <QMessageBox>
#include <QProgressDialog>
#include <QRegExp>
#include <QtConcurrent/QtConcurrent>
#include <QTextStream>
#include <QVariant>
#include <QUrl>

// Local includes

#include "databaseaccess.h"
#include "analysisgenerator.h"
#include "csvconverter.h"
#include "databaseparameters.h"
#include "report/reportgenerator.h"
#include "ihcscore.h"
#include <iostream>
#include "mainentrydialog.h"
#include "mainwindow.h"
#include "patientmanager.h"
#include "diseasehistory.h"
#include "reportwindow.h"
#include "resultwaitdialog.h"
#include "historyvalidator.h"
#include "parser/pathologyparser.h"
#include "pathologypropertiestableview.h"
#include "settings/encryptionsettings.h"
#include "settings/databasesettings.h"
#include "settings/mainsettings.h"
#include "encryption/authenticationwindow.h"
#include "authentication//userinformation.h"
#include "parser/historycsvparser.h"
#include "parser/labtablereader.h"
#include "parser/ngsresultcvsparser.h"
#include "parser/catoparser.h"
#include "parser/patientlistparser.h"
#include "systemictherapy.h"
#include "labresults.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("Innere Klinik (Tumorforschung)");
    QCoreApplication::setApplicationName("Tumorprofil");

    QIcon::setThemeName("silk");
    app.setWindowIcon(QIcon::fromTheme("folder_table"));

    QCommandLineParser parser;
    parser.setApplicationDescription(QObject::tr("Tumorprofil - Datenbankanwendung"));
    parser.addHelpOption();
    QCommandLineOption editOption("edit", QObject::tr("Öffne Fenster zur Befundeingabe"));
    parser.addOption(editOption);
    QCommandLineOption historyOption("history-edit", QObject::tr("Öffne Fenster zum Bearbeiten des Krankheitsverlaufs"));
    parser.addOption(historyOption);
    QCommandLineOption historyReadOption("history", QObject::tr("Öffne Fenster zum Einsehen des Krankheitsverlaufs"));
    parser.addOption(historyReadOption);
    QCommandLineOption reportOption("report", QObject::tr("Öffne Fenster zur Datenabfrage"));
    parser.addOption(reportOption);
    QCommandLineOption scriptOption("report", QObject::tr("Öffne Fenster für ein Abfrageskript"));
    parser.addOption(scriptOption);
    QCommandLineOption parseNGSCSVOption("parse-ngs", QObject::tr("Lese CSV-Datei mit NGS-Befunden ein"));
    parser.addOption(parseNGSCSVOption);
    QCommandLineOption parseCatoOption("parse-cato", QObject::tr("Lese CSV-Datei mit CATO-Export ein"));
    parser.addOption(parseCatoOption);
    QCommandLineOption parseCatoRemoveOption("parse-cato-delete", QObject::tr("Mache Einlesen der CSV-Datei mit CATO-Daten rückgängig"));
    parser.addOption(parseCatoRemoveOption);
    QCommandLineOption parseHistoryOption("parse-clinical-data", QObject::tr("Lese CSV-Datei mit klinischen Verlaufsdaten ein"));
    parser.addOption(parseHistoryOption);
    QCommandLineOption parsePatientListOption("parse-patient-list", QObject::tr("Lese CSV-Datei mit Patientennamen"));
    parser.addOption(parsePatientListOption);
    QCommandLineOption readLabOption("read-lab-db", QObject::tr("Lese Laborwerte aus Datenbanktabelle. "
                                                                "Statt [files...] bitte "
                                                                "[db.table Nachname-Feld Vorname-Feld DOB-Feld Datum-Feld Key-Feld Value-Feld]"));
    parser.addOption(readLabOption);
    QCommandLineOption dryRunOption("dry-run", QObject::tr("Lese Datei, aber schreibe nicht in Datenbank"));
    parser.addOption(dryRunOption);
    QCommandLineOption projectOption("project", QObject::tr("In Kombination mit --parse-patient-list: Projekt/Studie"), "project id");
    parser.addOption(projectOption);
    QCommandLineOption panelOption("panel", QObject::tr("In Kombination mit --parse-ngs: Panel"), "panel id");
    parser.addOption(panelOption);

    parser.addPositionalArgument("files", QObject::tr("Files to open (when applicable option is set)", "[files...]"));

    parser.process(app);

    DatabaseParameters params;
    params.readFromConfig();
    if(params.isMySQL())
    {
        if (!UserInformation::instance()->logIn())
        {
            return 1;
        }
    }
    else
    {
        DatabaseAccess::setParameters(params);
    }

    if (!PatientManager::instance()->initialize())
    {
        return 1;
    }

    {
        QFutureWatcher<void>* watcher = PatientManager::instance()->readDatabase();
        ResultWaitDialog waitDialog(0, QObject::tr("Lade Datenbank"), true);
        if (!waitDialog.waitForResult(watcher))
        {
            return 0;
        }
    }

    //ReportGenerator::test();
    //return 0;
    /*
    AnalysisGenerator generator;
    generator.ros1Project();
    return 0;
    */

    /*
    PathologyParser parser;
    QList<PatientParseResults> results =
    parser.parseFile("/home/marcel/Dokumente/Tumorprofil/Pathobefunde als Text/Pathobefunde 25.6.15 - 27.8.15.txt");
    QFile file("/home/marcel/Dokumente/Tumorprofil/Pathobefunde als Text/nichterkannt.txt");
    file.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream stream(&file);
    foreach (const PatientParseResults& result, results)
    {
        stream << result.unrecognizedText;
        stream << "\n\n";
    }

    return 0;
    */
    /*
    PathologyPropertiesTableModel model;
    PathologyPropertiesTableFilterModel filterModel;
    PathologyPropertiesTableView view;
    view.setModels(&model, &filterModel);
    model.setProperties(parser.results().first().properties);
    view.resize(600, 500);
    view.resizeColumnsToContents();
    view.show();
    return a.exec();
    */
    /*foreach (const Patient::Ptr& p, PatientManager::instance()->patients())
    {
        if (!p->hasDisease())
        {
            continue;
        }
        Disease& d = p->firstDisease();
        if (d.history.isEmpty())
        {
            continue;
        }
        if (d.history.testXmlEvent())
        {
            qDebug() << "Success" << p->id;
        }
        else
        {
            qDebug() << p->id << " " << p->firstName << " " << p->surname << " " << p->dateOfBirth;
            return 0;
        }
    }*/

    QPointer<MainEntryDialog> mainEntryDialog;
    if (parser.isSet(editOption))
    {
        MainEntryDialog::executeAction(MainEntryDialog::EditWindow);
    }
    else if (parser.isSet(reportOption))
    {
        MainEntryDialog::executeAction(MainEntryDialog::ReportWindow);
    }
    else if (parser.isSet(historyOption))
    {
        MainEntryDialog::executeAction(MainEntryDialog::HistoryWindow);
    }
    else if (parser.isSet(historyReadOption))
    {
        MainEntryDialog::executeAction(MainEntryDialog::HistoryWindowReadOnly);
    }
    else if (parser.isSet(scriptOption))
    {
        MainEntryDialog::executeAction(MainEntryDialog::ReportScriptWindow);
    }
    else if (parser.isSet(parseNGSCSVOption))
    {
        foreach (const QString& fileName, parser.positionalArguments())
        {
            NGSResultCVSParser ngs;
            ngs.setDryRun(parser.isSet(dryRunOption));
            ngs.setPanel(parser.value("panel"));
            ngs.parse(fileName);
        }
        return 0;
    }
    else if (parser.isSet(parseCatoOption))
    {
        foreach (const QString& fileName, parser.positionalArguments())
        {
            CatoParser cato;
            cato.setDryRun(parser.isSet(dryRunOption));
            cato.insert(fileName);
        }
        return 0;
    }
    else if (parser.isSet(parseCatoRemoveOption))
    {
        foreach (const QString& fileName, parser.positionalArguments())
        {
            CatoParser cato;
            cato.setDryRun(parser.isSet(dryRunOption));
            cato.remove(fileName);
        }
        return 0;
    }
    else if (parser.isSet(parseHistoryOption))
    {
        foreach (const QString& fileName, parser.positionalArguments())
        {
            HistoryCSVParser hp;
            hp.setDryRun(parser.isSet(dryRunOption));
            hp.parse(fileName);
        }
        return 0;
    }
    else if (parser.isSet(parsePatientListOption))
    {
        foreach (const QString& fileName, parser.positionalArguments())
        {
            PatientListParser sp;
            sp.setDryRun(parser.isSet(dryRunOption));
            sp.setProject(parser.value("project"));
            sp.parse(fileName);
        }
        return 0;
    }
    else if (parser.isSet(readLabOption))
    {
        QStringList options = parser.positionalArguments();
        LabTableReader reader(options, parser.isSet(dryRunOption));
        reader.setDryRun(parser.isSet(dryRunOption));
        reader.read();
        return 0;
    }
    else
    {
        mainEntryDialog = new MainEntryDialog;
        mainEntryDialog->setEnabledFromAccessRights();
        mainEntryDialog->show();
    }

    return app.exec();
}
