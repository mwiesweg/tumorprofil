/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef HISTORYREPORTER_H
#define HISTORYREPORTER_H

#include "patient.h"
#include "disease.h"
#include "historyelements.h"

class ReportGenerator;

class SurvivalResult
{
public:
    SurvivalResult() : days(0), eventReached(false) {}
    SurvivalResult(int days, bool eventReached) : days(days), eventReached(eventReached) {}
    bool isValid() const { return days; }
    int  days;
    bool eventReached;
};

class HistoryReporter
{
public:
    HistoryReporter(ReportGenerator* generator);
    ~HistoryReporter();

    enum OSDefinition
    {
        FromFirstTherapy,
        FromInitialDiagnosis,
        FromRelapseOrInitiallyIncurable // NOTE: invalid for "cured" patients
    };

    enum Category
    {
        NoTherapy,
        FirstLineFollowUpNoRecurrence,
        RelapseAfterFirstLineFollowUp,
        InitiallySystemicTherapy
    };

    void needOS(OSDefinition definition);
    void needTTF(int lineNumber);
    void needBestResponse(int lineNumber);
    void needPFS(int lineNumber);
    void needSubstanceSpecific(const QString& id, const QStringList& substances);
    void needTherapyCategory();
    void needLines();
    void needCurrentState();
    bool isRegisteredSubstance(const QString& id) const;

    void run(const Patient::Ptr& p, const Disease& disease);

    SurvivalResult os(OSDefinition definition = FromFirstTherapy);
    SurvivalResult os(int lineNumber, bool palliative);
    SurvivalResult os(const QString& substanceId, bool palliative);
    DiseaseState::State lastState() const;
    QVariant daysSinceLastFollowup() const; // needs currentState, OS
    SurvivalResult ttf(int lineNumber, bool palliative);
    SurvivalResult ttf(const QString& substanceId, bool palliative);
    QPair<QDate, QDate> lineDates(int lineNumber, bool palliative);
    QPair<QDate, QDate> lineDates(const QString& substanceId, bool palliative);
    QPair<QDate, QDate> ttfDates(int lineNumber, bool palliative);
    QPair<QDate, QDate> ttfDates(const QString& substanceId, bool palliative);
    QList<QString> substances(int lineNumber, bool palliative);
    QList<QString> substances(const QString& substanceId, bool palliative);
    QVariant includesDeescalation(int lineNumber, bool palliative);
    QVariant includesDeescalation(const QString& substanceId, bool palliative);
    QVariant daysToDeescalation(int lineNumber, bool palliative);
    QVariant daysToDeescalation(const QString& substanceId, bool palliative);
    QList<QString> descalatedSubstances(int lineNumber, bool palliative);
    QList<QString> descalatedSubstances(const QString& substanceId, bool palliative);
    QVariant ctxDays(int lineNumber, bool palliative);
    QVariant ctxDays(const QString& substanceId, bool palliative);
    QVariant toxicities(int lineNumber, bool palliative);
    QVariant toxicities(const QString& substanceId, bool palliative, bool all);
    QVariant substancesDosing(int lineNumber, bool palliative);
    QVariant substancesDosing(const QString& substanceId, bool palliative);
    QVariant substanceDoses(const QString& substanceId, bool palliative);
    QVariant surgery(int n, bool palliative);
    QVariant radiation(int n, bool palliative);
    QVariant bestResponse(int lineNumber, bool palliative) const;
    QVariant bestResponse(const QString& substanceId, bool palliative) const;
    SurvivalResult pfs(int lineNumber, bool palliative) const;
    SurvivalResult pfs(const QString& substanceId, bool palliative) const;
    int lineNumber(const QString& substanceId, bool palliative);
    SurvivalResult dfs() const;
    Category therapyCategory();
    int numberOfLines(bool palliative);
    int numberOfAdjuvantTherapies();
    QVariant dataCutOffTime(OSDefinition definition);
    QVariant dataCutOffTime(int lineNumber, bool palliative);
    QVariant dataCutOffTime(const QString& substanceId, bool palliative);
    QVariant relapseDate();

private:

    class HistoryReporterPriv;
    HistoryReporterPriv* const d;
};

#endif // HISTORYREPORTER_H
