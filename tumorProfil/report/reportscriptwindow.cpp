/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 15.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "reportscriptwindow.h"

#include <QtConcurrent/QtConcurrent>
#include <QAction>
#include <QActionGroup>
#include <QDebug>
#include <QDockWidget>
#include <QEventLoop>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFuture>
#include <QFutureWatcher>
#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QSettings>
#include <QSignalBlocker>
#include <QStackedWidget>
#include <QTextBlock>
#include <QTextCursor>
#include <QTextEdit>
#include <QTextStream>
#include <QToolBar>

#include "analysistableview.h"
#include "constants.h"
#include "csvfile.h"
#include "patientmanager.h"
#include "qvariantlisttablemodel.h"
#include "reportelements.h"
#include "reportgenerator.h"
#include "resultwaitdialog.h"

class ReportScriptWindow::ReportWindowPriv
{
public:

    ReportWindowPriv()
        : stack(0),
          tableView(0),
          model(0),
          edit(0),
          bottomDisplay(0),
          argumentsListWidget(0),
          toolBar(0),
          editAction(0),
          resultAction(0),
          openScriptAction(0),
          saveScriptAction(0),
          saveScriptAsAction(0),
          saveResultsAction(0),
          wasEdited(false),
          helpDisplaysArguments(false)
    {
    }

    ReportGenerator         generator;

    QStackedWidget*         stack;

    AnalysisTableView*      tableView;
    QAbstractTableModel*    model;

    QTextEdit*              edit;

    QWidget*                waitWidget;

    QDockWidget*            bottomDock;
    QDockWidget*            rightDock;
    QTextEdit*              bottomDisplay;
    QListWidget*            argumentsListWidget;

    QToolBar*               toolBar;
    QAction*                newScriptAction;
    QAction*                editAction;
    QAction*                resultAction;
    QAction*                openScriptAction;
    QAction*                saveScriptAction;
    QAction*                saveScriptAsAction;
    QAction*                saveResultsAction;

    QString                 fileName;
    bool                    wasEdited;
    bool                    helpDisplaysArguments;

    QString currentLine() const
    {
        QTextCursor cursor = edit->textCursor();
        cursor.movePosition(QTextCursor::StartOfLine);
        cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
        return cursor.selectedText();
    }
};

namespace
{
    const char* configGroupScript    = "ReportScriptWindow";
    const char* configLastScriptFile = "Last Script File";
    const char* configLastResultFile = "Last Result File";
}

ReportScriptWindow::ReportScriptWindow(QWidget *parent)
    : QMainWindow(parent),
      d(new ReportWindowPriv)
{
    setupUI();
    setupToolbar();

    editScript();

    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(configGroupScript);
    QString lastFile = settings.value(configLastScriptFile).toString();
    if (!lastFile.isNull() && QFileInfo::exists(lastFile))
    {
        loadScript(lastFile);
    }
}

ReportScriptWindow::~ReportScriptWindow()
{
    delete d;
}

void ReportScriptWindow::editScript()
{
    d->stack->setCurrentWidget(d->edit);
    d->rightDock->setVisible(true);
    d->newScriptAction->setVisible(true);
    d->openScriptAction->setVisible(true);
    d->saveScriptAction->setVisible(true);
    d->saveScriptAsAction->setVisible(true);
    d->saveResultsAction->setVisible(false);
    d->edit->setFocus();
    updateHelp();
    d->bottomDock->setWindowTitle(tr("Information"));

    if (d->model)
    {
        d->tableView->setModel(0);
        delete d->model;
        d->model = 0;
    }
}

void ReportScriptWindow::showResult()
{
    d->rightDock->setVisible(false);
    d->newScriptAction->setVisible(false);
    d->openScriptAction->setVisible(false);
    d->saveScriptAction->setVisible(false);
    d->saveScriptAsAction->setVisible(false);

    if (true) //d->wasEdited)
    {
        d->generator.create(d->edit->toPlainText());
        d->wasEdited = false;
        d->stack->setCurrentWidget(d->waitWidget);
        setCursor(Qt::WaitCursor);

        setEnabled(false);

        PatientManager::instance()->checkAllUpToDate(this);

        ResultWaitDialog dialog(this, tr("Abfrage wird erstellt"), true, Qt::WindowModal);
        dialog.connectTo(&d->generator);
        QFuture<bool> future = QtConcurrent::run(&d->generator, &ReportGenerator::run);
        dialog.waitForResult();

        setEnabled(true);
        unsetCursor();

        if (!future.result())
        {
            d->editAction->trigger();
            return;
        }
    }

    d->model = d->generator.toModel();
    d->tableView->setModel(d->model);
    d->bottomDisplay->setText(d->generator.errors().join('\n'));
    d->bottomDock->setWindowTitle(tr("Fehlermeldungen"));
    d->stack->setCurrentWidget(d->tableView);
    d->saveResultsAction->setVisible(true);
}

void ReportScriptWindow::openScript()
{
    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(configGroupScript);
    QString fileName = QFileDialog::getOpenFileName(this, tr("Skript öffnen"), settings.value(configLastScriptFile).toString());
    if (fileName.isNull())
    {
        return;
    }

    if (!askForSave())
    {
        return;
    }
    if (!loadScript(fileName))
    {
        return;
    }
    settings.setValue(configLastScriptFile, fileName);
}

bool ReportScriptWindow::loadScript(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("Öffnen fehlgeschlagen"), tr("Die Skriptdatei kann nicht gelesen werden:\n%1").arg(file.errorString()));
        return false;
    }
    QTextStream stream(&file);
    QString script = stream.readAll();
    d->edit->setPlainText(script);
    d->fileName = fileName;
    setWindowTitle(file.fileName());
    return true;
}

void ReportScriptWindow::saveScript()
{
    if (d->fileName.isNull())
    {
        return;
    }
    storeScript(d->fileName);
}

void ReportScriptWindow::saveScriptAs()
{
    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(configGroupScript);
    QString fileName = QFileDialog::getSaveFileName(this, tr("Skript speichern"), settings.value(configLastScriptFile).toString());
    if (fileName.isNull())
    {
        return;
    }
    if (!storeScript(fileName))
    {
        return;
    }
    settings.setValue(configLastScriptFile, fileName);
}

bool ReportScriptWindow::storeScript(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("Öffnen fehlgeschlagen"), tr("In die ausgewählte Datei kann nicht geschrieben werden: \n%1").arg(file.errorString()));
        return false;
    }
    QTextStream stream(&file);
    stream << d->edit->toPlainText();
    d->edit->document()->setModified(false);
    d->fileName = fileName;
    updateSaveActions();
    setWindowTitle(file.fileName());
    return true;
}

void ReportScriptWindow::saveResult()
{
    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(configGroupScript);
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Ergebnisse speichern"),
                                                    settings.value(configLastResultFile).toString(),
                                                    tr("CSV-Dateien (*.csv);;JSON-Dateien(*.json)"));
    if (fileName.isNull())
    {
        return;
    }
    if (storeResult(fileName))
    {
        settings.setValue(configLastResultFile, fileName);
    }
}

bool ReportScriptWindow::storeResult(const QString &fileName)
{
    QString errorTitle = tr("Öffnen fehlgeschlagen");
    QString errorMessage = tr("In die ausgewählte Datei kann nicht geschrieben werden:\n%1");
    if (fileName.endsWith("json", Qt::CaseInsensitive))
    {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            QMessageBox::warning(this, errorTitle, errorMessage.arg(file.errorString()));
            return false;
        }
        d->generator.writeJSON(file);
    }
    else
    {
        CSVFile file(';');
        if (!file.openForWriting(fileName))
        {
            QMessageBox::warning(this, errorTitle, errorMessage.arg(file.errorString()));
            return false;
        }
        d->generator.writeToCSV(file);
    }
    return true;
}

void ReportScriptWindow::newScript()
{
    if (!askForSave())
    {
        return;
    }
    d->fileName = QString();
    d->edit->clear();
    d->edit->setFocus();
}

void ReportScriptWindow::scriptEdited()
{
    // two levels of storing changes, one for running the generator, one for saving the script (d->edit->document()->isModified())
    d->wasEdited = true;
    updateHelp();
    updateSaveActions();
}

void ReportScriptWindow::updateSaveActions()
{
    bool saveEdited = d->edit->document()->isModified();
    d->saveScriptAction->setEnabled(!d->fileName.isEmpty() && saveEdited);
    d->saveScriptAsAction->setEnabled(true);
}

void ReportScriptWindow::indexActivated(const QModelIndex &index)
{
    Patient::Ptr p = d->generator.sourcePatient(index.row());
    emit activated(p);
}

void ReportScriptWindow::updateHelp()
{
    QString line = d->currentLine();
    QString syntaxName = line.section(' ', 0, 0);

    if (ReportElement::isValidSyntaxName(syntaxName) || syntaxName == "end")
    {
        d->argumentsListWidget->clear();
        QStringList possibleArguments = ReportElement::possibleArguments(syntaxName);
        foreach (const QString& arg, possibleArguments)
        {
            QListWidgetItem* item = new QListWidgetItem(arg);
            if (arg.contains('<')) // for placeholder fields in <> that shall not be copied verbatim
            {
                item->setFlags(Qt::NoItemFlags);
            }
            d->argumentsListWidget->addItem(item);
        }

        d->rightDock->setWindowTitle(tr("Mögliche Argumente"));
        d->helpDisplaysArguments = true;
        d->bottomDisplay->setText(ReportElement::description(syntaxName));
    }
    else
    {
        d->argumentsListWidget->clear();
        d->argumentsListWidget->addItems(ReportElement::possibleSyntaxNames());
        d->rightDock->setWindowTitle(tr("Mögliche Datenfelder"));
        d->helpDisplaysArguments = false;
        d->bottomDisplay->clear();
    }
}

void ReportScriptWindow::helpItemActivated(QListWidgetItem *item)
{
    if (!item)
    {
        return;
    }
    QTextCursor cursor = d->edit->textCursor();
    QString line = d->currentLine();
    QSignalBlocker blocker(d->edit);
    if (d->helpDisplaysArguments)
    {
        cursor.movePosition(QTextCursor::EndOfLine);
        QString text = item->text();
        if (!line.endsWith(' '))
        {
            text.prepend(' ');
        }
        text += '\n';
        cursor.insertText(text);
    }
    else
    {
        if (item->text().startsWith(line))
        {
            cursor.insertText(item->text().mid(line.size()));
        }
        else
        {
            cursor.movePosition(QTextCursor::StartOfLine);
            cursor.movePosition(QTextCursor::EndOfWord, QTextCursor::KeepAnchor);
            cursor.removeSelectedText();
            cursor.insertText(item->text());
        }
        if (ReportElement::possibleArguments(item->text()).isEmpty())
        {
            cursor.insertText("\n");
        }
    }
    cursor.movePosition(QTextCursor::EndOfWord);
    d->edit->setTextCursor(cursor);
    scriptEdited();
    d->edit->setFocus();
}

void ReportScriptWindow::helpItemSelected()
{
    QListWidgetItem* item = d->argumentsListWidget->currentItem();
    if (!item)
    {
        return;
    }
    d->bottomDisplay->setText(ReportElement::description(item->text()));
}

void ReportScriptWindow::closeEvent(QCloseEvent *event)
{
    bool accept = askForSave();
    if (accept)
    {
        event->accept();
    }
    else
    {
        event->ignore();
    }
}

bool ReportScriptWindow::askForSave()
{
    if (!d->edit->document()->isModified())
    {
        return true;
    }

    QMessageBox::StandardButton answer = QMessageBox::question(this, tr("Speichern?"),
                                                               tr("Das Skript wurde geändert. Möchsten Sie es speichern?"),
                                                               QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    if (answer == QMessageBox::Yes)
    {
        if (!d->fileName.isEmpty())
        {
            storeScript(d->fileName);
        }
        else
        {
            saveScriptAs();
        }
        return true;
    }
    else if (answer == QMessageBox::No)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ReportScriptWindow::setupUI()
{
    d->stack = new QStackedWidget;

    d->tableView = new AnalysisTableView;
    d->stack->addWidget(d->tableView);
    connect(d->tableView, &AnalysisTableView::activated, this, &ReportScriptWindow::indexActivated);

    d->edit      = new QTextEdit;
    d->stack->addWidget(d->edit);
    connect(d->edit, &QTextEdit::textChanged, this, &ReportScriptWindow::scriptEdited);
    connect(d->edit, &QTextEdit::cursorPositionChanged, this, &ReportScriptWindow::updateHelp);

    d->waitWidget = new QWidget;
    QHBoxLayout* waitLayout = new QHBoxLayout;
    QLabel* waitLabel = new QLabel;
    waitLabel->setPixmap(QIcon::fromTheme("hourglass").pixmap(64, 64));
    waitLayout->addStretch();
    waitLayout->addWidget(waitLabel, Qt::AlignCenter);
    waitLayout->addWidget(new QLabel(tr("Lade Daten...")), Qt::AlignCenter);
    waitLayout->addStretch();
    d->waitWidget->setLayout(waitLayout);
    d->stack->addWidget(d->waitWidget);

    d->bottomDisplay = new QTextEdit;
    d->argumentsListWidget = new QListWidget;
    d->argumentsListWidget->setWordWrap(true);
    connect(d->argumentsListWidget, &QListWidget::itemActivated, this, &ReportScriptWindow::helpItemActivated);
    connect(d->argumentsListWidget, &QListWidget::currentItemChanged, this, &ReportScriptWindow::helpItemSelected);

    d->rightDock = new QDockWidget(this);
    d->rightDock->setAllowedAreas(Qt::RightDockWidgetArea);
    d->rightDock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetVerticalTitleBar);
    d->rightDock->setWidget(d->argumentsListWidget);
    addDockWidget(Qt::RightDockWidgetArea, d->rightDock);
    d->bottomDock = new QDockWidget(this);
    d->bottomDock->setAllowedAreas(Qt::BottomDockWidgetArea);
    d->bottomDock->setFeatures(QDockWidget::DockWidgetFloatable);
    d->bottomDock->setWidget(d->bottomDisplay);
    addDockWidget(Qt::BottomDockWidgetArea, d->bottomDock);
    setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);

    setCentralWidget(d->stack);
}

void ReportScriptWindow::setupToolbar()
{
    d->toolBar = addToolBar(tr("Aktionen"));
    d->toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    QActionGroup* centralGroup = new QActionGroup(this);
    d->editAction = new QAction(QIcon::fromTheme("page_edit"),
                                tr("Skript bearbeiten"),
                                centralGroup);
    d->editAction->setCheckable(true);
    d->editAction->setChecked(true);
    connect(d->editAction, &QAction::triggered, this, &ReportScriptWindow::editScript);
    d->resultAction = new QAction(QIcon::fromTheme("application_view_columns"),
                                  tr("Ergebnisse"),
                                  centralGroup);
    d->resultAction->setCheckable(true);
    connect(d->resultAction, &QAction::triggered, this, &ReportScriptWindow::showResult);

    d->toolBar->addActions(centralGroup->actions());
    d->toolBar->addSeparator();

    d->newScriptAction    = d->toolBar->addAction(QIcon::fromTheme("page"),
                                                  tr("Neues Skript"),
                                                  this, &ReportScriptWindow::newScript);
    d->openScriptAction   = d->toolBar->addAction(QIcon::fromTheme("folder_table"),
                                                tr("Skript öffnen..."),
                                                this, &ReportScriptWindow::openScript);
    d->saveScriptAction   = d->toolBar->addAction(QIcon::fromTheme("disk"),
                                                tr("Skript speichern"),
                                                this, &ReportScriptWindow::saveScript);
    d->saveScriptAsAction = d->toolBar->addAction(QIcon::fromTheme("disk_multiple"),
                                                tr("Skript speichern als..."),
                                                this, &ReportScriptWindow::saveScriptAs);
    d->saveResultsAction  = d->toolBar->addAction(QIcon::fromTheme("disk"),
                                                tr("Ergebnisse speichern..."),
                                                this, &ReportScriptWindow::saveResult);

    d->saveScriptAction->setEnabled(false);
    d->saveScriptAsAction->setEnabled(false);
}
