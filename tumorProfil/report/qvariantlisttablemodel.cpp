/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "qvariantlisttablemodel.h"

QVariantListTableModel::QVariantListTableModel(QObject* parent)
    : QAbstractTableModel(parent),
      m_showRowNumbers(false)
{
}
QVariantListTableModel::QVariantListTableModel(const QList< QList<QVariant> >& data, QObject* parent)
    : QAbstractTableModel(parent),
      m_list(data),
      m_showRowNumbers(false)
{
}

void QVariantListTableModel::setData(const QList<QList<QVariant> > &data)
{
    beginResetModel();
    m_list = data;
    endResetModel();
}

void QVariantListTableModel::setColumnHeaders(const QStringList &h)
{
    m_headers = h;
}

void QVariantListTableModel::setShowRowNumbers(bool show)
{
    m_showRowNumbers = show;
}

int QVariantListTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return m_list.size();
}

int QVariantListTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid() || m_list.isEmpty())
    {
        return 0;
    }
    return m_list.first().size();
}

static QVariant forDisplay(const QVariant& v)
{
    switch (v.type())
    {
    case QVariant::Map:
    {
        QStringList l;
        QVariantMap map = v.toMap();
        for (QVariantMap::const_iterator it = map.begin(); it != map.end(); ++it)
        {
            l << it.key() + ":" + forDisplay(it.value()).toString();
        }
        return l.join(", ");
    }
    case QVariant::List:
    case QVariant::StringList:
    {
        return v.toStringList().join(", ");
    }
    default:
        return v;
    }
}

QVariant QVariantListTableModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }
    if (index.row() >= m_list.size())
    {
        return QVariant();
    }
    const QList<QVariant>& line = m_list.at(index.row());
    if (index.column() >= line.size())
    {
        return QVariant();
    }
    return forDisplay(line.at(index.column()));
}

QVariant QVariantListTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            if (section < m_headers.size())
            {
                return m_headers.at(section);
            }
        }
        else
        {
            if (m_showRowNumbers && section < m_list.size())
            {
                return section+1;
            }
        }
    }
    return QVariant();
}

