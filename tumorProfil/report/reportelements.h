/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date: 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef REPORTELEMENTS_H
#define REPORTELEMENTS_H

#include <QDebug>
#include <QObject>
#include <QMetaEnum>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptProgram>
#include <QtScript/QScriptString>

#include "historyreporter.h"
#include "pathologypropertyinfo.h"
#include "patientpropertymodelviewadapter.h"
#include "reportgenerator.h"

class SurvivalResult;
class LabDateValuePair;

template <typename Enum>
Enum stringToEnumChecked(const QString& arguments, Enum defaultValue = static_cast<Enum>(0))
{
    QString s(arguments);
    if (s.contains(' '))
    {
        s = s.section(' ', 0, 0, QString::SectionSkipEmpty);
    }
    QMetaEnum typeEnum = QMetaEnum::fromType<Enum>();
    bool ok;
    Enum value = static_cast<Enum>(typeEnum.keyToValue(s.toLatin1(), &ok));
    if (!ok)
    {
        value = defaultValue;
    }
    return value;
}

class ReportElement
{
public:

    static ReportElement* create(const QString& id);

    static QStringList possibleSyntaxNames();
    static bool isValidSyntaxName(const QString& syntaxName);
    static QStringList possibleArguments(const QString& syntaxName);
    static QString description(const QString& syntaxName);

    enum Type
    {
        Initializer,
        Datum,
        Finisher
    };

    ReportElement() : currentGenerator(0) {}
    virtual ~ReportElement() {}
    virtual void arguments(const QString&);
    virtual void run() = 0;
    virtual Type type() = 0;
    virtual bool isMultiLine() { return false; }

    static QStringList possibleArguments() { return QStringList(); }

    ReportGenerator* currentGenerator;

    // syntactic sugar
    ReportGenerator* generator() { return currentGenerator; }
    const Patient::Ptr patient() const { return currentGenerator->patient(); }
    const Disease& disease() const { return currentGenerator->disease(); }
    class DataStreamHelper
    {
    public:
        DataStreamHelper(ReportElement* element) : element(element) {}
        ReportElement* element;
        DataStreamHelper& operator <<(const QVariant& data) { element->currentGenerator->operator <<(data); return *this; }
        DataStreamHelper& operator <<(const SurvivalResult& data);
    };
    DataStreamHelper data() { return DataStreamHelper(this); }
    bool checkArgumentsNotEmpty(const QString& arguments, const QString& syntaxName);
};
#define RETURN_ON_EMPTY_ARGUMENTS(a) if (!checkArgumentsNotEmpty(a, syntaxName())) { return; }

class ReportElementMetadata
{
public:
    virtual ~ReportElementMetadata() {}
    virtual ReportElement::Type type() const = 0;
    virtual ReportElement* create() const = 0;
    virtual QLatin1String syntaxName() const = 0;
    virtual QString description() const = 0;
    virtual QStringList possibleArguments() const = 0;
};

template <class Element>
class ReportElementMetadataFromClass : public ReportElementMetadata
{
public:
    virtual ReportElement::Type type() const
    {
        return Element::staticType();
    }
    virtual ReportElement* create() const
    {
        return new Element;
    }
    virtual QLatin1String syntaxName() const
    {
        return Element::syntaxName();
    }
    virtual QString description() const
    {
        return Element::description();
    }
    virtual QStringList possibleArguments() const
    {
        return Element::possibleArguments();
    }
};

class ReportInitializer : public ReportElement
{
public:
    virtual ~ReportInitializer() {}
    virtual Type type() { return staticType(); }
    static  Type staticType() { return Initializer; }

    QPair<QString, QStringList> parseGroupList(const QString& arguments, const QString& syntaxName);
};

class FilteringReportInitializer : public ReportInitializer
{
public:
    virtual void run();
    virtual bool filter() = 0;
};

class ReportDatum : public ReportElement
{
public:
    virtual ~ReportDatum() {}
    virtual int columnCount() = 0;
    virtual QString columnHeader(int index) = 0;
    virtual Type type() { return staticType(); }
    static  Type staticType() { return Datum; }
};

class ReportFinisher : public ReportElement
{
public:
    virtual ~ReportFinisher() {}
    virtual void run() {}
    virtual void finished(const QList< QList<QVariant> >& results) = 0;
    virtual Type type() { return staticType(); }
    static  Type staticType() { return Finisher; }

    static QString fileNameFromArguments(const QString& arguments);
};

class FilteringReportFinisher : public ReportFinisher
{
public:
    virtual void run();
    virtual bool filter(const QList<QVariant>&data) = 0;
    void finished(const QList< QList<QVariant> >&) {}
};

#define REPORT_ELEMENT(syntaxname, descr) \
    static QLatin1String syntaxName() { return QLatin1String(syntaxname); } \
    static QString description()      { return QObject::tr(descr); } \

#define REPORT_DATUM(syntaxname, columnHeaderArg, descr) \
    REPORT_ELEMENT(syntaxname, descr) \
    virtual QString columnHeader(int) { return QObject::tr(columnHeaderArg); } \
    virtual int columnCount() { return 1; }

#define SIMPLE_REPORT_DATUM(Name, syntaxname, columnHeaderArg, description) \
    class Name##ReportDatum : public ReportDatum \
    { \
    public: \
    virtual void run(); \
    REPORT_DATUM(syntaxname, columnHeaderArg, description) \
    };

#define SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(Name, syntaxname, columnHeaderArg, description) \
    class Name##ReportDatum : public ReportDatum \
    { \
    public: \
    virtual void run(); \
    virtual void arguments(const QString& arguments); \
    REPORT_DATUM(syntaxname, columnHeaderArg, description) \
    };
#define REPORT_ELEMENT_POSSIBLE_ARGUMENTS(stream) \
    static QStringList possibleArguments() { return QStringList() << stream; }

template <typename Enum>
QStringList enumValues(int nSkipFirst = 0)
{
        QStringList list;
        QMetaEnum qenumEnum = QMetaEnum::fromType<Enum>();
        for (int i=nSkipFirst; i<qenumEnum.keyCount(); i++)
        {
            list << qenumEnum.key(i);
        }
        return list;
}

#define REPORT_ELEMENT_POSSIBLE_ARGUMENTS_QENUM(qenum) \
    static QStringList possibleArguments() { \
        return enumValues<qenum>(1); \
    }

#define SIMPLE_REPORT_DATUM_RUN(Name) void Name##ReportDatum::run()
#define SIMPLE_REPORT_DATUM_ARGUMENTS(Name) void Name##ReportDatum::arguments(const QString& arguments)

class ReportTypeInitializer : public ReportInitializer
{
    Q_GADGET
public:
    // Only Alias for adapter's enum!
    enum ReportType
    {
        InvalidType = PatientPropertyModelViewAdapter::InvalidReport,
        all         = PatientPropertyModelViewAdapter::OverviewReport,
        nsclcAdeno  = PatientPropertyModelViewAdapter::PulmonaryAdenoIHCMut,
        nsclcSq     = PatientPropertyModelViewAdapter::PulmonarySquamousIHCMut,
        nsclc       = PatientPropertyModelViewAdapter::NSCLCIHCMut,
        crc         = PatientPropertyModelViewAdapter::CRCIHCMut,
        breast      = PatientPropertyModelViewAdapter::BreastCaIHCMut,
        nsclcEGFR   = PatientPropertyModelViewAdapter::NSCLCEGFRMutation,
        nsclcBRAF   = PatientPropertyModelViewAdapter::NSCLCBRAFMutation,
        nsclcRAS    = PatientPropertyModelViewAdapter::NSCLCRASMutation,
        nsclcMET    = PatientPropertyModelViewAdapter::NSCLCcMETMutation,
        nsclcHER2   = PatientPropertyModelViewAdapter::NSCLCHER2Mutation,
        alk         = PatientPropertyModelViewAdapter::ALKRearrangement,
        ros1        = PatientPropertyModelViewAdapter::ROS1Rearrangement,
        crcRAS      = PatientPropertyModelViewAdapter::CRCRASMutation,
        crcPIK3CA   = PatientPropertyModelViewAdapter::CRCPIK3Mutation
    };
    Q_ENUM(ReportType)

    ReportTypeInitializer() : reportType(InvalidType) {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    ReportType reportType;

    REPORT_ELEMENT("loadPatients", "Alle Patienten mit Entität bzw. Pathobefund je nach Argument")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS_QENUM(ReportType)
};

class CSVFileInitializer : public ReportInitializer
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    QString fileName;

    REPORT_ELEMENT("loadPatientsFromCSV", "Lade Patienten aus CSV-Datei (Spalten: id, Nachname, Vorname, Geburtsdatum JJJJ-MM-DD; Erste Zeile Spaltenüberschriften)")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Dateiname>"))
};

class SkipInitializer : public FilteringReportInitializer
{
    Q_GADGET
public:
    SkipInitializer() : reason(InvalidReason) {}
    enum SkipReason
    {
        InvalidReason,
        withoutHistory,
        withoutProfile,
        external
    };
    Q_ENUM(SkipReason)

    virtual void arguments(const QString& arguments);
    virtual bool filter();
    SkipReason reason;

    REPORT_ELEMENT("skipPatients", "Patienten aus Liste streichen, wenn Bedingung (s. Argument) erfüllt.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS_QENUM(SkipReason)
};

class OptionReportInitializer : public ReportInitializer
{
    Q_GADGET
 public:

    OptionReportInitializer() : option(InvalidOption) {}

    enum Option
    {
        InvalidOption,
        use01ForTrueFalse,
        isoDate,
        dayDotMonthDotYearDate
    };
    Q_ENUM(Option)

    virtual void arguments(const QString& arguments);
    virtual void run();
    Option option;

    REPORT_ELEMENT("option", "Verschiedene Optionen")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS_QENUM(Option)
};

class CommonScriptCodeReportInitializer : public ReportInitializer
{
public:
    virtual bool isMultiLine() { return true; }
    virtual void arguments(const QString& arguments);
    virtual void run() {}

    REPORT_ELEMENT("scriptSharedCode", "Der enthaltene Code wird allen \"script\" und \"filterScript\" Elementen zur Verfügung gestellt")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Nächste Zeilen: Skript bis \"end\">>"))
};

class DateFilterReportInitializer : public FilteringReportInitializer
{
public:

    enum FilterType
    {
        FilterByInitialDiagnosis,
        FilterByPathologyDate
    };

    enum Strictness
    {
        FilterOnMissingValue,
        PassOnMissingValue
    };

    DateFilterReportInitializer(FilterType type) : filterType(type), strictness(PassOnMissingValue) {}

    virtual void arguments(const QString& arguments);
    virtual bool filter();
    const FilterType filterType;
    Strictness strictness;
    QDate begin;
    QDate end;
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Beginndatum, inklusiv (\u003E), dd.mm.yyyy oder yyyy-mm-dd>") <<
                                      QObject::tr("<optional: Enddatum, exklusiv (<), dd.mm.yyyy oder yyyy-mm-dd>") <<
                                      QObject::tr("strict"))
};

class InitialDiagnosisDateFilterReportInitializer : public DateFilterReportInitializer
{
public:
    InitialDiagnosisDateFilterReportInitializer() : DateFilterReportInitializer(FilterByInitialDiagnosis) {}
    REPORT_ELEMENT("withInitialDiagnosis", "Patienten nach Datum der Erstdiagnose filtern. Modus \"strict\": Datum der Erstdiagnose muss bekannt sein.")
};

class PathologyDateDateFilterReportInitializer : public DateFilterReportInitializer
{
public:
    PathologyDateDateFilterReportInitializer() : DateFilterReportInitializer(FilterByPathologyDate) {}
    REPORT_ELEMENT("withPathologyDate", "Patienten nach (erstem) Befunddatum filtern. Modus \"strict\": Ein Befunddatum muss bekannt sein.")
};

class PatientPropertyFilteringReportInitializer : public FilteringReportInitializer
{
public:

    enum Mode
    {
        RequireProperty,
        ExcludeProperty
    };

    PatientPropertyFilteringReportInitializer(Mode mode) : mode(mode) {}
    virtual bool filter();

    Mode mode;
    QString property;
    QString value;
    QString detailValue;
};

class RebiopsyFilteringReportInitializer : public FilteringReportInitializer
{
public:
    RebiopsyFilteringReportInitializer() : numberOfRebiopsies(1) {}
    virtual void arguments(const QString& arguments);
    virtual bool filter();
    REPORT_ELEMENT("rebiopsied", "Filtert Patienten mit Vorliegen von Rebiopsien. Optional: Anzahl von Rebiopsien")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Anzahl Rebiopsien (optional)"))

    int numberOfRebiopsies;
};


class FilteringReportInitializerGroup : public FilteringReportInitializer
{
public:
    virtual void run();
    virtual bool filter();
    QList<FilteringReportInitializer*> filters;
    virtual bool combine(const QVector<bool>& filterResults) = 0;
};

class FilteringReportInitializerGroupEnd : public ReportInitializer
{
public:
    virtual void run();
    REPORT_ELEMENT("end", "Ende einer logischen Gruppe (\"group\")")
};

class LogicalFilteringReportInitializerGroup : public FilteringReportInitializerGroup
{
public:
    enum Mode
    {
        And,
        Or
    };
    LogicalFilteringReportInitializerGroup() : mode(And) {}
    Mode mode;
    virtual void arguments(const QString& arguments);
    virtual bool combine(const QVector<bool>& filterResults);
    REPORT_ELEMENT("group", "Gruppiere Filterausdrücke")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS("and" << "or")
};

class TrialParticipationReportInitializer : public PatientPropertyFilteringReportInitializer
{
public:
    TrialParticipationReportInitializer() : PatientPropertyFilteringReportInitializer(RequireProperty) {}
    virtual void arguments(const QString& arguments);
    REPORT_ELEMENT("participatingInTrial", "Patienten nach Teilnahme an Studie/Projekt filtern.")
    static QStringList possibleArguments();
};

class TrialParticipationReportDatum: public ReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("trialParticipation", "Teilnahme an Studie/Projekt")
    static QStringList possibleArguments() { return TrialParticipationReportInitializer::possibleArguments(); }
    TrialContextInfo info;
};

class PathologyNetworkParticipationReportInitializer : public PatientPropertyFilteringReportInitializer
{
public:
    PathologyNetworkParticipationReportInitializer() : PatientPropertyFilteringReportInitializer(RequireProperty) {}
    virtual void arguments(const QString& arguments);
    REPORT_ELEMENT("participatingInNetwork", "Patienten nach Teilnahme an Studie/Projekt filtern.")
    static QStringList possibleArguments();
};

class PathologyNetworkParticipationReportDatum: public ReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("pathologyNetworkParticipation", "Teilnahme an Diagnostiknetzwerk")
    static QStringList possibleArguments() { return PathologyNetworkParticipationReportInitializer::possibleArguments(); }
    QString label;
    QString value;
};


class ScriptReportDatum : public ReportDatum
{
public:
    virtual bool isMultiLine() { return true; }
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int     columnCount();
    virtual QString columnHeader(int index);
    QStringList    names;
    QScriptProgram program;
    REPORT_ELEMENT("script",
                 "Script in ECMAScript (JavaScript, http://doc.qt.io/qt-5/ecmascript.html).\n"
                 "Argument: Name des berechneten Wertes (als Spaltenüberschrift genutzt), "
                 "oder mehrere Namen (getrennt mit Komma) wenn mehrere Werte via Array zurückgegeben werden. \n"
                 "Das Script beginnt in der nächsten Zeile und geht bis zu der nächsten Zeile in der nur \"end\" steht. \n"
                 "Als Rückgabewert gilt der letzte berechnete Ausdruck.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Name des berechneten Wertes") << QObject::tr("<nächste Zeilen: Skript bis \"end\">"))
};


class FilterScriptFilteringReportFinisher : public FilteringReportFinisher
{
public:
    virtual bool isMultiLine() { return true; }
    virtual void arguments(const QString& arguments);
    virtual bool filter(const QList<QVariant> &);
    QScriptProgram program;
    REPORT_ELEMENT("filterScript",
                   "Script in ECMAScript (JavaScript, http://doc.qt.io/qt-5/ecmascript.html)."
                   "Das Script beginnt in der nächsten Zeile und geht bis zu der nächsten Zeile in der nur \"end\" steht. \n"
                   "Als Rückgabewert gilt der Wahrheitswert des letzten berechneten Ausdrucks."
                   "Falls der Wert false ergibt, wird die aktuelle Datenzeile aus dem Ergebnis ausgefiltert.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Nächste Zeilen: Skript bis \"end\">"))
};

class SaveToCSVReportFinisher : public ReportFinisher
{
public:
    SaveToCSVReportFinisher(const QChar& delimiter = ';', const QChar& decimalPoint = ',') : delimiter(delimiter), decimalPoint(decimalPoint) {}
    virtual void arguments(const QString& arguments);
    virtual void finished(const QList< QList<QVariant> >& results);
    QString fileName;
    const QChar delimiter;
    const QChar decimalPoint;
    REPORT_ELEMENT("saveToCSV", "Speichert Daten als CSV, Trenner Semikolon, Dezimaltrenner Komma.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Dateiname>"))
};

class SaveToCSVWithCommaFinisher : public SaveToCSVReportFinisher
{
public:
    SaveToCSVWithCommaFinisher() : SaveToCSVReportFinisher(',', '.') {}
    REPORT_ELEMENT("saveToCSVWithComma", "Speichert Daten als CSV, Trenner Komma, Dezimaltrenner Punkt.")
};

class SaveToJSONReportFinisher : public ReportFinisher
{
public:
    virtual void arguments(const QString& arguments);
    virtual void finished(const QList< QList<QVariant> >& results);
    QString fileName;
    REPORT_ELEMENT("saveToJSON", "Speichert Daten als JSON-Datei.\n"
                                 "Im Vergleich zu CSV ineffizient, erlaubt aber das Speichern von eingebetteten Objekten (Laborwerte).\n"
                                 "Das Hauptobjekt der Datei ist ein Array der Zeilen, jede Zeile ein Object mit Spaltenname -> Wert.\n"
                                 "Einlesen in R: jsonlite::fromJSON(fileName) %>% as_tibble()\n"
                                 "Einlesen in Python: pd.read_json(filename)")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Dateiname>"))
};

SIMPLE_REPORT_DATUM(PatientId, "patientId", "id", "Patienten-Id (Datenbank)")
SIMPLE_REPORT_DATUM(MedicoId, "medicoId", "medico id", "Medico-Patientennummer (Per-Nr.)")
SIMPLE_REPORT_DATUM(DiseaseId, "diseaseId", "disease id", "Disease Id (Datenbank)")
SIMPLE_REPORT_DATUM(Name, "name", "surname", "Nachname")
SIMPLE_REPORT_DATUM(FirstName, "firstName", "first name", "Vorname")
SIMPLE_REPORT_DATUM(NameCommaFirstName, "nameCommaFirstName", "name, first name", "Name in der Form \"Name, Vorname\"")
SIMPLE_REPORT_DATUM(DateOfBirth, "dateOfBirth", "date of birth", "Geburtsdatum")
SIMPLE_REPORT_DATUM(Sex, "sex", "sex", "Geschlecht")
SIMPLE_REPORT_DATUM(AgeAtDx, "ageAtDiagnosis", "age at dx", "Alter bei Erstdiagnose")
SIMPLE_REPORT_DATUM(SmokingStatus, "smokingStatus", "smoking status", "Raucherstatus")
SIMPLE_REPORT_DATUM(SmokingPackYears, "smokingPackYears", "smoking pack years", "Packungsjahre")
SIMPLE_REPORT_DATUM(InitialDx, "initialDiagnosis", "initial diagnosis", "Datum der Erstdiagnose")
SIMPLE_REPORT_DATUM(PathologyDate, "pathologyDate", "pathology date", "Earliest date of pathology results")
SIMPLE_REPORT_DATUM(Entity, "entity", "entity", "Entität")
SIMPLE_REPORT_DATUM(T,"t","T","TNM: T")
SIMPLE_REPORT_DATUM(N,"n","N","TNM: N")
SIMPLE_REPORT_DATUM(NPlus,"n_plus","N (+)","TNM: N, allowing N+")
SIMPLE_REPORT_DATUM(M,"m","M","TNM: M")
SIMPLE_REPORT_DATUM(L,"l","L","TNM: L")
SIMPLE_REPORT_DATUM(V,"v","V","TNM: V")
SIMPLE_REPORT_DATUM(G,"g","G","TNM: G")
SIMPLE_REPORT_DATUM(R,"r","R","TNM: R")
SIMPLE_REPORT_DATUM(TNM,"tnm","TNM", "TNM (als Text)")
SIMPLE_REPORT_DATUM(PrimaryTumorLocation, "primaryTumorLocation", "primary tumor location", "Lokalisation Primärtumor")
SIMPLE_REPORT_DATUM(ResectabilityStatus, "resectabilityStatus", "resectability status", "Resektabilität")
SIMPLE_REPORT_DATUM(PathologyReportNumbers, "pathologyReportNumbers", "pathology report numbers", "Bekannte E-/R-/C-/N-Nummern")
//SIMPLE_REPORT_DATUM(, "", "")
SIMPLE_REPORT_DATUM(LastDocumentation, "lastDocumentation", "last documentation", "Zeitpunkt der letzten Dokumentation des Krankheitsverlaufs")
SIMPLE_REPORT_DATUM(LastValidation, "lastValidation", "last validation", "Zeitpunkt der letzten Validierung des Krankheitsverlaufs")

#endif // REPORTELEMENTS_H
