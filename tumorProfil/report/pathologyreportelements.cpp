/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "pathologyreportelements.h"

#include <QRegularExpression>

#include "combinedvalue.h"
#include "ihcscore.h"

void PathologyPropertyReportDatum::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    QString args(arguments);
    QRegularExpression re("\\ball\\b");
    QRegularExpressionMatch match = re.match(args);
    if (match.hasMatch())
    {
        writeMode = ReportGenerator::AllProperties;
        args = args.remove(match.capturedStart(), match.capturedLength()).trimmed();
    }
    property = PathologyPropertyInfo::info(args).property;
}

void PathologyPropertyReportDatum::run()
{
    generator()->writePathologyProperty(property, writeMode);
}

QString PathologyPropertyReportDatum::columnHeader(int)
{
    return PathologyPropertyInfo::info(property).label;
}

QStringList PathologyPropertyReportDatum::possibleArguments()
{
    return possibleArguments(QList<PathologyPropertyInfo::ValueTypeCategory>()
                             << PathologyPropertyInfo::Mutation
                             << PathologyPropertyInfo::MSI
                             << PathologyPropertyInfo::Fish
                             << PathologyPropertyInfo::IHCTwoDim
                             << PathologyPropertyInfo::IHCHScore
                             << PathologyPropertyInfo::IHCBoolean
                             << PathologyPropertyInfo::IHCClassical
                             << PathologyPropertyInfo::BooleanCombination);
}

QStringList PathologyPropertyReportDatum::possibleArguments(const QList<PathologyPropertyInfo::ValueTypeCategory> &valueTypes)
{
    QStringList possibleArgs;
    foreach (PathologyPropertyInfo::ValueTypeCategory valueType, valueTypes)
    {
        foreach (const PathologyPropertyInfo& info, PathologyPropertyInfo::allInfosWithType(valueType))
        {
            possibleArgs << info.id;
        }
    }
    return possibleArgs;
}

SIMPLE_REPORT_DATUM_RUN(PathologyPropertyIHCSplit)
{
    generator()->writeIHCPropertySplit(property, writeMode);
}

QString PathologyPropertyIHCSplitReportDatum::columnHeader(int index)
{
    QString label = PathologyPropertyReportDatum::columnHeader(0);
    if (index == 0)
        return label + " color intensity";
    else
        return label + " percentage";
}

SIMPLE_REPORT_DATUM_RUN(PathologyPropertyHScoreTriple)
{
    // TODO: writeMode
    PathologyPropertyInfo info(property);
    ValueTypeCategoryInfo ihcType(property);
    Property prop = disease().pathologyProperty(info.id);
    HScore hscore = ihcType.toMedicalValue(prop).value<HScore>();
    if (prop.isNull() || !hscore.isValid())
    {
        data() << QVariant() << QVariant() << QVariant();
    }
    else
    {
        data() << hscore.percentageWeak();
        data() << hscore.percentageMedium();
        data() << hscore.percentageStrong();
    }
}

QString PathologyPropertyHScoreTripleReportDatum::columnHeader(int index)
{
    QString label = PathologyPropertyReportDatum::columnHeader(0);
    return label + QString(" IHC %1+").arg(index+1);
}


void PathologyPropertyHScoreTPSReportDatum::run()
{
    PathologyPropertyInfo info(property);
    ValueTypeCategoryInfo ihcType(property);
    Property prop = disease().pathologyProperty(info.id);
    HScore hscore = ihcType.toMedicalValue(prop).value<HScore>();
    if (prop.isNull() || !hscore.isValid())
    {
        data() << QVariant();
    }
    else
    {
        data() << hscore.percentageWeak() + hscore.percentageMedium() + hscore.percentageStrong();
    }
}

QString PathologyPropertyHScoreTPSReportDatum::columnHeader(int)
{
    QString label = PathologyPropertyReportDatum::columnHeader(0);
    return label + " TPS";
}

SIMPLE_REPORT_DATUM_RUN(PathologyPropertyDetail) { generator()->writeDetailValue(property, writeMode); }

QString PathologyPropertyDetailReportDatum::columnHeader(int index)
{
    return PathologyPropertyReportDatum::columnHeader(index) + " detail";
}

QStringList PathologyPropertyDetailReportDatum::possibleArguments()
{
    QList<PathologyPropertyInfo::ValueTypeCategory> valueTypes;
    for (int v = PathologyPropertyInfo::FirstValueTypeCategory; v<=PathologyPropertyInfo::LastValueTypeCategory; v++)
    {
        ValueTypeCategoryInfo info(static_cast<PathologyPropertyInfo::ValueTypeCategory>(v));
        if (info.hasDetail())
        {
            valueTypes << info.category;
        }
    }
    return PathologyPropertyReportDatum::possibleArguments(valueTypes);
}

void PathologyPropertyDateReportDatum::run()
{
    generator()->writePathologyPropertyDate(property, writeMode);
}

QString PathologyPropertyDateReportDatum::columnHeader(int)
{
    QString label = PathologyPropertyReportDatum::columnHeader(0);
    return label + QString(" date");
}

static QVariantMap pathologyAsMap(const Pathology& path)
{
    QVariantMap map;
    map["date"] = path.date;
    map["entity"] = Pathology::entityLabel(path.entity);
    QVariantMap findings;
    foreach (const Property& prop, path.properties)
    {
        findings[prop.property] = QVariantList() << prop.value << prop.detail;
    }
    map["findings"] = findings;
    return map;
}

void PathologyPropertiesListReportDatum::arguments(const QString &arguments)
{
    if (arguments == "all")
    {
        definition = All;
    }
    else
    {
        bool ok;
        number = arguments.toInt(&ok);
        if (ok)
        {
            definition = Number;
        }
    }
}

void PathologyPropertiesListReportDatum::run()
{
    if (definition == All)
    {
        QVariantList list;
        foreach (const Pathology& path, disease().pathologies)
        {
            list << pathologyAsMap(path);
        }
        data() << list;
    }
    else
    {
        if (number < disease().pathologies.size())
        {
            data() << pathologyAsMap(disease().pathologies[number]);
        }
        else
        {
            data() << QVariant();
        }
    }
}

QString PathologyPropertiesListReportDatum::columnHeader(int)
{
    if (definition == All)
    {
        return "pathology results";
    }
    else
    {
        return QString("pathology results %1").arg(number);
    }
}

SIMPLE_REPORT_DATUM_RUN(PathologyReportNumbers)
{
    QSet<QString> numbers;
    QRegularExpression re("\\s+(?<refNumber>[RECN]\\d+\\/\\d+)\\s+");
    foreach (const Pathology& path, generator()->disease().pathologies)
    {
        foreach (const QString& report, path.reports)
        {
            QRegularExpressionMatchIterator it = re.globalMatch(report);
            while (it.hasNext())
            {
                QRegularExpressionMatch match = it.next();
                numbers += match.captured("refNumber");
            }
        }
    }
    data() << QVariant(numbers.values());
}



PathologyReportTextAroundDatum::PathologyReportTextAroundDatum()
{
}

void PathologyReportTextAroundDatum::arguments(const QString &arguments)
{
    searchText = QRegularExpression("[^\\.]*" + arguments + "[^\\.]*"); // Match search string and its full sentence
    if (!searchText.isValid())
    {
        generator()->reportError(searchText.errorString());
    }
}

void PathologyReportTextAroundDatum::run()
{
    if (!searchText.isValid())
    {
        data() << QVariant();
        return;
    }
    QStringList excerpts;
    foreach (const Pathology& path, generator()->disease().pathologies)
    {
        foreach (const QString& report, path.reports)
        {
            QRegularExpressionMatchIterator it = searchText.globalMatch(report);
            while (it.hasNext())
            {
                QRegularExpressionMatch match = it.next();
                excerpts += match.captured();
            }
        }
    }
    data() << excerpts.join(". ");
}

QString PathologyReportTextAroundDatum::columnHeader(int)
{
    return "pathology text containing " + searchText.pattern();
}
