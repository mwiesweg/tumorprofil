/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef PATHOLOGYREPORTELEMENTS_H
#define PATHOLOGYREPORTELEMENTS_H

#include <QRegularExpression>

#include "reportelements.h"


class PathologyPropertyReportDatum : public ReportDatum
{
public:
    PathologyPropertyReportDatum() : property(PathologyPropertyInfo::InvalidProperty), writeMode(ReportGenerator::FirstProperty) {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    PathologyPropertyInfo::Property property;
    ReportGenerator::PropertyWriteMode writeMode;
    REPORT_ELEMENT("path", "Pathologiebefund")
    static QStringList possibleArguments(const QList<PathologyPropertyInfo::ValueTypeCategory>& valueTypes);
    static QStringList possibleArguments();
};

class PathologyPropertyIHCSplitReportDatum : public PathologyPropertyReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int);
    virtual int columnCount() { return 2; }
    REPORT_ELEMENT("pathIHCSplit", "Pathologiebefund: IHC-Score als Stärke der Färbung, Anteil der gefärbten Zellen")
    static QStringList possibleArguments()
    { return PathologyPropertyReportDatum::possibleArguments(QList<PathologyPropertyInfo::ValueTypeCategory>() << PathologyPropertyInfo::IHCTwoDim); }
};

class PathologyPropertyHScoreTripleReportDatum : public PathologyPropertyReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 3; }
    REPORT_ELEMENT("pathHScoreTriple", "Pathologiebefund: H-Score als Anteil der niedrig-, mittel- und stark gefärbten Zellen")
    static QStringList possibleArguments()
    { return PathologyPropertyReportDatum::possibleArguments(QList<PathologyPropertyInfo::ValueTypeCategory>() << PathologyPropertyInfo::IHCHScore); }
};

class PathologyPropertyHScoreTPSReportDatum : public PathologyPropertyReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("pathHScoreTPS", "Pathologiebefund: TPS (Anteil gefärbter Zellen)")
    static QStringList possibleArguments()
    { return PathologyPropertyReportDatum::possibleArguments(QList<PathologyPropertyInfo::ValueTypeCategory>() << PathologyPropertyInfo::IHCHScore); }
};

class PathologyPropertyDetailReportDatum : public PathologyPropertyReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("pathDetail", "Pathologiebefund: Zusatzinformation (z.B. Mutation auf cDNA/Proteinebene, FISH-Ratio)")
    static QStringList possibleArguments();
};

class PathologyPropertyDateReportDatum : public PathologyPropertyReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("pathDate", "Pathologiebefund: Datum des Befundes)")
};

class PathologyPropertiesListReportDatum : public ReportDatum
{
public:
    enum Definition
    {
        All,
        Number
    };

    PathologyPropertiesListReportDatum() : definition(All), number(0) {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("pathologyList", "Pathologiebefunde strukturiert (JSON-Export dringend empfohlen), "
                                    "entweder der n-te Befund "
                                    "(Struktur: {date: Datum, entity: Entität, findings: Feld -> [Wert, Detailwert] }"
                                    "oder alle Befunde "
                                    "(Struktur: [ {date: Datum, findings: Feld -> [Wert, Detailwert] } ]")
    static QStringList possibleArguments()
    { return QStringList() << "all" << QObject::tr("<Nummer (nach Datum)>"); }

    Definition definition;
    int number;
};

class PathologyReportTextAroundDatum : public ReportDatum
{
public:
    PathologyReportTextAroundDatum();
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("pathologyTextAround", "Sätze aus Pathologiebefundtext, die Suchtext enthalten")
    static QStringList possibleArguments()
    { return QStringList() << QObject::tr("<Suchtext, regulärer Ausdruck möglich>"); }

    QRegularExpression searchText;
};


#endif // PATHOLOGYREPORTELEMENTS_H
