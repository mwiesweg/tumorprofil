/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef REPORTGENERATOR_H
#define REPORTGENERATOR_H

// Qt includes

#include <QAbstractTableModel>
#include <QList>
#include <QTextStream>

// Local includes

#include "pathologypropertyinfo.h"
#include "patient.h"
#include "disease.h"
#include "diseasehistory.h"

class QFile;
class CSVFile;
class FilteringReportInitializer;
class FilteringReportInitializerGroup;
class FilteringReportFinisher;
class HistoryReporter;
class QScriptEngine;

class ReportGenerator : public QObject
{
    Q_OBJECT
public:
    ReportGenerator();
    ~ReportGenerator();

    void create(const QString& specs);
    void create(QTextStream& stream);
    bool run();

    enum BoolMode
    {
        BoolAsBool,
        BoolAs01
    };
    void setBoolMode(BoolMode mode);
    enum DateMode
    {
        ISODate,
        DayMonthYearDotsDate
    };
    void setDateMode(DateMode mode);

    QStringList columnHeaders() const;
    QStringList datumsSpecifications() const;
    const QList< QList<QVariant> >& result() const;
    Patient::Ptr sourcePatient(int resultRow) const;

    QString dateToString(const QDate& date) const;
    QString dateTimeToString(const QDateTime& dateTime) const;
    void writeToCSV(const QString& fileName, const QChar& delimiter = ';') const;
    void writeToCSV(CSVFile& file) const;
    void writeJSON(const QString& fileName) const;
    void writeJSON(QFile& file) const;
    QAbstractTableModel* toModel() const;

    void addPreFilter(FilteringReportInitializer* filter);
    void addPreFilterGroup(FilteringReportInitializerGroup* filter);
    void closePreFilterGroup();
    void addPostFilter(FilteringReportFinisher* filter);
    void populate(const QList<Patient::Ptr>& patients);
    void needScripting();
    void addCommonScriptCode(const QString& scriptCode);
    QString commonScriptCode() const;
    void needHistory();
    void needLabResults();
    void reportError(const QString& error);
    void reportErrors(const QStringList& errors);
    QStringList errors() const;
    void populateFromCSVFile(const QString& fileName);
    void recordDatum(const QVariant& datum);
    ReportGenerator& operator <<(const QVariant& datum);

    const Patient::Ptr patient() const;
    const Disease& disease() const;
    HistoryReporter* historyReporter() const;
    QScriptEngine* scriptEngine() const;
    const LabResults& labResults() const;
    void registerLabGroup(const QString& id, const QStringList& keys);
    bool isLabGroup(const QString& id) const;
    QStringList labGroup(const QString& id) const;

    enum PropertyWriteMode
    {
        FirstProperty,
        AllProperties
    };

    QVariant writePathologyProperty(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);
    QVariant writeDetailValue(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);
    bool     hasDetailValue(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);
    void     writeIHCPropertySplit(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);
    void     writeIHCIsPositive(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);
    void     writePathologyPropertyDate(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode);

    QList<Patient::Ptr> patientsFromCSV(const QString& path);
    void writeActionableCombinations(const QList<Patient::Ptr>& patients);

    static void test();

signals:

    void started();
    void finished();
    void progressRangeChanged(int minimum, int maximum);
    void progressValueChanged(int progressValue);
    void progressTextChanged(const QString &progressText);

public slots:

    void cancel();

private:

    class ReportGeneratorPriv;
    ReportGeneratorPriv* const d;
};

#endif // REPORTGENERATOR_H
