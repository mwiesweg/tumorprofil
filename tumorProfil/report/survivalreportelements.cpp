/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "survivalreportelements.h"

#include <QRegularExpression>

void UseSubstanceGroupReportInitializer::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    QPair<QString, QStringList> pair = parseGroupList(arguments, syntaxName());
    if (!pair.first.isEmpty())
    {
        generator()->needHistory();
        id = pair.first;
        substances = pair.second;
    }
}

void UseSubstanceGroupReportInitializer::run()
{
    if (!id.isEmpty())
    {
        generator()->historyReporter()->needSubstanceSpecific(id, substances);
    }
}

void UseSubstanceReportInitializer::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    if (arguments.contains(","))
    {
        generator()->reportError(QString("In useSubstance: Argument with comma, you want to use useSubstanceGroup? Skipping: ") + arguments);
        return;
    }
    generator()->needHistory();
    substance = arguments;
}

void UseSubstanceReportInitializer::run()
{
    if (!substance.isEmpty())
    {
        generator()->historyReporter()->needSubstanceSpecific(substance, QStringList() << substance);
    }
}

SIMPLE_REPORT_DATUM_ARGUMENTS(TherapyCategory)
{
    ReportDatum::arguments(arguments);
    generator()->needHistory();
    generator()->historyReporter()->needTherapyCategory();
}

SIMPLE_REPORT_DATUM_RUN(TherapyCategory)
{
    data() << int(generator()->historyReporter()->therapyCategory());
}

SIMPLE_REPORT_DATUM_ARGUMENTS(NumberOfLines)
{
    ReportDatum::arguments(arguments);
    generator()->needHistory();
    generator()->historyReporter()->needLines();
}

SIMPLE_REPORT_DATUM_RUN(NumberOfLines)
{
    data() << generator()->historyReporter()->numberOfLines(false);
}

SIMPLE_REPORT_DATUM_ARGUMENTS(NumberOfPalliativeLines)
{
    ReportDatum::arguments(arguments);
    generator()->needHistory();
    generator()->historyReporter()->needLines();
    generator()->historyReporter()->needTherapyCategory();
}

SIMPLE_REPORT_DATUM_RUN(NumberOfPalliativeLines)
{
    data() << generator()->historyReporter()->numberOfLines(true);
}

SIMPLE_REPORT_DATUM_ARGUMENTS(NumberOfAdjuvantTherapies)
{
    ReportDatum::arguments(arguments);
    generator()->needHistory();
    generator()->historyReporter()->needLines();
    generator()->historyReporter()->needTherapyCategory();
}

SIMPLE_REPORT_DATUM_RUN(NumberOfAdjuvantTherapies)
{
    data() << generator()->historyReporter()->numberOfAdjuvantTherapies();
}

void SubstanceClassReportDatum::arguments(const QString& arguments)
{
    generator()->needHistory();
    id = parseKeywordPalliative(arguments);
}

QString SubstanceClassReportDatum::parseKeywordPalliative(const QString &arguments)
{
    QRegularExpression re("\\bpalliative\\b");
    QRegularExpressionMatch match = re.match(arguments);
    if (match.hasMatch())
    {
        palliative = true;
        generator()->needHistory();
        generator()->historyReporter()->needTherapyCategory();
        return QString(arguments).remove(match.capturedStart(), match.capturedLength()).trimmed();
    }
    palliative = false;
    return arguments;
}

void FirstLineWithReportDatum::run()
{
    int number = generator()->historyReporter()->lineNumber(id, palliative);
    if (number)
    {
        data() << number;
    }
    else
    {
        data() << QVariant();
    }
}

QString FirstLineWithReportDatum::columnHeader(int)
{
    return QString("first line with ") + id + (palliative ? " palliative" : "");
}

void OSReportDatum::arguments(const QString& arguments)
{
    generator()->needHistory();
    if (arguments.isEmpty())
    {
        definition = fromFirstTherapy;
        generator()->historyReporter()->needOS(static_cast<HistoryReporter::OSDefinition>(definition));
    }
    else
    {
        definition = stringToEnumChecked<Definition>(arguments, otherArgument);
        if (definition == otherArgument)
        {
            // is is a substance class id
            SubstanceClassOrLineReportDatum::arguments(arguments);
        }
        else
        {
            generator()->historyReporter()->needOS(static_cast<HistoryReporter::OSDefinition>(definition));
        }
    }
}

void OSReportDatum::run()
{
    switch (definition)
    {
    case fromFirstTherapy:
    case fromInitialDiagnosis:
        // SurvivalData: two data points
        data() << generator()->historyReporter()->os(static_cast<HistoryReporter::OSDefinition>(definition)); // values are sync'ed
        return;
    // The more "specialised" forms do not include the censoring data, which can be reused from one OS field.
    case fromPalliativeSetting:
    {
        SurvivalResult result = generator()->historyReporter()->os(static_cast<HistoryReporter::OSDefinition>(definition));
        if (result.isValid())
        {
            data() << result.days;
            return;
        }
        break;
    }
    case otherArgument:
    {
        SurvivalResult result;
        if (line > 0)
        {
            // 1-based index -> 0-based index
            result = generator()->historyReporter()->os(line-1, palliative);
        }
        else if (!id.isEmpty())
        {
            if (!generator()->historyReporter()->isRegisteredSubstance(id))
            {
                generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
            }
            result = generator()->historyReporter()->os(id, palliative);
        }
        if (result.isValid())
        {
            data() << result.days;
            return;
        }
    }
    }
    data() << QVariant();
}

QString OSReportDatum::columnHeader(int index)
{
    switch (definition)
    {
    case fromFirstTherapy:
        return (index == 0 ? "OS" : "OS reached");
    case fromInitialDiagnosis:
        return  (index == 0 ? "OS initialDx" : "OS reached");
    case fromPalliativeSetting:
        return  "OS palliative";
    case otherArgument:
        return columnHeaderForLineOrSubstance("OS%1", "OS %1");
    }
    return QString();
}

int OSReportDatum::columnCount()
{
    switch (definition)
    {
    case fromFirstTherapy:
    case fromInitialDiagnosis:
        return 2;
    case fromPalliativeSetting:
    case otherArgument:
        return 1;
    }
    return 1;
}

void DCOReportDatum::arguments(const QString &arguments)
{
    OSReportDatum::arguments(arguments);
}

void DCOReportDatum::run()
{
    switch (definition)
    {
    case fromFirstTherapy:
    case fromInitialDiagnosis:
    case fromPalliativeSetting:
        data() << generator()->historyReporter()->dataCutOffTime(static_cast<HistoryReporter::OSDefinition>(definition)); // values are sync'ed
        return;
    case otherArgument:
    {
        if (line > 0)
        {
            // 1-based index -> 0-based index
            data() << generator()->historyReporter()->dataCutOffTime(line-1, palliative);
        }
        else if (!id.isEmpty())
        {
            if (!generator()->historyReporter()->isRegisteredSubstance(id))
            {
                generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
            }
            data() << generator()->historyReporter()->dataCutOffTime(id, palliative);
        }
        return;
    }
    }
    data() << QVariant();
}

int DCOReportDatum::columnCount()
{
    return 1;
}

QString DCOReportDatum::columnHeader(int)
{
    switch (definition)
    {
    case fromFirstTherapy:
        return "DCO";
    case fromInitialDiagnosis:
        return  "DCO initiaDx";
    case fromPalliativeSetting:
        return  "DCO palliative";
    case otherArgument:
        return "DCO " + id;
    }
    return QString();
}

void SubstanceClassOrLineReportDatum::arguments(const QString &arguments)
{
    generator()->needHistory();
    QString argumentsParsed = parseKeywordPalliative(arguments);
    QRegularExpression re("\\D");
    if (re.match(argumentsParsed).hasMatch())
    {
        // dont call superclass arguments() because we already called parsePalliative() which alters arguments.
        id = argumentsParsed;
    }
    else
    {
        // all digits - 1-based index
        line = argumentsParsed.toInt();
    }

    if (line > 0 || !id.isEmpty())
    {
        generator()->historyReporter()->needLines();
    }
}

QString SubstanceClassOrLineReportDatum::columnHeaderForLineOrSubstance(const QString &lineHeader, const QString &substanceHeader)
{
    if (line > 0)
    {
        return lineHeader.arg(line) + (palliative ? " palliative" : "");
    }
    else
    {
        return substanceHeader.arg(id) + (palliative ? " palliative" : "");
    }
}

QString SubstanceClassOrLineReportDatumMultiple::parseKeywordAll(const QString &arguments)
{
    QRegularExpression re("\\ball\\b");
    QRegularExpressionMatch match = re.match(arguments);
    if (match.hasMatch())
    {
        all_lines = true;
        return QString(arguments).remove(match.capturedStart(), match.capturedLength()).trimmed();
    }
    all_lines = false;
    return arguments;
}

void SubstanceClassOrLineReportDatumMultiple::arguments(const QString &arguments)
{
    QString argumentsParsed = parseKeywordAll(arguments);
    SubstanceClassOrLineReportDatum::arguments(argumentsParsed);
}

void LinearLineNumberReportDatum::arguments(const QString &arguments)
{
    SubstanceClassOrLineReportDatum::arguments(arguments);
    if (line < 1)
    {
        generator()->reportError("Need numeric line number starting with 1");
    }
}


void TTFReportDatum::arguments(const QString& arguments)
{
    SubstanceClassOrLineReportDatum::arguments(arguments);
    if (line > 0)
    {
        generator()->historyReporter()->needTTF(line);
    }
}

void TTFReportDatum::run()
{
    if (line > 0)
    {
        // 1-based index -> 0-based index
        data() << generator()->historyReporter()->ttf(line-1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->ttf(id, palliative);
    }
}

QString TTFReportDatum::columnHeader(int index)
{
    QString name = columnHeaderForLineOrSubstance("TTF%1", "TTF %1");
    return index == 0 ? name : name + " reached";
}

void BestResponseReportDatum::arguments(const QString& arguments)
{
    SubstanceClassOrLineReportDatum::arguments(arguments);
    if (line > 0)
    {
        generator()->historyReporter()->needBestResponse(line);
    }
}

void BestResponseReportDatum::run()
{
    if (line > 0)
    {
        // 1-based index -> 0-based index
        data() << generator()->historyReporter()->bestResponse(line-1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->bestResponse(id, palliative);
    }
}


void PFSReportDatum::arguments(const QString &arguments)
{
    SubstanceClassOrLineReportDatum::arguments(arguments);
    if (line > 0)
    {
        generator()->historyReporter()->needPFS(line);
    }
}

void PFSReportDatum::run()
{
    if (line > 0)
    {
        // 1-based index -> 0-based index
        data() << generator()->historyReporter()->pfs(line-1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->pfs(id, palliative);
    }
}

QString PFSReportDatum::columnHeader(int index)
{
    QString name = columnHeaderForLineOrSubstance("PFS%1", "PFS %1");
    return index == 0 ? name : name + " reached";
}

QString BestResponseReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("best response line %1", "best response %1");
}

void LineStartDateReportDatum::run()
{
    QPair<QDate, QDate> dates;
    if (line > 0)
    {
        dates = generator()->historyReporter()->lineDates(line-1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        dates = generator()->historyReporter()->lineDates(id, palliative);
    }

    if (dates.first.isValid())
    {
        data() << dates.first;
    }
    else
    {
        data() << QVariant();
    }
}

QString LineStartDateReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("start date line %1", "start date %1");
}

void SubstancesOfLineReportDatum::run()
{
    QStringList substances;
    if (line > 0)
    {
        substances = generator()->historyReporter()->substances(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        substances = generator()->historyReporter()->substances(id, palliative);
    }

    if (substances.isEmpty())
    {
        data() << QVariant();
    }
    else
    {
        substances.sort();
        data() << substances;
    }
}

QString SubstancesOfLineReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("substances line %1", "substances %1");
}


void IncludesDeescalationTherapyReportDatum::run()
{
    QVariant hasMaintenance;
    if (line > 0)
    {
        hasMaintenance = generator()->historyReporter()->includesDeescalation(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        hasMaintenance = generator()->historyReporter()->includesDeescalation(id, palliative);
    }

    data() << hasMaintenance;
}


QString IncludesDeescalationTherapyReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("includes deescalation line %1", "includes deescalation %1");
}


void DeescalatedSubstancesReportDatum::run()
{
    QVariant substances;
    if (line > 0)
    {
        substances = QVariant(generator()->historyReporter()->descalatedSubstances(line - 1, palliative));
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        substances = QVariant(generator()->historyReporter()->descalatedSubstances(id, palliative));
    }

    data() << substances;
}

QString DeescalatedSubstancesReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("deescalation removed substances line %1", "deescalation removed substances %1");
}

void TimeToDeescalationReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->daysToDeescalation(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->daysToDeescalation(id, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString TimeToDeescalationReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("time to deescalation in line %1", "time to deescalation %1");
}

void DurationOfDeescalationReportDatum::run()
{
    // CtxDuration - TimeToDeescalation
    if (line > 0)
    {
        QVariant timeTo = generator()->historyReporter()->daysToDeescalation(line - 1, palliative);
        if (timeTo.isValid())
        {
            data() << generator()->historyReporter()->ctxDays(line - 1, palliative).toInt() - timeTo.toInt();
        }
        else
        {
            data() << QVariant();
        }
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        QVariant timeTo = generator()->historyReporter()->daysToDeescalation(id, palliative);
        if (timeTo.isValid())
        {
            data() << generator()->historyReporter()->ctxDays(id, palliative).toInt() - timeTo.toInt();
        }
        else
        {
            data() << QVariant();
        }
    }
    else
    {
        data() << QVariant();
    }
}

QString DurationOfDeescalationReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("duration of deescalation in line %1", "duration of deescalation %1");
}

void CtxDurationReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->ctxDays(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->ctxDays(id, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString CtxDurationReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("ctx duration %1", "ctx duration %1");
}

void ToxicitiesReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->toxicities(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->toxicities(id, palliative, all_lines);
    }
    else
    {
        data() << QVariant();
    }
}

QString ToxicitiesReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("toxicities %1", "toxicities %1");
}

void SurgeryReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->surgery(line - 1, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString SurgeryReportDatum::columnHeader(int)
{
    return QString("surgery %1").arg(line);
}

void RadiationReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->radiation(line - 1, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString RadiationReportDatum::columnHeader(int)
{
    return QString("radiation %1").arg(line);
}

void SubstancesDosingReportDatum::run()
{
    if (line > 0)
    {
        data() << generator()->historyReporter()->substancesDosing(line - 1, palliative);
    }
    else if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->substancesDosing(id, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString SubstancesDosingReportDatum::columnHeader(int)
{
    return columnHeaderForLineOrSubstance("substances and dosing %1", "substances and dosing %1");
}

void SubstanceDosesReportDatum::run()
{
    if (!id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(id));
        }
        data() << generator()->historyReporter()->substanceDoses(id, palliative);
    }
    else
    {
        data() << QVariant();
    }
}

QString SubstanceDosesReportDatum::columnHeader(int)
{
    return QString("substance doses %1").arg(id);
}

SIMPLE_REPORT_DATUM_ARGUMENTS(LastDiseaseState)
{
    Q_UNUSED(arguments)
    generator()->needHistory();
    generator()->historyReporter()->needCurrentState();
}

SIMPLE_REPORT_DATUM_RUN(LastDiseaseState)
{
    QMetaEnum stateEnum = QMetaEnum::fromType<DiseaseState::State>();
    data() << stateEnum.valueToKey(generator()->historyReporter()->lastState());
}

SIMPLE_REPORT_DATUM_ARGUMENTS(FirstRelapseDate)
{
    Q_UNUSED(arguments)
    generator()->needHistory();
    generator()->historyReporter()->needTherapyCategory();
}

SIMPLE_REPORT_DATUM_RUN(FirstRelapseDate)
{
    data() << generator()->historyReporter()->relapseDate();
}

SIMPLE_REPORT_DATUM_ARGUMENTS(TimeSinceLastFollowUp)
{
    Q_UNUSED(arguments)
    generator()->needHistory();
    generator()->historyReporter()->needCurrentState();
    generator()->historyReporter()->needOS(HistoryReporter::FromFirstTherapy);
}

SIMPLE_REPORT_DATUM_RUN(TimeSinceLastFollowUp)
{
    data() << disease().history.lastDocumentation().daysTo(QDate::currentDate());
}

void DFSReportDatum::arguments(const QString& arguments)
{
    Q_UNUSED(arguments)
    generator()->needHistory();
    generator()->historyReporter()->needTherapyCategory();
}

QString DFSReportDatum::columnHeader(int index)
{
    return (index == 0 ? "DFS" : "DFS reached");
}
void DFSReportDatum::run()
{
    data() << generator()->historyReporter()->dfs();
}
