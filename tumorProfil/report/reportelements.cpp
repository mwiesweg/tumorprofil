/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "reportelements.h"

// Qt includes

#include <QDebug>
#include <QDir>
#include <QMetaEnum>
#include <QRegularExpression>
#include <QtScript/QScriptSyntaxCheckResult>

// Local includes

// elements moved to separate files
#include "labreportelements.h"
#include "pathologyreportelements.h"
#include "survivalreportelements.h"

#include "combinedvalue.h"
#include "csvfile.h"
#include "databaseconstants.h"
#include "ihcscore.h"
#include "patientmodel.h"
#include "patientpropertyfiltermodel.h"

class ReportElementMetadataList : public QHash<QString, ReportElementMetadata*>
{
public:

#define REPORT_ELEMENT_METADATA(Name) \
    { \
        ReportElementMetadataFromClass<Name> *metadata = new ReportElementMetadataFromClass<Name>(); \
        insert(metadata->syntaxName(), metadata); \
        orderedSyntaxNames << metadata->syntaxName(); \
    }

    ReportElementMetadataList()
    {
        REPORT_ELEMENT_METADATA(ReportTypeInitializer)
                REPORT_ELEMENT_METADATA(CSVFileInitializer)
                REPORT_ELEMENT_METADATA(SkipInitializer)
                REPORT_ELEMENT_METADATA(PathologyDateDateFilterReportInitializer)
                REPORT_ELEMENT_METADATA(InitialDiagnosisDateFilterReportInitializer)
                REPORT_ELEMENT_METADATA(UseSubstanceGroupReportInitializer)
                REPORT_ELEMENT_METADATA(UseSubstanceReportInitializer)
                REPORT_ELEMENT_METADATA(UseLabValueGroupReportInitializer)
                REPORT_ELEMENT_METADATA(OptionReportInitializer)
                REPORT_ELEMENT_METADATA(TrialParticipationReportInitializer)
                REPORT_ELEMENT_METADATA(PathologyNetworkParticipationReportInitializer)
                REPORT_ELEMENT_METADATA(RebiopsyFilteringReportInitializer)
                REPORT_ELEMENT_METADATA(LogicalFilteringReportInitializerGroup)
                REPORT_ELEMENT_METADATA(FilteringReportInitializerGroupEnd)

                REPORT_ELEMENT_METADATA(PatientIdReportDatum)
                REPORT_ELEMENT_METADATA(MedicoIdReportDatum)
                REPORT_ELEMENT_METADATA(DiseaseIdReportDatum)
                REPORT_ELEMENT_METADATA(NameReportDatum)
                REPORT_ELEMENT_METADATA(FirstNameReportDatum)
                REPORT_ELEMENT_METADATA(NameCommaFirstNameReportDatum)
                REPORT_ELEMENT_METADATA(DateOfBirthReportDatum)
                REPORT_ELEMENT_METADATA(SexReportDatum)
                REPORT_ELEMENT_METADATA(InitialDxReportDatum)
                REPORT_ELEMENT_METADATA(SmokingStatusReportDatum)
                REPORT_ELEMENT_METADATA(SmokingPackYearsReportDatum)
                REPORT_ELEMENT_METADATA(PathologyDateReportDatum)
                REPORT_ELEMENT_METADATA(EntityReportDatum)
                REPORT_ELEMENT_METADATA(AgeAtDxReportDatum)
                REPORT_ELEMENT_METADATA(TReportDatum)
                REPORT_ELEMENT_METADATA(NReportDatum)
                REPORT_ELEMENT_METADATA(NPlusReportDatum)
                REPORT_ELEMENT_METADATA(MReportDatum)
                REPORT_ELEMENT_METADATA(LReportDatum)
                REPORT_ELEMENT_METADATA(VReportDatum)
                REPORT_ELEMENT_METADATA(GReportDatum)
                REPORT_ELEMENT_METADATA(RReportDatum)
                REPORT_ELEMENT_METADATA(TNMReportDatum)
                REPORT_ELEMENT_METADATA(PrimaryTumorLocationReportDatum)
                REPORT_ELEMENT_METADATA(ResectabilityStatusReportDatum)

                REPORT_ELEMENT_METADATA(PathologyPropertyReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertyDetailReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertyDateReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertyIHCSplitReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertyHScoreTripleReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertyHScoreTPSReportDatum)
                REPORT_ELEMENT_METADATA(PathologyPropertiesListReportDatum)
                REPORT_ELEMENT_METADATA(OSReportDatum)
                REPORT_ELEMENT_METADATA(TTFReportDatum)
                REPORT_ELEMENT_METADATA(PFSReportDatum)
                REPORT_ELEMENT_METADATA(BestResponseReportDatum)
                REPORT_ELEMENT_METADATA(LineStartDateReportDatum)
                REPORT_ELEMENT_METADATA(CtxDurationReportDatum)
                REPORT_ELEMENT_METADATA(ToxicitiesReportDatum)
                REPORT_ELEMENT_METADATA(SubstancesDosingReportDatum)
                REPORT_ELEMENT_METADATA(SubstanceDosesReportDatum)
                REPORT_ELEMENT_METADATA(SubstancesOfLineReportDatum)
                REPORT_ELEMENT_METADATA(IncludesDeescalationTherapyReportDatum)
                REPORT_ELEMENT_METADATA(DeescalatedSubstancesReportDatum)
                REPORT_ELEMENT_METADATA(TimeToDeescalationReportDatum)
                REPORT_ELEMENT_METADATA(DurationOfDeescalationReportDatum)
                REPORT_ELEMENT_METADATA(LineStartDateReportDatum)
                REPORT_ELEMENT_METADATA(NumberOfLinesReportDatum)
                REPORT_ELEMENT_METADATA(NumberOfPalliativeLinesReportDatum)
                REPORT_ELEMENT_METADATA(NumberOfAdjuvantTherapiesReportDatum)
                REPORT_ELEMENT_METADATA(FirstLineWithReportDatum)
                REPORT_ELEMENT_METADATA(RadiationReportDatum)
                REPORT_ELEMENT_METADATA(SurgeryReportDatum)
                REPORT_ELEMENT_METADATA(TherapyCategoryReportDatum)
                REPORT_ELEMENT_METADATA(FirstRelapseDateReportDatum)
                REPORT_ELEMENT_METADATA(DFSReportDatum)
                REPORT_ELEMENT_METADATA(LastDiseaseStateReportDatum)
                REPORT_ELEMENT_METADATA(LastDocumentationReportDatum)
                REPORT_ELEMENT_METADATA(LastValidationReportDatum)
                REPORT_ELEMENT_METADATA(TimeSinceLastFollowUpReportDatum)
                REPORT_ELEMENT_METADATA(PathologyReportNumbersReportDatum)
                REPORT_ELEMENT_METADATA(TrialParticipationReportDatum)                
                REPORT_ELEMENT_METADATA(PathologyNetworkParticipationReportDatum)
                REPORT_ELEMENT_METADATA(DCOReportDatum)
                REPORT_ELEMENT_METADATA(PathologyReportTextAroundDatum)

                REPORT_ELEMENT_METADATA(LabValueAtReportDatum)
                REPORT_ELEMENT_METADATA(LabValuesDuringReportDatum)
                REPORT_ELEMENT_METADATA(LabValuesAtStartOfLineReportDatum)
                REPORT_ELEMENT_METADATA(LabValuesDuringLineReportDatum)

                REPORT_ELEMENT_METADATA(CommonScriptCodeReportInitializer)
                REPORT_ELEMENT_METADATA(ScriptReportDatum)

                REPORT_ELEMENT_METADATA(FilterScriptFilteringReportFinisher)
                REPORT_ELEMENT_METADATA(SaveToCSVReportFinisher)
                REPORT_ELEMENT_METADATA(SaveToCSVWithCommaFinisher)
                REPORT_ELEMENT_METADATA(SaveToJSONReportFinisher)

                //REPORT_ELEMENT_METADATA()
    }
    ~ReportElementMetadataList()
    {
        for (iterator it=begin(); it != end(); ++it)
        {
            delete it.value();
        }
    }

    QStringList orderedSyntaxNames;
};

Q_GLOBAL_STATIC(ReportElementMetadataList, metadataList)

ReportElement* ReportElement::create(const QString& id)
{
    ReportElementMetadata* metadata = metadataList->value(id);
    if (!metadata)
    {
        return 0;
    }
    return metadata->create();
}

QStringList ReportElement::possibleSyntaxNames()
{
    return metadataList->orderedSyntaxNames;
}

bool ReportElement::isValidSyntaxName(const QString &syntaxName)
{
    return metadataList->contains(syntaxName);
}

QStringList ReportElement::possibleArguments(const QString &syntaxName)
{
    ReportElementMetadata* metadata = metadataList->value(syntaxName);
    QStringList arguments;
    if (metadata)
    {
        arguments = metadata->possibleArguments();
    }
    return arguments;
}

QString ReportElement::description(const QString &syntaxName)
{
    ReportElementMetadata* metadata = metadataList->value(syntaxName);
    if (metadata)
    {
        return metadata->description();
    }
    return QString();
}

void ReportElement::arguments(const QString &s)
{
    if (!s.isEmpty())
    {
        generator()->reportError("Unused argument " + s);
    }
}

bool ReportElement::checkArgumentsNotEmpty(const QString& a, const QString& name)
{
    if (a.isEmpty())
    {
        generator()->reportError(QString("Skipping %1: Empty argument").arg(name));
        return false;
    }
    return true;
}

ReportElement::DataStreamHelper& ReportElement::DataStreamHelper::operator<<(const SurvivalResult& data)
{
    if (data.isValid())
    {
        operator<<(data.days);
        operator<<(int(data.eventReached));
    }
    else
    {
        operator<<(QVariant());
        operator<<(QVariant());
    }
    return *this;
}

QPair<QString, QStringList> ReportInitializer::parseGroupList(const QString &arguments, const QString &syntaxName)
{
    if (!arguments.contains(':'))
    {
        generator()->reportError(QString("In %1: found no id followed by a ':' (%2)").arg(syntaxName).arg(arguments));
        return QPair<QString, QStringList>();
    }
    QString id = arguments.section(':', 0, 0).trimmed();
    QStringList args = arguments.mid(id.size()+1).split(',', Qt::SkipEmptyParts);
    if (args.isEmpty())
    {
        generator()->reportError(QString("In %1: no or insufficient arguments (%2").arg(syntaxName).arg(arguments));
        return QPair<QString, QStringList>();
    }
    QStringList keys;
    keys.reserve(args.size());
    foreach (const QString arg, args)
    {
        keys << arg.trimmed();
    }
    return qMakePair(id, keys);
}

void FilteringReportInitializer::run()
{
    generator()->addPreFilter(this);
}

void FilteringReportFinisher::run()
{
    generator()->addPostFilter(this);
}

void ReportTypeInitializer::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    reportType = stringToEnumChecked<ReportType>(arguments, InvalidType);
}

void ReportTypeInitializer::run()
{
    PatientPropertyModelViewAdapter models;
    models.setReportType(static_cast<PatientPropertyModelViewAdapter::ReportType>(reportType));
    const int size = models.filterModel()->rowCount();
    QList<Patient::Ptr> patients;
    for (int i=0; i<size; i++)
    {
        patients += PatientModel::retrievePatient(models.filterModel()->index(i, 0));
    }
    generator()->populate(patients);
}

void CSVFileInitializer::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    fileName = arguments;
    fileName.remove("'");
    fileName.remove('"');
    fileName = QDir::cleanPath(fileName);
    fileName.replace('~', QDir::homePath());
}

void CSVFileInitializer::run()
{
    generator()->populateFromCSVFile(fileName);
}

void SkipInitializer::arguments(const QString& arguments)
{
    reason = stringToEnumChecked<SkipReason>(arguments, InvalidReason);
}

bool SkipInitializer::filter()
{
    switch (reason)
    {
    case InvalidReason:
        break;
    case withoutHistory:
        return !disease().history.isEmpty();
    case withoutProfile:
        return disease().hasProfilePathology();
    case external:
        if (patient()->surname.contains("Dktk"))
        {
            return false;
        }
        break;
    }
    return true;
}

void OptionReportInitializer::arguments(const QString &arguments)
{
    option = stringToEnumChecked<Option>(arguments, InvalidOption);
}

void OptionReportInitializer::run()
{
    switch (option)
    {
    case InvalidOption:
        break;
    case use01ForTrueFalse:
        generator()->setBoolMode(ReportGenerator::BoolAs01);
        break;
    case isoDate:
        generator()->setDateMode(ReportGenerator::ISODate);
        break;
    case dayDotMonthDotYearDate:
        generator()->setDateMode(ReportGenerator::DayMonthYearDotsDate);
        break;
    }
}

void CommonScriptCodeReportInitializer::arguments(const QString &arguments)
{
    generator()->addCommonScriptCode(arguments);
}

void DateFilterReportInitializer::arguments(const QString &arguments)
{
    if (arguments.contains("strict"))
    {
        strictness = FilterOnMissingValue;
    }

    QList<QDate> dates;
    QRegularExpression re("(?<day>\\d{1,2})\\.(?<month>\\d{1,2})\\.(?<year>\\d{4})|(?<yeariso>\\d{4})-(?<monthiso>\\d{1,2})-(?<dayiso>\\d{1,2})");
    QRegularExpressionMatchIterator it = re.globalMatch(arguments);
    while (it.hasNext())
    {
        QRegularExpressionMatch match = it.next();
        QDate date;
        if (!match.captured("day").isNull())
        {
            // dd.mm.yyyy
            date = QDate(match.captured("year").toInt(), match.captured("month").toInt(), match.captured("day").toInt());
        }
        else
        {
            date = QDate(match.captured("yeariso").toInt(), match.captured("monthiso").toInt(), match.captured("dayiso").toInt());
        }
        if (!date.isValid())
        {
            generator()->reportError("In date filter: Invalid date " + match.captured());
            continue;
        }
        dates << date;
    }

    if (dates.isEmpty())
    {
        generator()->reportError("In date filter: Need dates to filter!");
        return;
    }

    if (dates.size() == 1 && arguments.startsWith("- "))
    {
        end = dates.first();
    }
    else
    {
        begin = dates.first();
    }

    if (dates.size() >= 2)
    {
        end = dates[1];
        if (dates.size() > 2)
        {
            generator()->reportError(QString("In date filter: %1 dates given, only using the first two (begin - end)").arg(dates.size()));
        }
    }
    if (begin.isValid() && end.isValid() && begin > end)
    {
        generator()->reportError("In date filter: Begin date > end date. Filtering out everything. " + arguments);
    }
}

bool DateFilterReportInitializer::filter()
{
    QDate value;
    switch (filterType)
    {
    case FilterByInitialDiagnosis:
        value = disease().initialDiagnosis;
        break;
    case FilterByPathologyDate:
        if (disease().hasProfilePathology())
        {
            value = disease().firstProfilePathology().date;
        }
        else if (disease().hasPathology())
        {
            value = disease().firstPathology().date;
        }
        break;
    }

    if (value.isNull())
    {
        switch (strictness)
        {
        case FilterOnMissingValue:
            return false;
        case PassOnMissingValue:
            return true;
        }
    }

    if (begin.isNull())
    {
        if (end.isNull())
        {
            return true;
        }
    }
    else
    {
        if (value < begin)
        {
            return false;
        }
        if (end.isNull())
        {
            return true;
        }
    }
    return value < end;
}

bool PatientPropertyFilteringReportInitializer::filter()
{
    if (property.isEmpty() && value.isEmpty() && detailValue.isEmpty())
    {
        return true;
    }

    bool hasProperty = patient()->patientProperties.hasProperty(property, value, detailValue);
    switch (mode)
    {
    case RequireProperty:
        return hasProperty;
    case ExcludeProperty:
        return !hasProperty;
    }
    return true;
}

void TrialParticipationReportInitializer::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    TrialContextInfo info = TrialContextInfo::info(arguments);
    if (info.id.isEmpty())
    {
        generator()->reportError("Unknown trial id " + arguments);
        return;
    }
    property = PatientPropertyName::trialParticipation();
    value    = info.id;
}

QStringList TrialParticipationReportInitializer::possibleArguments()
{
    QStringList arguments;
    for (int trial = TrialContextInfo::FirstTrial; trial <= TrialContextInfo::LastTrial; trial++)
    {
        arguments << TrialContextInfo::info(TrialContextInfo::Trial(trial)).id;
    }
    return arguments;
}

void TrialParticipationReportDatum::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    info = TrialContextInfo::info(arguments);
    if (info.id.isEmpty())
    {
        generator()->reportError("Unknown trial id " + arguments);
    }
}

void TrialParticipationReportDatum::run()
{
    data() << patient()->patientProperties.hasProperty(PatientPropertyName::trialParticipation(), info.id);
}

QString TrialParticipationReportDatum::columnHeader(int)
{
    return QString("participating in ") + info.label;
}



void PathologyNetworkParticipationReportInitializer::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    if (!possibleArguments().contains(arguments, Qt::CaseInsensitive))
    {
        generator()->reportError("Unknown trial id " + arguments);
        return;
    }
    property = PatientPropertyName::pathologyNetwork();
    if (arguments.compare(PatientPropertyValue::nngm(), Qt::CaseInsensitive) == 0)
    {
        value = PatientPropertyValue::nngm();
    }
}

QStringList PathologyNetworkParticipationReportInitializer::possibleArguments()
{
    return QStringList() << "nNGM";
}

void PathologyNetworkParticipationReportDatum::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    if (!possibleArguments().contains(arguments, Qt::CaseInsensitive))
    {
        generator()->reportError("Unknown trial id " + arguments);
        return;
    }
    if (arguments.compare(PatientPropertyValue::nngm(), Qt::CaseInsensitive) == 0)
    {
        label = "nNGM";
        value = PatientPropertyValue::nngm();
    }
}

void PathologyNetworkParticipationReportDatum::run()
{
    data() << patient()->patientProperties.hasProperty(PatientPropertyName::pathologyNetwork(), value);
}

QString PathologyNetworkParticipationReportDatum::columnHeader(int)
{
    return QString("participating in ") + label;
}


void FilteringReportInitializerGroup::run()
{
    // override ReportInitializer
    generator()->addPreFilterGroup(this);
}

bool FilteringReportInitializerGroup::filter()
{
    if (filters.isEmpty())
    {
        return true;
    }
    QVector<bool> results(filters.size());
    for (int i=0; i<filters.size(); i++)
    {
        FilteringReportInitializer* f = filters[i];
        f->currentGenerator = currentGenerator;
        results[i] = f->filter();
        f->currentGenerator = 0;
    }
    if (filters.size() == 1)
    {
        return results.first();
    }
    return combine(results);
}

template <class Functor>
static bool combineWithFunctor(const QVector<bool> &filterResults, const Functor& functor = Functor())
{
    bool result = filterResults.first();
    for (int i = 1; i<filterResults.size(); i++)
    {
        result = functor(result, filterResults[i]);
    }
    return result;
}

void LogicalFilteringReportInitializerGroup::arguments(const QString &arguments)
{
    if (arguments == "or")
    {
        mode = Or;
    }
    // default: and
}

bool LogicalFilteringReportInitializerGroup::combine(const QVector<bool> &filterResults)
{
    switch(mode)
    {
    case And:
        return combineWithFunctor<std::logical_and<bool> >(filterResults);
    case Or:
        return combineWithFunctor<std::logical_or<bool> >(filterResults);
    }
    return true;
}

void FilteringReportInitializerGroupEnd::run()
{
    generator()->closePreFilterGroup();
}

void RebiopsyFilteringReportInitializer::arguments(const QString &arguments)
{
    bool ok;
    int number = arguments.toInt(&ok);
    if (ok)
    {
        numberOfRebiopsies = number;
    }
    else
    {
        generator()->reportError("Invalid rebiopsy count " + arguments);
    }
}

bool RebiopsyFilteringReportInitializer::filter()
{
    return disease().pathologies.size() > numberOfRebiopsies;
}

SIMPLE_REPORT_DATUM_RUN(PatientId) { data() << patient()->id; }
SIMPLE_REPORT_DATUM_RUN(MedicoId) { data() << (patient()->medicoId ? patient()->medicoId : QVariant()); }
SIMPLE_REPORT_DATUM_RUN(DiseaseId) { data() << disease().id; }
SIMPLE_REPORT_DATUM_RUN(Name) { data() << patient()->surname; }
SIMPLE_REPORT_DATUM_RUN(FirstName) { data() << patient()->firstName; }
SIMPLE_REPORT_DATUM_RUN(NameCommaFirstName) { data() << patient()->surname + ", " + patient()->firstName; }
SIMPLE_REPORT_DATUM_RUN(DateOfBirth) { data() << patient()->dateOfBirth; }
SIMPLE_REPORT_DATUM_RUN(Sex)
{
    switch(patient()->gender)
    {
    case Patient::Male:
        data() << "m";
        return;
    case Patient::Female:
        data() << "f";
        return;
    case Patient::UnknownGender:
        break;
    }
    data() << QVariant();
}
SIMPLE_REPORT_DATUM_RUN(AgeAtDx)
{
    QDate dob = patient()->dateOfBirth;
    if (!disease().initialDiagnosis.isValid() || dob==QDate(1900, 1, 1))
    {
        data() << QVariant();
    }
    else
    {
        data() << (patient()->dateOfBirth.daysTo(disease().initialDiagnosis) / 365.0);
    }
}
SIMPLE_REPORT_DATUM_RUN(InitialDx) { data() << disease().initialDiagnosis; }
SIMPLE_REPORT_DATUM_RUN(SmokingStatus)
{
    Property prop = patient()->patientProperties.property(PatientPropertyName::smokingStatus());
    if (prop.isValid())
    {
        data() << prop.value.toLower();
    }
    else
    {
        data() << QVariant();
    }
}

SIMPLE_REPORT_DATUM_RUN(SmokingPackYears)
{
    Property prop = patient()->patientProperties.property(PatientPropertyName::smokingPackYears());
    if (prop.isValid())
    {
        data() << prop.value.toInt();
    }
    else
    {
        data() << QVariant();
    }
}

SIMPLE_REPORT_DATUM_RUN(PathologyDate)
{
    QDate earliest;
    foreach (const Pathology& path, disease().pathologies)
    {
        if (earliest.isNull())
        {
            earliest = path.date;
        }
        else
        {
            earliest = qMin(path.date, earliest);
        }
    }
    data() << earliest;
}
SIMPLE_REPORT_DATUM_RUN(Entity)
{
    foreach (const Pathology& path, disease().pathologies)
    {
        if (path.entity != Pathology::UnknownEntity)
        {
            data() << Pathology::shortEntityLabel(disease().firstPathology().entity);
            return;
        }
    }
    data() << QVariant();
}

SIMPLE_REPORT_DATUM_RUN(T) { data() << disease().initialTNM.Tnumber();}
SIMPLE_REPORT_DATUM_RUN(N) { data() << disease().initialTNM.Nnumber();}
SIMPLE_REPORT_DATUM_RUN(NPlus) { data() << disease().initialTNM.NnumberOrPlus();}
SIMPLE_REPORT_DATUM_RUN(M)
{
    TNM::MStatus m = disease().initialTNM.mstatus();
    /*if (m == TNM::Mx)
    {
        qDebug() << "Mx status for" << p->surname << p->firstName << disease.initialTNM.toText();
    }
    if (disease.initialTNM.toText().contains("Mx", Qt::CaseInsensitive))
    {
        qDebug() << "Real Mx status for" << p->surname << p->firstName << disease.initialTNM.toText();
    }*/
    data() << (m == TNM::Mx ? QVariant() : QVariant(int(m)));
}
SIMPLE_REPORT_DATUM_RUN(L) { data() << (disease().initialTNM.m_pTNM.L == 'x' ? QVariant() : QVariant(QString(disease().initialTNM.m_pTNM.L))); }
SIMPLE_REPORT_DATUM_RUN(V) { data() << (disease().initialTNM.m_pTNM.V == 'x' ? QVariant() : QVariant(QString(disease().initialTNM.m_pTNM.V))); }
SIMPLE_REPORT_DATUM_RUN(G) { data() << (disease().initialTNM.m_pTNM.G == 'x' ? QVariant() : QVariant(QString(disease().initialTNM.m_pTNM.G))); }
SIMPLE_REPORT_DATUM_RUN(R) { data() << (disease().initialTNM.m_pTNM.R == 'x' ? QVariant() : QVariant(QString(disease().initialTNM.m_pTNM.R))); }

SIMPLE_REPORT_DATUM_RUN(TNM) { data() << disease().initialTNM.toText(); }

SIMPLE_REPORT_DATUM_RUN(PrimaryTumorLocation)
{
    Property prop = disease().diseaseProperties.property(DiseasePropertyName::primaryTumorLocation());
    if (prop.isValid())
    {
        data() << prop.value;
    }
    else
    {
        data() << QVariant();
    }
}

SIMPLE_REPORT_DATUM_RUN(ResectabilityStatus)
{
    Property prop = disease().diseaseProperties.property(DiseasePropertyName::resectabilityStatus());
    if (prop.isValid())
    {
        data() << prop.value;
    }
    else
    {
        data() << QVariant();
    }
}

SIMPLE_REPORT_DATUM_RUN(LastDocumentation)
{
    data() << disease().history.lastDocumentation();
}

SIMPLE_REPORT_DATUM_RUN(LastValidation)
{
    data() << disease().history.lastValidation();
}

/*
 * alternative implementation
 * SIMPLE_REPORT_DATUM_RUN(PathologyReportNumbers)
{
    QSet<QString> numbers;
    QRegularExpression regex("[RECN]\\d+\\/\\d+|[RECN]-2\\d{3}-\\d+-");
    foreach (const Pathology& path, disease().pathologies)
    {
        foreach (const QString& report, path.reports)
        {
            QRegularExpressionMatchIterator it = regex.globalMatch(report);
            while (it.hasNext())
            {
                QRegularExpressionMatch match = it.next();
                numbers << match.captured();
            }
        }
    }
    data() << numbers.toList().join(' ');
}*/

int ScriptReportDatum::columnCount()
{
    return names.size();
}

QString ScriptReportDatum::columnHeader(int index)
{
    return names[index];
}

void ScriptReportDatum::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    int firstLineBreak = arguments.indexOf('\n');
    if (firstLineBreak == -1)
    {
        generator()->reportError("Scripts must have at least one line: " + arguments);
        return;
    }

    QString sourceCode = arguments.mid(firstLineBreak+1);
    if (sourceCode.isEmpty())
    {
        generator()->reportError("Empty script: " + arguments);
        return;
    }

    QScriptSyntaxCheckResult check = QScriptEngine::checkSyntax(sourceCode);
    if (check.state() != QScriptSyntaxCheckResult::Valid)
    {
        generator()->reportError( QString("Error in script syntax for script %1: ").arg(names.join(", "))
                                  + check.errorMessage()
                                  + QString(" line %1 column %2").arg(check.errorLineNumber()).arg(check.errorColumnNumber()) );
        return;
    }

    generator()->needScripting();
    foreach (const QString id, arguments.left(firstLineBreak).split(',', Qt::SkipEmptyParts))
    {
        names << id.trimmed();
    }
    program = QScriptProgram(generator()->commonScriptCode() + sourceCode);
}

void ScriptReportDatum::run()
{
    if (!columnCount())
    {
        return;
    }

    int written = 0;

    if (!program.isNull())
    {
        QScriptValue value = generator()->scriptEngine()->evaluate(program);
        QVariant var = value.toVariant();

        if (value.isArray())
        {
            QVariantList list = var.toList();
            for (int i=0; i<names.size() && i<list.size(); i++)
            {
                data() << list[i];
                written++;
            }
        }
        else if (!value.isError())
        {
            data() << var;
            written++;
        }
    }

    // fulfill promises - script may fail or be erroneous
    while (written<columnCount())
    {
        data() << QVariant();
        written++;
    }
}


QString ReportFinisher::fileNameFromArguments(const QString &arguments)
{
    QString fileName = arguments;
    fileName.remove("'");
    fileName.remove('"');
    fileName = QDir::cleanPath(fileName);
    fileName.replace('~', QDir::homePath());
    return fileName;
}

void FilterScriptFilteringReportFinisher::arguments(const QString& arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    QScriptSyntaxCheckResult check = QScriptEngine::checkSyntax(arguments);
    if (check.state() != QScriptSyntaxCheckResult::Valid)
    {
        generator()->reportError( QString("Error in script syntax for filter script: ")
                                  + check.errorMessage()
                                  + QString(" line %1 column %2").arg(check.errorLineNumber()).arg(check.errorColumnNumber()) );
        return;
    }
    generator()->needScripting();
    program = QScriptProgram(generator()->commonScriptCode() + arguments);
}

bool FilterScriptFilteringReportFinisher::filter(const QList<QVariant> &)
{
    if (program.isNull())
    {
        return true;
    }
    QScriptValue value = generator()->scriptEngine()->evaluate(program);
    if (value.isError() || value.isUndefined())
    {
        generator()->reportError(QString("Script error:") + value.toString());
        return true; // invalid, do not filter;
    }
    if (value.isBool() || value.isNumber())
    {
        return value.toBool();
    }
    QVariant var = value.toVariant();
    if (var.canConvert<bool>())
    {
        return var.toBool();
    }
    generator()->reportError(QString("filterScript: Returned value cannot be converted to boolean: ") + value.toString());
    return true;
}

void SaveToCSVReportFinisher::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    fileName = fileNameFromArguments(arguments);
}

void SaveToCSVReportFinisher::finished(const QList<QList<QVariant> > &)
{
    CSVFile file(delimiter, decimalPoint);
    if (!file.openForWriting(fileName))
    {
        generator()->reportError(QString("Failed to open file %1 for CSV output: %2").arg(fileName).arg(file.errorString()));
        return;
    }
    generator()->writeToCSV(file);
}

void SaveToJSONReportFinisher::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    fileName = fileNameFromArguments(arguments);
}

void SaveToJSONReportFinisher::finished(const QList<QList<QVariant> > &)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        generator()->reportError(QString("Failed to open file %1 for JSON output: %2").arg(fileName).arg(file.errorString()));
        return;
    }
    generator()->writeJSON(fileName);
}
