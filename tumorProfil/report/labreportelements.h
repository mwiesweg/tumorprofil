/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABREPORTELEMENTS_H
#define LABREPORTELEMENTS_H

#include "reportelements.h"
#include "survivalreportelements.h"

class LabSetForName;

class UseLabValueGroupReportInitializer : public ReportInitializer
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    QString id;
    QStringList keys;
    static QStringList possibleArguments();
    REPORT_ELEMENT("useLabValueGroup", "Fasst Laborbestimmungen zu einer Gruppe zusammen. \n"
                                       "Argumente: ein Kürzel (z.B. \"Blutbild\"), dann Doppelpunkt, dann eine Liste von Laborbestimmungen (alles getrennt mit Kommata).")
};

class LabValueReportDatum : public ReportDatum
{
public:
    virtual int columnCount() { return 1; }
    static QStringList possibleArguments();

    void run(const QDate& from, const QDate& to);
    void run(const QDate& date, const QDate& min, const QDate& max);
    void run(const QString& dateSpec);

protected:
    QVariant asVariant(const LabDateValuePair& value, const QDate& referenceDate);
    QVariant asVariant(const LabSetForName& values, const QDate& referenceDate);
    QVariant rangeValues(const QString& name, const QDate& from, const QDate& to);
    QVariant pointValue(const QString& name, const QDate& date, const QDate& min, const QDate& max);
    QVariant specValue(const QString& name, const QString& spec);

    enum DateReportMode
    {
        ReportAbsolute,
        ReportRelative,
        ReportNoDate
    };
    virtual DateReportMode dateReportMode() { return ReportAbsolute; }

    QString key;
};

class LabValueAtReportDatum : public LabValueReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);

    QDate min;
    QDate max;
    QDate date;
    QString dateSpec;
    REPORT_ELEMENT("labValueAt",
                   "Laborwert für x am Datum d bzw. am nächsten zu Datum d, bzw. max. v Tage vor d oder n Tage nach d.\n"
                   "Erzeugt eine Datenstruktur, Datum -> Wert. Export via JSON ist empfohlen.\n"
                   "Argument: x (Laborwert) d (Datum, dd.mm.yyyy, yyyy-mm-dd, \"first\", \"last\") v (Toleranz: früher als d, falls nicht angegeben: 7)"
                   "n (Toleranz: später als d, falls nicht angegeben, v)")
};
class LabValuesDuringReportDatum : public LabValueAtReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("labValuesDuring",
                   "Laborwerte für x von Datum d1 bis Datum d2.\n"
                   "Erzeugt eine Datenstruktur, Liste [Datum -> Wert]. Export via JSON ist empfohlen.\n"
                   "Argument: x (Laborwert) d1 d2 (Datum von - bis, dd.mm.yyyy oder yyyy-mm-dd")
};

class LabValuesDuringLineReportDatum : public LabValueReportDatum
{
public:
    LabValuesDuringLineReportDatum() : before(7), after(7) {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);

    TTFReportDatum ttfDatum;
    int before;
    int after;
    REPORT_ELEMENT("labValuesDuringLine",
                   "Laborwerte für x während Linie l, inklusive v Tage vor und n Tage nach d.\n"
                   "Erzeugt eine Datenstruktur, Liste [Relativer Zeitpunt -> Wert]. Export via JSON ist empfohlen.\n"
                   "Argument: x (Laborwert) l (Zahl: n-te Linie, Substanzgruppe: erste Linie mit Substanzgruppe) v (Tage vor Beginn der Linie)"
                   "n (Tage nach Ende der Linie)")

protected:
    virtual DateReportMode dateReportMode() { return ReportRelative; }
};

class LabValuesAtStartOfLineReportDatum : public LabValuesDuringLineReportDatum
{
public:
    virtual QString columnHeader(int index);
    virtual void run();
    REPORT_ELEMENT("labValuesAtStartOfLine",
                   "Laborwerte für x zum Start von Linie l, mit Toleranz von max. v Tage vor d oder n Tage nach dem Start.\n"
                   "Erzeugt eine Datenstruktur, Liste [Relativer Zeitpunkt -> Wert]. Export via JSON ist empfohlen.\n"
                   "Argument: x (Laborwert) l (Zahl: n-te Linie, Substanzgruppe: erste Linie mit Substanzgruppe) v (Toleranz Tage vor Beginn der Linie)"
                   "n (Toleranz Tage nach Ende der Linie)")
    protected:
        virtual DateReportMode dateReportMode() { return ReportNoDate; }
};

#endif // LABREPORTELEMENTS_H
