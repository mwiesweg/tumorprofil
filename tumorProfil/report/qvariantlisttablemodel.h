/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef QVARIANTLISTTABLEMODEL_H
#define QVARIANTLISTTABLEMODEL_H

#include <QAbstractTableModel>

class QVariantListTableModel : public QAbstractTableModel
{
public:
    QVariantListTableModel(QObject* parent = 0);
    QVariantListTableModel(const QList< QList<QVariant> >& data, QObject* parent = 0);
    void setData(const QList< QList<QVariant> >& data);

    void setColumnHeaders(const QStringList& m_headers);
    void setShowRowNumbers(bool show);

    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

protected:

    QList< QList<QVariant> >       m_list;
    QStringList                    m_headers;
    bool                           m_showRowNumbers;
};


#endif // QVARIANTLISTTABLEMODEL_H
