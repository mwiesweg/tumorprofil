/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labreportelements.h"

#include "labresults.h"

void UseLabValueGroupReportInitializer::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments)
    QPair<QString, QStringList> pair = parseGroupList(arguments, syntaxName());
    if (!pair.first.isEmpty())
    {
        id = pair.first;
        keys = pair.second;
    }
}

void UseLabValueGroupReportInitializer::run()
{
    if (!id.isEmpty())
    {
        generator()->registerLabGroup(id, keys);
    }
}

QStringList UseLabValueGroupReportInitializer::possibleArguments()
{
    return QStringList() << QObject::tr("<Kürzel>: <Substanz1>, <Substanz2>, <Substanz3> ...") << LabValueReportDatum::possibleArguments();
}

QStringList LabValueReportDatum::possibleArguments()
{
    QStringList keys = LabResults::types().keys();
    std::sort(keys.begin(), keys.end());
    return keys;
}

void LabValueReportDatum::run(const QDate &from, const QDate &to)
{
    // Output all values of key between from and to

    if (key.isNull() || from.isNull() || to.isNull())
    {
        data() << QVariant();
        return;
    }

    if (generator()->isLabGroup(key))
    {
        QStringList keys = generator()->labGroup(key);
        QVariantMap map;
        foreach (const QString& key, keys)
        {
            QVariant values = rangeValues(key, from, to);
            if (values.isNull())
            {
                continue;
            }
            map[key] = values;
        }
        if (map.isEmpty())
        {
            data() << QVariant();
        }
        else
        {
            data() << map;
        }
    }
    else
    {
        data() << rangeValues(key, from, to);
    }
}

void LabValueReportDatum::run(const QDate &date, const QDate &min, const QDate &max)
{
    // Display one value of key, best at date, but with a tolerance between min and max

    if (key.isNull() || date.isNull() || min.isNull() || max.isNull())
    {
        data() << QVariant();
        return;
    }

    if (generator()->isLabGroup(key))
    {
        QStringList keys = generator()->labGroup(key);
        QVariantMap map;
        foreach (const QString& key, keys)
        {
            QVariant value = pointValue(key, date, min, max);
            if (value.isNull())
            {
                continue;
            }
            map[key] = value;
        }
        if (map.isEmpty())
        {
            data() << QVariant();
        }
        else
        {
            data() << map;
        }
    }
    else
    {
        data() << pointValue(key, date, min, max);
    }
}

void LabValueReportDatum::run(const QString &dateSpec)
{
    if (key.isNull() || dateSpec.isNull())
    {
        data() << QVariant();
        return;
    }

    if (generator()->isLabGroup(key))
    {
        QStringList keys = generator()->labGroup(key);
        QVariantMap map;
        foreach (const QString& key, keys)
        {
            QVariant value = specValue(key, dateSpec);
            if (value.isNull())
            {
                continue;
            }
            map[key] = value;
        }
        if (map.isEmpty())
        {
            data() << QVariant();
        }
        else
        {
            data() << map;
        }
    }
    else
    {
        data() << specValue(key, dateSpec);
    }
}

QVariant LabValueReportDatum::asVariant(const LabDateValuePair &value, const QDate &referenceDate)
{
    if (dateReportMode() == ReportNoDate)
    {
        return value.value;
    }
    QVariantMap map;
    if (dateReportMode() == ReportAbsolute)
    {
        map[generator()->dateToString(value.dateTime.date())] = value.value;
    }
    else
    {
        map[QString::number(referenceDate.daysTo(value.dateTime.date()))] = value.value;
    }
    return map;
}

QVariant LabValueReportDatum::asVariant(const LabSetForName &values, const QDate &referenceDate)
{
    if (dateReportMode() == ReportNoDate)
    {
        QVariantList list;
        foreach (const LabDateValuePair& value, values)
        {
            list << value.value;
        }
        return list;
    }
    else
    {
        QVariantMap map;
        foreach (const LabDateValuePair& value, values)
        {
            if (dateReportMode() == ReportAbsolute)
            {
                map[generator()->dateToString(value.dateTime.date())] = value.value;
            }
            else
            {
                map[QString::number(referenceDate.daysTo(value.dateTime.date()))] = value.value;
            }

        }
        return map;
    }
}

QVariant LabValueReportDatum::rangeValues(const QString &name, const QDate &from, const QDate &to)
{
    const LabResults& results = generator()->labResults();
    LabSetForName values = results.valuesForName(name, from, to);
    if (values.isEmpty())
    {
        return QVariant();
    }
    return asVariant(values, from);
}

QVariant LabValueReportDatum::pointValue(const QString &name, const QDate &date, const QDate &min, const QDate &max)
{
    const LabResults& results = generator()->labResults();
    LabDateValuePair value = results.valueForNameAt(name, date, min, max);
    if (value.isNull())
    {
        return QVariant();
    }
    return asVariant(value, date);
}

QVariant LabValueReportDatum::specValue(const QString &name, const QString &spec)
{
    const LabResults& results = generator()->labResults();
    LabDateValuePair value = results.valueForNameAt(name, spec);
    if (value.isNull())
    {
        return QVariant();
    }
    return asVariant(value, results.earliest().date());
}

static QDate parseDateArgument(const QString& arg)
{
    if (arg.contains('-'))
    {
        return QDate::fromString(arg, Qt::ISODate);
    }
    return QDate::fromString(arg, "dd.MM.yyyy");
}

void LabValueAtReportDatum::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    QStringList args = arguments.split(' ', Qt::SkipEmptyParts);
    if (args.size() < 2)
    {
        generator()->reportError("labValue: Need at least two arguments, type of value and date. Got: " + arguments);
        return;
    }

    generator()->needLabResults();
    key = args[0];

    if (args[1] == "first" || args[1] == "last")
    {
        dateSpec = args[1];
        date = min = max = QDate();
    }
    else
    {
        QDate parsedDate = parseDateArgument(args[1]);
        if (!parsedDate.isValid())
        {
            generator()->reportError("labValue: Failed to parse date. Please use dd.mm.yyyy oder yyyy-mm-dd. Got: " + args[1]);
        }
        date = parsedDate;
        int before = 7, after = 7;
        if (args.size() >= 3)
        {
            before = args[2].toInt();
            if (args.size() >= 4)
            {
                after = args[3].toInt();
            }
            else
            {
                after = before;
            }
        }
        min = date.addDays(-before);
        max = date.addDays(after);
    }
}

void LabValueAtReportDatum::run()
{
    if (!dateSpec.isEmpty())
    {
        LabValueReportDatum::run(dateSpec);
    }
    else
    {
        LabValueReportDatum::run(date, min, max);
    }
}

QString LabValueAtReportDatum::columnHeader(int)
{
    QString header = QString("lab value %1 %2").arg(key);
    if (date.isValid())
    {
        return header.arg(date.toString(Qt::ISODate));
    }
    else
    {
        return header.arg(dateSpec);
    }
}

void LabValuesDuringReportDatum::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    QStringList args = arguments.split(' ', Qt::SkipEmptyParts);
    if (args.size() < 3)
    {
        generator()->reportError("labValue: Need at least three arguments, type of value, start date, end date. Got: " + arguments);
        return;
    }
    QDate parseStart = parseDateArgument(args[1]);
    QDate parseEnd = parseDateArgument(args[2]);
    if (!parseStart.isValid() || !parseEnd.isValid())
    {
        generator()->reportError("labValue: Failed to parse date. Please use dd.mm.yyyy oder yyyy-mm-dd. Got: " + arguments);
        return;
    }
    generator()->needLabResults();
    key = args[0];
    min = parseStart;
    max = parseEnd;
}

void LabValuesDuringReportDatum::run()
{
    LabValueReportDatum::run(min, max);
}

QString LabValuesDuringReportDatum::columnHeader(int)
{
    return QString("lab value %1 %2-%3").arg(key).arg(min.toString(Qt::ISODate).arg(max.toString(Qt::ISODate)));
}

void LabValuesDuringLineReportDatum::arguments(const QString &arguments)
{
    RETURN_ON_EMPTY_ARGUMENTS(arguments);
    QStringList args = arguments.split(' ', Qt::SkipEmptyParts);
    if (args.size() < 2)
    {
        generator()->reportError("labValue: Need at least two arguments, type of value, and treatment line specification. Got: " + arguments);
        return;
    }

    generator()->needLabResults();
    generator()->historyReporter()->needLines();
    key = args[0];

    ttfDatum.currentGenerator = generator();
    ttfDatum.arguments(args[1]);
    int nextArgIndex = 2;
    if (args.size() > nextArgIndex && args[nextArgIndex] == "palliative")
    {
        ttfDatum.palliative = true;
        nextArgIndex++;
    }

    if (args.size() > nextArgIndex)
    {
        before = args[nextArgIndex].toInt();
        nextArgIndex++;
        if (args.size() > nextArgIndex)
        {
            after = args[nextArgIndex].toInt();
        }
        else
        {
            after = before;
        }
    }

    //qDebug() << this << ttfDatum.palliative;
}

QString LabValuesDuringLineReportDatum::columnHeader(int)
{
    QString header = ttfDatum.columnHeader(0);
    header.replace(0, 3,  QString("lab value %1 during line").arg(key)); // replace "TTF"
    return header;
}

void LabValuesDuringLineReportDatum::run()
{
    QPair<QDate, QDate> dates;
    if (ttfDatum.line > 0)
    {
        // 1-based index -> 0-based index
        dates = generator()->historyReporter()->ttfDates(ttfDatum.line-1, ttfDatum.palliative);
    }
    else if (!ttfDatum.id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(ttfDatum.id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(ttfDatum.id));
        }
        dates = generator()->historyReporter()->ttfDates(ttfDatum.id, ttfDatum.palliative);
    }
    LabValueReportDatum::run(dates.first.addDays(-before), dates.second.addDays(after));
}

QString LabValuesAtStartOfLineReportDatum::columnHeader(int)
{
    QString header = ttfDatum.columnHeader(0);
    header.replace(0, 3,  QString("lab value %1 at start of line ").arg(key)); // replace "TTF"
    return header;

}

void LabValuesAtStartOfLineReportDatum::run()
{
    QDate begin;
    if (ttfDatum.line > 0)
    {
        // 1-based index -> 0-based index
        begin = generator()->historyReporter()->ttfDates(ttfDatum.line-1, ttfDatum.palliative).first;
    }
    else if (!ttfDatum.id.isEmpty())
    {
        if (!generator()->historyReporter()->isRegisteredSubstance(ttfDatum.id))
        {
            generator()->reportError(QString("Using unregistered substance id %1: Must call useSubstance / useSubstanceGroup!").arg(ttfDatum.id));
        }
        begin = generator()->historyReporter()->ttfDates(ttfDatum.id, ttfDatum.palliative).first;
    }
    LabValueReportDatum::run(begin, begin.addDays(-before), begin.addDays(after));
}

