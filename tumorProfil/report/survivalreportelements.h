/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 19.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef SURVIVALREPORTELEMENTS_H
#define SURVIVALREPORTELEMENTS_H

#include "reportelements.h"


class UseSubstanceGroupReportInitializer : public ReportInitializer
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    QString id;
    QStringList substances;
    REPORT_ELEMENT("useSubstanceGroup", "Fasst Substanznamen zu einer Gruppe zusammen. \n"
                                        "Argumente: ein Kürzel (z.B. \"Platin\", kein Substanzname!), dann Doppelpunkt, dann eine Liste von Substanznamen (alles getrennt mit Kommata).")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Kürzel>: <Substanz1>, <Substanz2>, <Substanz3> ..."))
};

class UseSubstanceReportInitializer : public ReportInitializer
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    QString  substance;
    REPORT_ELEMENT("useSubstance", "Einzelner Substanzname, der in einer Abfrage benutzt werden soll. Für Gruppen von Substanzen gibt es useSubstanceGroup.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Substanzname>"))
};

class SubstanceClassReportDatum : public ReportDatum
{
public:
    SubstanceClassReportDatum() : palliative(false) {}
    virtual void arguments(const QString& arguments);
    QString id;
    bool palliative;

    QString parseKeywordPalliative(const QString& arguments);
};

class FirstLineWithReportDatum : public SubstanceClassReportDatum
{
public:
    virtual void run();
    virtual int columnCount() { return 1; }
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("firstLineWith", "Nummer der ersten Linie, die eine der genannten Substanzen enthält. \n"
                   "Argument: Substanz aus useSubstance oder Kürzel der Substanzgruppe aus useSubstanceGroup.")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Substanzname oder -kürzel>"))
};

class SubstanceClassOrLineReportDatum : public SubstanceClassReportDatum
{
public:
    SubstanceClassOrLineReportDatum() : line(0) {}
    int line;
    virtual void arguments(const QString& arguments);
    virtual int columnCount() { return 1; }

    QString columnHeaderForLineOrSubstance(const QString& lineHeader, const QString& substanceHeader);
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Substanzname oder -kürzel [palliative]>") << QObject::tr("<Liniennummer [palliative]>"))
};

class OSReportDatum : public SubstanceClassOrLineReportDatum
{
    Q_GADGET
public:

    enum Definition
    {
        fromFirstTherapy = HistoryReporter::FromFirstTherapy,
        fromInitialDiagnosis    = HistoryReporter::FromInitialDiagnosis,
        fromPalliativeSetting   = HistoryReporter::FromRelapseOrInitiallyIncurable,
        otherArgument
    };
    Q_ENUM(Definition)

    OSReportDatum() : definition(fromFirstTherapy) {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int columnCount();
    virtual QString columnHeader(int index);
    Definition definition;
    REPORT_ELEMENT("os", "OS und OS-erreicht (0=zensiert, 1=gestorben). \n"
                   "Argument: keines (=fromFirstTherapy), fromFirstTherapy, fromInitialDiagnosis, \n"
                   "fromPalliativeSetting: ab Beginn einer palliativen Therapie, ohne mögliche kurative Erstlinie; \n"
                   "Substanz aus useSubstance oder Kürzel der Substanzgruppe aus defineSubstanceGroup [palliative] ; \n"
                   "ab Therapielinie <Zahl> [palliative]")
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QStringList()
                                      << "fromFirstTherapy"
                                      << "fromInitialDiagnosis"
                                      << "fromPalliativeSetting"
                                      << QObject::tr("<Substanzname oder -kürzel>") << QObject::tr("<Liniennummer>"))
};

class DCOReportDatum : public OSReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int columnCount();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("dco", "Data cutoff time: Zeit vom Beginn (definiert durch Argument) bis zur letzten Dokumentation. \n"
                   "Argument: keines (=fromFirstTherapy), fromFirstTherapy, fromInitialDiagnosis, \n"
                   "fromPalliativeSetting: ab Beginn einer palliativen Therapie, ohne mögliche kurative Erstlinie; \n"
                   "Substanz aus useSubstance oder Kürzel der Substanzgruppe aus defineSubstanceGroup [palliative] ; \n")

};

class SubstanceClassOrLineReportDatumMultiple : public SubstanceClassOrLineReportDatum
{
public:
    SubstanceClassOrLineReportDatumMultiple() : all_lines(false) {}
    bool all_lines;
    QString parseKeywordAll(const QString& arguments);
    virtual void arguments(const QString& arguments);
    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Substanzname oder -kürzel [palliative] [all]>") << QObject::tr("<Liniennummer [palliative]>"))
};

// Reuses line number and palliative, but ignores substance name/class
class LinearLineNumberReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void arguments(const QString& arguments);

    REPORT_ELEMENT_POSSIBLE_ARGUMENTS(QObject::tr("<Behandlungsnummer [palliative]>"))
};

class TTFReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    TTFReportDatum() {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int columnCount() { return 2; }
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("ttf", "TTF der n-ten Linie oder TTF nach Substanzgruppe x. \n"
                          "Argument: n (Zahl) oder x (Substanz aus useSubstance oder Kürzel der Substanzgruppe aus useSubstanceGroup) [palliative].")
};

class BestResponseReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    BestResponseReportDatum() {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("bestResponse", "Best Response unter der n-ten Linie oder Best Response in erster Linie mit Substanzgruppe x. \n"
                          "Argument: n (Zahl) oder x (Substanz aus useSubstance oder Kürzel der Substanzgruppe aus useSubstanceGroup) [palliative].")
};

class PFSReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    PFSReportDatum() {}
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int columnCount() { return 2; }
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("pfs", "PFS der n-ten Linie oder PFS nach Substanzgruppe x. \n"
                          "Argument: n (Zahl) oder x (Substanz aus useSubstance oder Kürzel der Substanzgruppe aus useSubstanceGroup).")
};

class LineStartDateReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("lineStartDate", "Zeitpunkt des Beginns der n-ten Linie / der Linie mit Substangruppe x")
};

class SubstancesOfLineReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("substancesOfLine", "Substanzen in der n-ten Linie / der Linie mit Substangruppe x")
};

class IncludesDeescalationTherapyReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("includesDeescalationInLine", "Die n-te Linie / der Linie mit Substangruppe x beinhaltet eine Erhaltungstherapie")
};

class DeescalatedSubstancesReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("deescalatedSubstances", "Im Rahmen der Therapiedeeskalation beendete Substanzen in der n-ten Linie / der Linie mit Substangruppe x")
};

class TimeToDeescalationReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("timeToDeescalation", "Tage bis zur ersten Therapiedeeskalation unter Chemotherapie in der n-ten Linie / der Linie mit Substangruppe x")
};

class DurationOfDeescalationReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("durationOfDeescalation", "Tage ab der ersten Therapiedeeskalation unter Chemotherapie in der n-ten Linie / der Linie mit Substangruppe x")
};

class CtxDurationReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("ctxDuration", "Tage unter Chemotherapie in der n-ten Linie / der Linie mit Substangruppe x")
};

class ToxicitiesReportDatum : public SubstanceClassOrLineReportDatumMultiple
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("toxicities", "Toxizitäten unter Chemotherapie in der n-ten Linie / der Linie mit Substangruppe x / allen Linien mit Substanzgruppe x (\"all\")")
};

class SubstancesDosingReportDatum : public SubstanceClassOrLineReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("substancesdosing", "Substanzen / Dosierungen unter Chemotherapie in der n-ten Linie / der Linie mit Substanzgruppe x")
};

class SubstanceDosesReportDatum : public SubstanceClassReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    virtual int columnCount() { return 1; }
    REPORT_ELEMENT("substancedoses", "Dosierungen der Chemotherapie der Substanz(gruppe) x")
};

class SurgeryReportDatum : public LinearLineNumberReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("surgery", "Operationen oder Interventionen, linear durchgezählt")
};

class RadiationReportDatum : public LinearLineNumberReportDatum
{
public:
    virtual void run();
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("radiation", "Bestrahlungen, linear durchgezählt")
};

class DFSReportDatum : public ReportDatum
{
public:
    virtual void arguments(const QString& arguments);
    virtual void run();
    virtual int columnCount() { return 2; }
    virtual QString columnHeader(int index);
    REPORT_ELEMENT("dfs", "Dauer vom Therapiebeginn bis zum ersten Rezidiv")
};


SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(TherapyCategory, "therapyCategory", "therapy category", "Therapiekategorie: 0 - Keine Therapie; 1 - Kurativ, kein Rezidiv; 2 - Kurativ, mit Rezidiv; 3 - Nie kurativ.")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(NumberOfLines, "numberCtxLines", "number ctx lines", "Anzahl systemischer Therapielinien.")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(NumberOfPalliativeLines, "numberPalliativeCtxLines", "number palliative ctx lines", "Anzahl systemischer Therapielinien im palliativen Kontext.")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(NumberOfAdjuvantTherapies, "numberAdjuvantTherapies", "number of adjuvant therapy lines", "Anzahl adjuvanter Therapien.")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(LastDiseaseState, "lastDiseaseState", "last disease state",
                                   "Aktueller/letzter Zustand der Doku des Krankheitsverlaufs: "
                                   "InitialDiagnosis, Therapy, BestSupportiveCare, FollowUp, Deceased, LossOfContact, WatchAndWait")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(TimeSinceLastFollowUp, "timeSinceLastFollowUp", "time since last follow up", "Zeit (Tage) seit letzter Dokumentation.")
SIMPLE_REPORT_DATUM_WITH_ARGUMENTS(FirstRelapseDate, "firstRelapseDate", "first relapse date", "Zeitpunkt des ersten Rezidivs")


#endif // SURVIVALREPORTELEMENTS_H
