/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "historyreporter.h"

#include <QDebug>
#include <QMetaEnum>
#include <QMultiMap>

#include "historyelements.h"
#include "history/historyiterator.h"
#include "reportgenerator.h"

class TherapyLinesContainer
{
public:
    TherapyLinesContainer()
        : treatmentLinesIterator(0)
    {
    }

    NewTreatmentLineIterator* treatmentLinesIterator;
    QList<TherapyGroup>       therapies;
    QList<TherapyGroup>       ctxTherapies;
    QList<QDate>              lineDates;
    QList<QDate>              ctxLineDates;
    QVector<HistoryElement*>  firstTherapies;
    QVector<int>              firstTherapyLines;
    QVector< QVector<Therapy*> > therapiesOfInterest;
    QVector< QSet<int> > therapyLinesOfInterest;
    QDate                     lastEndDate;

    QList<TherapyResponseIterator*> responseIterators;

    void clearResults()
    {
        delete treatmentLinesIterator;
        treatmentLinesIterator = 0;

        therapies.clear();
        ctxTherapies.clear();
        lineDates.clear();
        ctxLineDates.clear();
        firstTherapies.clear();
        firstTherapyLines.clear();
        therapiesOfInterest.clear();
        therapyLinesOfInterest.clear();
        lastEndDate = QDate();

        qDeleteAll(responseIterators);
        responseIterators.clear();
    }

    void loadResponses(const DiseaseHistory& history)
    {
        foreach (const TherapyGroup& group, ctxTherapies)
        {
            TherapyResponseIterator* it = new TherapyResponseIterator;
            it->set(history, group);
            it->start();
            responseIterators << it;
        }
    }
};

class LocalTherapy
{
public:
    LocalTherapy()
        : therapy(0),
          withinCTxLine(false),
          inAfterCTxLineNumber(0)
    {
    }

    bool isValid() const { return therapy; }
    const Therapy* therapy;
    bool           withinCTxLine;
    int            inAfterCTxLineNumber;
};

class HistoryReporter::HistoryReporterPriv : public HistoryProofreader
{
public:
    HistoryReporterPriv()
        : generator(0),
          needOS(false),
          needLines(false),
          needSubstanceTherapies(false),
          needCategory(false),
          needCurrentState(false),
          needResponses(false),
          osIterator(0),
          currentStateIterator(0),
          category(HistoryReporter::NoTherapy),
          relapseElement(0)
    {
    }

    ReportGenerator* generator;

    // Setup
    bool                      needOS;
    bool                      needLines;
    bool                      needSubstanceTherapies;
    bool                      needCategory;
    bool                      needCurrentState;
    bool                      needResponses;
    QMultiMap<QString, QString> specificTherapySubstances;
    QStringList               specificTherapyShortcuts;

    // Results
    OSIterator*               osIterator;
    CurrentStateIterator*     currentStateIterator;
    QDate                     lastEndDate; // overall last end date
    TherapyLinesContainer     lines;
    TherapyLinesContainer     palliativeLines;
    HistoryReporter::Category category;
    HistoryElement*           relapseElement;
    QList<Therapy*>           adjuvantTherapies;
    QDate                     lastDocumentation;

    virtual void problem(const HistoryElement*, const QString& problem)
    {
        Q_UNUSED(problem)
        //generator->reportError();
        //qDebug() << problem;
    }

    void clearResults()
    {
        delete osIterator;
        osIterator = 0;
        delete currentStateIterator;
        currentStateIterator = 0;
        lastEndDate = QDate();
        lines.clearResults();
        palliativeLines.clearResults();
        category = NoTherapy;
        relapseElement = 0;
        adjuvantTherapies.clear();
        lastDocumentation = QDate();
    }

    const TherapyLinesContainer& linesContainer(bool palliative) const
    {
        return palliative ? palliativeLines : lines;
    }
    QPair<QDate, QDate> ttfDates(const TherapyLinesContainer& l, int line, const QDate& sharpBegin = QDate());
    QPair<QDate, QDate> lineDates(const TherapyLinesContainer& l, int line);
    QPair<QDate, QDate> ctxDates(const TherapyLinesContainer& l, int line);
    int effectiveCtxDuration(const TherapyLinesContainer& l, int line);
    QMap<QString, QVariant> toxicities(const TherapyLinesContainer& l, QList<int> line);
    QMap<QString, QVariant> substancesDosing(const TherapyLinesContainer& l, int line);
    QList<QVariant> substanceDoses(const TherapyLinesContainer& l, const QString& id);
    LocalTherapy localTherapy(const TherapyLinesContainer &l, int n, bool includeSurgery, bool includeRTx);
    SurvivalResult ttf(const TherapyLinesContainer& l, int line, const QDate& sharpBegin = QDate());
    SurvivalResult pfs(const TherapyLinesContainer& l, int line, const QDate& sharpBegin = QDate());
    SurvivalResult os(const TherapyLinesContainer& l, int line, const QDate& sharpBegin = QDate());
    void loadLinesContainer(const DiseaseHistory& history, TherapyLinesContainer &l, HistoryElement* startElement = 0);
    HistoryReporter::Category therapyCategory(const Patient::Ptr &p, const DiseaseHistory& history);
    void loadAdjuvantTherapies(const DiseaseHistory& history);    
    QDate lineBeginDate(const TherapyLinesContainer &l, int line, const QDate &sharpBegin);
};

HistoryReporter::HistoryReporter(ReportGenerator* generator)
    : d(new HistoryReporterPriv)
{
    d->generator = generator;
}

HistoryReporter::~HistoryReporter()
{
    d->clearResults();
    delete d;
}

void HistoryReporter::needOS(OSDefinition definition)
{
    d->needOS = true;
    if (definition == FromRelapseOrInitiallyIncurable)
    {
        d->needLines    = true;
        d->needCategory = true;
    }
}

void HistoryReporter::needTTF(int lineNumber)
{
    Q_UNUSED(lineNumber)
    d->needLines = true;
}

void HistoryReporter::needBestResponse(int lineNumber)
{
    Q_UNUSED(lineNumber)
    d->needLines     = true;
    d->needResponses = true;
}

void HistoryReporter::needPFS(int lineNumber)
{
    Q_UNUSED(lineNumber)
    d->needLines     = true;
    d->needResponses = true;
}

void HistoryReporter::needSubstanceSpecific(const QString& id, const QStringList& substances)
{
    d->needOS                 = true;
    d->needLines              = true;
    d->needSubstanceTherapies = true;
    d->needResponses          = true;
    d->specificTherapyShortcuts << id;
    foreach (const QString& substance, substances)
    {
        d->specificTherapySubstances.insert(id, substance);
    }
}

bool HistoryReporter::isRegisteredSubstance(const QString &id) const
{
    return d->specificTherapySubstances.contains(id);
}

void HistoryReporter::needTherapyCategory()
{
    d->needOS       = true;
    d->needLines    = true;
    d->needCategory = true;
}

void HistoryReporter::needLines()
{
    d->needLines = true;
}

void HistoryReporter::needCurrentState()
{
    d->needCurrentState = true;
}

SurvivalResult HistoryReporter::os(OSDefinition definition)
{
    if (!d->osIterator->isValid())
    {
        return SurvivalResult();
    }
    switch (definition)
    {
    case FromFirstTherapy:
        return SurvivalResult(
                    d->osIterator->days(OSIterator::FromFirstTherapy),
                    d->osIterator->endpointReached()
                    );
    case FromInitialDiagnosis:
        return SurvivalResult(
                    d->osIterator->days(OSIterator::FromInitialDiagnosis),
                    d->osIterator->endpointReached()
                    );
    case FromRelapseOrInitiallyIncurable:
        switch (d->category)
        {
        case NoTherapy:
        case FirstLineFollowUpNoRecurrence:
            return SurvivalResult();
        case RelapseAfterFirstLineFollowUp:
            return SurvivalResult(
                        d->osIterator->days(d->relapseElement),
                        d->osIterator->endpointReached()
                        );
        case InitiallySystemicTherapy:
            return os(FromFirstTherapy);
        }
    }
    return SurvivalResult();
}

SurvivalResult HistoryReporter::os(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return SurvivalResult();
    }
    return d->os(l, lineNumber);
}

SurvivalResult HistoryReporter::os(const QString& substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return SurvivalResult();
    }
    return d->os(l, l.firstTherapyLines[i], l.firstTherapies[i]->date);
}

SurvivalResult HistoryReporter::ttf(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return SurvivalResult();
    }
    return d->ttf(l, lineNumber);
}

SurvivalResult HistoryReporter::ttf(const QString& substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return SurvivalResult();
    }
    return d->ttf(l, l.firstTherapyLines[i], l.firstTherapies[i]->date);
}

QPair<QDate, QDate> HistoryReporter::lineDates(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QPair<QDate, QDate>();
    }
    return d->lineDates(l, lineNumber);
}

QPair<QDate, QDate> HistoryReporter::lineDates(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QPair<QDate, QDate>();
    }
    return d->lineDates(l, l.firstTherapyLines[i]);
}

QPair<QDate, QDate> HistoryReporter::ttfDates(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QPair<QDate, QDate>();
    }
    return d->ttfDates(l, lineNumber);
}

QPair<QDate, QDate> HistoryReporter::ttfDates(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QPair<QDate, QDate>();
    }
    return d->ttfDates(l, l.firstTherapyLines[i], l.firstTherapies[i]->date);
}

QList<QString> HistoryReporter::substances(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QStringList();
    }
    return l.ctxTherapies[lineNumber].substances().values();
}

QVariant HistoryReporter::includesDeescalation(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    return l.ctxTherapies[l.firstTherapyLines[i]].includesDeescalation();
}

QVariant HistoryReporter::includesDeescalation(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QVariant();
    }
    return l.ctxTherapies[lineNumber].includesDeescalation();
}

QVariant HistoryReporter::daysToDeescalation(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QVariant();
    }
    QList<TherapyGroup> groups = l.ctxTherapies[lineNumber].groupsSplitAtDeescalation();
    if (groups.size() > 1)
    {
        return groups.at(0).beginDate().daysTo(groups.at(1).beginDate());
    }
    return QVariant();
}

QVariant HistoryReporter::daysToDeescalation(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    QList<TherapyGroup> groups = l.ctxTherapies[l.firstTherapyLines[i]].groupsSplitAtDeescalation();
    if (groups.size() > 1)
    {
        return groups.at(0).beginDate().daysTo(groups.at(1).beginDate());
    }
    return QVariant();
}

QList<QString> HistoryReporter::descalatedSubstances(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QStringList();
    }
    return l.ctxTherapies[lineNumber].deescalatedSubstances().values();
}

QList<QString> HistoryReporter::descalatedSubstances(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QStringList();
    }
    return l.ctxTherapies[l.firstTherapyLines[i]].deescalatedSubstances().values();

}

QList<QString> HistoryReporter::substances(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QStringList();
    }
    return l.ctxTherapies[l.firstTherapyLines[i]].substances().values();
}

QVariant HistoryReporter::ctxDays(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QVariant();
    }
    return d->effectiveCtxDuration(l, lineNumber);
}

QVariant HistoryReporter::ctxDays(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    return d->effectiveCtxDuration(l, l.firstTherapyLines[i]);
}

QVariant HistoryReporter::toxicities(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QVariant();
    }
    return d->toxicities(l, QList<int>() << lineNumber);
}

QVariant HistoryReporter::toxicities(const QString &substanceId, bool palliative, bool all)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    if (all)
    {
        QList<int> relevantLines = l.therapyLinesOfInterest[i].values();
        return d->toxicities(l, relevantLines);
    }
    else
    {
        return d->toxicities(l, QList<int>() << l.firstTherapyLines[i]);
    }
}

QVariant HistoryReporter::substancesDosing(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.ctxLineDates.size() || lineNumber <0)
    {
        return QVariant();
    }
    return d->substancesDosing(l, lineNumber)    ;
}

QVariant HistoryReporter::substancesDosing(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    return d->substancesDosing(l, l.firstTherapyLines[i]);
}

QVariant HistoryReporter::substanceDoses(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    return d->substanceDoses(l, substanceId);
}

QVariant HistoryReporter::surgery(int n, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    LocalTherapy therapy = d->localTherapy(l, n, true, false);
    if (!therapy.isValid())
    {
        return QVariant();
    }

    if (therapy.withinCTxLine)
    {
        return QObject::tr("%1 während Therapielinie %2")
                .arg(therapy.therapy->description)
                .arg(therapy.inAfterCTxLineNumber+1);
    }
    else
    {
        return QObject::tr("%1 zwischen Therapielinie %2 und %3")
                .arg(therapy.therapy->description)
                .arg(therapy.inAfterCTxLineNumber+1)
                .arg(therapy.inAfterCTxLineNumber+2);
    }
}

QVariant HistoryReporter::radiation(int n, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    LocalTherapy therapy = d->localTherapy(l, n, false, true);
    if (!therapy.isValid())
    {
        return QVariant();
    }

    QStringList locations;
    foreach (const TherapyElement* elem, therapy.therapy->elements)
    {
        if (elem->is<Radiotherapy>())
        {
            locations << elem->as<Radiotherapy>()->location;
        }
    }

    if (therapy.withinCTxLine)
    {
        return QObject::tr("RTx %1 während Therapielinie %2")
                .arg(locations.join(", "))
                .arg(therapy.inAfterCTxLineNumber+1);
    }
    else
    {
        return QObject::tr("RTx %1 zwischen Therapielinie %2 und %3")
                .arg(locations.join(", "))
                .arg(therapy.inAfterCTxLineNumber+1)
                .arg(therapy.inAfterCTxLineNumber+2);
    }
}

/* duplicate / alternative
SurvivalResult HistoryReporter::os(const QString& substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return SurvivalResult();
    }
    return SurvivalResult(
                d->osIterator->days(l.firstTherapies[i]),
                d->osIterator->endpointReached()
                );
}
*/

QVariant HistoryReporter::bestResponse(int lineNumber, bool palliative) const
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.responseIterators.size() || lineNumber <0)
    {
        return QVariant();
    }
    Finding* f = l.responseIterators.at(lineNumber)->bestResponse();
    if (f)
    {
        QMetaEnum resultEnum = QMetaEnum::fromType<Finding::Result>();
        return resultEnum.valueToKey(f->result);
    }
    else
    {
        DiseaseState::State finalState = l.responseIterators.at(lineNumber)->finalState();
        if (finalState != DiseaseState::UnknownState)
        {
            QMetaEnum stateEnum = QMetaEnum::fromType<DiseaseState::State>();
            return QString("(%1)").arg(stateEnum.valueToKey(finalState));
        }
        return QVariant();
    }
}

QVariant HistoryReporter::bestResponse(const QString &substanceId, bool palliative) const
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    return bestResponse(l.firstTherapyLines[i], palliative);
}

SurvivalResult HistoryReporter::pfs(int lineNumber, bool palliative) const
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.responseIterators.size() || lineNumber <0)
    {
        return SurvivalResult();
    }
    return d->pfs(l, lineNumber);
}

SurvivalResult HistoryReporter::pfs(const QString &substanceId, bool palliative) const
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return SurvivalResult();
    }
    return d->pfs(l, l.firstTherapyLines[i], l.firstTherapies[i]->date);
}

SurvivalResult HistoryReporter::dfs() const
{
    switch (d->category)
    {
    case NoTherapy:
    case InitiallySystemicTherapy:
    default:
        return SurvivalResult();
    case FirstLineFollowUpNoRecurrence:
    case RelapseAfterFirstLineFollowUp:
        if (d->linesContainer(false).lineDates.isEmpty())
        {
            return SurvivalResult();
        }
        QDate begin = d->linesContainer(false).lineDates.first();
        if (d->relapseElement)
        {
            return SurvivalResult(begin.daysTo(d->relapseElement->date), true);
        }
        if (!d->osIterator->isValid())
        {
            return SurvivalResult();
        }
        return SurvivalResult(begin.daysTo(d->osIterator->endDate()), false);
    }
}

int HistoryReporter::lineNumber(const QString &substanceId, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return 0;
    }
    return l.firstTherapyLines[i] + 1; // index is 0-based, line number is one-based
}

HistoryReporter::Category HistoryReporter::therapyCategory()
{
    return d->category;
}

int HistoryReporter::numberOfLines(bool palliative)
{
    return d->linesContainer(palliative).ctxLineDates.size();
}

int HistoryReporter::numberOfAdjuvantTherapies()
{
    return d->adjuvantTherapies.size();
}

QVariant HistoryReporter::dataCutOffTime(HistoryReporter::OSDefinition definition)
{
    if (!d->lastDocumentation.isValid())
    {
        return QVariant();
    }

    QDate begin;
    switch (definition)
    {
    case FromFirstTherapy:
        begin = d->osIterator->beginDate(OSIterator::FromFirstTherapy);
        break;
    case FromInitialDiagnosis:
        begin = d->osIterator->beginDate(OSIterator::FromInitialDiagnosis);
        break;
    case FromRelapseOrInitiallyIncurable:
        switch (d->category)
        {
        case NoTherapy:
        case FirstLineFollowUpNoRecurrence:
            return QVariant();
        case RelapseAfterFirstLineFollowUp:
            begin = d->relapseElement->date;
            break;
        case InitiallySystemicTherapy:
            begin = d->osIterator->beginDate(OSIterator::FromFirstTherapy);
            break;
        }
    }
    return begin.daysTo(d->lastDocumentation);
}

QVariant HistoryReporter::dataCutOffTime(int lineNumber, bool palliative)
{
    const TherapyLinesContainer& l = d->linesContainer(palliative);
    if (lineNumber >= l.firstTherapies.size() || lineNumber <0)
    {
        return QVariant();
    }
    return l.firstTherapies[lineNumber]->date.daysTo(d->lastDocumentation);
}

QVariant HistoryReporter::dataCutOffTime(const QString &substanceId, bool palliative)
{
    if (!d->lastDocumentation.isValid())
    {
        return QVariant();
    }

    int i = d->specificTherapyShortcuts.indexOf(substanceId);
    const TherapyLinesContainer& l = d->linesContainer(palliative);

    if (i == -1 || i>=l.firstTherapies.length() || !l.firstTherapies[i])
    {
        return QVariant();
    }
    return l.firstTherapies[i]->date.daysTo(d->lastDocumentation);
}

QVariant HistoryReporter::relapseDate()
{
    if (!d->relapseElement)
    {
        return QVariant();
    }
    return d->relapseElement->date;
}

DiseaseState::State HistoryReporter::lastState() const
{
    return d->currentStateIterator->effectiveState();
}

QVariant HistoryReporter::daysSinceLastFollowup() const
{
    if (d->osIterator->endpointReached())
    {
        return QVariant();
    }
    return d->currentStateIterator->effectiveHistoryEnd().daysTo(QDate::currentDate());
}

void HistoryReporter::run(const Patient::Ptr &p, const Disease &disease)
{
    d->clearResults();
    if (d->needOS || d->needLines)
    {
        d->osIterator = new OSIterator(disease);
        d->osIterator->setProofreader(d);
        d->lastDocumentation = disease.history.lastDocumentation();
    }
    if (d->needCurrentState)
    {
        d->currentStateIterator = new CurrentStateIterator(disease.history);
    }
    if (d->needLines || d->needSubstanceTherapies)
    {
        d->loadLinesContainer(disease.history, d->lines);
    }
    if (d->needCategory)
    {
        d->category = d->therapyCategory(p, disease.history);

        if (d->needLines || d->needSubstanceTherapies)
        {
            // palliative lines
            switch(d->category)
            {
            case NoTherapy:
            case FirstLineFollowUpNoRecurrence:
                break;
            case RelapseAfterFirstLineFollowUp:
                d->loadLinesContainer(disease.history, d->palliativeLines, d->relapseElement);
                break;
            case InitiallySystemicTherapy:
                d->loadLinesContainer(disease.history, d->palliativeLines, 0);
            }

            // adjuvant therapies
            d->loadAdjuvantTherapies(disease.history);
        }
    }
    if (d->needResponses)
    {
        d->lines.loadResponses(disease.history);
        d->palliativeLines.loadResponses(disease.history);
    }
}

QDate HistoryReporter::HistoryReporterPriv::lineBeginDate(const TherapyLinesContainer &l, int line, const QDate &sharpBegin)
{
    if (sharpBegin.isValid())
    {
        // usually, the day of the administration of the substance
        return sharpBegin;
    }
    else
    {
        // usually, the beginning of the therapy group
        return l.ctxLineDates[line];
    }
}

QPair<QDate, QDate> HistoryReporter::HistoryReporterPriv::ttfDates(const TherapyLinesContainer &l, int line, const QDate &sharpBegin)
{
    QDate begin = lineBeginDate(l, line, sharpBegin);
    QDate end;
    // If there is a following line of treatment, TTF is reached per definition
    if (l.ctxLineDates.size() > line+1)
    {
        end = l.ctxLineDates[line+1];
    }
    else
    {
        end = osIterator->endDate();
    }
    return qMakePair(begin, end);
}

QPair<QDate, QDate> HistoryReporter::HistoryReporterPriv::lineDates(const TherapyLinesContainer &l, int line)
{
    QDate begin = l.ctxLineDates[line];
    QDate end = l.ctxTherapies[line].effectiveEndDate();
    return qMakePair(begin, end);
}

QPair<QDate, QDate> HistoryReporter::HistoryReporterPriv::ctxDates(const TherapyLinesContainer &l, int line)
{
    QDate begin, end;
    foreach (const Therapy* t, l.ctxTherapies[line])
    {
        if (t->containsCTx())
        {
            if (!begin.isValid() || t->begin() < begin)
            {
                begin = t->begin();
            }
            if (!end.isValid() || t->end > end)
            {
                end = t->end;
            }
        }
    }
    if (!end.isValid())
    {
        end = l.ctxTherapies[line].effectiveEndDate();
    }
    return qMakePair(begin, end);
}

int HistoryReporter::HistoryReporterPriv::effectiveCtxDuration(const TherapyLinesContainer &l, int line)
{
    int days = 0;
    foreach (const Therapy* t, l.ctxTherapies[line])
    {
        // TODO: Merge possibly overlapping intervals
        if (t->containsCTx())
        {
            QDate end = t->end;
            if (!end.isValid())
            {
                end = l.ctxTherapies[line].effectiveEndDate();
            }
            days += t->begin().daysTo(t->end);
        }
    }
    return days;
}

QMap<QString, QVariant> HistoryReporter::HistoryReporterPriv::toxicities(const TherapyLinesContainer &l, QList<int> lines)
{
    QMap<QString, QVariant> toxicities;
    foreach (int line, lines)
    {
        foreach (const Therapy* t, l.ctxTherapies[line])
        {
            foreach (TherapyElement* element, t->elements)
            {
                if (element->is<Toxicity>())
                {
                    Toxicity* tox = element->as<Toxicity>();
                    toxicities[tox->description] = std::max(tox->grade, toxicities.value(tox->description, 0).toInt());
                }
            }
        }
    }
    return toxicities;
}

QMap<QString, QVariant> HistoryReporter::HistoryReporterPriv::substancesDosing(const TherapyLinesContainer &l, int line)
{
    // list all substances and doses for given therapy line
    QMap<QString, QVariant> dosing;
    foreach (const Therapy* t, l.ctxTherapies[line])
    {
        foreach (TherapyElement* element, t->elements)
        {
            if (element->is<Chemotherapy>())
            {
                Chemotherapy* ctx = element->as<Chemotherapy>();
                int dose = ctx->dose;
                if (dose == 0)
                {
                    dose = ctx->absdose;
                }
                dosing[ctx->substance] = dose;
            }
        }
    }
    return dosing;
}

QList<QVariant> HistoryReporter::HistoryReporterPriv::substanceDoses(const TherapyLinesContainer &l, const QString &id)
{
    // list all doses for drug(s) identified by id
    QList<QVariant> doses;
    QStringList substances = specificTherapySubstances.values(id);
    int i = specificTherapyShortcuts.indexOf(id);
    if (substances.isEmpty() || i == -1 || i>=l.therapiesOfInterest.length() || l.therapiesOfInterest[i].isEmpty())
    {
        return doses;
    }
    foreach (const Therapy* t, l.therapiesOfInterest[i])
    {
        foreach (TherapyElement* element, t->elements)
        {
            if (element->is<Chemotherapy>())
            {
                Chemotherapy* ctx = element->as<Chemotherapy>();
                foreach (const QString& substance, substances)
                {
                    if (ctx->hasSubstance(substance))
                    {
                        int dose = ctx->dose;
                        if (dose == 0)
                        {
                            dose = ctx->absdose;
                        }
                        doses << dose;
                    }
                }
            }
        }
    }
    return doses;
}

LocalTherapy HistoryReporter::HistoryReporterPriv::localTherapy(const TherapyLinesContainer &l, int n, bool includeSurgery, bool includeRTx)
{
    LocalTherapy result;
    int ctxLineCounter = 0;
    foreach (const TherapyGroup& group, l.therapies)
    {
        if ( (includeSurgery && group.hasSurgery()) || (includeRTx && group.hasRadiotherapy()) )
        {
            foreach (const Therapy* t, group)
            {
                if ( (includeSurgery && t->isSurgery()) || (includeRTx && t->containsRTx()) )
                {
                    if (n == 0)
                    {
                        result.therapy = t;
                        result.withinCTxLine = group.hasChemotherapy();
                        result.inAfterCTxLineNumber = ctxLineCounter;
                        return result;
                    }
                    else
                    {
                        n--;
                    }
                }
            }
        }

        if (group.hasChemotherapy())
        {
            ctxLineCounter++;
        }
    }
    return result;
}

SurvivalResult HistoryReporter::HistoryReporterPriv::ttf(const TherapyLinesContainer& l, int line, const QDate& sharpBegin)
{
    QPair<QDate, QDate> dates = ttfDates(l, line, sharpBegin);
    SurvivalResult result;
    result.days = dates.first.daysTo(dates.second);
    // If there is a following line of treatment, TTF is reached per definition
    if (l.ctxLineDates.size() > line+1)
    {
        result.eventReached = true;
    }
    else
    {
        result.eventReached = osIterator->endpointReached();
    }
    return result;
}

SurvivalResult HistoryReporter::HistoryReporterPriv::pfs(const TherapyLinesContainer& l, int line, const QDate& sharpBegin)
{
    SurvivalResult result;
    int pfs = l.responseIterators.at(line)->daysToFirstProgression();
    if (pfs != -1)
    {
        // we recorded a progression within this line and have reached the PFS
        return SurvivalResult(pfs, true);
    }

    // We have not recorded a progression. Get TTF for further calculation.
    QPair<QDate, QDate> dates = ttfDates(l, line, sharpBegin);
    result.days = dates.first.daysTo(dates.second);
    // If there is a following line of treatment without progression, PFS is undefined and censored at TTF
    if (l.ctxLineDates.size() > line+1)
    {
        result.eventReached = false;
    }
    else
    {
        // PFS is reached if patient died, otherwise censored like TTF
        result.eventReached = osIterator->endpointReached();
    }
    return result;
}

SurvivalResult HistoryReporter::HistoryReporterPriv::os(const TherapyLinesContainer& l, int line, const QDate& sharpBegin)
{
    QDate begin = lineBeginDate(l, line, sharpBegin);
    return SurvivalResult(osIterator->days(begin), osIterator->endpointReached());
}

void HistoryReporter::HistoryReporterPriv::loadLinesContainer(const DiseaseHistory& history, TherapyLinesContainer &l, HistoryElement* begin)
{
    l.treatmentLinesIterator = new NewTreatmentLineIterator;
    l.treatmentLinesIterator->setProofreader(this);
    l.treatmentLinesIterator->set(history, begin);
    l.treatmentLinesIterator->iterateToEnd();
    l.therapies = l.treatmentLinesIterator->therapies();
    l.lastEndDate = history.begin();

    if (needSubstanceTherapies)
    {
        // one entry for each substance group shortcut, containing first therapy line or all relevant therapy lines
        l.firstTherapies.resize(specificTherapyShortcuts.size());
        l.firstTherapyLines.resize(specificTherapyShortcuts.size());
        l.therapiesOfInterest.resize(specificTherapyShortcuts.size());
        l.therapyLinesOfInterest.resize(specificTherapyShortcuts.size());
    }

    foreach (const TherapyGroup& group, l.therapies)
    {
        if (group.hasChemotherapy())
        {
            l.ctxLineDates << group.beginDate();
            l.ctxTherapies << group;
        }

        if (needSubstanceTherapies)
        {
            for (int i=0; i<specificTherapyShortcuts.size(); ++i)
            {
                const QString& id = specificTherapyShortcuts[i];
                // already found a first line for this substance group?
                bool hasFirst = l.firstTherapies[i];
                int lineNumber = l.ctxLineDates.size() - 1;

                // Try all possible substances, check if this line contains the substance
                foreach (const QString& substance, specificTherapySubstances.values(id))
                {
                    if (group.hasSubstance(substance))
                    {
                        foreach (Therapy*t, group)
                        {
                            if (t->elements.hasSubstance(substance))
                            {
                                if (!hasFirst)
                                {
                                    l.firstTherapies[i] = t;
                                    l.firstTherapyLines[i] = lineNumber;
                                    hasFirst = true;
                                }
                                // append to the list, which also includes later lines
                                l.therapiesOfInterest[i].append(t);
                                l.therapyLinesOfInterest[i].insert(lineNumber);
                            }
                        }
                    }
                }
            }
        }

        // skip groups fully contained in another group
        if (group.effectiveEndDate() > lastEndDate || lastEndDate == history.begin())
        {
            l.lineDates << group.beginDate();
        }
        l.lastEndDate = group.effectiveEndDate();
    }
}

HistoryReporter::Category HistoryReporter::HistoryReporterPriv::therapyCategory(const Patient::Ptr& p, const DiseaseHistory &history)
{
    if (lines.therapies.isEmpty())
    {
         return NoTherapy;
    }

    ProgressionIterator progressionIterator(ProgressionIterator::OnlyRecurrence);
    const TherapyGroup& firstGroup = lines.therapies.first();

    // check if the patient entered follow up during or after the first-line therapy (recurrence can happen while on adjuvant therapy)
    EffectiveStateIterator effectiveStateIterator;
    effectiveStateIterator.set(history, firstGroup.firstTherapy());
    while (effectiveStateIterator.next() == HistoryIterator::Match)
    {
        if (effectiveStateIterator.effectiveState() == DiseaseState::FollowUp)
        {
            break;
        }
    }

    if (effectiveStateIterator.effectiveState() == DiseaseState::FollowUp)
    {
        // Ok, we entered Follow Up at some point.
        // This fulfills the definition of group 1 or 2. All the rest goes into group 3.

        // Now, lets have a look. Do we see recurrence?
        progressionIterator.set(history, effectiveStateIterator.currentElement());
        if (progressionIterator.next() == ProgressionIterator::Match)
        {
            relapseElement = progressionIterator.currentElement();
            return RelapseAfterFirstLineFollowUp;
        }
        else
        {
            // Looks like FirstLineFollowUpNoRecurrence;
            // double check
            while (effectiveStateIterator.next() == HistoryIterator::Match)
            {
                switch (effectiveStateIterator.effectiveState())
                {
                case DiseaseState::Therapy:
                case DiseaseState::WatchAndWait:
                case DiseaseState::BestSupportiveCare:
                {
                    problem(effectiveStateIterator.currentElement(),
                               QString("No recurrence for %1 %2 but state %3 after FollowUp. Please check. Assuming recurrent disease.")
                               .arg(p->surname).arg(p->firstName).arg(effectiveStateIterator.effectiveState()));
                    relapseElement = effectiveStateIterator.currentElement();
                    return RelapseAfterFirstLineFollowUp;
                    break;
                }
                default:
                    break;
                }
            }
            return FirstLineFollowUpNoRecurrence;
        }
    }
    else
    {
        return InitiallySystemicTherapy;
    }
}

void HistoryReporter::HistoryReporterPriv::loadAdjuvantTherapies(const DiseaseHistory &)
{
    switch(category)
    {
    case NoTherapy:
    case InitiallySystemicTherapy:
        return;
    case FirstLineFollowUpNoRecurrence:
    case RelapseAfterFirstLineFollowUp:
        break;
    }

    if (!lines.therapies.size())
    {
        return;
    }

    const TherapyGroup& firstLine = lines.therapies.at(0);
    Therapy* lastSurgery = 0;

    // first look for manual specification
    foreach (Therapy* t, firstLine)
    {
        if (t->description.contains("adjuvant", Qt::CaseInsensitive))
        {
            adjuvantTherapies << t;
        }
        if (t->isSurgery())
        {
            lastSurgery = t;
        }
    }

    // if none is manually specified, but there was surgery, try to infere
    if (adjuvantTherapies.isEmpty() && lastSurgery)
    {
        foreach (Therapy* t, firstLine)
        {
            if (t->date >= lastSurgery->date && t->containsCTx())
            {
                adjuvantTherapies << t;
            }
        }
    }
}

