/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 15.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef REPORTSCRIPTWINDOW_H
#define REPORTSCRIPTWINDOW_H

#include <QMainWindow>

#include "patient.h"

class QListWidgetItem;

class ReportScriptWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit ReportScriptWindow(QWidget *parent = 0);
    ~ReportScriptWindow();

signals:

    void activated(Patient::Ptr);

public slots:

    void editScript();
    void showResult();
    void openScript();
    void saveScript();
    void saveScriptAs();
    void saveResult();

    bool loadScript(const QString& fileName);
    bool storeScript(const QString& fileName);
    bool storeResult(const QString& fileName);

private slots:

    void newScript();
    void scriptEdited();
    void indexActivated(const QModelIndex& index);
    void updateHelp();
    void updateSaveActions();
    void helpItemActivated(QListWidgetItem*);
    void helpItemSelected();

protected:

    virtual void closeEvent(QCloseEvent*);
    bool askForSave();

private:

    void setupUI();
    void setupToolbar();

    class ReportWindowPriv;
    ReportWindowPriv* const d;
};

#endif // REPORTSCRIPTWINDOW_H
