/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 10.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "reportgenerator.h"

// Qt includes

#include <QDebug>
#include <QElapsedTimer>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QPointer>
#include <QStack>
#include <QTextStream>
#include <QtScript/QScriptEngine>

// Local includes

#include "combinedvalue.h"
#include "csvfile.h"
#include "history/historyiterator.h"
#include "historyreporter.h"
#include "ihcscore.h"
#include "labresults.h"
#include "patientmanager.h"
#include "reportelements.h"
#include "qvariantlisttablemodel.h"
#include "parser/patientcsvfile.h"


class ReportGenerator::ReportGeneratorPriv
{
public:
    ReportGeneratorPriv()
        : boolMode(ReportGenerator::BoolAsBool),
          dateMode(ReportGenerator::DayMonthYearDotsDate),
          historyReporter(0),
          needLab(false),
          scriptEngine(0),
          currentDisease(0),
          running(false),
          progress(0)
    {
    }

    ReportGenerator::BoolMode  boolMode;
    ReportGenerator::DateMode  dateMode;
    HistoryReporter*           historyReporter;
    bool                       needLab;
    LabResults                 labResults;
    QScriptEngine*             scriptEngine;

    QList<ReportInitializer*>  initializers;
    QList<ReportDatum*>        data;
    QList<ReportFinisher*>     finisher;
    QList<FilteringReportInitializer*> prefilters; // subset of initializers
    QList<FilteringReportFinisher*>    postfilters; // subset of finishers
    QStack<FilteringReportInitializerGroup*> filterGroups; // subset of initializers, temporary during construction
    QStringList                datumSpecs; // corresponds to data
    QStringList                commonCode;
    QMap<QString, QStringList> labGroups;

    QStringList                errors;
    QList<Patient::Ptr>        patients;

    QStringList                headers;
    QList< QList<QVariant> >   result;
    QList<QVariant>            line;
    QList<int>                 sourcePatientIds;

    Patient::Ptr               currentPatient;
    Disease*                   currentDisease;
    Disease                    nullDisease;

    bool                       running;
    int                        progress;
    QElapsedTimer              progressTime;

    void setCurrentPatient(const Patient::Ptr& p)
    {
        currentPatient = p;
        if (p->hasDisease())
        {
            currentDisease = &p->firstDisease();
        }
        else
        {
            currentDisease = &nullDisease;
        }
    }

    void clearElements()
    {
        delete historyReporter;
        historyReporter = 0;
        needLab = false;
        labResults = LabResults();
        delete scriptEngine;
        scriptEngine = 0;

        qDeleteAll(initializers);
        qDeleteAll(data);
        qDeleteAll(finisher);
        initializers.clear();
        data.clear();
        finisher.clear();

        prefilters.clear();
        postfilters.clear();
        filterGroups.clear();
        datumSpecs.clear();
        errors.clear();
        commonCode.clear();
        labGroups.clear();
    }

    void bindScriptProperties(QScriptValue& scriptObject,
                              const QVector<QScriptString>& propertyNames, const QList<QVariant>& values, int startIndex, int count)
    {
        const int end = qMin(startIndex + count, values.size());
        for (int i=startIndex; i<end; i++)
        {
            const QVariant& var = values[i];
            scriptObject.setProperty(propertyNames[i], scriptEngine->toScriptValue(var));
        }
    }

    bool progressStep(int granularity = 1)
    {
        ++progress;
        if (progress % granularity == 0 && progressTime.elapsed() > 10)
        {
            progressTime.restart();
            return true;
        }
        return false;
    }
};

class CurrentGeneratorSetter
{
public:
    CurrentGeneratorSetter(ReportElement* element, ReportGenerator* generator)
        : element(element)
    {
        element->currentGenerator = generator;
    }
    ~CurrentGeneratorSetter()
    {
        element->currentGenerator = 0;
    }

    ReportElement* element;
};
#define SetCurrentGenerator(element) CurrentGeneratorSetter _setter_(element, this)

ReportGenerator::ReportGenerator()
    : d(new ReportGeneratorPriv)
{
}

ReportGenerator::~ReportGenerator()
{
    d->clearElements();
    delete d;
}

void ReportGenerator::setBoolMode(BoolMode mode)
{
    d->boolMode = mode;
}

void ReportGenerator::setDateMode(ReportGenerator::DateMode mode)
{
    d->dateMode = mode;
}

void ReportGenerator::create(const QString& givenSpecs)
{
    QString specs(givenSpecs);
    QTextStream stream(&specs, QIODevice::ReadOnly);
    create(stream);
}

void ReportGenerator::create(QTextStream &stream)
{
    d->clearElements();
    while (!stream.atEnd())
    {
        QString spec = stream.readLine().trimmed();
        QString id, arguments;
        if (spec.contains(' '))
        {
            id = spec.section(' ', 0, 0, QString::SectionSkipEmpty);
            arguments = spec.mid(id.length());
        }
        else
        {
            id = spec;
        }
        if (id.isEmpty() || id.startsWith("//") || id.startsWith("#"))
        {
            continue;
        }

        ReportElement* element = ReportElement::create(id);

        if (!element)
        {
            reportError(QString("Unknown element: %1").arg(spec));
            continue;
        }

        if (element->isMultiLine())
        {
            while (!stream.atEnd())
            {
                QString line = stream.readLine().trimmed();
                if (line.startsWith("end"))
                {
                    break;
                }
                else
                {
                    arguments += '\n';
                    arguments += line;
                }
            }
        }

        {
            SetCurrentGenerator(element);
            element->arguments(arguments.trimmed());
        }

        switch (element->type())
        {
        case ReportElement::Initializer:
            d->initializers << static_cast<ReportInitializer*>(element);
            break;
        case ReportElement::Datum:
            d->data << static_cast<ReportDatum*>(element);
            d->datumSpecs << spec;
            break;
        case ReportElement::Finisher:
            d->finisher<< static_cast<ReportFinisher*>(element);
            break;
        }
    }
}

void ReportGenerator::addPreFilter(FilteringReportInitializer *filter)
{
    if (d->filterGroups.isEmpty())
    {
        d->prefilters << filter;
    }
    else
    {
        d->filterGroups.top()->filters << filter;
    }
}

void ReportGenerator::addPreFilterGroup(FilteringReportInitializerGroup *group)
{
    addPreFilter(group);
    d->filterGroups.push(group);
}

void ReportGenerator::closePreFilterGroup()
{
    if (d->filterGroups.isEmpty())
    {
        reportError("Unment \"end\" element");
    }
    else
    {
        d->filterGroups.pop();
    }
}

void ReportGenerator::addPostFilter(FilteringReportFinisher *filter)
{
    d->postfilters << filter;
}

void ReportGenerator::populate(const QList<Patient::Ptr>& patients)
{
    d->patients.reserve(patients.size());
    bool patientsWasEmpty = d->patients.isEmpty();
    foreach (const Patient::Ptr& p, patients)
    {
        if (!p)
        {
            continue;
        }
        if (!p->hasDisease())
        {
            reportError(QString("No disease in database for patient %1 %2 %3, skipping").arg(p->id).arg(p->firstName).arg(p->surname));
            continue;
        }
        if (patientsWasEmpty || !d->patients.contains(p))
        {
            d->patients += p;
        }
    }
}

void ReportGenerator::needScripting()
{
    if (!d->scriptEngine)
    {
        d->scriptEngine = new QScriptEngine;
    }
}

void ReportGenerator::addCommonScriptCode(const QString &scriptCode)
{
    d->commonCode += scriptCode;
}

QString ReportGenerator::commonScriptCode() const
{
    return d->commonCode.join('\n') + '\n';
}

void ReportGenerator::needHistory()
{
    if (!d->historyReporter)
    {
        d->historyReporter = new HistoryReporter(this);
    }
}

void ReportGenerator::needLabResults()
{
    d->needLab = true;
}

HistoryReporter* ReportGenerator::historyReporter() const
{
    return d->historyReporter;
}

QScriptEngine* ReportGenerator::scriptEngine() const
{
    return d->scriptEngine;
}

const LabResults &ReportGenerator::labResults() const
{
    return d->labResults;
}

void ReportGenerator::registerLabGroup(const QString &id, const QStringList &keys)
{
    d->labGroups[id] = keys;
}

bool ReportGenerator::isLabGroup(const QString &id) const
{
    return d->labGroups.contains(id);
}

QStringList ReportGenerator::labGroup(const QString &id) const
{
    return d->labGroups.value(id);
}

bool ReportGenerator::run()
{
    d->running = true;
    d->progress = 0;
    d->progressTime.start();
    emit started();

    // Run initializers, populate
    emit progressRangeChanged(0, d->initializers.size());
    emit progressValueChanged(0);
    emit progressTextChanged(tr("Lade Patienten"));
    d->patients.clear();
    foreach (ReportInitializer* initializer, d->initializers)
    {
        SetCurrentGenerator(initializer);
        initializer->run();
        if (d->progressStep(10))
            emit progressValueChanged(d->progress);
    }

    // Run pre filters
    d->progress = 0;
    emit progressRangeChanged(0, d->patients.size());
    emit progressValueChanged(0);
    emit progressTextChanged(tr("Filtere Patienten"));
    if (!d->prefilters.isEmpty())
    {
        QList<Patient::Ptr> filteredPatients;
        filteredPatients.reserve(d->patients.size());
        foreach (Patient::Ptr p, d->patients)
        {
            d->setCurrentPatient(p);
            bool result = true;
            foreach (FilteringReportInitializer* filter, d->prefilters)
            {
                SetCurrentGenerator(filter);
                if (!filter->filter())
                {
                    result = false;
                    break;
                }
            }
            if (result)
            {
                filteredPatients << p;
            }

            if (d->progressStep(10))
                emit progressValueChanged(d->progress);
        }
        d->patients = filteredPatients;
    }

    // Compute results
    d->progress = 0;
    emit progressRangeChanged(0, d->patients.size());
    emit progressValueChanged(0);
    emit progressTextChanged(tr("Lade Daten"));

    d->result.clear();
    d->result.reserve(d->patients.size());
    d->sourcePatientIds.clear();
    d->sourcePatientIds.reserve(d->patients.size());

    int columnCount = 0;
    foreach (ReportDatum *datum, d->data)
    {
        columnCount += datum->columnCount();
    }

    d->headers.clear();
    d->headers.reserve(columnCount);
    foreach (ReportDatum *datum, d->data)
    {
        SetCurrentGenerator(datum);
        for (int i=0; i<datum->columnCount(); i++)
        {
            d->headers += datum->columnHeader(i);
        }
    }

    if (d->patients.isEmpty())
    {
        reportError("Keine Patienten!");
    }

    QVector<QScriptString> scriptPropertyNames;
    QScriptValue scriptResultObject;
    scriptPropertyNames.reserve(columnCount);
    if (d->scriptEngine)
    {
        scriptPropertyNames.reserve(columnCount);
        for (int i=0; i<d->data.size(); i++)
        {
            // Property names: the exact spec;
            // for multi-columns values, column 2,3... are named <spec>-2, <spec>-3 ...
            const QString& spec = d->datumSpecs[i];
            scriptPropertyNames << d->scriptEngine->toStringHandle(spec);
            if (d->data[i]->columnCount() > 1)
            {
                for (int c=1; c<d->data[i]->columnCount(); c++)
                {
                    scriptPropertyNames << d->scriptEngine->toStringHandle(spec + QString("-%1").arg(c+1));
                }
            }
        }
        scriptResultObject = d->scriptEngine->newObject();
        d->scriptEngine->globalObject().setProperty("values", scriptResultObject);
    }

    foreach (Patient::Ptr p, d->patients)
    {
        if (!p)
        {
            continue;
        }
        d->setCurrentPatient(p);
        // values are written by data to d->line
        d->line.clear();
        d->line.reserve(columnCount);
        // Compute history values
        if (d->historyReporter)
        {
            d->historyReporter->run(d->currentPatient, *d->currentDisease);
        }
        if (d->needLab)
        {
            d->labResults = d->currentPatient->labResults();
        }
        // generate data
        foreach (ReportDatum *datum, d->data)
        {
            SetCurrentGenerator(datum);
            datum->run();

            if (d->scriptEngine)
            {
                d->bindScriptProperties(scriptResultObject, scriptPropertyNames, d->line, d->line.size() - datum->columnCount(), datum->columnCount());
            }
        }
        // append new line to result
        d->result << d->line;
        d->sourcePatientIds << p->id;
        if (d->progressStep())
            emit progressValueChanged(d->progress);

        if (!d->running)
        {
            d->clearElements();
            return false;
        }
    }

    // Run Finishers - this is before postfiltering, because post filters are finishers...
    foreach (ReportFinisher* finisher, d->finisher)
    {
        SetCurrentGenerator(finisher);
        finisher->run();
    }

    // Run post filters
    d->progress = 0;
    emit progressRangeChanged(0, d->result.size());
    emit progressValueChanged(0);
    emit progressTextChanged(tr("Filtere Ergebnisse"));
    if (!d->postfilters.isEmpty())
    {
        QList< QList<QVariant> > filteredResult;
        filteredResult.reserve(d->result.size());
        QList<int> filteredSourcePatientIds;
        filteredSourcePatientIds.reserve(d->result.size());
        const int size = d->result.size();
        for (int i=0; i<size; i++)
        {
            d->setCurrentPatient(d->patients[i]);
            const QList<QVariant>& line = d->result[i];

            if (d->scriptEngine)
            {
                d->bindScriptProperties(scriptResultObject, scriptPropertyNames, line, 0, line.size());
            }

            bool result = true;
            foreach (FilteringReportFinisher* filter, d->postfilters)
            {
                SetCurrentGenerator(filter);
                if (!filter->filter(line))
                {
                    result = false;
                    break;
                }
            }
            if (result)
            {
                filteredResult << line;
                filteredSourcePatientIds << d->currentPatient->id;
            }
            if (d->progressStep(10))
                emit progressValueChanged(d->progress);
        }
        d->result = filteredResult;
        d->sourcePatientIds = filteredSourcePatientIds;
    }

    // Finish finishers
    d->progress = 0;
    emit progressRangeChanged(0, d->finisher.size());
    emit progressValueChanged(0);
    emit progressTextChanged(tr("Speichere Ergebnisse"));
    foreach (ReportFinisher* finisher, d->finisher)
    {
        SetCurrentGenerator(finisher);
        finisher->finished(d->result);
        if (d->progressStep(10))
            emit progressValueChanged(d->progress);
    }

    emit finished();
    return true;
}

void ReportGenerator::recordDatum(const QVariant &datum)
{
    if (d->boolMode == BoolAs01 && datum.type() == QVariant::Bool)
    {
        d->line << (datum.toBool() ? 1 : 0);
    }
    else
    {
        d->line << datum;
    }
}

ReportGenerator& ReportGenerator::operator <<(const QVariant& datum)
{
    recordDatum(datum);
    return *this;
}

const Patient::Ptr ReportGenerator::patient() const
{
    return d->currentPatient;
}

const Disease& ReportGenerator::disease() const
{
    return *d->currentDisease;
}


QStringList ReportGenerator::columnHeaders() const
{
    return d->headers;
}

QStringList ReportGenerator::datumsSpecifications() const
{
    return d->datumSpecs;
}

const QList< QList<QVariant> >& ReportGenerator::result() const
{
    return d->result;
}

Patient::Ptr ReportGenerator::sourcePatient(int resultRow) const
{
    return PatientManager::instance()->patientForId(d->sourcePatientIds[resultRow]);
}

QString ReportGenerator::dateToString(const QDate &date) const
{
    switch (d->dateMode)
    {
    case ISODate:
        return date.toString(Qt::ISODate);
    case DayMonthYearDotsDate:
        return date.toString("dd.MM.yyyy");
    }
    return QString();
}

QString ReportGenerator::dateTimeToString(const QDateTime &dateTime) const
{
    switch (d->dateMode)
    {
    case ISODate:
        return dateTime.toString(Qt::ISODate);
    case DayMonthYearDotsDate:
        return dateTime.toString("dd.MM.yyyy hh:mm");
    }
    return QString();
}

void ReportGenerator::reportError(const QString &error)
{
    if (!d->errors.contains(error))
    {
        d->errors << error;
    }
}

void ReportGenerator::reportErrors(const QStringList &errors)
{
    d->errors += errors;
}

QStringList ReportGenerator::errors() const
{
    return d->errors;
}

void ReportGenerator::writeToCSV(const QString& fileName, const QChar& delimiter) const
{
    CSVFile file(delimiter);
    file.openForWriting(fileName);
    writeToCSV(file);
}

void ReportGenerator::writeToCSV(CSVFile &file) const
{
    file.setDateFormat(d->dateMode == DayMonthYearDotsDate ? "dd.MM.yyyy" : QString());
    QVariantList headers;
    headers.reserve(d->headers.size());
    foreach (const QString& header, d->headers)
    {
        headers << header;
    }
    file.writeNextLine(headers);

    foreach (const QList<QVariant>& line, d->result)
    {
        file.writeNextLine(line);
    }
}

void ReportGenerator::writeJSON(const QString &fileName) const
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        return;
    }
    writeJSON(file);
}

static QJsonValue variantToJSONValue(const ReportGenerator* q, const QVariant& v)
{
    // only our date conversion is non-standard, all the rest should cleanly map to JSON types
    switch (v.type())
    {
    case QVariant::Date:
        return q->dateToString(v.toDate());
    case QVariant::DateTime:
        return q->dateTimeToString(v.toDateTime());
    default:
        return QJsonValue::fromVariant(v);
    }
}

void ReportGenerator::writeJSON(QFile &file) const
{
    QJsonArray jsonResults;
    foreach (const QList<QVariant>& line, d->result)
    {
        QJsonObject jsonLine;
        int size = line.size();
        for (int i=0; i<size; i++)
        {
            jsonLine[d->headers[i]] = variantToJSONValue(this, line[i]);
        }
        jsonResults.append(jsonLine);
    }
    QJsonDocument doc(jsonResults);
    file.write(doc.toJson());
}

void ReportGenerator::populateFromCSVFile(const QString &fileName)
{
    // Wants a header line and a first column containing (optionally) the id, and/or three more columns surname, first name, date of birth
    PatientCSVFile source;
    if (!source.parse(fileName))
    {
        return;
    }
    QList<Patient::Ptr> patients = source.patients();
    reportErrors(source.errors());
    populate(patients);
}

QAbstractTableModel* ReportGenerator::toModel() const
{
    QVariantListTableModel* model = new QVariantListTableModel(d->result);
    model->setColumnHeaders(d->headers);
    model->setShowRowNumbers(true);
    return model;
}

QVariant ReportGenerator::writePathologyProperty(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode)
{
    PathologyPropertyInfo info(id);
    ValueTypeCategoryInfo typeInfo(info.valueType);
    QVariant value;
    if (typeInfo.category == PathologyPropertyInfo::BooleanCombination)
    {
        CombinedValue comb(info);
        comb.combine(disease());
        value = comb.toValue();
    }
    else
    {
        PropertyList props = disease().pathologyProperties(info.id);
        if (!props.isEmpty())
        {
            if (writeMode == FirstProperty)
            {
                value = typeInfo.toVariantData(props.first());
            }
            else
            {
                QVariantList list;
                foreach (const Property& prop, props)
                {
                    list << typeInfo.toVariantData(prop);
                }
                value = list;
            }
        }
    }
    recordDatum(value);
    return value;
}

bool ReportGenerator::hasDetailValue(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode)
{
    PathologyPropertyInfo info(id);
    PropertyList props = disease().pathologyProperties(info.id);
    if (props.isEmpty())
    {
        return false;
    }
    foreach (const Property& prop, props)
    {
        if (!prop.detail.isEmpty())
        {
            return true;
        }
        if (writeMode == FirstProperty)
        {
            return false;
        }
    }
    return true;
}

QVariant ReportGenerator::writeDetailValue(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode)
{
    PathologyPropertyInfo info(id);
    PropertyList props = disease().pathologyProperties(info.id);
    if (props.isEmpty())
    {
        recordDatum(QString());
        return QVariant();
    }
    ValueTypeCategoryInfo typeInfo(info.valueType);

    QVariantList list;
    foreach (const Property& prop, props)
    {
        QVariant value = prop.detail;
        if (typeInfo.category == PathologyPropertyInfo::Mutation &&
                prop.detail.isEmpty() &&
                typeInfo.toVariantData(prop).toBool())
        {
            value = "mutation unspecified";
        }

        if (writeMode == FirstProperty)
        {
            recordDatum(value);
            return value;
        }
        list << value;
    }

    if (list.isEmpty())
    {
        recordDatum(QVariant());
        return QVariant();
    }
    else
    {
        recordDatum(list);
        return list;
    }
}

void ReportGenerator::writeIHCPropertySplit(PathologyPropertyInfo::Property id, PropertyWriteMode)
{
    //TODO: writeMode
    PathologyPropertyInfo info(id);
    Property prop = disease().pathologyProperty(info.id);
    ValueTypeCategoryInfo typeInfo(info.valueType);
    IHCScore score = typeInfo.toIHCScore(prop);
    if (prop.isNull())
    {
        recordDatum(QVariant());
        recordDatum(QVariant());
        return;
    }
    recordDatum(score.colorIntensity);
    recordDatum(score.positiveRatio());
}

void ReportGenerator::writeIHCIsPositive(PathologyPropertyInfo::Property id, PropertyWriteMode)
{
    //TODO: writeMode
    PathologyPropertyInfo info(id);
    Property prop = disease().pathologyProperty(info.id);
    ValueTypeCategoryInfo typeInfo(info.valueType);
    IHCScore score = typeInfo.toIHCScore(prop);
    if (prop.isNull() || !score.isValid())
    {
        recordDatum(QVariant());
        return;
    }
    recordDatum(score.isPositive(info.property));
}

void ReportGenerator::writePathologyPropertyDate(PathologyPropertyInfo::Property id, PropertyWriteMode writeMode)
{
    PathologyPropertyInfo info(id);
    QVariantList list;
    foreach (const Pathology& path, disease().pathologies)
    {
        Property p = path.properties.property(info.id);
        if (p.isValid())
        {
            if (writeMode == FirstProperty)
            {
                recordDatum(path.date);
                return;
            }
            else
            {
                list << path.date;
            }
        }
    }

    if (list.isEmpty())
    {
        recordDatum(QVariant());
    }
    else
    {
        recordDatum(list);
    }
}

#include "analysistableview.h"
#include <QApplication>
void ReportGenerator::test()
{
    QString specs =
            "loadPatients nsclcAdeno\n"
            "skipPatients withoutHistory\n"
            "skipPatients external\n"
            "patientId \n"
            "name \n"
            "firstName \n"
            "dateOfBirth\n"
            "sex\n"
            "ageAtDiagnosis\n"
            "tnm\n"
            "t\n"
            "n\n"
            "m\n"
            "r\n"
            "v\n"
            "g\n"
            "r\n"
            "path mut/kras?exon=2\n"
            "path mut/egfr?exon=19,21\n"
            "pathDetail mut/egfr?exon=19,21\n"
            "pathDetail mut/kras?exon=2\n"
            "useSubstanceGroup Platin: Cisplatin, Carboplatin\n"
            "useSubstance Pemetrexed\n"
            "os\n"
            "os Pemetrexed\n"
            "os Platin\n"
            "ttf 1\n"
            "ttf 2\n"
            "ttf Platin\n"
            "ttf Pemetrexed\n"
            "therapyCategory \n"
            "script egfrOrKras \n"
            "if (values['path mut/egfr?exon=19,21'] || values['path mut/kras?exon=2']) \n"
            "{ \n"
            "values['pathDetail mut/egfr?exon=19,21']; \n"
            "} \n"
            "else \n"
            "{ \n"
            "values['pathDetail mut/kras?exon=2'] \n"
            "} \n"
            "end \n"
            "filterScript\n"
            "(values['path mut/egfr?exon=19,21'] || values['path mut/kras?exon=2']);\n"
            "end \n"
            ;
    ReportGenerator g;
    g.create(specs);
    g.setBoolMode(ReportGenerator::BoolAs01);
    g.run();
    qDebug() << "Errors:" << g.errors();
    QVariantListTableModel model;
    model.setData(g.result());
    model.setColumnHeaders(g.columnHeaders());
    AnalysisTableView view;
    view.setModel(&model);
    view.show();
    qApp->exec();
}

void ReportGenerator::cancel()
{
    d->running = false;
}
