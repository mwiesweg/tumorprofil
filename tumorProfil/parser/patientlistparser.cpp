/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 27.06.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "patientlistparser.h"

#include "patientmanager.h"
#include "parsertools.h"
#include "databaseconstants.h"
#include "pathologypropertyinfo.h"

PatientListParser::PatientListParser()
    : m_dryRun(false)
{

}

void PatientListParser::setDryRun(bool dryRun)
{
    m_dryRun = dryRun;
}

void PatientListParser::setProject(const QString &project)
{
    m_project = project;
}

void PatientListParser::parse(CSVFile &file)
{
    CSVFile outFile;
    if (!m_dryRun)
    {
        QString outFileName = file.filePath();
        outFileName = outFileName.left(outFileName.lastIndexOf('.')) + "-output.csv";
        if (!outFile.openForWriting(outFileName))
        {
            qDebug() << "Could not open file to write output" << outFile.errorString();
        }
        outFile << "KIS ID" << "name" << "firstName" << "dob" << "entity" << "gender" << "Marcel ID";
        outFile.newLine();
    }

    PropertyList patientPropertiesToAdd;
    for (int trial = TrialContextInfo::FirstTrial; trial <= TrialContextInfo::LastTrial; trial++)
    {
        TrialContextInfo info = TrialContextInfo::info(TrialContextInfo::Trial(trial));
        if (info.id == m_project)
        {
            patientPropertiesToAdd << Property(PatientPropertyName::trialParticipation(), info.id);
            break;
        }
    }

    QList<QVariant> header = file.parseNextLine();
    int n=0;
    while (!file.atEnd())
    {
        // KIS ID; surname; first name; date of birth; entity; sex (optional)
        QList<QVariant> line = file.parseNextLine();
        if (line.size() < 5)
        {
            continue;
        }
        uint kisId = line[0].toUInt();
        Patient patientData;
        patientData.surname = line[1].toString();
        patientData.firstName = line[2].toString();
        patientData.dateOfBirth = line[3].toDate();
        if (line.size() >= 6)
        {
            QString genderString = line[5].toString();
            if (genderString.compare("m", Qt::CaseInsensitive) == 0)
            {
                patientData.gender = Patient::Male;
            }
            else if (genderString.compare("w", Qt::CaseInsensitive) == 0 ||
                     genderString.compare("f", Qt::CaseInsensitive) == 0 )
            {
                patientData.gender = Patient::Female;
            }
        }
        n++;

        Pathology::Entity entity = Pathology::UnknownEntity;
        QString entityString = line[4].toString();
        if (entityString == "aNSCLC")
        {
            entity = Pathology::PulmonaryAdeno;
        }
        else if (entityString == "sqNSCLC")
        {
            entity = Pathology::PulmonarySquamous;
        }
        else if (entityString == "asqNSCLC")
        {
            entity = Pathology::PulmonaryAdenosquamous;
        }
        else if (entityString == "LCNEC")
        {
            entity = Pathology::PulmonaryLCNEC;
        }
        else if (entityString == "NSCLC undiff" || line[4].toString() == "lc NSCLC")
        {
            entity = Pathology::PulmonaryNSCLCNOS;
        }
        else if (entityString == "Pankreas" || entityString == "Pancreas" || entityString == "PDAC")
        {
            entity = Pathology::PancreaticCarcinoma;
        }
        else if (entityString == "CCC" || entityString == "Cholangiocarcinoma" || entityString == "BTC")
        {
            entity = Pathology::Cholangiocarcinoma;
        }
        else
        {
            qDebug() << "Unknown entity" << line[4];
        }

        if (!m_dryRun)
        {
            outFile << kisId << patientData.surname << patientData.firstName << patientData.dateOfBirth.toString(Qt::ISODate) << entity;
        }

        if (!patientData.isValidAllowUnknownGender())
        {
            qDebug() << "Invalid patient data from" << line;
            continue;
        }

        if (!m_dryRun && patientData.gender == Patient::UnknownGender)
        {
            patientData.gender = ParserTools::genderForFirstName(patientData.firstName);
        }

        Patient::Ptr p = ParserTools::existingPatient(patientData, false);
        if (p)
        {
            if (m_dryRun)
            {
                qDebug() << "Patient" << patientData.firstName << patientData.surname << "already in db";
                if (p->firstDisease().entity() != entity)
                {
                    qDebug() << " but entity differs! " << line[4].toString();
                }
                if (p->medicoId && kisId && p->medicoId != kisId)
                {
                    qDebug() << " but KIS id differs!";
                }
            }
            else
            {
                PatientManager::ChangeFlags flags;
                if (!p->medicoId && kisId)
                {
                    p->medicoId = kisId;
                    flags |= PatientManager::ChangedPatientMetadata;
                }
                if (!patientPropertiesToAdd.isEmpty())
                {
                    p->patientProperties.add(patientPropertiesToAdd);
                    flags |= PatientManager::ChangedPatientProperties;
                }
                PatientManager::instance()->updateData(p, flags);
            }
        }
        else
        {
            if (m_dryRun)
            {
                qDebug().noquote() << "Adding patient" << patientData.firstName <<  patientData.surname << patientData.dateOfBirth << patientData.gender;
            }
            else
            {
                p = ParserTools::saveData(p, patientData, QDate(), entity, QDate(), PropertyList(), QStringList(), ParserTools::SimonParser, patientPropertiesToAdd);
            }
        }

        if (!m_dryRun)
        {
            outFile << p->gender << p->id;
            outFile.newLine();
        }
    }
    qDebug() << "Parsed a file with" << n << "patients";
}

void PatientListParser::parse(const QString &fileName)
{
    CSVFile file(';');
    if (!file.openForReading(fileName))
    {
        qDebug() << QString("Failed to open file %1: %2").arg(fileName).arg(file.errorString());
        return;
    }
    parse(file);
}
