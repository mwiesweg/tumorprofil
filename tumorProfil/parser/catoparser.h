/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef CATOPARSER_H
#define CATOPARSER_H

#include <QString>

#include "csvfile.h"

class CatoParser
{
public:
    CatoParser();
    void setDryRun(bool dryRun);

    void insert(const QString& fileName);
    void remove(const QString& fileName);

protected:
    QHash<int, QList<QVariant> > parse(const QString& fileName, bool createIds);
    QHash<int, QList<QVariant> > parse(CSVFile& file, bool createIds);
    void insert(const QHash<int, QList<QVariant> >& toInsert);
    void remove(const QHash<int, QList<QVariant> >& toInsert);
    bool m_dryRun;
};

#endif // CATOPARSER_H
