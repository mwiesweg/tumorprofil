/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef PATIENTCSVFILE_H
#define PATIENTCSVFILE_H

#include "patient.h"
#include "util/csvfile.h"


class PatientCSVFile : public CSVFile
{
public:
    PatientCSVFile(const QChar& delimiter = ';', const QChar& decimalPoint = ',');

    // Parses the file and calls lineData for each row
    bool parse(const QString& fileName);

    // Converts patientData to Patient::Ptr.
    // For patients without a match, the list contains 0 and error messages are added.
    QList<Patient::Ptr>     patients();

    QList<QVariant>         headerData() const;
    QList<Patient>          patientData() const;
    QList<QList<QVariant> > associatedData() const;
    int                     rowCount() const;
    QStringList             errors() const;

    // makes additional attempts above CSVFile's; use if you know it's date
    static QDate strongDateConversion(const QVariant& data);

protected:

    QList<QVariant>         m_header;
    QList<Patient>          m_patientData;
    QList<QList<QVariant> > m_associatedData;
    QStringList             m_errors;

    // The default implementation stores the data in m_patientData and m_data
    virtual void lineData(const Patient& patientData, const QList<QVariant>& associatedData);
};

#endif // PATIENTCSVFILE_H
