/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 17.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "ngsresultcvsparser.h"

#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "databaseconstants.h"
#include "parsertools.h"
#include "patientmanager.h"

NGSResultCVSParser::NGSResultCVSParser()
    : m_count(0),
      m_dryRun(false),
      m_ngsPanels(ParserTools::ngsPanels())
{
}

void NGSResultCVSParser::setDryRun(bool dryRun)
{
    m_dryRun = dryRun;
}

int NGSResultCVSParser::count() const
{
    return m_count;
}

void NGSResultCVSParser::setPanel(const QString &panel)
{
    m_presetPanel = panel;
}

static QString panelFromFileName(const QString& fileName)
{
    if (fileName.contains(" CCP3 "))
    {
        return "CCP3";
    }
    else if (fileName.contains(" MAPK1 "))
    {
        return "MAPK1";
    }
    else if (fileName.contains(" NNGML1 "))
    {
        return "NNGML1";
    }
    else if (fileName.contains("NNGML2.1"))
    {
        return "NNGML2.1";
    }
    else
    {
        qDebug() << "Cannot identify NGS panel for file" << fileName;
        return QString();
    }
}

void NGSResultCVSParser::parse(const QString &fileName)
{
    CSVFile file;
    if (!file.openForReading(fileName))
    {
        qDebug() << QString("Failed to open file %1: %2").arg(fileName).arg(file.errorString());
        return;
    }    
    parse(file);
    qDebug().noquote() << "Parsed file with" << m_count << "patients.";
}

static QDate dateFromFileName(const QString& fileName)
{
    QRegularExpression re("(\\d\\d)\\.(\\d\\d)\\.(\\d\\d\\d\\d)\\.XLSX");
    QRegularExpressionMatch match = re.match(fileName);
    if (match.hasMatch())
    {
        return QDate(match.captured(3).toInt(), match.captured(2).toInt(), match.captured(1).toInt());
    }
    return QDate();
}

static QDate heuristicDateFromPathId(const QString& pathId)
{
    QRegularExpression re("([E,C,R])[-_](\\d+)[-_](\\d+)[-_]");
    QRegularExpressionMatch match = re.match(pathId);
    if (match.hasMatch())
    {
        double denominator = 38000;
        QString ecr = match.captured(1);
        if (ecr == "C")
            denominator = 9000;
        else if (ecr == "R")
            denominator = 1100;
        double monthfraction = match.captured(3).toDouble() * 12.5/ denominator;
        int month = qBound(1, qRound(monthfraction), 12);
        int day = 1; //(monthfraction - double(month) < 0.5) ? 1 : 15;
        int year  = match.captured(2).toInt();
        return QDate(year, month, day);
    }
    return QDate();
}

class NGSResultCVSParser::ResultSet
{
public:
    ResultSet() : panWTMarker(false) {}
    QString      fileName;
    QString      pathId;
    QString      rawReport;
    Patient      patientData;
    PropertyList properties;
    PropertyList positiveProperties;
    PropertyList parseErrorProperties;
    QString      panel;
    bool         panWTMarker;
};

void NGSResultCVSParser::parse(CSVFile &file)
{
    ResultSet set;
    ValueTypeCategoryInfo typeInfo(PathologyPropertyInfo::Mutation);
    while (!file.atEnd())
    {
        QList<QVariant> line = file.parseNextLine();
        if (line.isEmpty())
        {
            continue;
        }
        /*if (line.size() == 1 || line.first().toString().endsWith(".XLSX"))
        {
            followingFile = line.first().toString();
            continue;
        }*/
        if (line.size() < 7)
        {
            continue;
        }

        // empty patient lines?
        /*if (!line[1].toString().isEmpty() && line[4].toString().isEmpty() )
        {
            continue;
        }*/

        // beginning new patient?
        if (!line[1].toString().isEmpty())
        {
            Patient newPatient;
            newPatient.surname     = line[1].toString().remove("Prof.").remove("Dr.").trimmed();
            newPatient.firstName   = line[2].toString().remove("Prof.").remove("Dr.").trimmed();
            newPatient.dateOfBirth = line[3].toDate(); //.addYears(-4).addDays(-1); // Excel 1900 vs. 1904 issue with xlsx2csv

            if (newPatient.surname != set.patientData.surname ||
                    newPatient.firstName != set.patientData.firstName ||
                    newPatient.dateOfBirth != set.patientData.dateOfBirth
                    )
            {
                storeResults(set);
                set = ResultSet();
                set.patientData = newPatient;
                set.pathId      = line[0].toString().trimmed();
                if (m_dryRun)
                {
                    set.patientData.gender  = Patient::UnknownGender;
                }
                else
                {
                    set.patientData.gender  = ParserTools::genderForFirstName(set.patientData.firstName);
                }

                if (!m_presetPanel.isEmpty())
                {
                    set.panel = m_presetPanel;
                }
                if (line.size() >= 11)
                {
                    set.fileName                = line[10].toString();
                    set.panel                   = panelFromFileName(set.fileName);
                }
            }

            if ( set.panWTMarker ||
                 (line.size() >= 6 && line[4].toString() == "wt" && line[6].toString() == "Wildtyp") ||
                 (line.size() >= 10 &&
                  (line[9].toString().contains("wt") ||
                   line[9].toString().contains("Es lassen sich keine Mutationen nachweisen") ||
                   line[9].toString().contains("Es lassen sich keine Veränderungen nachweisen") ||
                   line[9].toString().contains("Es lassen sich keine Veränderungen in den untersuchten Genen nachweisen") ||
                   line[9].toString().contains("Es lassen sich keine Mutationen in den untersuchten Genen nachweisen") ||
                   line[9].toString().contains("Am vorliegenden Material lassen sich keine klinisch relevanten Veränderungen nachweisen") ||
                   line[9].toString().contains("Am untersuchten Material lassen sich keine Mutationen nachweisen") ||
                   line[9].toString().contains("Es lassen sich am vorliegenden Material keine Mutationen nachweisen."))
                  )
                 )
            {
                set.panWTMarker = true;
            }

        }

        set.rawReport += file.currentLine();
        set.rawReport += '\n';

        QString gene = line[4].toString();
        if (gene.isEmpty() || (gene == "wt" && set.panWTMarker))
        {
            continue;
        }
        QList<Property> possibleGeneProps = ParserTools::propertiesForGene(gene);
        if (possibleGeneProps.isEmpty())
        {
            qDebug() << "No properties for gene" << gene << set.pathId << file.currentLine();
            continue;
        }
        PropertyList propsForLine;
        QList<int> exons;
        foreach (const QString exon, line[5].toString().split(','))
        {
            bool ok;
            int exonNumber = exon.trimmed().toInt(&ok);
            if (ok)
            {
                exons << exonNumber;
            }
        }

        if (exons.size() != 1 && line.size() >= 8)
        {
            QString mutation = line[7].toString();
            if (gene == "KRAS" || gene == "NRAS")
            {
                if (mutation.contains(QRegularExpression("p\\.(G|Gly)1(2|3)[a-zA-Z]{1,3}")))
                {
                    exons = QList<int>() << 2;
                }
                else if (mutation.contains(QRegularExpression("p\\.(Q|Gln)61[a-zA-Z]{1,3}")))
                {
                    exons = QList<int>() << 3;
                }
                else if (mutation.contains(QRegularExpression("p\\.[a-zA-Z]{1,3}(117|146)[a-zA-Z]{1,3}")))
                {
                    exons = QList<int>() << 4;
                }
            }
            if (gene == "EGFR" || gene == "BRAF")
            {
                QRegularExpression re("p\\.[a-zA-Z]{1,3}(\\d+)(?:_|[a-zA-Z]{1,3})");
                QRegularExpressionMatch match = re.match(mutation);
                if (match.hasMatch())
                {
                    int aa = match.captured(1).toInt();
                    if (gene == "EGFR")
                    {
                        if (aa >= 761 && aa <= 775)
                        {
                            exons = QList<int>() << 20;
                        }
                        if (aa >= 744 && aa <= 760)
                        {
                            exons = QList<int>() << 19;
                        }
                        if (aa < 720)
                        {
                            exons = QList<int>() << 18;
                        }
                        if (aa >= 776)
                        {
                            exons = QList<int>() << 21;
                        }
                    }
                    else if (gene == "BRAF")
                    {
                        if (aa >= 581)
                        {
                            exons = QList<int>() << 15;
                        }
                        else if (aa <= 470)
                        {
                            exons = QList<int>() << 11;
                        }

                    }
                }
            }
        }

        if (exons.isEmpty())
        {
            if (possibleGeneProps.size() > 1)
            {
                qDebug() << "No specified exon, but multiple possible properties: Suggesting all" << set.pathId << file.currentLine();
            }
            foreach (const Property& prop, possibleGeneProps)
            {
                propsForLine.setProperty(prop.property, prop.value); // checks for duplicates
            }
        }
        foreach (int exon, exons)
        {
            Property prop = ParserTools::propertyForExon(possibleGeneProps, gene, exon);
            propsForLine.setProperty(prop.property, prop.value); // checks for duplicates
        }


        if (propsForLine.isEmpty())
        {
            qDebug() << "No properties for gene and exon" << set.pathId << file.currentLine();
            continue;
        }

        QString result = line[6].toString().trimmed();
        if (result.isEmpty() || result == "kein Ergebnis")
        {
            continue;
        }
        if (result == "Wildtyp")
        {
            // value is prepared as "negative"
            set.properties.merge(propsForLine);
        }
        else if (result.startsWith("c."))
        {
            if (propsForLine.size() > 1)
            {
                qDebug() << "Multiple properties for line with a positive finding! Dont know which to choose, taking first" << set.pathId << file.currentLine();
                propsForLine = PropertyList() << propsForLine.first();
            }
            /*if (exons.size() > 1)
            {
                qDebug() << "Multiple exons for positive finding!" << set.pathId << file.currentLine();
            }*/
            foreach (const Property& prop, propsForLine)
            {
                QString value = typeInfo.toPropertyValue(true);

                QString detail = result;
                QString sep = "\t";
                if (line.size() >= 8)
                {
                    detail += sep + line[7].toString().remove("( )").replace("( ", "(").trimmed();
                }

                if (line.size() >= 9)
                {
                    detail += sep + line[8].toString().trimmed();
                }
                else
                {
                    qDebug() << "No allele frequency:" << set.pathId << file.currentLine() << ", detail will be:" << detail;
                }
                if (exons.size() == 1)
                {
                    QStringList exonStrings;
                    foreach (int exon, exons)
                        exonStrings += QString::number(exon);
                    detail += sep + "Exon " + exonStrings.join(',');
                }

                if (set.positiveProperties.hasProperty(prop.property))
                {
                    // Merge two mutations for the same gene
                    detail.prepend(set.positiveProperties.property(prop.property).detail + sep);
                    set.positiveProperties.setProperty(prop.property, value, detail);
                }
                else
                {
                    set.positiveProperties.addProperty(prop.property, value, detail);
                }
            }
        }
        else
        {
            set.parseErrorProperties << propsForLine;
            qDebug() << "Unhandled result (not starting with \"c.\") in line" << set.pathId << file.currentLine();
            continue;
        }
    }
    storeResults(set);
}

void NGSResultCVSParser::storeResults(const ResultSet& resultSetGiven)
{
    ResultSet set(resultSetGiven);

    if (set.properties.isEmpty() && set.positiveProperties.isEmpty() && !set.panWTMarker)
    {
        if (!set.pathId.isEmpty())
        {
            qDebug() << "Skipping" << set.pathId << m_count << "because there are no properties";
            if (!set.rawReport.contains("kein Ergebnis"))
            {
                qDebug() << set.rawReport;
            }
        }
        return;
    }

    // If "found no mutations" was reported, or only positive results were reported, fill in the negative results from current panel
    if (set.panWTMarker || (!set.positiveProperties.isEmpty() && set.properties.isEmpty()))
    {
        if (!m_ngsPanels.contains(set.panel))
        {
            qDebug() << "Filling negative results: Unidentified panel" << set.panel << "for patient" << set.fileName;
        }
        QStringList panelGenes = m_ngsPanels.value(set.panel);
        foreach (const QString& gene, panelGenes)
        {
            set.properties << ParserTools::propertiesForGene(gene);
        }
    }

    // Merge positive properties last so that they take precedence
    set.properties.merge(set.positiveProperties);
    // Remove parse errors: Better store nothing than wrong negative
    foreach (const Property& prop, set.parseErrorProperties)
    {
        set.properties.removeProperty(prop.property);
    }

    QDate resultDate;
    if (!set.fileName.isEmpty())
    {
        resultDate = dateFromFileName(set.fileName);
        m_lastDate = resultDate;
    }
    else if (!resultDate.isValid() && !set.pathId.isEmpty())
    {
        resultDate = heuristicDateFromPathId(set.pathId);
    }
    else if (!resultDate.isValid())
    {
        resultDate = m_lastDate;
    }

    Patient::Ptr existingPatient = ParserTools::existingPatient(set.patientData, false);

    /*
        qDebug().noquote() << p << set.patientData.surname << set.patientData.firstName << set.patientData.dateOfBirth << resultDate << set.patientData.gender;
        foreach (const Property& p, set.properties)
        {
            qDebug().noquote() << p.property << p.value << p.detail;
        }
    */

    m_count++;
    if (!m_dryRun)
    {
        QString rawReport = set.rawReport;
        rawReport.replace("≥", ">="); // quick fix for MYSQL encoding problem
        PropertyList patientProps;
        if (set.panel == "NNGML2.1")
        {
            patientProps << Property(PatientPropertyName::pathologyNetwork(), PatientPropertyValue::nngm());
        }
        ParserTools::saveData(existingPatient, set.patientData, QDate(), Pathology::UnknownEntity, resultDate,
                              set.properties, QStringList() << rawReport, ParserTools::NGSCVSParser, patientProps);
    }
}
