/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABTABLEREADER_H
#define LABTABLEREADER_H

#include <QMap>
#include <QStringList>

class LabTableReader
{
public:
    LabTableReader(const QStringList& options, bool dryRun);

    void read();
    void setDryRun(bool dryRun);

private:

    QStringList m_options;
    bool m_dryRun;
    QMap<QString, QString> m_aliasMap;
};

#endif // LABTABLEREADER_H
