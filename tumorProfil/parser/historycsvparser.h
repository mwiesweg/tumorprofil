/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef HISTORYCSVPARSER_H
#define HISTORYCSVPARSER_H

#include "disease.h"
#include "patient.h"

class PatientCSVFile;

class HistoryCSVParser
{
public:
    HistoryCSVParser();
    void setDryRun(bool dryRun);

    void parse(const QString& fileName);

protected:

    bool setOS(const Patient::Ptr& p, Disease& disease, const QDate& date, const QVariant& osReached, bool ltfu);
    bool setECOG(Patient::Ptr& p, const QDate& date, int ecog);
    bool setSmokingStatus(Patient::Ptr& p, PatientPropertyValue::SmokingStatus status, int packYears);

    void read(PatientCSVFile& file);
    bool m_dryRun;
};

#endif // HISTORYCSVPARSER_H
