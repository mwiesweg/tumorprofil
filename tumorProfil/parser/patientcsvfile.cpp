/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "patientcsvfile.h"

#include "patientmanager.h"

PatientCSVFile::PatientCSVFile(const QChar &delimiter, const QChar &decimalPoint)
    : CSVFile(delimiter, decimalPoint)
{

}

bool PatientCSVFile::parse(const QString &fileName)
{
    m_errors.clear();
    m_patientData.clear();
    m_associatedData.clear();
    m_header.clear();

    if (!openForReading(fileName))
    {
        m_errors << QString("Failed to open source file ") + fileName + ": " + errorString();
        return false;
    }

    // store header line, dont use it here
    m_header = parseNextLine().mid(4);

    while (!atEnd())
    {
        QList<QVariant> lineData = parseNextLine();
        if (lineData.size() < 3)
            continue;
        Patient patientData;
        bool ok;
        patientData.id = lineData.first().toInt(&ok);
        if (lineData.size() >= 3)
        {
            patientData.surname = lineData[1].toString().trimmed();
            patientData.firstName = lineData[2].toString().trimmed();
        }
        if (lineData.size() >= 4)
        {
            patientData.dateOfBirth = strongDateConversion(lineData[3]);

            if (!patientData.dateOfBirth.isValid())
            {
                m_errors << QString("Failed to parse date of birth %1").arg(lineData[3].toString());
            }
        }

        m_patientData    << patientData;
        m_associatedData << lineData.mid(4);
    }
    return true;
}

QList<Patient::Ptr> PatientCSVFile::patients()
{
    QList<Patient::Ptr> patients;
    patients.reserve(m_patientData.size());

    foreach (const Patient& patientData, m_patientData)
    {
        Patient::Ptr patientById;
        if (patientData.id)
        {
            Patient::Ptr patientById = PatientManager::instance()->patientForId(patientData.id);
            // Do we have only the id?
            if (!patientData.isValidAllowUnknownGender())
            {
                patients << patientById;
                if (!patientById)
                {
                    m_errors << QString("Did not identify patient with id %1").arg(patientData.id);
                }
                continue;
            }
            // if we have additional lineData, check that it matches the id
        }

        if ( patientData.surname.isEmpty() ||
             (patientData.firstName.isEmpty() && !patientData.dateOfBirth.isValid())
             )
        {
            m_errors << QString("Patient insufficiently specified: %1 %2 %3 %4")
                                .arg(patientData.id).arg(patientData.surname).arg(patientData.firstName).arg(patientData.dateOfBirth.toString());
            patients << Patient::Ptr();
            continue;
        }

        QList<Patient::Ptr> candidates = PatientManager::instance()->findPatients(patientData);

        if (patientById)
        {
            if (candidates.isEmpty() || !candidates.contains(patientById))
            {
                m_errors << QString("Mismatch between id and lineData for patient %1 %2 %3 %4! Using id, please check.")
                            .arg(patientData.id).arg(patientData.surname).arg(patientData.firstName).arg(patientData.dateOfBirth.toString());
            }
            patients << patientById;
        }
        else
        {
            if (candidates.isEmpty())
            {
                m_errors << QString("Cannot identify patient %1 %2 %3 %4")
                            .arg(patientData.id).arg(patientData.surname).arg(patientData.firstName).arg(patientData.dateOfBirth.toString());
                patients << Patient::Ptr();
                continue;
            }
            if (candidates.size() > 1)
            {
                m_errors << QString("Identified multiple patients for %1 %2 %3, taking first")
                            .arg(patientData.surname).arg(patientData.firstName).arg(patientData.dateOfBirth.toString());
            }
            patients << candidates.first();
        }
    }

    return patients;
}

QList<QVariant> PatientCSVFile::headerData() const
{
    return m_header;
}

QList<Patient> PatientCSVFile::patientData() const
{
    return m_patientData;
}

QList<QList<QVariant> > PatientCSVFile::associatedData() const
{
    return m_associatedData;
}

int PatientCSVFile::rowCount() const
{
    return m_patientData.size();
}

void PatientCSVFile::lineData(const Patient &patientData, const QList<QVariant> &data)
{
    m_patientData << patientData;
    m_associatedData << data;
}

QStringList PatientCSVFile::errors() const
{
    return m_errors;
}

QDate PatientCSVFile::strongDateConversion(const QVariant &data)
{
    // many formats are already covered by CSVFile parsing
    if (data.type() == QVariant::Date)
    {
        return data.toDate();
    }
    else
    {
        QString dateString = data.toString().trimmed();
        if (dateString.contains('-'))
        {
            return QDate::fromString(dateString, "yyyy-MM-dd");
        }
        else if (dateString.contains('/'))
        {
            return QDate::fromString(dateString, "M/d/yyyy");
        }
    }
    return QDate();
}
