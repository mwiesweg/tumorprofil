/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 17.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef PARSERTOOLS_H
#define PARSERTOOLS_H

#include <QDate>
#include <QList>

#include "ihcscore.h"
#include "pathologypropertyinfo.h"
#include "patient.h"

class Pathology;

namespace ParserTools
{
bool isSamePatient(const Patient& a, const Patient& b);

enum ParserType
{
    ImportTool,
    NGSCVSParser,
    SimonParser
};

Patient::Ptr saveData(const Patient::Ptr& existingPatient, const Patient& patientData, const QDate& initialDx, Pathology::Entity entity,
                      const QDate& resultsDate, const PropertyList& pathologyProperties, const QStringList& reports, ParserType parser,
                      PropertyList patientProperties = PropertyList());
Patient::Ptr existingPatient(const Patient& patientData, bool genderIsConfirmed);
Pathology *findSuitablePathology(const Patient::Ptr& p, const QDate& reportDate);
int findSuitablePathology(const QList<Pathology> &pathologies, const QDate& reportDate);
void adjustSuitablePathology(Pathology* path, const QDate& reportDate);
void mergeNewProperties(Pathology* path, const PropertyList& newProps, ParserType parser, const Patient& patientData = Patient());
QDate initialDxFromResultDate(const QDate& resultDate);

Patient::Gender genderForFirstName(const QString& firstName);

QList<Property> propertiesForGene(const QString& gene);
Property&       propertyForExon(QList<Property>& properties, const QString& gene, int exon);
QMap<QString, QStringList> ngsPanels();
QList<Property> mergeGeneProperties(const QList<Property>& currentProperties, const QList<Property>& propertiesToAdd);

IHCScore::Intensity             textToIntensity(const QString& text);
PathologyPropertyInfo::Property textToIHCProperty(const QString& protein);
PathologyPropertyInfo::Property textToFISHProperty(const QString& protein);
IHCScore::Intensity             textToQualitativeIntensity(const QString& rawText);

QDate dateFromFilePath(const QString& path);
QDate dateFromInsideString(const QString& s);
}

#endif // PARSERTOOLS_H
