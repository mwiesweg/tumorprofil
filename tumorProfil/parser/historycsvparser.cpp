/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "historycsvparser.h"

#include <QDebug>

#include "history/historyiterator.h"
#include "labresults.h"
#include "patientcsvfile.h"
#include "patientmanager.h"
#include "parsertools.h"

HistoryCSVParser::HistoryCSVParser()
    : m_dryRun(false)
{

}

void HistoryCSVParser::setDryRun(bool dryRun)
{
    m_dryRun = dryRun;
}

void HistoryCSVParser::parse(const QString &fileName)
{
    PatientCSVFile file;
    qDebug() << "Parsing file" << fileName;
    if (!file.parse(fileName))
    {
        qDebug() << QString("Failed to parse file %1: ").arg(fileName);
        qDebug() << file.errors();
        return;
    }
    read(file);
}

enum DataType
{
    UnknownDataType,
    InitialDx,
    OSDate,
    OSReached,
    DeathDate,
    LTFU,
    LastContact,
    SmokingStatus,
    PackYears,
    ECOGStatus,
    ECOGDate,
    PrimaryTumorLocation,
    TNM
};

void HistoryCSVParser::read(PatientCSVFile &file)
{
    QList<Patient::Ptr> patients = file.patients();
    if (file.errors().size())
    {
        qDebug() << "Converted to patients:";
        foreach (const QString& error, file.errors())
        {
            qDebug() << error;
        }
    }

    QDate fileDate = ParserTools::dateFromFilePath(file.filePath());
    QDate referenceDate = fileDate.isNull() ? QDate::currentDate() : fileDate;

    QList<QVariant> header = file.headerData();
    QList<DataType> dataTypes;
    foreach (const QVariant& h, header)
    {
        QString heading = h.toString();

        // A date which needs further specification via OS reached
        if (heading == "OS date" || heading == "OS-Datum")
        {
            dataTypes << OSDate;
        }
        // A date which implies that the endpoint has been reached
        else if (heading == "Todesdatum")
        {
            dataTypes << DeathDate;
        }
        // censoring indicator for OS
        else if (heading == "OS reached" || heading == "OSreached" || heading == "OSerreicht" || heading == "OS erreicht")
        {
            dataTypes << OSReached;
        }
        else if (heading == "ED" || heading == "initialDx" || heading == "initialdx" || heading == "idx" || heading == "initial diagnosis")
        {
            dataTypes << InitialDx;
        }
        // explicitly indicate LTFU state
        else if (heading == "ltfu" || heading == "LTFU")
        {
            dataTypes << LTFU;
        }
        // date of last contact, including LTFU which is assumed after a time threshold
        // of 6 months to reference date, which is extracted from the file name with dd.mm.yyyy format, or current date.
        else if (heading == "last contact" || heading == "LK-Datum")
        {
            dataTypes << LastContact;
        }
        else if (heading == "smoking status")
        {
            dataTypes << SmokingStatus;
        }
        else if (heading == "pack years")
        {
            dataTypes << PackYears;
        }
        else if (heading == "ECOG")
        {
             dataTypes << ECOGStatus;
        }
        else if (heading == "ECOG date")
        {
             dataTypes << ECOGDate;
        }
        else if (heading == "location" || heading == "primary tumor location")
        {
             dataTypes << PrimaryTumorLocation;
        }
        else if (heading == "TNM")
        {
             dataTypes << TNM;
        }
        else
        {
            qDebug() << "Unknown data type" << heading;
            dataTypes << UnknownDataType;
        }
    }

    if ( (dataTypes.contains(ECOGStatus) && !dataTypes.contains(ECOGDate)) ||
         (!dataTypes.contains(ECOGStatus) && dataTypes.contains(ECOGDate))
         )
    {
        qDebug() << "Can use ECOG only in conjunction with ECOG date in another column!";
    }

    if (dataTypes.contains(PackYears) && !dataTypes.contains(SmokingStatus))
    {
        qDebug() << "Can use pack years only in conjunction with smoking status in another column!";
    }

    QList<QList<QVariant> > associatedDataLists = file.associatedData();
    for (int i=0; i<patients.size(); i++)
    {
        Patient::Ptr p = patients[i];
        if (!p)
        {
            continue;
        }
        QList<QVariant> associatedData = associatedDataLists[i];
        QDate osDate, initialDx;
        QVariant osReached;
        bool ltfu = false;
        int ecog = -1;
        QDate ecogDate;
        PatientPropertyValue::SmokingStatus smokingStatus = PatientPropertyValue::SmokingStatusUnknown;
        int packYears = 0;
        QString tnm, location;
        bool ok;

        for (int j=0; j<dataTypes.size() && j<associatedData.size(); j++)
        {
            if (associatedData[j].toString().isEmpty())
            {
                continue;
            }
            switch (dataTypes[j])
            {
            case UnknownDataType:
                break;
            case OSDate:
                osDate = file.strongDateConversion(associatedData[j]);
                break;
            case OSReached:
                osReached = associatedData[j];
                break;
            case DeathDate:
                osDate = file.strongDateConversion(associatedData[j]);
                osReached = true;
                break;
            case InitialDx:
                initialDx = file.strongDateConversion(associatedData[j]);
                break;
            case LTFU:
            {
                QString ltfuString = associatedData[j].toString().trimmed();
                if (ltfuString == "*" || ltfuString == "1" || QString::compare(ltfuString, "true", Qt::CaseInsensitive))
                {
                    ltfu = true;
                }
                break;
            }
            case LastContact:
            {
                osDate = file.strongDateConversion(associatedData[j]);
                osReached = false;
                // assume LTFU if date of last contact > 6 months prior to date of observation
                if (osDate.daysTo(referenceDate) > 6*30)
                {
                    ltfu = true;
                }
                break;
            }
            case ECOGDate:
                ecogDate = file.strongDateConversion(associatedData[j]);
                break;
            case ECOGStatus:
                ecog = associatedData[j].toInt(&ok);
                if (!ok)
                {
                    ecog = -1;
                }
                break;
            case SmokingStatus:
                smokingStatus = PatientPropertyValue::smokingStatus(associatedData[j].toString());
                break;
            case PackYears:
                packYears = associatedData[j].toInt();
                break;
            case TNM:
                tnm = associatedData[j].toString();
                break;
            case PrimaryTumorLocation:
                location = associatedData[j].toString();
                break;
            }
        }

        if (m_dryRun)
        {
            qDebug() << "- Updating data for" << p->firstName << p->surname;
        }
        PatientManager::ChangeFlags flags;

        if (!p->hasDisease())
        {
            if (m_dryRun)
            {
                qDebug() << "Dry run, not adding a disease";
                continue;
            }
            // add here support for multiple diseases...
            Disease disease;
            p->diseases << disease;
            flags |= PatientManager::ChangedDiseaseMetadata;
        }

        Disease& disease = p->diseases.first();
        if (initialDx.isValid() && disease.initialDiagnosis != initialDx)
        {
            if (m_dryRun)
            {
                qDebug().noquote() << "Updating initial diagnosis from" << disease.initialDiagnosis << "to" << initialDx;
            }
            else
            {
                disease.initialDiagnosis = initialDx;
                flags |= PatientManager::ChangedDiseaseHistory;
            }
        }

        if (!tnm.isNull() && disease.initialTNM.toText() != tnm)
        {
            if (m_dryRun)
            {
                if (disease.initialTNM.isEmpty())
                {
                    qDebug().noquote() << "Setting tnm" << tnm;
                }
                else
                {
                    qDebug().noquote() << "Updating tnm from" << disease.initialTNM.toText() << "to" << tnm;
                }
            }
            else
            {
                disease.initialTNM.setTNM(tnm);
                flags |= PatientManager::ChangedDiseaseMetadata;
            }
        }

        if (!location.isNull())
        {
            QString previous = disease.diseaseProperties.property(DiseasePropertyName::primaryTumorLocation()).value;
            if (previous != location)
            {
                if (m_dryRun)
                {
                    if (previous.isEmpty())
                    {
                        qDebug().noquote() << "Setting primary location" << location;
                    }
                    else
                    {
                        qDebug().noquote() << "Updating primary tumor location from" << previous << "to" << location;
                    }
                }
                else
                {
                    disease.diseaseProperties.setProperty(DiseasePropertyName::primaryTumorLocation(), location);
                    flags |= PatientManager::ChangedDiseaseProperties;
                }
            }
        }

        if (setOS(p, disease, osDate, osReached, ltfu))
        {
            flags |= PatientManager::ChangedDiseaseHistory;
        }

        setECOG(p, ecogDate, ecog);

        if (setSmokingStatus(p, smokingStatus, packYears))
        {
            flags |= PatientManager::ChangedPatientProperties;
        }

        if (!m_dryRun)
        {
            PatientManager::instance()->updateData(p, flags);
        }
    }
}

bool HistoryCSVParser::setOS(const Patient::Ptr &, Disease &disease, const QDate &date, const QVariant &osReached, bool ltfu)
{
    if (!date.isValid() || !osReached.isValid())
    {
        return false;
    }

    SilentHistoryProofreader silentProofReader;
    OSIterator os(disease);
    os.setProofreader(&silentProofReader);
    bool reached = osReached.toBool();
    if (date == os.endDate() && reached == os.endpointReached())
    {
        if (ltfu && !(os.lastElement() && os.lastElement()->is<DiseaseState>() && os.lastElement()->as<DiseaseState>()->state == DiseaseState::LossOfContact))
        {
            if (m_dryRun)
            {
                qDebug().noquote() << "Adding a new DiseaseState::LossOfContact for" << date.addDays(1);
            }
            else
            {
                DiseaseState* state = new DiseaseState;
                state->state = DiseaseState::LossOfContact;
                state->date  = date.addDays(1);
                disease.history.add(state);
                return true;
            }
        }
        else
        {
            if (m_dryRun)
            {
                qDebug().noquote().nospace() << "Ignoring information about date " << date << ", recorded last entry is identical";
            }
        }
        return false;
    }

    //qDebug().noquote() << "Setting new OS endpoint to" << date << reached << "formerly" << os.endDate() << os.endpointReached();

    if (reached)
    {
        if (os.endpointReached())
        {
            // We need to adjust the endpoint date
            for (int i=0; i<disease.history.size(); i++)
            {
                HistoryElement* e = disease.history[i];
                if (e->is<Finding>())
                {
                    Finding* f = e->as<Finding>();
                    if (f->type == Finding::Death)
                    {
                        if (m_dryRun)
                        {
                            qDebug().noquote() << "Changing date of Finding::Death from" << f->date <<"to" << date;
                        }
                        else
                        {
                        f->date = date;
                        }
                    }
                }
                else if (e->is<DiseaseState>())
                {
                    DiseaseState* s = e->as<DiseaseState>();
                    if (s->state == DiseaseState::Deceased)
                    {
                        if (m_dryRun)
                        {
                            qDebug().noquote() << "Changing date of DiseaseState::Deceased from" << s->date <<"to" << date;
                        }
                        else
                        {
                            s->date = date;
                        }
                    }
                }
            }
            if (!m_dryRun)
            {
                disease.history.sort();
            }
        }
        else
        {
            // Add the new endpoint event to history
            if (m_dryRun)
            {
                qDebug().noquote() << "Adding new endpoint Finding::Death at" << date;
            }
            else
            {
                Finding* f = new Finding;
                f->type = Finding::Death;
                f->date = date;
                disease.history.add(f);
            }
        }
    }
    else
    {
        if (os.endpointReached())
        {
            // We need to remove endpoint elements which are apparenly wrong
            if (m_dryRun)
            {
                qDebug().noquote() << " !!! Removing wrong death endpoints at" << os.endDate();
            }
            else
            {
                forever
                {
                    OSIterator nextOsIt(disease);
                    nextOsIt.setProofreader(&silentProofReader);
                    if (!nextOsIt.endpointReached())
                    {
                        break;
                    }
                    disease.history.remove(nextOsIt.endpointElement());
                }
            }
        }
        else
        {
            if (os.endDate() > date)
            {
                if (m_dryRun)
                {
                    qDebug().noquote().nospace() << "Ignoring contact date " << date << ", recorded last contact is later: " << os.endDate();
                }
                return false;
            }

            if (os.lastElement() && os.lastElement()->is<DiseaseState>() && os.lastElement()->as<DiseaseState>()->state == DiseaseState::LossOfContact)
            {
                // Do nothing. Do not remove previous loss of contact; the loss of contact is still true, even if we gather new survival information
            }

            if (m_dryRun)
            {
                qDebug().noquote() << "Adding a new Finding::Contact for" << date;
                if (ltfu)
                {
                    qDebug().noquote() << "Adding a new DiseaseState::LossOfContact for" << date.addDays(1);
                }
            }
            else
            {
                // Add a generic "contact" to signal the patient was alive at that date
                Finding* f = new Finding;
                f->type = Finding::Contact;
                f->date = date;
                disease.history.add(f);

                if (ltfu)
                {
                    DiseaseState* state = new DiseaseState;
                    state->state = DiseaseState::LossOfContact;
                    state->date  = date.addDays(1);
                    disease.history.add(state);
                }
            }
        }
    }
    return true;
}

bool HistoryCSVParser::setECOG(Patient::Ptr& p, const QDate &date, int ecog)
{
    if (!p || !date.isValid() || ecog < 0 || ecog > 4)
    {
        return false;
    }

    if (m_dryRun)
    {
        qDebug() << "Setting ECOG status to" << ecog << "at" << date;
    }
    else
    {
        LabResults results = p->labResults();
        results.edit(date.startOfDay(), LabSeries::clinicalType(), "ECOG", ecog);
        PatientManager::instance()->setLabResults(p, results);
    }
    return true;
}

bool HistoryCSVParser::setSmokingStatus(Patient::Ptr& p, PatientPropertyValue::SmokingStatus status, int packYears)
{
    if (!p || status == PatientPropertyValue::SmokingStatusUnknown)
    {
        return false;
    }
    if (m_dryRun)
    {
        qDebug() << "Setting smoking status to" << PatientPropertyValue::smokingStatus(status) << "pack years" << packYears;
    }
    else
    {
        p->setSmokingStatus(status, packYears);
    }
    return true;
}


