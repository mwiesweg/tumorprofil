/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "catoparser.h"

#include <QDebug>
#include <QRegularExpression>

#include "databaseaccess.h"
#include "patientdb.h"

CatoParser::CatoParser() : m_dryRun(false)
{
}

void CatoParser::setDryRun(bool dryRun)
{
    m_dryRun = dryRun;
}

void CatoParser::insert(const QString &fileName)
{
    insert(parse(fileName, true));
}

void CatoParser::remove(const QString &fileName)
{
    remove(parse(fileName, false));
}

QHash<int, QList<QVariant> > CatoParser::parse(const QString &fileName, bool createIds)
{
    CSVFile file(';');
    if (!file.openForReading(fileName))
    {
        qDebug() << QString("Failed to open file %1: %2").arg(fileName).arg(file.errorString());
        return QHash<int, QList<QVariant> >();
    }
    file.setParseMode(CSVFile::UnparsedText);
    qDebug() << "Parsing file" << fileName;
    return parse(file, createIds);
}

QHash<int, QList<QVariant> > CatoParser::parse(CSVFile &file, bool createIds)
{
    QList<QRegularExpression> dateRes;
    dateRes << QRegularExpression("(?<day>\\d{1,2})\\.(?<month>\\d{1,2})\\.(?<year>\\d{2,4})\\s+\\d{2}:\\d{2}:\\d{2}")
            << QRegularExpression("(?<year>\\d{4})-(?<month>\\d{2})-(?<day>\\d{2})\\s+\\d{2}:\\d{2}:\\d{2}")
            << QRegularExpression("(?<day>\\d{1,2})\\.(?<month>\\d{1,2})\\.(?<year>\\d{2,4})")
            << QRegularExpression("(?<month>\\d{1,2})\\/(?<day>\\d{1,2})\\/(?<year>\\d{2,4})\\s+\\d{1,2}:\\d{1,2}");
    QRegularExpression toRemove(" \\(fl\\.\\)|"
                                " Amp\\.|"
                                " Dstfl\\.|"
                                " Inj\\.lsg\\.|"
                                " Trsubst\\.|"
                                " \\(tr.\\)|"
                                " Inf\\.lsgs\\.konz\\.|"
                                " Inf\\.lsg\\.|"
                                " Trstamp\\.|"
                                " Inf\\.konz\\.|"
                                " ,Körpergewicht Erwachsene|"
                                " ,KOF\\* Erwachsene"); // If you add regexps, dont forget the '|'

    QHash<QString, int> idHash = DatabaseAccess().db()->identifierHash();
    QHash<int, QList<QVariant> > toInsert;

    int idPos, substancePos, datePos;
    QList<QVariant> header = file.parseNextLine();
    idPos = header.indexOf("Patient/Batch");
    substancePos = header.indexOf("Präparat");
    datePos = header.indexOf("Datum/Zeit");
    if (idPos == -1 || substancePos == -1 || datePos == -1)
    {
        qDebug() << "Failed to parse header" << idPos << substancePos << datePos << header;
        return toInsert;
    }
    int counter = 0; // only for dry run

    while (!file.atEnd())
    {
        QList<QVariant> line = file.parseNextLine();
        if (line.size() < qMax(idPos, qMax(substancePos, datePos)))
        {
            continue;
        }

        QString textIdentifier = line[idPos].toString();
        QString substance      = line[substancePos].toString();
        if (textIdentifier.isEmpty() || substance.isEmpty())
        {
            qDebug() << "Invalid data in line" << file.currentLine();
            continue;
        }
        substance.remove(toRemove);

        //uniqueWords.unite(QSet<QString>::fromList(substance.remove(QRegularExpression("\\d")).split(' ', QString::SkipEmptyParts)));

        QDate date;
        foreach (const QRegularExpression& dateRe, dateRes)
        {
            QRegularExpressionMatch match = dateRe.match(line[datePos].toString());
            date = QDate( (match.capturedLength("year") == 2 ? 2000 : 0) + match.captured("year").toInt(),
                          match.captured("month").toInt(), match.captured("day").toInt());
            if (date.isValid())
                break;
        }
        if (!date.isValid())
        {
            if (line[datePos].toString() != "Datum/Zeit") // dont report error for column header
            {
                qDebug() << "Invalid date in line" << line[3] << file.currentLine();
            }
            continue;
        }

        if (m_dryRun)
        {
            counter++;
            continue;
        }

        int id;
        if (createIds)
        {
            id = DatabaseAccess().db()->findOrCreateIdentifierId(textIdentifier, &idHash);
        }
        else
        {
            id = DatabaseAccess().db()->findIdentifierId(textIdentifier, &idHash);
            if (!id)
            {
                continue;
            }
        }
        toInsert[id] << substance << date;
    }

    if (m_dryRun)
    {
        qDebug() << "Dry run, but would return" << counter << "entries to insert";
    }

    //qDebug().noquote() << uniqueWords;
    return toInsert;
}

void CatoParser::insert(const QHash<int, QList<QVariant> > &toInsert)
{
    qDebug() << "Have" << toInsert.size() << "patient keys. Inserting into database.";

    DatabaseAccess access;
    int count=0;
    for (QHash<int, QList<QVariant> >::const_iterator it = toInsert.begin(); it != toInsert.end(); ++it)
    {
        access.db()->insertValuesForIdentifier(PatientDB::CATOReports, it.key(), it.value());
        count += it.value().size() / 2;
        qDebug() << "Inserted" << it.value().size() / 2 << "entries";
    }

    qDebug() << "Finished inserting" << count << "medications";
}

void CatoParser::remove(const QHash<int, QList<QVariant> > &toInsert)
{
    qDebug() << "Have" << toInsert.size() << "entries. Removing from database.";

    DatabaseAccess access;
    int count=0;
    for (QHash<int, QList<QVariant> >::const_iterator it = toInsert.begin(); it != toInsert.end(); ++it)
    {
        access.db()->removeValuesForIdentifier(PatientDB::CATOReports, it.key(), it.value());
        count += it.value().size() / 2;
        qDebug() << "Removed" << it.value().size() / 2 << "entries";
    }

    qDebug() << "Finished removing" << count << "medications";
}

