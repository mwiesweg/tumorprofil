/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 17.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "parsertools.h"

#include <QDate>
#include <QDebug>
#include <QEventLoop>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QRegularExpression>
#include <QTextStream>
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>

#include "databaseconstants.h"
#include "patientmanager.h"

QList<Property> ParserTools::propertiesForGene(const QString& gene)
{
    // Works in conjunction with propertyForExon
    /*
    BRAF    11,15
    DDR2    15-18
    EGFR    18,19,20,21
    ERBB2   5,6,15,20,23,29
    FGFR1   3,7,13,17
    FGFR3   7,9
    HRAS    2,3,4
    KIT     9,10,11,13,17,18
    KRAS    2,3,4
    MET     3,8,11,14,19
    NRAS    2,3,4
    PDGFRa  12,14,18
    PIK3CA  3,5,10,16,21
    RET     7,10,11,13,14,15,16
    TP53    1-10
    IDH1    4
    IDH2    4
    STK11   1-9
    */
    QList<Property> props;
    QList<PathologyPropertyInfo> ids;
    if (gene == "BRAF")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BRAF_11);
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BRAF_15);
    }
    else if (gene == "DDR2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_DDR2);
    }
    else if (gene == "EGFR")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_EGFR_19_21);
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_EGFR_18_20);
    }
    else if (gene == "ERBB2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ERBB2);
    }
    else if (gene == "FGFR1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_FGFR1);
    }
    else if (gene == "FGFR2" || gene == "FGFR2 Tr-B" || gene == "FGFR2 Tr-A")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_FGFR2);
    }
    else if (gene == "FGFR3")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_FGFR3);
    }
    else if (gene == "FGFR4")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_FGFR4);
    }
    else if (gene == "HRAS")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_HRAS_2_4);
    }
    else if (gene == "KIT")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KIT);
    }
    else if (gene == "KRAS")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KRAS_2);
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KRAS_3);
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KRAS_4);
    }
    else if (gene == "MET")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MET);
    }
    else if (gene == "NRAS")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_NRAS_2_4);
    }
    else if (gene == "PDGFRa" || gene == "PDGFRA")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_PDGFRa);
    }
    else if (gene == "PIK3CA")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_PIK3CA);
    }
    else if (gene == "RET")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_RET);
    }
    else if (gene == "TP53")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_TP53);
    }
    else if (gene == "IDH1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_IDH1);
    }
    else if (gene == "IDH2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_IDH2);
    }
    else if (gene == "STK11")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_STK11);
    }
    else if (gene == "ALK")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ALK);
    }
    else if (gene == "CTNNB1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_CTNNB1);
    }
    else if (gene == "MAP2K1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MAP2K1);
    }
    else if (gene == "PTEN")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_PTEN);
    }
    else if (gene == "ROS1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ROS1);
    }
    else if (gene == "SF3B1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_SF3B1);
    }
    else if (gene == "BCLAF1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BCLAF1);
    }
    else if (gene == "BRCA1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BRCA1);
    }
    else if (gene == "BRCA2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BRCA2);
    }
    else if (gene == "PALB2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_PALB2);
    }
    else if (gene == "RPA1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_RPA1);
    }
    else if (gene == "ATM")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ATM);
    }
    else if (gene == "MLH1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MLH1);
    }
    else if (gene == "MSH2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MSH2);
    }
    else if (gene == "BAP1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_BAP1);
    }
    else if (gene == "ARID1A")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ARID1A);
    }
    else if (gene == "ARID1B")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_ARID1B);
    }
    else if (gene == "SMARCA2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_SMARCA2);
    }
    else if (gene == "SMARCA4")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_SMARCA4);
    }
    else if (gene == "SMARCB1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_SMARCB1);
    }
    else if (gene == "KDM6A")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KDM6A);
    }
    else if (gene == "PBRM1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_PBRM1);
    }
    else if (gene == "SMAD4")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_SMAD4);
    }
    else if (gene == "MDM2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MDM2);
    }
    else if (gene == "RNF43")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_RNF43);
    }
    else if (gene == "GNAS")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_GNAS);
    }
    else if (gene == "TSC1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_TSC1);
    }
    else if (gene == "TSC2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_TSC2);
    }
    else if (gene == "NF1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_NF1);
    }
    else if (gene == "MAP2K2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MAP2K2);
    }
    else if (gene == "MAPK3")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MAPK3);
    }
    else if (gene == "MAPK1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_MAPK1);
    }
    else if (gene == "AKT1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_AKT1);
    }
    else if (gene == "AKT2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_AKT2);
    }
    else if (gene == "GNAQ")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_GNAQ);
    }
    else if (gene == "GNA11")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_GNA11);
    }
    else if (gene == "RAF1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_RAF1);
    }
    else if (gene == "KEAP1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_KEAP1);
    }
    else if (gene == "NTRK1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_NTRK1);
    }
    else if (gene == "NTRK2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_NTRK2);
    }
    else if (gene == "NTRK3")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Mut_NTRK3);
    }
    else if (gene == "Fusion ALK")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_ALK);
    }
    else if (gene == "Fusion ROS1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_ROS1);
    }
    else if (gene == "Fusion RET")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_RET);
    }
    else if (gene == "Fusion NTRK1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_NTRK1);
    }
    else if (gene == "Fusion NTRK2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_NTRK2);
    }
    else if (gene == "Fusion NTRK3")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_NTRK3);
    }
    else if (gene == "Fusion MET")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_MET);
    }
    else if (gene == "Fusion AXL")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_AXL);
    }
    else if (gene == "Fusion BRAF")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_BRAF);
    }
    else if (gene == "Fusion CCND1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_CCND1);
    }
    else if (gene == "Fusion FGFR1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_FGFR1);
    }
    else if (gene == "Fusion FGFR2")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_FGFR2);
    }
    else if (gene == "Fusion FGFR3")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_FGFR3);
    }
    else if (gene == "Fusion NRG1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_NRG1);
    }
    else if (gene == "Fusion PPARG")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_PPARG);
    }
    else if (gene == "Fusion RAF1")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_RAF1);
    }
    else if (gene == "Fusion THADA")
    {
        ids << PathologyPropertyInfo::info(PathologyPropertyInfo::Fusion_THADA);
    }
    else
    {
        qDebug() << "Error: No property known for gene" << gene;
    }

    foreach (PathologyPropertyInfo id, ids)
    {
        ValueTypeCategoryInfo typeInfo(id);
        Property prop;
        prop.property = id.id;
        // Prepare as a negative value
        prop.value = typeInfo.toPropertyValue(typeInfo.negativeValue());
        props << prop;
    }
    return props;
}

Property& ParserTools::propertyForExon(QList<Property>& properties, const QString& gene, int exon)
{
    // this is the check-free simple solution for the majority of cases
    if (properties.size() == 1)
    {
        return properties.first();
    }
    if (gene == "BRAF")
    {
        if (exon == 11)
        {
            return properties[0];
        }
        else
        {
            return properties[1];
        }
    }
    else if (gene == "EGFR")
    {
        switch (exon)
        {
        case 19:
        case 21:
        default:
            return properties[0];
        case 18:
        case 20:
            return properties[1];
        }
    }
    else if (gene == "KRAS")
    {
        switch (exon)
        {
        case 2:
        default:
            return properties[0];
        case 3:
            return properties[1];
        case 4:
            return properties[2];
        }
    }

    qDebug() << "propertyForExon: unhandled case" << gene << exon << " returning first";
    // in the end we return the first property, also if in doubt
    return properties.first();
}

IHCScore::Intensity ParserTools::textToIntensity(const QString& text)
{
    // (Schwache|Mäßige|Starke|Keine)
    if (text == "Schwache")
    {
        return IHCScore::WeakIntensity;
    }
    if (text == "Mäßige" || text == "Mittelgradige")
    {
        return IHCScore::MediumIntensity;
    }
    if (text == "Starke" || text == "Kräftige")
    {
        return IHCScore::StrongIntensity;
    }
    if (text == "Keine")
    {
        return IHCScore::NoIntensity;
    }
    return IHCScore::InvalidIntensity;
}

PathologyPropertyInfo::Property ParserTools::textToIHCProperty(const QString& protein)
{
    // PTEN|c-MET|p-ERK|ALK|ROS1|p-AKTS473|HER2\/neu|p-p70S6-Kinase
    if (protein == "PTEN")
    {
        return PathologyPropertyInfo::IHC_PTEN;
    }
    if (protein == "c-MET")
    {
        return PathologyPropertyInfo::IHC_cMET;
    }
    if (protein == "p-ERK")
    {
        return PathologyPropertyInfo::IHC_pERK;
    }
    if (protein == "ALK")
    {
        return PathologyPropertyInfo::IHC_ALK;
    }
    if (protein == "ROS1")
    {
        return PathologyPropertyInfo::IHC_ROS1;
    }
    if (protein == "p-AKTS473" || protein == "p-AKT")
    {
        return PathologyPropertyInfo::IHC_pAKT;
    }
    if (protein == "HER2/neu")
    {
        return PathologyPropertyInfo::IHC_HER2;
    }
    if (protein == "p-p70S6-Kinase" || protein == "p-p70s6-Kinase")
    {
        return PathologyPropertyInfo::IHC_pP70S6K;
    }
    if (protein == "c-MET")
    {
        return PathologyPropertyInfo::IHC_cMET;
    }
    if (protein == "PD-L1")
    {
        return PathologyPropertyInfo::IHC_PDL1;
    }
    if (protein == "MLH1")
    {
        return PathologyPropertyInfo::IHC_MLH1;
    }
    if (protein == "MSH2")
    {
        return PathologyPropertyInfo::IHC_MSH2;
    }
    if (protein == "MSH6")
    {
        return PathologyPropertyInfo::IHC_MSH6;
    }
    if (protein == "PMS2")
    {
        return PathologyPropertyInfo::IHC_PMS2;
    }
    if (protein == "panTRK" || protein.contains("NTRK"))
    {
        return PathologyPropertyInfo::IHC_panTRK;
    }
    qDebug() << "Unhandled IHC" << protein << "in scanner";
    return PathologyPropertyInfo::InvalidProperty;
}

QMap<QString, QStringList> ParserTools::ngsPanels()
{
    QMap<QString, QStringList> panels;
    QFile maskFile(":/medical/ngs-panels");
    if (maskFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&maskFile);
        stream.setCodec("UTF-8");
        QString line;
        while ( !stream.atEnd() )
        {
            line = stream.readLine();
            QStringList genes = line.split(' ', Qt::SkipEmptyParts);
            if (genes.isEmpty())
            {
                continue;
            }
            for (QStringList::iterator it = genes.begin(); it != genes.end(); ++it)
            {
                it->replace('_', ' ');
            }
            QString name = genes.takeFirst();
            panels[name] = genes;
        }
    }
    else
    {
        qDebug() << "Failed to open ngs-panels resource file!";
    }
    return panels;
}

PathologyPropertyInfo::Property ParserTools::textToFISHProperty(const QString& protein)
{
    // three different regexps for four FISHes
    if (protein == "HER2")
    {
        return PathologyPropertyInfo::Fish_HER2;
    }
    if (protein == "ALK")
    {
        return PathologyPropertyInfo::Fish_ALK;
    }
    if (protein == "ROS1")
    {
        return PathologyPropertyInfo::Fish_ROS1;
    }
    if (protein == "RET")
    {
        return PathologyPropertyInfo::Fish_RET;
    }
    if (protein == "FGFR1")
    {
        return PathologyPropertyInfo::Fish_FGFR1;
    }
    if (protein == "MET")
    {
        return PathologyPropertyInfo::Fish_cMET;
    }
    qDebug() << "Unhandled FISH" << protein << "in scanner";
    return PathologyPropertyInfo::InvalidProperty;
}

IHCScore::Intensity ParserTools::textToQualitativeIntensity(const QString &rawText)
{
    QString text(rawText);
    text.replace('\n', ' ');
    if (text == "positiv")
    {
        return IHCScore::WeakIntensity;
    }
    if (text == "negativ" || text == "nicht signifikant positiv")
    {
        return IHCScore::NoIntensity;
    }
    qDebug() << "Unhandled qualitative IHC text" << text << "in scanner";
    return IHCScore::NoIntensity;
}

bool ParserTools::isSamePatient(const Patient& a, const Patient& b)
{
    // Ignores gender (may be unknown)
    return a.surname == b.surname &&
            a.firstName == b.firstName &&
            a.dateOfBirth == b.dateOfBirth;
}

Patient::Ptr ParserTools::saveData(const Patient::Ptr& existingPatient,
                                   const Patient& patientData,
                                   const QDate& initialDx,
                                   Pathology::Entity entity,
                                   const QDate& resultsDate,
                                   const PropertyList& pathologyProperties,
                                   const QStringList& reports,
                                   ParserTools::ParserType parser,
                                   PropertyList patientProperties)
{
    bool addedByMachine = false;
    bool confirmedByHuman = false;
    QString pathologySource, patientSource;
    switch (parser)
    {
    case ImportTool:
        addedByMachine   = false;
        confirmedByHuman = true;
        // pathologySource blank
        break;
    case NGSCVSParser:
        addedByMachine   = true;
        confirmedByHuman = false;
        pathologySource  = PathologyPropertyValue::ngsParser();
        patientSource    = PatientPropertyValue::ngsParser();
        break;
    case SimonParser:
        addedByMachine   = false;
        confirmedByHuman = true;
        break;
    }

    Patient::Ptr p = existingPatient;
    if (!p)
    {
        p = PatientManager::instance()->addPatient(patientData);
        if (!p)
        {
            qDebug() << "PatientManager refused to add a patient for name" << patientData.surname << patientData.firstName << "dob" << patientData.dateOfBirth;
            return 0;
        }
        if (addedByMachine)
        {
            p->patientProperties.addProperty(PatientPropertyName::addedByMachine(), patientSource);
        }
    }

    if (confirmedByHuman)
    {
        p->patientProperties.removeProperty(PatientPropertyName::addedByMachine());
    }
    p->patientProperties.add(patientProperties);

    if (!p->hasDisease())
    {
        // add here support for multiple diseases...
        Disease disease;
        disease.initialDiagnosis = initialDx;
        if (!disease.initialDiagnosis.isValid())
        {
            disease.initialDiagnosis = initialDxFromResultDate(resultsDate);
        }
        p->diseases << disease;
    }

    Disease& disease = p->diseases.first();
    if (initialDx.isValid())
    {
        disease.initialDiagnosis = initialDx;
    }

    if (entity != Pathology::UnknownEntity)
    {
        for (int i=0; i<disease.pathologies.size(); ++i)
        {
            disease.pathologies[i].entity = entity;
        }
    }

    Pathology* path = findSuitablePathology(p, resultsDate);
    // reuse pathology within +- 2 months
    if (path)
    {
        adjustSuitablePathology(path, resultsDate);
    }
    else
    {
        Pathology newPath;
        newPath.context = PathologyContextInfo(PathologyContextInfo::Tumorprofil).id;
        newPath.date    = resultsDate;
        newPath.entity  = entity;
        disease.pathologies << newPath;
        path = &disease.pathologies.last();
    }

    // Merge pathology properties with differential decision
    mergeNewProperties(path, pathologyProperties, parser, *p);

    // Add our parser's fingerprint
    if (!pathologySource.isEmpty() && !path->specialProperties.hasProperty(PathologyPropertyName::pathologySource(), pathologySource))
    {
        path->specialProperties.addProperty(PathologyPropertyName::pathologySource(), pathologySource);
    }
    path->reports += reports;

    PatientManager::instance()->updateData(p,
                                           PatientManager::ChangedPathologyData |
                                           PatientManager::ChangedDiseaseMetadata |
                                           PatientManager::ChangedPatientProperties |
                                           PatientManager::ChangedDiseaseProperties);

    return p;
}


Patient::Gender ParserTools::genderForFirstName(const QString &firstNameGiven)
{
    QString firstName(firstNameGiven);
    firstName.replace('-', ' ');
    firstName.replace('.', ". "); // so that it splits
    QStringList names = firstName.split(' ', Qt::SkipEmptyParts);
    for (QStringList::iterator it = names.begin(); it != names.end(); )
    {
        // Remove Dr., Prof. etc.
        if (it->endsWith('.'))
        {
            it = names.erase(it);
            continue;
        }

        // Look in our database
        QList<Patient::Ptr> otherPatients = PatientManager::instance()->findPatients(QString(), *it);
        foreach (const Patient::Ptr& p, otherPatients)
        {
            if (p->gender != Patient::UnknownGender)
            {
                return p->gender;
            }
        }

        ++it;
    }

    QUrl genderizeUrl("https://api.genderize.io/");
    QNetworkAccessManager nam;
    // Use web service
    foreach (const QString& name, names)
    {
        QUrlQuery query;
        query.addQueryItem("name", name);
        //query.addQueryItem("country_id", "de");
        genderizeUrl.setQuery(query);
        QNetworkRequest request(genderizeUrl);
        QNetworkReply* reply = nam.get(request);
        QEventLoop loop;
        while (!reply->isFinished())
        {
            loop.processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        QByteArray data = reply->readAll();
        reply->deleteLater();
        loop.processEvents();
        QJsonDocument doc  = QJsonDocument::fromJson(data);
        QJsonObject object = doc.object();
        QString gender  = object.value("gender").toString();
        if (gender == "male")
        {
            return Patient::Male;
        }
        else if (gender == "female")
        {
            return Patient::Female;
        }
        // "null": skip
    }
    return Patient::UnknownGender;
}

Patient::Ptr ParserTools::existingPatient(const Patient &patientData, bool genderIsConfirmed)
{
    Patient searchData(patientData);
    if (!genderIsConfirmed)
    {
        searchData.gender = Patient::UnknownGender; // may be set by heuristics
    }
    QList<Patient::Ptr> ps = PatientManager::instance()->findPatients(searchData);
    Patient::Ptr existingPatient;
    if (!ps.isEmpty())
    {
        if (ps.size() > 1)
        {
            qDebug() << "Multiple candidates for patient" << patientData.firstName << patientData.surname << patientData.dateOfBirth;
        }
        existingPatient = ps.first();
    }
    return existingPatient;
}

Pathology *ParserTools::findSuitablePathology(const Patient::Ptr &p, const QDate &resultsDate)
{
    // add here support for multiple diseases...
    if (!p  || !p->hasDisease())
    {
        return 0;
    }
    Disease& disease = p->diseases.first();
    int pathIndex = findSuitablePathology(disease.pathologies, resultsDate);
    if (pathIndex == -1)
    {
        return 0;
    }
    return &disease.pathologies[pathIndex];
}

int ParserTools::findSuitablePathology(const QList<Pathology>& pathologies, const QDate &reportDate)
{
    int dummyPathologyIndex = -1;
    for (int i = 0; i<pathologies.size(); i++)
    {
        const Pathology& path = pathologies[i];
        //qDebug() << "Checking" << path.context << path.date << qAbs(path.date.daysTo(reportDate));
        if (qAbs(path.date.daysTo(reportDate)) < 60
                && path.context == PathologyContextInfo(PathologyContextInfo::Tumorprofil).id)
        {
            return i;
        }
        if (path.date.isNull() && path.context.isEmpty())
        {
            dummyPathologyIndex = i;
        }
    }
    return dummyPathologyIndex;
}

void ParserTools::adjustSuitablePathology(Pathology *path, const QDate &reportDate)
{
    // as the method's name tells us, the given pathology is suitable as per findSuitablePathology

    if (reportDate.isValid())
    {
        if (!path->date.isValid())
        {
            path->date = reportDate;
        }
        else
        {
            path->date = qMin(path->date, reportDate);
        }
    }

    if (path->context.isEmpty())
    {
        path->context = PathologyContextInfo(PathologyContextInfo::Tumorprofil).id;
    }
}

void ParserTools::mergeNewProperties(Pathology *path, const PropertyList &newProps, ParserType parser, const Patient &patientData)
{
    bool wasEnteredByExactParser = path->specialProperties.hasProperty(PathologyPropertyName::pathologySource(), PathologyPropertyValue::ngsParser());
    foreach (const Property& prop, newProps)
    {
        PropertyList oldProps = path->properties.properties(prop.property);
        if (oldProps.isEmpty())
        {
            path->properties.addProperty(prop);
        }
        else
        {
            if (oldProps.hasProperty(prop.property, prop.value, prop.detail))
            {
                // already has identical entry
                continue;
            }
            else
            {
                qDebug() << "Have conflicting data for patient" << patientData.surname << patientData.firstName;
                // NGS CVS parser is considered very exact, takes precedence
                if (parser == ImportTool)
                {
                    if (wasEnteredByExactParser)
                    {
                        qDebug() << "Was entered by exact machine, Keeping previous data";
                        continue;
                    }
                    else
                    {
                        qDebug() << "Replacing" << prop.property << oldProps.first().value << "with" << prop.value;
                        path->properties.setProperty(prop);
                    }
                }
                else if (parser == NGSCVSParser)
                {
                    if (wasEnteredByExactParser && prop.value != oldProps.first().value)
                    {
                        qDebug() << "Appending" << prop.property << prop.value;
                        path->properties.addProperty(prop);
                    }
                    else
                    {
                        qDebug() << "Replacing" << prop.property << oldProps.first().value << "with" << prop.value;
                        path->properties.setProperty(prop);
                    }
                }
                else // add more cases
                {
                    qDebug() << "Unspecified case for parser" << parser << ", keeping previous data";
                    continue;
                }
            }
        }
    }
}


QDate ParserTools::initialDxFromResultDate(const QDate &resultsDate)
{
    QDate referenceDate = resultsDate;
    if (!referenceDate.isValid())
    {
        qWarning() << "Warning: No initialDx, no result date, using current date.";
        referenceDate = QDate::currentDate();
    }
    QDate date;
    date.setDate(referenceDate.year(), referenceDate.month(), 1);
    if (date.daysTo(referenceDate) < 15)
    {
        date = date.addMonths(-1);
    }
    return date;
}


QDate ParserTools::dateFromInsideString(const QString &s)
{
    QRegularExpression re("(?<day>\\d{2})\\.(?<month>\\d{2})\\.(?<year>\\d{4})");
    QRegularExpressionMatch match = re.match(s);
    if (match.hasMatch())
    {
        QDate fileDate = QDate(match.captured("year").toInt(), match.captured("month").toInt(), match.captured("day").toInt());
        if (fileDate.isValid())
        {
            return fileDate;
        }
    }
    return QDate();
}

QDate ParserTools::dateFromFilePath(const QString &path)
{
    QFileInfo info(path);
    QDate date = dateFromInsideString(info.fileName());
    if (date.isNull())
    {
        date = dateFromInsideString(path);
        if (date.isNull())
        {
            date = dateFromInsideString(info.canonicalFilePath());
        }
    }
    return date;
}

QList<Property> ParserTools::mergeGeneProperties(const QList<Property> &currentProperties, const QList<Property> &propertiesToAdd)
{
    //TODO: merge duplicate properties (two mutations in one gene) into one property

    PropertyList properties(currentProperties);
    foreach (const Property& prop, propertiesToAdd)
    {
        PathologyPropertyInfo info = PathologyPropertyInfo::info(prop.property);
        ValueTypeCategoryInfo valueType(info);
        if (valueType.toValue(prop.value).toBool())
        {
            if (properties.hasProperty(prop.property))
            {
                Property previous = properties.property(prop.property);
                qDebug() << "Warning: Overwriting previous entry" << previous.property << previous.detail << "with new property" << prop.property << prop.detail;
            }
            // New property adds a mutation. Overwrite possible previous entries, in particular, wild types.
            properties.setProperty(prop);
        }
        else
        {
            // New property adds a wildtype. Only add if there is not yet an entry (in particular, if there is not yet a mutation)
            if (!properties.hasProperty(prop.property))
            {
                properties << prop;
            }
            else
            {
                Property previous = properties.property(prop.property);
                qDebug() << "Warning: Dropping WT entry, currently having positive finding" << previous.property << previous.detail;
            }
        }
    }
    return properties;
}
