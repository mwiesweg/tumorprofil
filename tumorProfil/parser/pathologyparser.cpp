/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 28.06.2015
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "pathologyparser.h"

#include <boost/icl/interval_set.hpp>

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QRegularExpression>

#include "databaseconstants.h"
#include "parsertools.h"
#include "patientmanager.h"

PatientParseResults::PatientParseResults()
    : nngm(false)
{
}

PatientParseResults::PatientParseResults(const Patient& data)
    : patientData(data),
      nngm(false)
{
}

PatientParseResults::PatientParseResults(Patient::Ptr p)
    : patientData(*p),
      patient(p),
      nngm(false)
{
}

bool PatientParseResults::operator==(const Patient& other) const
{
    if (patient)
    {
        return ParserTools::isSamePatient(*patient, other);
    }
    return ParserTools::isSamePatient(patientData, other);
}


class RegExpContainer
{
public:

    enum RegExpVariant
    {
        InvalidRegExp,
        RegExpPatientIdPDF,
        RegExpIHC,
        RegExpALKROS1panTRK,
        RegExpPDL1,
        RegExpNGSTableHeader,
        RegExpNGSTableRow,
        RegExpArcherTableHeader,
        RegExpArcherTableRow,
        RegExpFISH,
        RegExpNNGM,
        RegExpIgnore,
        RegExpPageBreakJunk
    };

    QList<QRegularExpression>& expressions(RegExpVariant var)
    { return regexps[var]; }

    void load();

private:
    QMap<RegExpVariant, QList<QRegularExpression> > regexps;
};

void RegExpContainer::load()
{
    QMap<QString, RegExpVariant> keywords;
    keywords.insert("patient-id-pdf", RegExpPatientIdPDF);
    keywords.insert("ihc-global", RegExpIHC);
    keywords.insert("alk-ros1-pantrk", RegExpALKROS1panTRK);
    keywords.insert("pd-l1", RegExpPDL1);
    keywords.insert("ngs-table-header", RegExpNGSTableHeader);
    keywords.insert("ngs-table-row", RegExpNGSTableRow);
    keywords.insert("archer-table-header", RegExpArcherTableHeader);
    keywords.insert("archer-table-row", RegExpArcherTableRow);
    keywords.insert("fish", RegExpFISH);
    keywords.insert("nngm", RegExpNNGM);
    keywords.insert("ignore", RegExpIgnore);
    keywords.insert("page-break-junk", RegExpPageBreakJunk);

    // allow override with updated regexps in the current dir
    QFile file(QDir::currentPath() + "/pathology-regexps");
    if (!file.exists())
    {
        file.setFileName(":/regexps/pathology-regexps");
    }
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Failed to open regexp file" << file.fileName() <<" - Parsing will not work!";
    }
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    QString line;
    RegExpVariant currentVariant = InvalidRegExp;
    while ( !(line = stream.readLine()).isNull() )
    {
        if (keywords.contains(line))
        {
            currentVariant = keywords.value(line);
        }
        else if (currentVariant != InvalidRegExp)
        {
            bool dotMatchesEverything = false;
            if (line.startsWith("__option-s__"))
            {
                dotMatchesEverything = true;
                line = line.mid(12);
            }
            QRegularExpression re(line, QRegularExpression::MultilineOption | QRegularExpression::UseUnicodePropertiesOption);
            if (!re.isValid())
            {
                qDebug() << "Regexp" << line << "is invalid" << re.errorString() << re.patternErrorOffset() << line.mid(re.patternErrorOffset());
                continue;
            }
            if (dotMatchesEverything)
            {
                re.setPatternOptions(re.patternOptions() | QRegularExpression::DotMatchesEverythingOption);
            }
            regexps[currentVariant] << re;
        }
        else
        {
            qDebug() << "Regexp loading: failed to parse line" << line;
        }
    }
}

class PathologyParser::PathologyParserPriv
{
public:
    PathologyParserPriv()
    {
    }

    PatientParseResults& findResults(const Patient& patientData);
    void appendPatientText(PatientParseResults* results, const QString& chunk);    
    QList<Property> parseNGSTable(PatientParseResults& results, int begin, int end, boost::icl::interval_set<int>& excerpts);
    QList<Property> parseRNANGSTable(PatientParseResults& results, int begin, int end, boost::icl::interval_set<int>& excerpts);

    RegExpContainer regExpContainer;
    QList<PatientParseResults> results;
};

PathologyParser::PathologyParser()
    : d(new PathologyParserPriv)
{
    d->regExpContainer.load();
}

PathologyParser::~PathologyParser()
{
    delete d;
}

QList<PatientParseResults> PathologyParser::results() const
{
    return d->results;
}

void PathologyParser::clear()
{
    d->results.clear();
}

QList<PatientParseResults> PathologyParser::parseFile(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Failed to open file" << fileName;
    }
    QTextStream stream(&file);
    stream.setCodec("ISO 8859-1");
    splitPerPatient(stream);
    parsePerPatient();

    return d->results;
}

QList<PatientParseResults> PathologyParser::parse(const QString& s)
{
    QString text(s);
    QTextStream stream(&text, QIODevice::ReadOnly);
    splitPerPatient(stream);
    parsePerPatient();

    return d->results;
}

void PathologyParser::splitPerPatient(QTextStream& stream)
{
    // First tour of parsing: Split into per-patient chunks.
    // We assume that the RegExps is exact (^...$)
    QString line, nextLine, patientText;
    PatientParseResults* currentResults = 0;
    while ( !(line = stream.readLine()).isNull() )
    {
        QRegularExpressionMatch match;
        foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpPatientIdPDF))
        {
            match = re.match(line, 0, QRegularExpression::PartialPreferFirstMatch);
            // patient id PDF is multi-line
            while (match.hasPartialMatch() && !(nextLine = stream.readLine()).isNull() )
            {
                line += "\n" + nextLine;
                match = re.match(line, 0, QRegularExpression::PartialPreferCompleteMatch);
            }
            if (match.hasMatch())
            {
                break;
            }
        }

        // the patient id regexps matched
        if (match.hasMatch())
        {
            d->appendPatientText(currentResults, patientText);
            patientText.clear();

            Patient patientData;
            patientData.surname = match.captured("name");
            patientData.firstName = match.captured("firstName");
            patientData.dateOfBirth = QDate(match.captured("year").toInt(), match.captured("month").toInt(), match.captured("day").toInt());
            currentResults = &d->findResults(patientData);
        }

        patientText += line + "\n";
    }
    d->appendPatientText(currentResults, patientText);
}

void PathologyParser::parsePerPatient()
{
    // The text is now broken into per-patient chunks, stored in results.
    // for each patient, parse their results, Properties and metadata are stored in results.
    for (int i=0; i<d->results.size(); ++i)
    {
        parseText(d->results[i]);
    }
}

void PathologyParser::parseText(PatientParseResults& results)
{
    boost::icl::interval_set<int> excerpts;
    results.resultsDate = QDate();
    results.referenceNumbers.clear();
    results.properties.clear();
    results.unrecognizedText.clear();

    // There is junk at the page break of the PDFs. This may be placed just inside any content, confusing the analysis.
    // So remove it here once and forever (regexp will be very specific)
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpPageBreakJunk))
    {
        results.text.remove(re);
    }

    // we perform global matches with the (very specific) regular expressions

    // Look for id strings, fill finding date (earliest) and R/E numbers
    foreach (const QRegularExpression& re,
             d->regExpContainer.expressions(RegExpContainer::RegExpPatientIdPDF))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            QDate date(match.captured("refyear").toInt(), match.captured("refmonth").toInt(), match.captured("refday").toInt());
            if (date.isValid() && (results.resultsDate.isNull() || date < results.resultsDate))
            {
                results.resultsDate = date;
            }
            QString number = match.captured("refNumber");
            if (!number.isEmpty() && !results.referenceNumbers.contains(number))
            {
                results.referenceNumbers << number;
            }
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpNNGM))
    {
        QRegularExpressionMatch match = re.match(results.text);
        if (match.hasMatch())
        {

            results.nngm = true;
            break;
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpIHC))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            // Some regexps may match the same line. Skip if this excerpt was already parsed.
            if (boost::icl::contains(excerpts, boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd())))
            {
                continue;
            }
            IHCScore::Intensity intensity = ParserTools::textToQualitativeIntensity(match.captured("quality"));
            PathologyPropertyInfo::Property id = ParserTools::textToIHCProperty(match.captured("protein"));
            PathologyPropertyInfo info = PathologyPropertyInfo::info(id);
            ValueTypeCategoryInfo typeInfo(info);

            Property prop;
            prop.property = info.id;
            // we usually do not get information on exact percentage any more
            typeInfo.fillIHCScore(prop, intensity, QString());

            results.properties << prop;

            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpALKROS1panTRK))
    {
        QRegularExpressionMatch match = re.match(results.text);
        if (match.hasMatch())
        {
            QList<PathologyPropertyInfo::Property> ids = QList<PathologyPropertyInfo::Property>() << PathologyPropertyInfo::IHC_ALK
                                                                                                  << PathologyPropertyInfo::IHC_ROS1
                                                                                                  << PathologyPropertyInfo::IHC_panTRK;
            foreach (PathologyPropertyInfo::Property id, ids)
            {
                PathologyPropertyInfo info = PathologyPropertyInfo::info(id);
                ValueTypeCategoryInfo typeInfo(info);
                Property prop;
                prop.property = info.id;
                if (!results.properties.hasProperty(prop.property))
                {
                    typeInfo.fillIHCScore(prop, IHCScore::NoIntensity, QString());
                    results.properties << prop;
                    excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
                }
            }
        }
    }

    // PD-L1 may be given at multiple places. Several RegExps in order of priority.
    bool hasPDL1TPS = false;
    bool hasPDL1IC = false;
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpPDL1))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            // Some regexps may match the same line. Skip if this excerpt was already parsed.
            if (boost::icl::contains(excerpts, boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd())))
            {
                continue;
            }

            if (!hasPDL1TPS)
            {
                PathologyPropertyInfo pdl1Info = PathologyPropertyInfo::info(PathologyPropertyInfo::IHC_PDL1);
                ValueTypeCategoryInfo pdl1TypeInfo(pdl1Info);

                // PD-L1 TPS - for historical reasons, stored as H-Score
                QString tpsStr = match.captured("tps").remove(' ');
                int tps;
                if (tpsStr == "<1")
                {
                    tps = 0;
                }
                else
                {
                    tps = tpsStr.toInt();
                }
                Property prop;
                prop.property = pdl1Info.id;
                pdl1TypeInfo.fillHSCore(prop, HScore(0, 0, tps));
                results.properties << prop;

                hasPDL1TPS = true;
            }

            if (!hasPDL1IC)
            {
                // PD-L1 IC - for historical reasons, stored as IHCBoolean
                QString icStr = match.captured("ic");
                if (!icStr.isNull())
                {
                    PathologyPropertyInfo ic_info = PathologyPropertyInfo::info(PathologyPropertyInfo::IHC_PDL1_immunecell);
                    ValueTypeCategoryInfo c1TypeInfo(ic_info);
                    Property prop;
                    prop.property = ic_info.id;
                    prop.value = c1TypeInfo.toPropertyValue(icStr != "0");
                    prop.detail = "IC" + icStr;
                    results.properties << prop;

                    hasPDL1IC = true;
                }
            }

            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpFISH))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            PathologyPropertyInfo id = ParserTools::textToFISHProperty(match.captured("protein"));
            ValueTypeCategoryInfo typeInfo(id);
            Property prop;
            prop.property = id.id;
            QString positivityClause = match.captured("positivity");
            bool isPositive = (positivityClause == "positiv" || positivityClause == "high-level");
            prop.value      = typeInfo.toPropertyValue(isPositive);
            if (match.captured("protein") == "MET")
            {
                // "Ratio: , Copy Number: , Nuclei: , Signals: , Signals CEN: , Result: "
                prop.detail =
                        "Ratio: " + match.captured("ratio").replace(',', '.') +
                        ", Copy Number: " + match.captured("copynumber").replace(',', '.') +
                        ", Nuclei: " + match.captured("nuclei") +
                        ", Signals: " + match.captured("signals").remove('.')  +
                        ", Signals CEN: " + match.captured("censignals") +
                        ", Result:" + (positivityClause == "keine" ? "negative" : positivityClause);
            }
            else
            {
                prop.detail = match.captured("ratio").replace(',', '.');
            }
            results.properties << prop;

            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpIgnore))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    // this comes after everything else as it honors which parts have already been processed
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpNGSTableHeader))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();

            bool hasVariants = match.captured("hasvariants") != "Es lassen sich keine pathogenen Varianten nachweisen";

            if (hasVariants)
            {
                // We have no specific reg exp for the table data or its end.
                // So find the beginning of the next excerpt (next part already read) and read everything till there
                const int begin = match.capturedEnd();
                int end = results.text.size();
                for (boost::icl::interval_set<int>::const_iterator eit = excerpts.begin(); eit != excerpts.end(); ++eit)
                {
                    if (eit->lower() > begin && eit->lower() < end)
                    {
                        end = eit->lower();
                    }
                }
                QList<Property> mutationProperties = d->parseNGSTable(results, begin, end, excerpts);
                results.properties = ParserTools::mergeGeneProperties(results.properties, mutationProperties);
            }
            // add WT findings
            foreach (const QString& gene, ParserTools::ngsPanels().value("NNGML2.1"))
            {
                QList<Property> geneProps = ParserTools::propertiesForGene(gene);
                // will only add if missing so far
                results.properties = ParserTools::mergeGeneProperties(results.properties, geneProps);
            }
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }
    foreach (const QRegularExpression& re, d->regExpContainer.expressions(RegExpContainer::RegExpArcherTableHeader))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(results.text);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();

            bool hasVariants = match.captured("hasvariants") != "Es lassen sich keine pathogenen Varianten nachweisen";

            if (hasVariants)
            {
                // We have no specific reg exp for the table data or its end.
                // So find the beginning of the next excerpt (next part already read) and read everything till there
                const int begin = match.capturedEnd();
                int end = results.text.size();
                for (boost::icl::interval_set<int>::const_iterator eit = excerpts.begin(); eit != excerpts.end(); ++eit)
                {
                    if (eit->lower() > begin && eit->lower() < end)
                    {
                        end = eit->lower();
                    }
                }
                QList<Property> mutationProperties = d->parseRNANGSTable(results, begin, end, excerpts);
                results.properties = ParserTools::mergeGeneProperties(results.properties, mutationProperties);
            }
            // add WT findings
            foreach (const QString& gene, ParserTools::ngsPanels().value("ARCHER"))
            {
                QList<Property> geneProps = ParserTools::propertiesForGene(gene);
                // will only add if missing so far
                results.properties = ParserTools::mergeGeneProperties(results.properties, geneProps);
            }
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart(), match.capturedEnd());
        }
    }

    /*qDebug() << results.patientData.firstName << results.patientData.surname << results.resultsDate << results.referenceNumbers;
    foreach (const Property& prop, results.properties)
    {
        qDebug() << prop.property << prop.value << prop.detail;
    }*/

    boost::icl::interval_set<int> unread;
    unread += boost::icl::interval<int>::right_open(0, results.text.size());
    unread -= excerpts;
    QStringList unrecognizedParts;
    for (boost::icl::interval_set<int>::const_iterator eit = unread.begin(); eit != unread.end(); ++eit)
    {
        unrecognizedParts << results.text.mid(eit->lower(), eit->upper() - eit->lower()).trimmed();
    }
    results.unrecognizedText = unrecognizedParts.join('\n').replace(QRegularExpression("\\n{3,}"), "\n\n");

    results.guessedEntity = Pathology::UnknownEntity;

    if (results.text.contains("Adenokarzinom") || results.text.contains("Adenocarcinom"))
    {
        results.guessedEntity = Pathology::PulmonaryAdeno;
    }
    else if (results.text.contains("Plattenepithelkarzinom")|| results.text.contains("Plattenepithelcarcinom"))
    {
        results.guessedEntity = Pathology::PulmonarySquamous;
    }
    else if (results.text.contains("Kolorektales Karzinom"))
    {
        results.guessedEntity = Pathology::ColorectalAdeno;
    }
    /*
    QMap<PathologyPropertyInfo::Property, Pathology::Entity> diseaseDefiningProperties;
    diseaseDefiningProperties.insert(PathologyPropertyInfo::IHC_ALK, Pathology::PulmonaryAdeno);
    diseaseDefiningProperties.insert(PathologyPropertyInfo::IHC_ROS1, Pathology::PulmonaryAdeno);
    diseaseDefiningProperties.insert(PathologyPropertyInfo::IHC_pP70S6K, Pathology::ColorectalAdeno);
    diseaseDefiningProperties.insert(PathologyPropertyInfo::Fish_FGFR1, Pathology::PulmonarySquamous);
    diseaseDefiningProperties.insert(PathologyPropertyInfo::IHC_MSH2, Pathology::ColorectalAdeno);
    foreach (const Property& prop, results.properties)
    {
        PathologyPropertyInfo info = PathologyPropertyInfo::info(prop.property);
        if (diseaseDefiningProperties.contains(info.property))
        {
            results.guessedEntity = diseaseDefiningProperties.value(info.property);
            break;
        }
    }
    */

    // work around MariaDB charset weirdness
    results.text.replace("≥", "");
    for (QStringList::iterator it = results.textPassages.begin(); it != results.textPassages.end(); ++it)
    {
        it->replace("≥", "");
    }
}

QList<Property> PathologyParser::PathologyParserPriv::parseNGSTable(PatientParseResults& results, int begin, int end, boost::icl::interval_set<int>& excerpts)
{
    ValueTypeCategoryInfo typeInfo(PathologyPropertyInfo::Mutation);
    QString propValue = typeInfo.toPropertyValue(true);
    QString restText = results.text.mid(begin, end-begin);

    QList<Property> mutationProperties;
    foreach (const QRegularExpression& re, regExpContainer.expressions(RegExpContainer::RegExpNGSTableRow))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(restText);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            //qDebug() << "Matched" << re.pattern() << "at" << match.capturedStart() << match.capturedEnd();

            QString gene = match.captured("gene");
            int exonNumber = match.captured("exon").toInt();
            QList<Property> props = ParserTools::propertiesForGene(gene);
            if (props.isEmpty())
            {
                qDebug() << "Error: Did not get property for" << gene;
            }
            else
            {
                Property& prop = ParserTools::propertyForExon(props, gene, exonNumber);
                //qDebug() << "Property" << prop.property << "for exon" << exonNumber;
                prop.value = propValue; // yes, is mutated
                prop.detail += match.captured("c") + '\t'
                        + match.captured("p");
                if (!match.captured("allelefrequency").isEmpty())
                {
                    prop.detail += '\t' + match.captured("allelefrequency");
                }
                if (!match.captured("exon").isEmpty())
                {
                    prop.detail += "\tExon " + match.captured("exon");
                }

                // consolidate whitespace and \n's
                prop.detail.replace('\n', " ");
                prop.detail.replace(QRegularExpression("\\s{2,}"), " ");
                prop.detail.replace(" %", "%");
                prop.detail = prop.detail.trimmed();

                mutationProperties << prop;
            }

            // Note: we prevent matching the same text once again with a different regexp
            restText.remove(match.capturedStart(), match.capturedLength());

            // working on a subtext with offset begin
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart() + begin, match.capturedEnd() + begin);
        }
    }
    if (mutationProperties.isEmpty())
    {
        qDebug() << "Expected mutations but did not find any:" << results.text.mid(begin, end-begin);
    }
    return mutationProperties;
}

QList<Property> PathologyParser::PathologyParserPriv::parseRNANGSTable(PatientParseResults& results, int begin, int end, boost::icl::interval_set<int>& excerpts)
{
    ValueTypeCategoryInfo typeInfo(PathologyPropertyInfo::Mutation);
    QString propValue = typeInfo.toPropertyValue(true);
    QString restText = results.text.mid(begin, end-begin);

    QList<Property> mutationProperties;
    foreach (const QRegularExpression& re, regExpContainer.expressions(RegExpContainer::RegExpArcherTableRow))
    {
        QRegularExpressionMatchIterator it = re.globalMatch(restText);
        while (it.hasNext())
        {
            QRegularExpressionMatch match = it.next();
            //qDebug() << "Matched" << re.pattern() << "at" << match.capturedStart() << match.capturedEnd();

            QString gene1 = match.captured("gene1");
            //int exonNumber1 = match.captured("exon1").toInt();
            QString gene2 = match.captured("gene2");
            //int exonNumber2 = match.captured("exon2").toInt();
            QList<Property> props = ParserTools::propertiesForGene("Fusion " + gene1) +
                    ParserTools::propertiesForGene("Fusion " + gene2);
            if (props.isEmpty())
            {
                qDebug() << "Error: Did not get property for" << gene1 << gene2;
            }
            else
            {
                if (props.size() > 2)
                {
                    qDebug() << "Got two properties for fusion, assuming only one partner to be relevant, taking first" << match.captured();
                }
                Property& prop = props.first();
                //qDebug() << "Property" << prop.property << "for exon" << exonNumber;
                prop.value = propValue; // yes, is mutated
                prop.detail += match.captured();

                // consolidate whitespace and \n's
                prop.detail.replace('\n', " ");
                prop.detail.replace(QRegularExpression("\\s{2,}"), " ");
                prop.detail.replace(" %", "%");
                prop.detail = prop.detail.trimmed();

                mutationProperties << prop;
            }

            // Note: we prevent matching the same text once again with a different regexp
            restText.remove(match.capturedStart(), match.capturedLength());

            // working on a subtext with offset begin
            excerpts += boost::icl::interval<int>::right_open(match.capturedStart() + begin, match.capturedEnd() + begin);
        }
    }
    if (mutationProperties.isEmpty())
    {
        qDebug() << "Expected fusions but did not find any:" << results.text.mid(begin, end-begin);
    }
    return mutationProperties;
}

PatientParseResults& PathologyParser::PathologyParserPriv::findResults(const Patient& patientData)
{
    QList<PatientParseResults>::iterator it = std::find(results.begin(), results.end(), patientData);
    if (it != results.end())
    {
        return *it;
    }
    PatientParseResults r;
    QList<Patient::Ptr> ps= PatientManager::instance()->findPatients(patientData);
    if (!ps.isEmpty())
    {
        if (ps.size() > 1)
        {
            qDebug() << "Multiple candidates for patient" << patientData.firstName << patientData.surname << patientData.dateOfBirth;
        }
        results << PatientParseResults(ps.first());
        qDebug() << "Existing patient for" << patientData.firstName << patientData.surname << patientData.dateOfBirth << results.last().resultsDate;
    }
    else
    {
        // use heuristics
        Patient data(patientData);
        data.gender = ParserTools::genderForFirstName(data.firstName);
        results << PatientParseResults(data);
    }
    return results.last();
}

void PathologyParser::PathologyParserPriv::appendPatientText(PatientParseResults* results, const QString& chunk)
{
    if (results && !results->text.contains(chunk))
    {
        // remove some invalid Unicode characters that occur
        QString cleanChunk = QString(chunk).remove("\uF0A7");

        results->text += cleanChunk;
        results->textPassages += cleanChunk;
    }
}

