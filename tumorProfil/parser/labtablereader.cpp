/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 18.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labtablereader.h"

#include "databaseaccess.h"
#include "labresults.h"
#include "patientdb.h"
#include "patientmanager.h"

LabTableReader::LabTableReader(const QStringList& options, bool dryRun)
    : m_options(options),
      m_dryRun(dryRun)
{
    m_aliasMap = LabResults::medicoAliases();
}

void LabTableReader::read()
{
    // [db.table Nachname-Feld Vorname-Feld DOB-Feld Datum-Feld Key-Feld Value-Feld]
    if (m_options.size() != 7)
    {
        qDebug() << "Must provide seven arguments: [db.table Nachname-Feld Vorname-Feld DOB-Feld Datum-Feld Key-Feld Value-Feld].";
        qDebug() << "Got only" << m_options.size() << ":" << m_options;
        return;
    }

    QString table = m_options[0];
    QStringList idColumns, valueColumns;
    // surname, name, dob
    idColumns << m_options[1] << m_options[2] << m_options[3];
    // date, key, value
    valueColumns << m_options[4] << m_options[5] << m_options[6];

    QList<QList<QVariant> > ids = DatabaseAccess().db()->idsFromForeignTable(table, idColumns);

    foreach (const QList<QVariant>& id, ids)
    {
        Patient::Ptr p;
        QList<Patient::Ptr> ps = PatientManager::instance()->findPatients(
                    id[0].toString(), id[1].toString(), QDate::fromString(id[2].toString(), Qt::ISODate), Patient::UnknownGender);
        if (!ps.isEmpty())
        {
            if (ps.size() > 1)
            {
                qDebug() << "Detected" << ps.size() << "patients for" << id << ", taking first.";
            }
            p = ps[0];
        }
        else
        {
            qDebug() << "Did not identify patient for" << id;
            continue;
        }

        QList<QList<QVariant> > values = DatabaseAccess().db()->valuesForIdFromForeignTable(table, idColumns, id, valueColumns);
        if (values.isEmpty())
        {
            continue;
        }

        LabResults results = p->labResults();
        foreach (const QList<QVariant>& valueSet, values)
        {
            QDate date = QDate::fromString(valueSet[0].toString(), Qt::ISODate);
            QString name = m_aliasMap.value(valueSet[1].toString());
            if (name.isNull())
            {
                qDebug() << "Unknown Medico lab key:" << valueSet[1];
                continue;
            }
            bool ok;
            double value = valueSet[2].toDouble(&ok);
            if (!ok)
            {
                qDebug() << "Failed to parse numeric value:" << value;
            }
            results.edit(date.startOfDay(), LabSeries::clinicalType(), name, value);
        }
        qDebug() << "Edited lab for" << p->firstName << p->surname;
        //qDebug() << results;

        if (!m_dryRun)
        {
            PatientManager::instance()->setLabResults(p, results);
        }
    }
}

void LabTableReader::setDryRun(bool dryRun)
{
    m_dryRun = dryRun;
}

//float Leukozyten Erythrozyten Hämoglobin Hämatokrit MCV MCH MCHC MPV Neutrophile Eosinophile Basophile Lymphozyten Monozyten Kalium Calcium Phosphat Kreatinin Harnstoff-N Harnsäure Bilirubin Ges.-Eiweiß Albumin CRP GFR_(MDRD) Harnstoff
//int Thrombozyten Natrium Chlorid GOT GPT AP GGT LDH Glukose_(Serum) ECOG
