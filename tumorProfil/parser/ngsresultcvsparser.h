/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 17.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef NGSRESULTCVSPARSER_H
#define NGSRESULTCVSPARSER_H

#include "csvfile.h"
#include "patient.h"
#include "property.h"

class NGSResultCVSParser
{
public:

    NGSResultCVSParser();

    void setDryRun(bool dryRun);
    int count() const;
    void setPanel(const QString& panel);

    void parse(CSVFile& file);
    void parse(const QString& fileName);

protected:

    class ResultSet;
    void storeResults(const ResultSet& resultSet);
    int  m_count;
    bool m_dryRun;
    QDate m_lastDate;
    QString m_presetPanel;
    QMap<QString, QStringList> m_ngsPanels;
};

#endif // NGSRESULTCVSPARSER_H
