/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 04.04.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "csvfile.h"


#include <QDate>
#include <QDebug>

CSVFile::CSVFile(const QChar& delimiter, const QChar &decimalPoint)
    : m_delimiter(delimiter),
      m_decimalPoint(decimalPoint),
      m_parseMode(ParseDateAndNumbers)
{
}

void CSVFile::setParseMode(CSVFile::ParseMode mode)
{
    m_parseMode = mode;
}

void CSVFile::setDateFormat(const QString &format)
{
    m_dateFormat = format;
}

bool CSVFile::openForReading(const QString& filePath)
{
    m_file.setFileName(filePath);
    if (!m_file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << m_file.errorString();
        return false;
    }
    m_stream.setDevice(&m_file);
    return true;
}

bool CSVFile::openForWriting(const QString& filePath)
{
    m_file.setFileName(filePath);
    if (!m_file.open(QIODevice::WriteOnly))
    {
        qDebug() << m_file.errorString();
        return false;
    }
    m_stream.setDevice(&m_file);
    return true;
}

QString CSVFile::errorString() const
{
    return m_file.errorString();
}

QString CSVFile::filePath() const
{
    return m_file.fileName();
}

QString CSVFile::currentLine() const
{
    return m_currentLine;
}

void CSVFile::finishWriting()
{
    m_stream.flush();
    m_file.close();
    m_stream.setDevice(0);
}

void CSVFile::writeToString(QString *string)
{
    m_stream.setString(string);
}

bool CSVFile::atEnd() const
{
    return m_stream.atEnd();
}

CSVFile& CSVFile::operator<<(const QVariant& record)
{
    m_buffer << record;
    return *this;
}

void CSVFile::newLine()
{
    writeNextLine(m_buffer);
    m_buffer.clear();
}

static QString prepareWithinQuotes(QString& s)
{
    s.replace('"', "\"\""); // quotes to preserve in value -> double quotes
    if (s.contains(':') && !(s.startsWith('"') && s.endsWith('"')) )
    {
        s = '"' + s + '"';
        s.replace('"', "\"\""); // second round
    }
    return s;
}

QString CSVFile::variantToString(const QVariant& v) const
{
    switch (v.type())
    {
    case QVariant::Date:
        if (m_dateFormat.isNull())
        {
            return v.toDate().toString(Qt::ISODate);
        }
        else
        {
            return v.toDate().toString(m_dateFormat);
        }
    case QVariant::Bool:
        return QString::number(v.toInt());
    case QVariant::Double:
    case QMetaType::Float:
    {
        QString num;
        num.setNum(v.toDouble(), 'f', 6);
        num.replace('.', m_decimalPoint);
        return num;
    }
    case QVariant::Map:
    {
        QVariantMap map = v.toMap();
        QStringList mappings;
        for (QVariantMap::const_iterator it = map.begin(); it != map.end(); ++it)
        {
            QString mapValue = variantToString(it.value());
            mapValue = prepareWithinQuotes(mapValue);
            mappings += it.key() + ":" + mapValue;
        }
        return '"' + mappings.join(m_delimiter) + '"';
    }
    case QVariant::StringList:
    {
        QStringList results;
        foreach (const QString& s, v.toStringList())
        {
            QString prepared(s);
            prepared = prepareWithinQuotes(prepared);
            results += prepared;
        }
        return '"' + results.join(m_delimiter) + '"';
    }
    case QVariant::List:
    {
        QStringList results;
        foreach (const QVariant& var, v.toList())
        {
            QString prepared = variantToString(var);
            prepared = prepareWithinQuotes(prepared);
            results += prepared;
        }
        return '"' + results.join(m_delimiter) + '"';
    }
    case QVariant::String:
    default:
    {
        QString s = v.toString();
        if (s.contains(m_delimiter) || s.contains('\n'))
        {
            return '"' + s + '"';
        }
        else
        {
            return s;
        }
    }
    }
}

void CSVFile::writeNextLine(const QList<QVariant>& records)
{
    //qDebug() << "Writing line with" << records.size();
    if (records.isEmpty())
    {
        return;
    }

    bool first = true;
    foreach (const QVariant& v, records)
    {
        if (!first)
            m_stream << m_delimiter;
        first = false;
        m_stream << variantToString(v);
    }

    m_stream << '\n';
}

QList<QVariant> CSVFile::parseNextLine()
{
    m_currentLine = m_stream.readLine();
    QList<QVariant> records;

    if (m_currentLine.isEmpty())
    {
        return records;
    }

    // code from http://www.zedwood.com/article/112/cpp-csv-parser
    int linepos = 0;
    bool inquotes = false;
    bool hadquotes = false;
    QChar c;
    const int linemax = m_currentLine.length();
    QString curstring;

    while (linepos < linemax)
    {

        c = m_currentLine.at(linepos);

        if (!inquotes && curstring.length()==0 && c=='"')
        {
            //beginquotechar
            inquotes = true;
            hadquotes = true;
        }
        else if (inquotes && c == '"')
        {
            //quotechar
            if ( (linepos+1 <linemax) && (m_currentLine[linepos+1]=='"') )
            {
                //encountered 2 double quotes in a row (resolves to 1 double quote)
                curstring.push_back(c);
                linepos++;
            }
            else
            {
                //endquotechar
                inquotes=false;
            }
        }
        else if (!inquotes && c == m_delimiter)
        {
            //end of field
            records << toVariant(curstring, hadquotes);
            curstring.clear();
            hadquotes = false;
        }
        else if (!inquotes && (c=='\r' || c=='\n') )
        {
            records << toVariant(curstring, hadquotes);
            return records;
        }
        else
        {
            curstring.append(c);
        }
        linepos++;
    }
    records << toVariant(curstring, hadquotes);

    return records;
}

QVariant CSVFile::toVariant(const QString& s, bool wasQuoted) const
{
    if (m_parseMode == UnparsedText)
    {
        return s.trimmed();
    }

    if (s.count('.') == 2)
    {
        if (s.length() == 10)
        {
            QDate date = QDate::fromString(s, "dd.MM.yyyy");
            if (date.isValid())
            {
                return date;
            }
        }
        else if (s.length() == 8)
        {
            QDate date = QDate::fromString(s, "dd.MM.yy");
            if (!date.isValid())
            {
                date = QDate::fromString(s, "d.M.yyyy");
            }
            if (date.isValid())
            {
                return date;
            }
        }
        else
        {
            QDate date = QDate::fromString(s, "d.M.yy");
            if (date.isValid())
            {
                return date;
            }
        }
    }
    else if (s.count('/') == 2)
    {
        QDate date = QDate::fromString(s, "MM/dd/yyyy");
        if (date.isValid())
        {
            return date;
        }
    }
    else if (s.count('-') == 2)
    {
        QDate date = QDate::fromString(s, Qt::ISODate);
        if (date.isValid())
        {
            return date;
        }
    }

    if (wasQuoted)
    {
        return s.trimmed();
    }
    else
    {
        bool ok;
        int i;
        i = s.toInt(&ok);
        if (ok)
        {
            return i;
        }

        double d = s.toDouble(&ok);
        if (ok)
        {
            return d;
        }
        if (m_decimalPoint != '.' && s.count(m_decimalPoint) == 1)
        {
            QString withPoint(s);
            withPoint.replace(m_decimalPoint, '.');
            d = s.toDouble(&ok);
            if (ok)
            {
                return d;
            }
        }

        return s.trimmed();
    }
}
