/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 04.04.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef CSVFILE_H
#define CSVFILE_H


#include <QFile>
#include <QString>
#include <QTextStream>
#include <QVariant>

class CSVFile
{
public:

    enum ParseMode
    {
        ParseDateAndNumbers,
        UnparsedText
    };

    CSVFile(const QChar& delimiter = ';', const QChar& decimalPoint = ',');

    void setParseMode(ParseMode mode);
    void setDateFormat(const QString& format);

    bool openForReading(const QString& filePath);
    bool openForWriting(const QString& filePath);
    void finishWriting();
    void writeToString(QString *string);

    // Reading
    QList<QVariant> parseNextLine();
    QString currentLine() const;
    bool atEnd() const;

    // Writing
    // write a line stepwise. Finish line by calling newLine
    CSVFile& operator<<(const QVariant& record);
    // finish the current line
    void newLine();
    // write a line in one go
    void writeNextLine(const QList<QVariant>& records);

    QString errorString() const;
    QString filePath() const;

private:

    QVariant toVariant(const QString& s, bool wasQuoted) const;
    QString variantToString(const QVariant& v) const;

    QChar m_delimiter;
    QChar m_decimalPoint;
    QString m_dateFormat;
    QFile m_file;
    ParseMode m_parseMode;
    QTextStream m_stream;
    QList<QVariant> m_buffer;
    QString m_currentLine;
};


#endif // CSVFILE_H
