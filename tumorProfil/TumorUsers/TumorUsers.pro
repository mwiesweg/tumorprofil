#-------------------------------------------------
#
# Project created by QtCreator 2015-08-30T11:41:25
#
#-------------------------------------------------

QT       += core gui xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TumorUsers
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    databaseconfigelement.cpp \
    userwidget.cpp \
    databaseguioptions.cpp \
    schemaupdater.cpp \
    databaseaccess.cpp \
    databaseparameters.cpp \
    aesutils.cpp \
    useradddialog.cpp \
    adminuser.cpp \
    mymessagebox.cpp \
    masterkeystable.cpp \
    addkeywidget.cpp \
    userqueryutils.cpp \
    abstractqueryutils.cpp

HEADERS  += mainwindow.h \
    databaseconfigelement.h \
    userwidget.h \
    databaseguioptions.h \
    schemaupdater.h \
    databaseaccess.h \
    databaseparameters.h \
    aesutils.h \
    useradddialog.h \
    adminuser.h \
    mymessagebox.h \
    masterkeystable.h \
    addkeywidget.h \
    tumoruserconstants.h \
    userqueryutils.h \
    abstractqueryutils.h

FORMS    +=

RESOURCES += \
    storage.qrc

win32|!exists(/usr/include/libcryptopp/aes.h) {
    INCLUDEPATH += $$PWD/../../cryptopp563
    DEPENDPATH += $$PWD/../../cryptopp563
    # for local Windows compilation
    #win32: LIBS += -L$$OUT_PWD/../../cryptopp563/ -lcryptopp563
    # for Linux-to-Windows MXE cross-compilation
    win32:CONFIG(debug, debug|release) LIBS += $$OUT_PWD/../../cryptopp563/debug/libcryptopp563.a
    win32:CONFIG(release, debug|release) LIBS += $$OUT_PWD/../../cryptopp563/release/libcryptopp563.a
    unix:  LIBS += $$OUT_PWD/../../cryptopp563/libcryptopp563.a
} else:unix {
    INCLUDEPATH += /usr/include/cryptopp
    LIBS += -lcryptopp
}


