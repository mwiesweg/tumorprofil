#include "authenticationwindow.h"

#include <QDialogButtonBox>
#include <QLineEdit>
#include <QLabel>
#include <QListWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QSettings>

#include "constants.h"

class AuthenticationWindow::Private
{
public:
    Private()
    {

    }
    QDialogButtonBox*   buttons;
    QLineEdit*      username;
    QLineEdit*      password;
    bool            login;
};

namespace
{
    const char* configGroupLogin   = "Login";
    const char* configLastUsername = "Last user name";
}

AuthenticationWindow::AuthenticationWindow(QWidget *parent)
    :QDialog(parent), d(new Private())
{
    setModal(true);

    d->buttons = new QDialogButtonBox(this);
    QPushButton* skipButton = d->buttons->addButton(tr("Settings"), QDialogButtonBox::ActionRole);
    d->buttons->addButton(QDialogButtonBox::Ok);
    d->buttons->addButton(QDialogButtonBox::Cancel);
    d->buttons->button(QDialogButtonBox::Ok)->setDefault(true);

    setupUi();

    connect(d->buttons->button(QDialogButtonBox::Ok), &QPushButton::clicked,
            this, &AuthenticationWindow::accept);

    connect(skipButton, &QPushButton::clicked,
            this, &AuthenticationWindow::settingsClicked);

    connect(d->buttons->button(QDialogButtonBox::Cancel), &QPushButton::clicked,
            this, &AuthenticationWindow::reject);
}

AuthenticationWindow::ReturnCode AuthenticationWindow::logIn(const QString& givenUsername)
{
    QString username(givenUsername);
    if (username.isEmpty())
    {
        QSettings qs(ORGANIZATION, APPLICATION);
        qs.beginGroup(configGroupLogin);
        username = qs.value(configLastUsername).toString();
    }

    if (username.isEmpty())
    {
        d->username->setFocus();
    }
    else
    {
        d->username->setText(username);
        d->password->setFocus();
    }

    int result = exec();

    if (result == Ok)
    {
        QSettings qs(ORGANIZATION, APPLICATION);
        qs.beginGroup(configGroupLogin);
        qs.setValue(configLastUsername, d->username->text());
    }

    return AuthenticationWindow::ReturnCode(result);
}


QString AuthenticationWindow::username()
{
    return d->username->text();
}

QString AuthenticationWindow::password()
{
    return d->password->text();
}

void AuthenticationWindow::settingsClicked()
{
    done(Settings);
}

void AuthenticationWindow::setupUi()
{
    QLabel *mainLabel = new QLabel(this);
    mainLabel->setWordWrap(true);

    mainLabel->setText(tr("Please provide the Username and Password"));


    QGridLayout* gLayout = new QGridLayout();

    QLabel* userLabel = new QLabel(this);
    userLabel->setText(tr("Username:"));
    d->username = new QLineEdit(this);


    QLabel* passwordLabel = new QLabel(this);
    passwordLabel->setText(tr("Password:"));
    d->password = new QLineEdit(this);
    d->password->setEchoMode(QLineEdit::Password);

    gLayout->addWidget(mainLabel, 1, 1, 1, 4);
    gLayout->addWidget(userLabel, 2, 1, 1, 2);
    gLayout->addWidget(d->username, 2, 3, 1, 2);
    gLayout->addWidget(passwordLabel, 3, 1, 1, 2);
    gLayout->addWidget(d->password, 3, 3, 1, 2);

    QVBoxLayout* const vbx = new QVBoxLayout(this);
    QHBoxLayout* const hbx = new QHBoxLayout(this);

    hbx->addLayout(gLayout);

    vbx->addLayout(hbx);
    vbx->addWidget(d->buttons);

    d->username->setFocus();
}
