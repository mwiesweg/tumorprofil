#ifndef AUTHENTICATIONWINDOW_H
#define AUTHENTICATIONWINDOW_H

#include <QDialog>


class UserData
{
public:
    QString username;
    QString password;
    bool    success;
};

class AuthenticationWindow : public QDialog
{
    Q_OBJECT
public:

    enum ReturnCode
    {
        Cancel = QDialog::Rejected, // 0
        Ok     = QDialog::Accepted, // 1
        Settings
    };

    AuthenticationWindow(QWidget* parent = 0);

    ReturnCode logIn(const QString& username = QString());

    QString username();
    QString password();

protected slots:

    void settingsClicked();

private:
    void setupUi();

    class Private;
    Private *d;
};

#endif // AUTHENTICATIONWINDOW_H
