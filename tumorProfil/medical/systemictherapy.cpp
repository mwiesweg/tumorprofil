/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "systemictherapy.h"

#include <QDebug>
#include <QFile>
#include <QMap>
#include <QTextStream>

class SystemicTherapy::SystemicTherapyPriv
{
public:

    QMap<QString, QStringList> substanceMap;
};

class SystemicTherapyCreator { public: SystemicTherapy object; };
Q_GLOBAL_STATIC(SystemicTherapyCreator, creator)

static SystemicTherapy* instance()
{
    return &creator()->object;
}

SystemicTherapy::SystemicTherapy()
    : d(new SystemicTherapyPriv)
{
    QFile file(":/medical/chemotherapy-substances");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString line;
        while ( !stream.atEnd() )
        {
            line = stream.readLine();
            QStringList names = line.split(' ', Qt::SkipEmptyParts);
            if (names.isEmpty())
            {
                continue;
            }
            for (QStringList::iterator it = names.begin(); it != names.end(); ++it)
            {
                it->replace('_', ' ');
            }
            QString canonicalName = names.takeFirst();
            d->substanceMap.insert(canonicalName, names);
        }
    }
    else
    {
        qDebug() << "Failed to open resource file";
    }
}

SystemicTherapy::~SystemicTherapy()
{
    delete  d;
}

QStringList SystemicTherapy::substances()
{
    return instance()->d->substanceMap.keys();
}

QString SystemicTherapy::toCanonicalSubstance(const QString &s, QString *identifiedSubstring)
{
    const SystemicTherapyPriv* d = instance()->d;
    // First round: primary names
    for (QMap<QString, QStringList>::const_iterator it = d->substanceMap.begin(); it != d->substanceMap.end(); ++it)
    {
        if (s.contains(it.key(), Qt::CaseInsensitive))
        {
            if (identifiedSubstring)
            {
                *identifiedSubstring = it.key();
            }
            return it.key();
        }
    }
    // Second round: trade names
    for (QMap<QString, QStringList>::const_iterator it = d->substanceMap.begin(); it != d->substanceMap.end(); ++it)
    {
        foreach (const QString& tradeName, it.value())
        {
            if (s.contains(tradeName, Qt::CaseInsensitive))
            {
                if (identifiedSubstring)
                {
                    *identifiedSubstring = tradeName;
                }
                return it.key();
            }
        }
    }
    // give up
    if (identifiedSubstring)
    {
        *identifiedSubstring = QString();
    }
    return s;
}

