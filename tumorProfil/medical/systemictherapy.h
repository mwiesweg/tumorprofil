/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 29.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef SYSTEMICTHERAPY_H
#define SYSTEMICTHERAPY_H

#include <QStringList>

class SystemicTherapy
{
public:

    // Returns a list with all canonical substance names
    static QStringList substances();

    // Will try to detect substance or trade names in the given string and return the substance name.
    // Returns s if not found in list.
    // Optionally sets identifiedSubstring to that part of the string which matched the returned substance name.
    static QString toCanonicalSubstance(const QString& s, QString* identifiedSubstring = 0);

private:

    friend class SystemicTherapyCreator;
    class SystemicTherapyPriv;
    SystemicTherapyPriv* const d;

    SystemicTherapy();
    ~SystemicTherapy();
};

#endif // SYSTEMICTHERAPY_H
