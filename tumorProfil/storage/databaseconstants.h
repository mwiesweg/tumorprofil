/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 28.02.2013
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef DATABASECONSTANTS_H
#define DATABASECONSTANTS_H

#include <QLatin1String>

class PatientPropertyName
{
public:    
    static QLatin1String timeStamp();

    static QLatin1String trialParticipation();
    static QLatin1String addedByMachine();
    static QLatin1String smokingStatus();
    static QLatin1String smokingPackYears();
    static QLatin1String pathologyNetwork();
};

class PatientPropertyValue
{
public:
    static QLatin1String ngsParser();

    enum SmokingStatus
    {
        SmokingStatusUnknown,
        NeverSmoker,
        FormerSmoker,
        CurrentSmoker,
        PresumedSmoker,
        PresumedNeverSmoker
    };

    static QLatin1String smokingStatus(SmokingStatus status);
    static SmokingStatus smokingStatus(const QString& string);
    static QLatin1String nngm();
};


class DiseasePropertyName
{
public:
    static QLatin1String diseaseHistory();
    static QLatin1String primaryTumorLocation();
    static QLatin1String resectabilityStatus();
};

class PathologyPropertyName
{
public:

    // Note: Most pathology property names are defined in pathologypropertyinfo.cpp
    // Here come only those with a "special" meaning
    static QLatin1String pathologyReport();
    static QLatin1String pathologySource();
};

class PathologyPropertyValue
{
public:
    static QLatin1String ngsParser();
};

#endif // DATABASECONSTANTS_H
