/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 02.02.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef PATIENTDB_H
#define PATIENTDB_H

// Qt includes

#include <QString>
#include <QVariant>

// Local includes

#include "event.h"
#include "labresults.h"
#include "patient.h"
#include "property.h"

class DatabaseCoreBackend;

class PatientDB
{
public:

    PatientDB(DatabaseCoreBackend* db);
    ~PatientDB();

    void setSetting(const QString& keyword, const QString& value);
    QString setting(const QString& keyword);

    int addPatient(const Patient& p);
    void updatePatient(const Patient& p);
    /** Finds patients matching criteria defined by p.
        Any null field of p will be treated as a wildcard.
        A null patient will retrieve all patients.
        Please note that properties are ignored for this purpose.
        */
    QList<Patient> findPatients(const Patient& p = Patient());

    int addDisease(int patientId, const Disease& d);
    void updateDisease(const Disease& dis);
    QList<Disease> findDiseases(int patientId);

    int addPathology(int diseaseId, const Pathology& path);
    void updatePathology(const Pathology& path);
    QList<Pathology> findPathologies(int diseaseId);

    enum PropertyType
    {
        PatientProperties,
        DiseaseProperties,
        PathologyProperties
    };

    QList<Property> properties(PropertyType e, int id, const QString& property = QString());
    void addProperty(PropertyType e, int id, const QString& property,
                     const QString& value, const QString& detail);
    void removeProperties(PropertyType e, int id,
                          const QString& property = QString(),
                          const QString& value = QString());

    void insertEvents(int diseaseId, const QList<Event> events);
    void removeEvents(int diseaseId);
    void replaceEvents(int diseaseId, const QList<Event> events);
    QList<Event> findEvents(int diseaseId);

    void insertLabResults(int patientId, const LabResults& results);
    void removeLabResults(int patientId);
    void replaceLabResults(int patientId, const LabResults& results);
    void insertLabValues(int seriesId, const QVector<LabValue>& values);
    LabResults findLabResults(int patientId);
    QVector<LabValue> findLabValues(int seriesId);

    QHash<QString, int> identifierHash();
    int findIdentifierId(const QString& textIdentifier, QHash<QString, int>* optionalCache = 0);
    int findOrCreateIdentifierId(const QString& textIdentifier, QHash<QString, int>* optionalCache = 0);
    int createIdentifierId(const QString& textIdentifier);
    QHash<int, QString> possiblePatientTextIdentifiers(const Patient& p);
    enum IllDefinedValues
    {
        CATOReports
    };
    QList<QVariant> findValuesForIdentifier(IllDefinedValues type, int identifierId);
    void insertValuesForIdentifier(IllDefinedValues type, int identifierId, QList<QVariant> values);
    void removeValuesForIdentifier(IllDefinedValues type, int identifierId, QList<QVariant> values);

    void deletePatient(int id);
    void deleteDisease(int diseaseId);
    void deletePathology(int pathologyId);

    QList<QList<QVariant> > idsFromForeignTable(const QString& table, const QStringList& columnNames);
    QList<QList<QVariant> > valuesForIdFromForeignTable(const QString& table, const QStringList& idColumnNames,
                                                         const QList<QVariant> &id, const QStringList& valueColumnNames);

    bool acquireWriteLock(int patientId);
    void releaseWriteLock(int patientId);
    QList<QString> lockedByInfo(int patientId);

private:


    class PatientDBPriv;
    PatientDBPriv* const d;
};

#endif // PATIENTDB_H
