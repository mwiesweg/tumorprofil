/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 02.02.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "patientdb.h"

// Qt includes

#include <QDebug>
#include <QVector>

// Local includes

#include "databasecorebackend.h"
#include "property.h"

class PatientDB::PatientDBPriv
{
public:
    PatientDBPriv()
        : db(0)
    {
    }

    DatabaseCoreBackend* db;

    inline QString tableName(PatientDB::PropertyType e)
    {
        switch (e)
        {
        case PatientDB::PatientProperties:
            return "PatientProperties";
        case PatientDB::DiseaseProperties:
            return "DiseaseProperties";
        case PatientDB::PathologyProperties:
            return "PathologyProperties";
        }
        return QString();
    }

    inline QString idName(PatientDB::PropertyType e)
    {
        switch (e)
        {
        case PatientDB::PatientProperties:
            return "patientid";
        case PatientDB::DiseaseProperties:
            return "diseaseid";
        case PatientDB::PathologyProperties:
            return "pathologyid";
        }
        return QString();
    }
};

PatientDB::PatientDB(DatabaseCoreBackend* db)
    : d(new PatientDBPriv)
{
    d->db = db;
}

PatientDB::~PatientDB()
{
    delete d;
}

void PatientDB::setSetting(const QString& keyword, const QString& value)
{
    d->db->execSql( QString("REPLACE into Settings VALUES (?,?);"),
                    keyword, value );
}

QString PatientDB::setting(const QString& keyword)
{
    QList<QVariant> values;
    d->db->execSql( QString("SELECT value FROM Settings "
                            "WHERE keyword=?;"),
                    keyword, &values );

    if (values.isEmpty())
    {
        return QString();
    }
    else
    {
        return values.first().toString();
    }
}

int PatientDB::addPatient(const Patient& p)
{
    qDebug() << "Adding patient to db";
    QVariant id;
    Patient p_copy(p);
    p_copy.encrypt();
    d->db->execSql("INSERT INTO Patients (firstName, surname, dateOfBirth, gender, medicoId) "
                   "VALUES (?, ?, ?, ?, ?)",
                   QVariantList() << p_copy.firstName << p_copy.surname << p_copy.encryptedDateOfBirth << p_copy.gender
                                  << (p_copy.medicoId ? p_copy.medicoId : QVariant()), 0, &id);

    return id.toInt();
}

void PatientDB::updatePatient(const Patient& p)
{
    Patient p_copy(p);
    p_copy.encrypt();
    d->db->execSql("UPDATE Patients SET firstName=?, surname=?, dateOfBirth=?, gender=?, medicoId=? WHERE id=?;",
                    QVariantList() << p_copy.firstName << p_copy.surname
                                   << p_copy.encryptedDateOfBirth << p_copy.gender << p_copy.medicoId << p_copy.id);
}

void PatientDB::deletePatient(int id)
{
    removeProperties(PatientProperties, id);

    QList<QVariant> values;
    d->db->execSql("SELECT id FROM Diseases WHERE patientid = ?;", id, &values);
    foreach (const QVariant& diseaseId, values)
    {
        deleteDisease(diseaseId.toInt());
    }

    d->db->execSql("DELETE FROM Patients WHERE id=?", id);
}

void PatientDB::deleteDisease(int diseaseId)
{
    removeProperties(DiseaseProperties, diseaseId);
    d->db->execSql("DELETE FROM PathologyProperties WHERE pathologyid IN (SELECT id FROM Pathologies WHERE diseaseid=?);", diseaseId);
    d->db->execSql("DELETE FROM Pathologies WHERE diseaseid=?;", diseaseId);
    d->db->execSql("DELETE FROM Diseases WHERE id=?;", diseaseId);
}

void PatientDB::deletePathology(int pathologyId)
{
    removeProperties(PathologyProperties, pathologyId);
    d->db->execSql("DELETE FROM Pathologies WHERE id=?;", pathologyId);
}

QList<QList<QVariant> > PatientDB::idsFromForeignTable(const QString &table, const QStringList &columnNames)
{
    if (columnNames.isEmpty())
    {
        return QList<QList<QVariant> >();
    }

    QString query = "SELECT DISTINCT " + columnNames.join(", ") + " FROM " + table + ";";
    QList<QVariant> values;
    d->db->execSql(query, &values);

    QList<QList<QVariant> > results;
    int setSize = columnNames.length();
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        QList<QVariant> v;
        v.reserve(setSize);
        for (int i=0; i<setSize; i++)
        {
            v << *it;
            ++it;
        }
        results << v;
    }
    return results;
}

QList<QList<QVariant> > PatientDB::valuesForIdFromForeignTable(const QString &table,
                                                                const QStringList &idColumnNames,
                                                                const QList<QVariant> &id,
                                                                const QStringList &valueColumnNames)
{
    if (valueColumnNames.isEmpty() || id.isEmpty())
    {
        return QList<QList<QVariant> >();
    }

    QString query = "SELECT " + valueColumnNames.join(", ") + " FROM " + table + " WHERE " + idColumnNames.join("=? AND ") + "=?;";
    QList<QVariant> values;
    d->db->execSql(query, id, &values);

    QList<QList<QVariant> > results;
    int setSize = valueColumnNames.length();
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        QList<QVariant> v;
        v.reserve(setSize);
        for (int i=0; i<setSize; i++)
        {
            v << *it;
            ++it;
        }
        results << v;
    }
    return results;
}

bool PatientDB::acquireWriteLock(int patientId)
{
    QByteArray uuid = d->db->connectionUuid();
    QString username = qgetenv("USER");
    if (username.isEmpty())
    {
        username = qgetenv("USERNAME");
    }
    d->db->execSql("INSERT IGNORE INTO PatientWriteLocks SET patientid=?, uuid=?, user=?, hostname=?;", patientId, uuid, username, QSysInfo::machineHostName());

    QList<QVariant> values;
    d->db->execSql("SELECT uuid FROM PatientWriteLocks WHERE patientid=?", patientId, &values);

    return values.size() && values[0].toByteArray() == uuid;
}

void PatientDB::releaseWriteLock(int patientId)
{
    d->db->execSql("DELETE FROM PatientWriteLocks WHERE patientId=?", patientId);
}

QList<QString> PatientDB::lockedByInfo(int patientId)
{
    QList<QVariant> values;
    d->db->execSql("SELECT uuid, user, hostname FROM PatientWriteLocks WHERE patientid=?", patientId, &values);
    QStringList l;
    foreach (const QVariant& value, values)
    {
        l << value.toString();
    }
    return l;
}

QList<Patient> PatientDB::findPatients(const Patient& p)
{
    QList<QVariant> values;

    Patient p_copy(p);

    qDebug() << "Find patients" << p.firstName << " " << p.surname << " " << p.gender << " " << p.dateOfBirth;
    // we can only search for encrypted data
    // if no encryption, this does nothing
    p_copy.encrypt();

    QString sql = "SELECT id, firstName, surname, dateOfBirth, gender, medicoId FROM Patients ";
    QString whereClause;
    QVariantList boundValues;
    if (!p_copy.firstName.isNull())
    {
        whereClause += "firstName = ? AND ";
        boundValues << p_copy.firstName;
    }
    if (!p_copy.surname.isNull())
    {
        whereClause += "surname = ? AND ";
        boundValues << p_copy.surname;
    }
    if (p_copy.gender != Patient::UnknownGender)
    {
        whereClause += "gender = ? AND ";
        boundValues << p_copy.gender;
    }
    if (!p_copy.encryptedDateOfBirth.isEmpty())
    {
        whereClause += "firstName = ? AND ";
        boundValues << p_copy.encryptedDateOfBirth;
    }

    if (!whereClause.isEmpty())
    {
        sql += "WHERE (" + whereClause + ");";
    }

    d->db->execSql(sql, boundValues, &values);

    QList<Patient> patients;
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        Patient p;

        p.id          = it->toInt();
        ++it;
        p.firstName   = it->toString();
        ++it;
        p.surname     = it->toString();
        ++it;
        p.encryptedDateOfBirth = it->toString();
        ++it;
        p.gender      = (Patient::Gender)it->toInt();
        ++it;
        p.medicoId    = it->toInt();
        ++it;

        // decrypt found patients
        p.decrypt();

        patients << p;
    }

    return patients;
}

int PatientDB::addDisease(int patientId, const Disease& dis)
{
    QVariant id;
    d->db->execSql("INSERT INTO Diseases (patientId, initialDiagnosis, cTNM, pTNM) "
                   "VALUES (?, ?, ?, ?)",
                   patientId, dis.initialDiagnosis.toString(Qt::ISODate),
                   dis.initialTNM.toText(), QString(), 0, &id);
    return id.toInt();
}

void PatientDB::updateDisease(const Disease& dis)
{
    d->db->execSql("UPDATE Diseases SET initialDiagnosis=?, cTNM=?, pTNM=? WHERE id=?;",
                   dis.initialDiagnosis.toString(Qt::ISODate),
                   dis.initialTNM.toText(), QString(), dis.id);
}

QList<Disease> PatientDB::findDiseases(int patientId)
{
    QList<QVariant> values;

    d->db->execSql("SELECT id, initialDiagnosis, cTNM, pTNM FROM Diseases WHERE patientId = ?;",
                   patientId, &values);

    QList<Disease> diseases;
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        Disease d;

        d.id        = it->toInt();
        ++it;
        d.initialDiagnosis = QDate::fromString(it->toString(), Qt::ISODate);
        ++it;
        d.initialTNM.setTNM(it->toString()); // cTNM string
        ++it;
        d.initialTNM.addTNM(it->toString()); // ignore
        ++it;

        diseases << d;
    }

    return diseases;
}

int PatientDB::addPathology(int diseaseId, const Pathology& path)
{
    QVariant id;
    d->db->execSql("INSERT INTO Pathologies (diseaseId, entity, sampleOrigin, context, date) "
                   "VALUES (?, ?, ?, ?, ?)",
                   QVariantList() << diseaseId << path.entity << path.sampleOrigin << path.context
                   << path.date.toString(Qt::ISODate), 0, &id);
    return id.toInt();
}

void PatientDB::updatePathology(const Pathology& path)
{
    d->db->execSql("UPDATE Pathologies SET entity=?, sampleOrigin=?, context=?, date=? WHERE id=?;",
                   QVariantList() << path.entity << path.sampleOrigin
                   << path.context << path.date.toString(Qt::ISODate) << path.id );
}

QList<Pathology> PatientDB::findPathologies(int diseaseId)
{
    QList<QVariant> values;

    d->db->execSql("SELECT id, entity, sampleOrigin, context, date FROM Pathologies WHERE diseaseId = ?;",
                   diseaseId, &values);

    QList<Pathology> pathologies;
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        Pathology p;

        p.id           = it->toInt();
        ++it;
        p.entity       = (Pathology::Entity)it->toInt();
        ++it;
        p.sampleOrigin = (Pathology::SampleOrigin)it->toInt();
        ++it;
        p.context      = it->toString();
        ++it;
        p.date         = QDate::fromString(it->toString(), Qt::ISODate);
        ++it;

        pathologies << p;
    }

    return pathologies;
}

QList<Property> PatientDB::properties(PropertyType e, int id, const QString &property)
{
    QList<QVariant> boundValues, values;

    QString query = "SELECT property, value, detail FROM " + d->tableName(e) + " WHERE " + d->idName(e) + "=?";
    boundValues << id;
    if (!property.isEmpty())
    {
        query += " AND property=?";
        boundValues << property;
    }
    query += ";";

    d->db->execSql(query, boundValues, &values);

    QList<Property> properties;

    if (values.isEmpty())
    {
        return properties;
    }

    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        Property property;

        property.property = (*it).toString();
        ++it;
        property.value    = (*it).toString();
        ++it;
        property.detail   = (*it).toString();
        ++it;

        properties << property;
    }

    return properties;
}

void PatientDB::addProperty(PropertyType e, int id, const QString& property,
                            const QString& value, const QString& detail)
{
    d->db->execSql("INSERT INTO " + d->tableName(e) +
                   " (" + d->idName(e) + ", property, value, detail) VALUES(?, ?, ?, ?);",
                   id, property, value, detail);
}

void PatientDB::removeProperties(PropertyType e, int id, const QString& property, const QString& value)
{
    if (property.isNull())
    {
        d->db->execSql("DELETE FROM " + d->tableName(e) + " WHERE " +
                       d->idName(e) + "=?;",
                       id);
    }
    else if (value.isNull())
    {
        d->db->execSql("DELETE FROM " + d->tableName(e) + " WHERE " +
                       d->idName(e) + "=? AND property=?;",
                       id, property);
    }
    else
    {
        d->db->execSql("DELETE FROM " + d->tableName(e) + " WHERE " +
                       d->idName(e) + "=? AND property=? AND value=?;",
                       id, property, value);
    }
}

void PatientDB::insertEvents(int diseaseId, const QList<Event> events)
{
    SqlQuery eventInsertQuery = d->db->prepareQuery("INSERT INTO Events (diseaseid, class, date, type) VALUES (?, ?, ?, ?);");
    SqlQuery infoInsertQuery  = d->db->prepareQuery("INSERT INTO EventInfos (eventid, type, info) VALUES (?, ?, ?);");
    QVariant id;

    foreach (const Event& event, events)
    {
        d->db->execSql(eventInsertQuery, diseaseId, event.eventClass, event.date.toString(Qt::ISODate), event.type, 0, &id);
        foreach (const EventInfo& info, event.infos)
        {
            d->db->execSql(infoInsertQuery, id.toInt(), info.type, info.info);
        }
    }
}

void PatientDB::replaceEvents(int diseaseId, const QList<Event> events)
{
    removeEvents(diseaseId);
    insertEvents(diseaseId, events);
}

void PatientDB::removeEvents(int diseaseId)
{
    d->db->execSql("DELETE FROM EventInfos WHERE eventid IN (SELECT id FROM Events WHERE diseaseid=?);", diseaseId);
    d->db->execSql("DELETE FROM Events WHERE diseaseid=?;", diseaseId);
}

QList<Event> PatientDB::findEvents(int diseaseId)
{
    QList<QVariant> values;

    d->db->execSql( "SELECT id, class, date, type FROM Events WHERE diseaseid=?;",
                    diseaseId, &values );

    int numberOfEvents = values.size() / 4;
    QList<Event> events;
    QVector<int> ids;
    events.reserve(numberOfEvents);
    ids.reserve(numberOfEvents);

    if (values.isEmpty())
    {
        return events;
    }

    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        Event event;

        ids              << (*it).toInt();
        ++it;
        event.eventClass  = (*it).toString();
        ++it;
        event.date        = QDate::fromString(it->toString(), Qt::ISODate);
        ++it;
        event.type        = (*it).toString();
        ++it;

        events << event;
    }

    SqlQuery query = d->db->prepareQuery("SELECT type, info FROM EventInfos WHERE eventid=?;");
    for (int i=0; i<events.size(); i++)
    {
        Event& event = events[i];

        d->db->execSql(query, ids[i], &values);

        for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
        {
            EventInfo info;

            info.type        = (*it).toString();
            ++it;
            info.info        = (*it).toString();
            ++it;

            event.infos << info;
        }

    }

    return events;
}

void PatientDB::insertLabResults(int patientId, const LabResults &results)
{
    SqlQuery insertSeriesQuery = d->db->prepareQuery("INSERT INTO LabSeries (patientid, date, type) VALUES (?, ?, ?);");
    QVariant id;

    foreach (const LabSeries& series, results.series)
    {
        d->db->execSql(insertSeriesQuery, patientId,
                       series.dateTime.time() == QTime(0,0,0,0) ? series.dateTime.date().toString(Qt::ISODate) : series.dateTime.toString(Qt::ISODate),
                       series.type, 0, &id);
        insertLabValues(id.toInt(), series.values);
    }
}

void PatientDB::removeLabResults(int patientId)
{
    d->db->execSql("DELETE FROM LabValues WHERE seriesid IN (SELECT id FROM LabSeries WHERE patientid=?);", patientId);
    d->db->execSql("DELETE FROM LabTextValues WHERE seriesid IN (SELECT id FROM LabSeries WHERE patientid=?);", patientId);
    d->db->execSql("DELETE FROM LabBinaryValues WHERE seriesid IN (SELECT id FROM LabSeries WHERE patientid=?);", patientId);
    d->db->execSql("DELETE FROM LabSeries WHERE patientid=?;", patientId);
}

void PatientDB::replaceLabResults(int patientId, const LabResults &results)
{
    removeLabResults(patientId);
    insertLabResults(patientId, results);
}

void PatientDB::insertLabValues(int seriesId, const QVector<LabValue> &values)
{
    SqlQuery insertDoubleQuery = d->db->prepareQuery("INSERT INTO LabValues (seriesid, name, value) VALUES (?, ?, ?);");
    SqlQuery insertTextQuery   = d->db->prepareQuery("INSERT INTO LabTextValues (seriesid, name, value) VALUES (?, ?, ?);");
    SqlQuery insertBlobQuery   = d->db->prepareQuery("INSERT INTO LabBinaryValues (seriesid, name, value) VALUES (?, ?, ?);");

    foreach (const LabValue& value, values)
    {
        switch (value.value.type())
        {
        case QVariant::Double:
        case QVariant::Int:
        case QVariant::UInt:
        case QVariant::LongLong:
        case QVariant::ULongLong:
            d->db->execSql(insertDoubleQuery, seriesId, value.name, value.value.toDouble());
            break;
        case QVariant::ByteArray:
            d->db->execSql(insertBlobQuery, seriesId, value.name, value.value.toByteArray());
            break;
        case QVariant::String:
            d->db->execSql(insertTextQuery, seriesId, value.name, value.value.toString());
            break;
        default:
            if (value.value.canConvert<QString>())
            {
                d->db->execSql(insertTextQuery, seriesId, value.name, value.value.toString());
            }
            break;
        }
    }

}

class LabValueLoader
{
public:
    LabValueLoader(DatabaseCoreBackend* db):
        db(db)
    {
        preparedQueries << db->prepareQuery("SELECT name, value FROM LabValues WHERE seriesid=?;")
                        << db->prepareQuery("SELECT name, value FROM LabTextValues WHERE seriesid=?;")
                        << db->prepareQuery("SELECT name, value FROM LabBinaryValues WHERE seriesid=?;");
    }
    DatabaseCoreBackend *db;
    QList<SqlQuery> preparedQueries;

    QVector<LabValue> values(int seriesId)
    {
        QList<QVariant>   values;
        QVector<LabValue> labValues;

        db->execSql(preparedQueries[0], seriesId, &values);
        labValues.reserve(values.size());
        for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
        {
            labValues << LabValue(it->toString(), *(it+1));
            it += 2;
        }

        db->execSql(preparedQueries[1], seriesId, &values);
        labValues.reserve(labValues.size() + values.size());
        for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
        {
            labValues << LabValue(it->toString(), *(it+1));
            it += 2;
        }

        db->execSql(preparedQueries[2], seriesId, &values);
        labValues.reserve(labValues.size() + values.size());
        for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
        {
            labValues << LabValue(it->toString(), *(it+1));
            it += 2;
        }

        return labValues;
    }

};

LabResults PatientDB::findLabResults(int patientId)
{
    LabResults        results;
    QList<QVariant>   values;
    LabValueLoader    valueLoader(d->db);

    d->db->execSql("SELECT id, date, type FROM LabSeries WHERE patientid=?;", patientId, &values);
    results.series.reserve(values.size());
    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        int id      = it->toInt();
        ++it;

        LabSeries series;
        series.dateTime = QDateTime::fromString(it->toString(), Qt::ISODate);
        ++it;
        series.type = it->toString();
        ++it;

        series.values = valueLoader.values(id);

        results.series << series;
    }
    return results;
}

QVector<LabValue> PatientDB::findLabValues(int seriesId)
{
    LabValueLoader loader(d->db);
    return loader.values(seriesId);
}

QHash<QString, int> PatientDB::identifierHash()
{
    QHash<QString, int> hash;
    SqlQuery query = d->db->execQuery("SELECT identifier, id FROM PatientTextIdentifiers");
    while (query.next())
    {
        hash.insert(query.value(0).toString(), query.value(1).toInt());
    }
    return hash;
}

int PatientDB::findIdentifierId(const QString &textIdentifier, QHash<QString, int> *optionalCache)
{
    if (optionalCache)
    {
        int cachedId = optionalCache->value(textIdentifier);
        if (cachedId)
        {
            return cachedId;
        }
    }
    else
    {
        QList<QVariant> values;
        d->db->execSql("SELECT id FROM PatientTextIdentifiers WHERE identifier=?;", textIdentifier, &values);
        if (!values.isEmpty())
        {
            return values.first().toInt();
        }
    }
    return 0;
}

int PatientDB::findOrCreateIdentifierId(const QString &textIdentifier, QHash<QString, int>* optionalCache)
{
    int id = findIdentifierId(textIdentifier, optionalCache);
    if (!id)
    {
        id = createIdentifierId(textIdentifier);

        if (optionalCache)
        {
            optionalCache->insert(textIdentifier, id);
        }
    }
    return id;
}

int PatientDB::createIdentifierId(const QString &textIdentifier)
{
    // If textIdentifier (UNIQUE) already exists, the INSERT will fail
    QVariant lastInsertId;
    d->db->execSql("INSERT INTO PatientTextIdentifiers (identifier) VALUES (?);", textIdentifier, 0, &lastInsertId);
    return lastInsertId.toInt();
}

QHash<int, QString> PatientDB::possiblePatientTextIdentifiers(const Patient &p)
{
    QHash<int, QString> ids;
    QList<QVariant>     values;

    QString firstNamePattern = '%' + p.firstName + '%';
    QString surnamePattern   = '%' + p.surname + '%';
    d->db->execSql("SELECT id, identifier FROM PatientTextIdentifiers WHERE identifier LIKE ? AND identifier LIKE ?;", surnamePattern, firstNamePattern, &values);

    for (QList<QVariant>::const_iterator it = values.constBegin(); it != values.constEnd();)
    {
        int id = it->toInt();
        ++it;
        QString identifier = it->toString();
        ++it;
        ids.insert(id, identifier);
    }

    return ids;
}

QList<QVariant> PatientDB::findValuesForIdentifier(PatientDB::IllDefinedValues type, int identifierId)
{
    QList<QVariant> values;

    switch (type)
    {
    case CATOReports:
        d->db->execSql("SELECT substance, date FROM CatoReports WHERE identifierid=?;", identifierId, &values);
        for (int i=0; i+1<values.size(); i += 2)
        {
            values[i+1] = QDate::fromString(values[i+1].toString(), Qt::ISODate);
        }
        break;
    }

    return values;
}

void PatientDB::insertValuesForIdentifier(PatientDB::IllDefinedValues type, int identifierId, QList<QVariant> values)
{
    switch (type)
    {
    case CATOReports:
    {
        // Expecting a flat list of pairs substance, date (QDate)
        SqlQuery insertQuery = d->db->prepareQuery("INSERT INTO CatoReports (identifierid, substance, date) VALUES (?, ?, ?);");
        for (int i=0; i+1<values.size(); i += 2)
        {
            d->db->execSql(insertQuery, identifierId, values[i], values[i+1].toDate().toString(Qt::ISODate));
        }
        break;
    }
    }
}

void PatientDB::removeValuesForIdentifier(PatientDB::IllDefinedValues type, int identifierId, QList<QVariant> values)
{
    switch (type)
    {
    case CATOReports:
    {
        // Expecting a flat list of pairs substance, date (QDate)
        SqlQuery insertQuery = d->db->prepareQuery("DELETE FROM CatoReports WHERE identifierid=? AND substance=? AND date=?;");
        for (int i=0; i+1<values.size(); i += 2)
        {
            d->db->execSql(insertQuery, identifierId, values[i], values[i+1].toDate().toString(Qt::ISODate));
        }
        break;
    }
    }
}

