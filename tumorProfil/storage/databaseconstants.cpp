/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 28.02.2013
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "databaseconstants.h"

QLatin1String PatientPropertyName::timeStamp()
{
    return QLatin1String("Last Change");
}

QLatin1String PatientPropertyName::trialParticipation()
{
    return QLatin1String("Trial Participation");
}

QLatin1String PatientPropertyName::addedByMachine()
{
    return QLatin1String("Added by machine");
}

QLatin1String PatientPropertyName::smokingStatus()
{
    return QLatin1String("Smoking Status");
}

QLatin1String PatientPropertyName::smokingPackYears()
{
    return QLatin1String("Smoking Pack Years");
}

QLatin1String PatientPropertyName::pathologyNetwork()
{
    return QLatin1String("Pathology Network");
}

QLatin1String PatientPropertyValue::ngsParser()
{
    return QLatin1String("NGS CVS Parser");
}

QLatin1String PatientPropertyValue::smokingStatus(PatientPropertyValue::SmokingStatus status)
{
    switch (status)
    {
    case SmokingStatusUnknown:
        break;
    case NeverSmoker:
        return QLatin1String("never");
    case FormerSmoker:
        return QLatin1String("former");
    case CurrentSmoker:
        return QLatin1String("current");
    case PresumedSmoker:
        return QLatin1String("presumed");
    case PresumedNeverSmoker:
        return QLatin1String("presumed never");
    }
    return QLatin1String();
}

PatientPropertyValue::SmokingStatus PatientPropertyValue::smokingStatus(const QString &string)
{
    if (string.isNull())
    {
        return SmokingStatusUnknown;
    }
    else if (string == "never")
    {
        return NeverSmoker;
    }
    else if (string == "former")
    {
        return FormerSmoker;
    }
    else if (string == "current")
    {
        return CurrentSmoker;
    }
    else if (string == "presumed")
    {
        return PresumedSmoker;
    }
    else if (string == "presumed never")
    {
        return PresumedNeverSmoker;
    }
    return SmokingStatusUnknown;
}

QLatin1String PatientPropertyValue::nngm()
{
    return QLatin1String("nNGM");
}

QLatin1String DiseasePropertyName::diseaseHistory()
{
    return QLatin1String("Disease History");
}

QLatin1String DiseasePropertyName::primaryTumorLocation()
{
     return QLatin1String("Primary Tumor Location");
}

QLatin1String DiseasePropertyName::resectabilityStatus()
{
     return QLatin1String("Primary Tumor Resectability");
}

QLatin1String PathologyPropertyName::pathologyReport()
{
    return QLatin1String("text/pathology-report");
}

QLatin1String PathologyPropertyName::pathologySource()
{
    return QLatin1String("text/pathology-source");
}
// If you add new pathology property names, think of PatientManager's loading and storing in specialPropertes

QLatin1String PathologyPropertyValue::ngsParser()
{
    return PatientPropertyValue::ngsParser();
}
