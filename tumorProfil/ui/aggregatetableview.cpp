/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 20.05.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "aggregatetableview.h"

// Qt includes

#include <QFileDialog>
#include <QHeaderView>
#include <QMessageBox>
#include <QSettings>

// Local includes

#include "constants.h"
#include "csvfile.h"
#include "dataaggregationmodel.h"
#include "dataaggregationfiltermodel.h"
#include "patientpropertymodel.h"

class AggregateTableView::AggregateTableViewPriv
{
public:
    AggregateTableViewPriv()
        : model(0)
    {
    }

    DataAggregationModel* model;
    DataAggregationFilterModel* filterModel;
};

namespace
{
    const char* configGroup = "AggregateTableView";
    const char* configLastAggregateResultFile = "Last Aggregate Result File";
}

AggregateTableView::AggregateTableView(QWidget *parent) :
    AnalysisTableView(parent),
    d(new AggregateTableViewPriv)
{
    installDelegateToDisplayPercentages();
    d->model = new DataAggregationModel(this);
    d->filterModel = new DataAggregationFilterModel(this);
    d->filterModel->setSourceModel(d->model);
    setModel(d->filterModel);

    connect(this, SIGNAL(activated(QModelIndex)),
            this, SLOT(slotActivated(QModelIndex)));
}

AggregateTableView::~AggregateTableView()
{
    delete d;
}

void AggregateTableView::setSourceModel(QAbstractItemModel* source)
{
    d->model->setSourceModel(source);
}

DataAggregationModel* AggregateTableView::dataAggregationModel() const
{
    return d->model;
}

void AggregateTableView::saveAggregateData()
{
    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(configGroup);
    QString fileName = QFileDialog::getSaveFileName(this, tr("Als CSV speichern"), settings.value(configLastAggregateResultFile).toString());
    if (fileName.isNull())
    {
        return;
    }
    QMap<QString, QVariant> map = d->model->aggregationData();
    CSVFile file;
    if (!file.openForWriting(fileName))
    {
        QMessageBox::warning(this, tr("Öffnen fehlgeschlagen"), tr("In die ausgewählte Datei kann nicht geschrieben werden: \n%1").arg(file.errorString()));
        return;
    }
    for (QMap<QString, QVariant>::const_iterator it = map.begin(); it != map.end(); ++it)
    {
        file << it.key() << it.value();
        file.newLine();
    }
    settings.setValue(configLastAggregateResultFile, fileName);
}

void  AggregateTableView::slotActivated(const QModelIndex& index)
{
    QList<QModelIndex> referenceIndexes = d->model->sourceModelReferenceIndexes(d->filterModel->mapToSource(index));
    emit activatedReferenceIndexes(referenceIndexes);
}
