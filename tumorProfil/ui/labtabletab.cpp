/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labtabletab.h"

#include <QVBoxLayout>

#include "lab/labtablewidget.h"

LabTableTab::LabTableTab(QWidget *parent)
    : QWidget(parent)
{
    QVBoxLayout* layout = new QVBoxLayout;

    m_widget = new LabTableWidget;
    layout->addWidget(m_widget);

    setLayout(layout);
}

QString LabTableTab::tabLabel() const
{
    return tr("Laborwerte");
}

void LabTableTab::setEditingEnabled(bool enabled)
{
    m_widget->setEditingEnabled(enabled);
}

void LabTableTab::setDisease(const Patient::Ptr &p, int)
{
    m_widget->setPatient(p);
}

void LabTableTab::save()
{
    m_widget->save();
}
