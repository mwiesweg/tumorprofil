/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 08.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "primarytumorlocationbox.h"

#include "databaseconstants.h"

PrimaryTumorLocationBox::PrimaryTumorLocationBox(QWidget* parent)
    : QComboBox(parent),
      m_initialIndex(-1)
{

}

bool PrimaryTumorLocationBox::changed() const
{
    return m_initialIndex != currentIndex();
}

void PrimaryTumorLocationBox::save(Disease &disease) const
{
    if (currentIndex() > 0)
    {
        disease.diseaseProperties.setProperty(DiseasePropertyName::primaryTumorLocation(), currentText());
    }
    else
    {
        // no items, or item 0 which is a pseudo entry
        disease.diseaseProperties.removeProperty(DiseasePropertyName::primaryTumorLocation());
    }
}

void PrimaryTumorLocationBox::load(const Disease &disease)
{
    clear();
    switch (disease.entity())
    {
    case Pathology::ColorectalAdeno:
        addItems(QStringList()
                 << tr("Unbekannt")
                 << "Rectum" <<  "Sigmoid" << "Descendens"
                 << "Transversum"
                 << "Ascendens" << "Coecum"
                 );
        break;
    case Pathology::Cholangiocarcinoma:
        addItems(QStringList()
                 << tr("Unbekannt")
                 << "Intrahepatic"
                 << "Perihilar"
                 << "Distal"
                 << "Gall Bladder");
        break;
    default:
        break;
    }

    if (count())
    {
        show();
        emit visibilityChanged(true);
        QString loc = disease.diseaseProperties.property(DiseasePropertyName::primaryTumorLocation()).value;
        int index = findText(loc);
        if (index != -1)
        {
            setCurrentIndex(index);
        }
        else
        {
            setCurrentIndex(0);
        }
    }
    else
    {
        hide();
        emit visibilityChanged(false);
    }
    m_initialIndex = currentIndex();
}

void PrimaryTumorLocationBox::reset()
{
    clear();
    hide();
    emit visibilityChanged(false);
}
