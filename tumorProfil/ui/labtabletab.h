/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABTABLETAB_H
#define LABTABLETAB_H

#include <QWidget>

#include "mainviewtabinterface.h"
#include "patient.h"

class LabTableWidget;

class LabTableTab : public QWidget, public MainViewTabInterface
{
    Q_OBJECT
public:
    explicit LabTableTab(QWidget *parent = 0);

    virtual QString tabLabel() const;

public slots:

    void setEditingEnabled(bool enabled);
    void setDisease(const Patient::Ptr& p, int diseaseIndex);
    void save();

protected:

    LabTableWidget* m_widget;
};

#endif // LABTABLETAB_H
