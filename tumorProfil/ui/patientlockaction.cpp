/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 25.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "patientlockaction.h"

#include <QMessageBox>
#include <QToolBar>

#include "patientmanager.h"

PatientLockAction::PatientLockAction(QObject *parent) : QAction(parent)
{
    setVisible(false);
    setIcon(QIcon::fromTheme("exclamation"));
    setText(tr("Patient für Änderungen gesperrt"));
    QFont f = font();
    f.setBold(true);
    setFont(f);

    connect(this, &PatientLockAction::triggered, this, &PatientLockAction::displayInformation);
}

void PatientLockAction::messageBox(QWidget *parent, const PatientWriteLock &lock)
{
    messageBox(parent, lock.lockedBy());
}

void PatientLockAction::messageBox(QWidget *parent, const QString &lockInfo)
{
    QMessageBox::information(parent,
                             tr("Patient für Änderungen gesperrt"),
                             tr("Dieser Patient wird aktuell von einem anderen Nutzer bearbeitet:\n") + lockInfo);

}

void PatientLockAction::setInformation(const PatientWriteLock *lock)
{
    if (!lock || lock->hasLock())
    {
        lockInfo = QString();
        setVisible(false);
    }
    else
    {
        lockInfo = lock->lockedBy();
        setVisible(true);
    }
}

void PatientLockAction::displayInformation()
{
    QList<QWidget*> widgets = associatedWidgets();
    qDebug() << widgets;
    messageBox(widgets.isEmpty() ? 0 : widgets.first(), lockInfo);
}

PatientLocking::PatientLocking()
    : readOnly(true),
      action(0),
      lock(0)
{
}

PatientLocking::~PatientLocking()
{
    reset();
    delete action;
}

void PatientLocking::setPatient(const Patient::Ptr &p)
{
    reset();
    if (p)
    {
        patient = p;
        acquirePatientWriteAccess();
    }
}

void PatientLocking::reset()
{
    delete lock;
    lock = 0;
    patient = Patient::Ptr();
}

bool PatientLocking::patientEditable() const
{
    // lock is only nonzero if it holds the lock
    return lock;
}

void PatientLocking::setReadOnly(bool r)
{
    readOnly = r;
    acquirePatientWriteAccess();
}

void PatientLocking::addActionToToolBar(QToolBar *bar, bool withSpacer)
{
    if (!action)
    {
        action = new PatientLockAction;
    }

    if (withSpacer)
    {
        QWidget* spacer = new QWidget();
        spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        bar->addWidget(spacer);
    }

    bar->addAction(action);
}

void PatientLocking::acquirePatientWriteAccess()
{
    if (!patient || readOnly || lock)
    {
        return;
    }

    lock = new PatientWriteLock(patient);
    action->setInformation(lock);

    // delete lock object if locking failed
    if (!lock->hasLock())
    {
        delete lock;
        lock = 0;
    }
}
