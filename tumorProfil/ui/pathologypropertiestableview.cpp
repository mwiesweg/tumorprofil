/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 15.06.2015
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "pathologypropertiestableview.h"

#include <QAction>
#include <QDateEdit>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QIcon>
#include <QMenu>
#include <QPointer>
#include <QPushButton>
#include <QStyledItemDelegate>

#include "pathologypropertiestablemodel.h"
#include "pathologypropertyinfo.h"
#include "pathologypropertywidget.h"
#include "parser/parsertools.h"

class PathologyPropertiesTableViewDelegate : public QStyledItemDelegate
{
public:
    PathologyPropertiesTableViewDelegate(QObject* parent = 0)
        : QStyledItemDelegate(parent)
    {
    }

    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
};

class PathologyPropertiesTableViewEditorWidget : public QWidget
{
public:
    PathologyPropertiesTableViewEditorWidget(PathologyPropertyInfo::Property property, QWidget* parent)
        : QWidget(parent)
    {
        widget = new PathologyPropertyWidget(property, this);
        QFormLayout* layout = new QFormLayout;
        widget->addToLayout(layout);
        setLayout(layout);
        setAutoFillBackground(true);
    }

    PathologyPropertyWidget *widget;
};


PathologyPropertiesTableView::PathologyPropertiesTableView()
    : m_dateForNewEntriesMode(AddToLastPathology),
      m_lastEnteredDate(QDate::currentDate())
{
    horizontalHeader()->setStretchLastSection(true);
    setItemDelegate(new PathologyPropertiesTableViewDelegate(this));
}

PathologyPropertiesTableModel* PathologyPropertiesTableView::propertiesModel() const
{
    return filterModel()->sourceModel();
}

PathologyPropertiesTableFilterModel* PathologyPropertiesTableView::filterModel() const
{
    return static_cast<PathologyPropertiesTableFilterModel*>(QTableView::model());
}

void PathologyPropertiesTableView::setModels(PathologyPropertiesTableModel* model, PathologyPropertiesTableFilterModel* filterModel)
{
    filterModel->setSourceModel(model);
    QTableView::setModel(filterModel);
}

QWidget* PathologyPropertiesTableViewDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    switch (index.data(PathologyPropertiesTableModel::ColumnsRole).value<PathologyPropertiesTableModel::Columns>())
    {
    case PathologyPropertiesTableModel::PropertyValueColumn:
    {
        PathologyPropertyInfo info = index.data(PathologyPropertiesTableModel::PathologyPropertyInfoRole).value<PathologyPropertyInfo>();
        return new PathologyPropertiesTableViewEditorWidget(info.property, parent);
    }
    case PathologyPropertiesTableModel::PathologyDateColumn:
        return QStyledItemDelegate::createEditor(parent, option, index);
    default:
        return 0;
    }
    return 0;
}

void PathologyPropertiesTableViewDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    switch (index.data(PathologyPropertiesTableModel::ColumnsRole).value<PathologyPropertiesTableModel::Columns>())
    {
    case PathologyPropertiesTableModel::PropertyValueColumn:
    {
        PathologyPropertiesTableViewEditorWidget* w = static_cast<PathologyPropertiesTableViewEditorWidget*>(editor);
        model->setData(index, QVariant::fromValue(w->widget->currentProperty()));
        break;
    }
    case PathologyPropertiesTableModel::PathologyDateColumn:
        QStyledItemDelegate::setModelData(editor, model, index);
        break;
    default:
        break;
    }
}

void PathologyPropertiesTableViewDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    switch (index.data(PathologyPropertiesTableModel::ColumnsRole).value<PathologyPropertiesTableModel::Columns>())
    {
    case PathologyPropertiesTableModel::PropertyValueColumn:
    {
        PathologyPropertiesTableViewEditorWidget* w = static_cast<PathologyPropertiesTableViewEditorWidget*>(editor);
        w->widget->setValue(index.data(PathologyPropertiesTableModel::PathologyPropertyRole).value<Property>());
        break;
    }
    case PathologyPropertiesTableModel::PathologyDateColumn:
        QStyledItemDelegate::setEditorData(editor, index);
        break;
    default:
        break;
    }
}

QPushButton *PathologyPropertiesTableView::createAddEntryButton()
{
    QPushButton* addNewFindingButton = new QPushButton(QIcon::fromTheme("add"), tr("Eintrag hinzufügen"));
    addNewFindingButton->setMenu(buildPropertyMenu());
    connect(addNewFindingButton->menu(), &QMenu::triggered, this, &PathologyPropertiesTableView::addPropertyTriggered);
    return addNewFindingButton;
}

void PathologyPropertiesTableView::setDateForNewEntriesMode(PathologyPropertiesTableView::DateForNewEntriesMode mode)
{
    m_dateForNewEntriesMode = mode;
}

QMenu* PathologyPropertiesTableView::buildPropertyMenu()
{
    QMenu* menu = new QMenu;

    QMenu* ihcMenu = menu->addMenu(tr("IHC"));
    QAction* allAdenoAction = ihcMenu->addAction(tr("IHC Adenokarzinom Lunge"));
    QAction* allPDL1Action = ihcMenu->addAction(tr("IHC PD-L1"));
    QAction* allCRCAction = ihcMenu->addAction(tr("IHC CRC"));
    QAction* allHER2Action = ihcMenu->addAction(tr("HER2 IHC/DAKO"));
    QAction* ihcALKROS1Action = ihcMenu->addAction(tr("ALK und ROS1"));
    QAction* ihcALKROS1NTRKAction = ihcMenu->addAction(tr("ALK, ROS1, panTRK"));
    fillPropertySubmenu(ihcMenu, PathologyPropertyInfo::allIHC());

    QMenu* mutationSubMenu = menu->addMenu(tr("Mutationsanalyse"));
    QAction* ccp3Action = mutationSubMenu->addAction(tr("NGS Panel CCP3"));
    QAction* nngmAction = mutationSubMenu->addAction(tr("NGS Panel NNGM"));
    QAction* archerAction = mutationSubMenu->addAction(tr("NGS Archer Panel"));
    fillPropertySubmenu(mutationSubMenu, PathologyPropertyInfo::allMutations());
    QAction* msiAction = mutationSubMenu->addAction(tr("MSI PCR"));

    QMenu* fishSubMenu = menu->addMenu(tr("FISH"));
    fillPropertySubmenu(fishSubMenu, PathologyPropertyInfo::allFish());

    allAdenoAction->setData(QVariantList()
                            << PathologyPropertyInfo::IHC_ALK
                            << PathologyPropertyInfo::IHC_ROS1
                            << PathologyPropertyInfo::IHC_PDL1
                            << PathologyPropertyInfo::IHC_PDL1_immunecell
                            << PathologyPropertyInfo::IHC_panTRK);
    allAdenoAction->setShortcut(Qt::Key_I + Qt::CTRL);

    allPDL1Action->setData(QVariantList()
                            << PathologyPropertyInfo::IHC_PDL1
                            << PathologyPropertyInfo::IHC_PDL1_immunecell);
    allPDL1Action->setShortcut(Qt::Key_P + Qt::CTRL);

    allCRCAction->setData(QVariantList()
                            << PathologyPropertyInfo::IHC_HER2
                            << PathologyPropertyInfo::IHC_HER2_DAKO
                            << PathologyPropertyInfo::IHC_MLH1
                            << PathologyPropertyInfo::IHC_MSH2
                            << PathologyPropertyInfo::IHC_PMS2);

    ccp3Action->setData(QVariantList()
                        << PathologyPropertyInfo::Mut_BRAF_11
                        << PathologyPropertyInfo::Mut_BRAF_15
                        << PathologyPropertyInfo::Mut_EGFR_18_20
                        << PathologyPropertyInfo::Mut_EGFR_19_21
                        << PathologyPropertyInfo::Mut_ERBB2
                        << PathologyPropertyInfo::Mut_FGFR1
                        << PathologyPropertyInfo::Mut_FGFR3
                        << PathologyPropertyInfo::Mut_HRAS_2_4
                        << PathologyPropertyInfo::Mut_NRAS_2_4
                        << PathologyPropertyInfo::Mut_KIT
                        << PathologyPropertyInfo::Mut_KRAS_2
                        << PathologyPropertyInfo::Mut_KRAS_3
                        << PathologyPropertyInfo::Mut_KRAS_4
                        << PathologyPropertyInfo::Mut_MET
                        << PathologyPropertyInfo::Mut_PDGFRa
                        << PathologyPropertyInfo::Mut_PIK3CA
                        << PathologyPropertyInfo::Mut_IDH1
                        << PathologyPropertyInfo::Mut_IDH2
                        << PathologyPropertyInfo::Mut_STK11
                        << PathologyPropertyInfo::Mut_TP53);

    //TODO: Consolidate with ParserTools::ngsPanels()
    nngmAction->setData(QVariantList()
                        << PathologyPropertyInfo::Mut_ALK
                        << PathologyPropertyInfo::Mut_BRAF_11
                        << PathologyPropertyInfo::Mut_BRAF_15
                        << PathologyPropertyInfo::Mut_CTNNB1
                        << PathologyPropertyInfo::Mut_EGFR_18_20
                        << PathologyPropertyInfo::Mut_EGFR_19_21
                        << PathologyPropertyInfo::Mut_ERBB2
                        << PathologyPropertyInfo::Mut_FGFR1
                        << PathologyPropertyInfo::Mut_FGFR2
                        << PathologyPropertyInfo::Mut_FGFR3
                        << PathologyPropertyInfo::Mut_FGFR4
                        << PathologyPropertyInfo::Mut_HRAS_2_4
                        << PathologyPropertyInfo::Mut_IDH1
                        << PathologyPropertyInfo::Mut_IDH2
                        << PathologyPropertyInfo::Mut_KEAP1
                        << PathologyPropertyInfo::Mut_KRAS_2
                        << PathologyPropertyInfo::Mut_KRAS_3
                        << PathologyPropertyInfo::Mut_KRAS_4
                        << PathologyPropertyInfo::Mut_MAP2K1
                        << PathologyPropertyInfo::Mut_MET
                        << PathologyPropertyInfo::Mut_NRAS_2_4
                        << PathologyPropertyInfo::Mut_NTRK1
                        << PathologyPropertyInfo::Mut_NTRK2
                        << PathologyPropertyInfo::Mut_NTRK3
                        << PathologyPropertyInfo::Mut_PIK3CA
                        << PathologyPropertyInfo::Mut_PTEN
                        << PathologyPropertyInfo::Mut_RET
                        << PathologyPropertyInfo::Mut_ROS1
                        << PathologyPropertyInfo::Mut_STK11
                        << PathologyPropertyInfo::Mut_TP53);

    archerAction->setData(QVariantList()
                          << PathologyPropertyInfo::Fusion_ALK
                          << PathologyPropertyInfo::Fusion_ROS1
                          << PathologyPropertyInfo::Fusion_RET
                          << PathologyPropertyInfo::Fusion_NTRK1
                          << PathologyPropertyInfo::Fusion_NTRK2
                          << PathologyPropertyInfo::Fusion_NTRK3
                          << PathologyPropertyInfo::Fusion_MET
                          << PathologyPropertyInfo::Fusion_AXL
                          << PathologyPropertyInfo::Fusion_BRAF
                          << PathologyPropertyInfo::Fusion_CCND1
                          << PathologyPropertyInfo::Fusion_FGFR1
                          << PathologyPropertyInfo::Fusion_FGFR2
                          << PathologyPropertyInfo::Fusion_FGFR3
                          << PathologyPropertyInfo::Fusion_NRG1
                          << PathologyPropertyInfo::Fusion_PPARG
                          << PathologyPropertyInfo::Fusion_RAF1
                          << PathologyPropertyInfo::Fusion_THADA);

    msiAction->setData((int)PathologyPropertyInfo::MSI_Panel);

    allHER2Action->setData(QVariantList()
                           << PathologyPropertyInfo::IHC_HER2
                           << PathologyPropertyInfo::IHC_HER2_DAKO);

    ihcALKROS1Action->setData(QVariantList()
                           << PathologyPropertyInfo::IHC_ALK
                           << PathologyPropertyInfo::IHC_ROS1);

    ihcALKROS1NTRKAction->setData(QVariantList()
                                  << PathologyPropertyInfo::IHC_ALK
                                  << PathologyPropertyInfo::IHC_ROS1
                                  << PathologyPropertyInfo::IHC_panTRK);
    ihcALKROS1NTRKAction->setShortcut(Qt::Key_R + Qt::CTRL);

    return menu;
}

void PathologyPropertiesTableView::fillPropertySubmenu(QMenu* menu, const QList<PathologyPropertyInfo>& infos)
{
    foreach (const PathologyPropertyInfo& info, infos)
    {
        QAction* action = menu->addAction(info.label);
        action->setData((int)info.property);
    }
}

void PathologyPropertiesTableView::addPropertyTriggered(QAction *action)
{
    QVariant propertyData = action->data();
    QList<PathologyPropertyInfo> infos;
    if (propertyData.type() == QVariant::Int)
    {
        infos << PathologyPropertyInfo::info((PathologyPropertyInfo::Property)propertyData.toInt());
    }
    else if (propertyData.type() == QVariant::List)
    {
        foreach (const QVariant& element, propertyData.toList())
        {
            infos << PathologyPropertyInfo::info((PathologyPropertyInfo::Property)element.toInt());
        }
    }

    PropertyList newProperties;
    foreach (const PathologyPropertyInfo& info, infos)
    {
        Property prop;
        prop.property = info.id;
        ValueTypeCategoryInfo valueInfo(info);
        prop.value = valueInfo.toPropertyValue(valueInfo.negativeValue());
        newProperties << prop;
    }

    switch (m_dateForNewEntriesMode)
    {
    case AddToLastPathology:
        propertiesModel()->addProperties(newProperties);
        break;
    case AskForDate:
    {
        QPointer<QDialog> dialog = new QDialog(this);
        QLabel* label = new QLabel(tr("Befunddatum:"));
        QDateEdit* edit = new QDateEdit;
        QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        QVBoxLayout* layout = new QVBoxLayout;
        layout->addWidget(label);
        layout->addWidget(edit);
        layout->addWidget(buttons);
        dialog->setLayout(layout);
        connect(buttons, &QDialogButtonBox::accepted, dialog.data(), &QDialog::accept);
        connect(buttons, &QDialogButtonBox::rejected, dialog.data(), &QDialog::reject);
        edit->setDate(m_lastEnteredDate);
        //d->buttons->button(QDialogButtonBox::Ok)->setDefault(true);
        if (dialog->exec() == QDialog::Accepted)
        {
            m_lastEnteredDate = edit->date();
            int pathIndex = ParserTools::findSuitablePathology(propertiesModel()->pathologies(), edit->date());
            if (pathIndex == -1)
            {
                pathIndex = propertiesModel()->addPathology(edit->date());
            }
            propertiesModel()->addProperties(pathIndex, newProperties);
        }
        break;
    }
    }

}
