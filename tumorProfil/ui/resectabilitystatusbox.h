#ifndef RESECTABILITYSTATUSBOX_H
#define RESECTABILITYSTATUSBOX_H

#include <QComboBox>

#include "disease.h"
#include "property.h"

class ResectabilityStatusBox : public QComboBox
{
    Q_OBJECT
public:
    explicit ResectabilityStatusBox(QWidget *parent = nullptr);

    bool changed() const;

signals:

    void visibilityChanged(bool visible);

public slots:
    void save(Disease& disease) const;
    void load(const Disease& disease);
    void reset();

protected:

    int m_initialIndex;
};

#endif // RESECTABILITYSTATUSBOX_H
