/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 01.08.2015
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "extrainformationtab.h"

#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QVBoxLayout>

#include "databaseconstants.h"
#include "pathologypropertyinfo.h"
#include "patientmanager.h"
#include "smokerwidget.h"

class ExtraInformationTab::ExtraInformationTabPriv
{
public:
    ExtraInformationTabPriv()
        : editingEnabled(false),
          edited(true),
          smokerWidget(0)
    {
    }

    Patient::Ptr                              currentPatient;
    bool                                      editingEnabled;
    bool                                      edited;
    QMap<TrialContextInfo::Trial, QCheckBox*> trialCheckboxes;
    QMap<QString, QCheckBox*>                 pathologyNetworkCheckboxes;
    SmokerWidget*                             smokerWidget;
};

ExtraInformationTab::ExtraInformationTab(QWidget* parent)
    : QWidget(parent),
      d(new ExtraInformationTabPriv)
{
    QVBoxLayout* layout = new QVBoxLayout;

    QGroupBox* box = new QGroupBox(tr("Teilnahme an"));
    QVBoxLayout* boxlayout = new QVBoxLayout;
    for (int t = TrialContextInfo::FirstTrial; t<= TrialContextInfo::LastTrial; ++t)
    {
        TrialContextInfo info((TrialContextInfo::Trial)t);
        QCheckBox* cb = new QCheckBox(info.label);
        d->trialCheckboxes[info.trial] = cb;
        boxlayout->addWidget(cb);
    }
    // Pathology Networks - only one so far. Add enumerator class if more are added.
    QCheckBox* nngmCheckBox = new QCheckBox("nNGM");
    d->pathologyNetworkCheckboxes[PatientPropertyValue::nngm()] = nngmCheckBox;
    boxlayout->addWidget(nngmCheckBox);

    box->setLayout(boxlayout);
    layout->addWidget(box);

    QFormLayout* patientPropLayout = new QFormLayout;

    d->smokerWidget = new SmokerWidget;
    patientPropLayout->addRow(tr("Raucherstatus:"), d->smokerWidget);

    layout->addLayout(patientPropLayout);

    layout->addStretch();
    setLayout(layout);
}

ExtraInformationTab::~ExtraInformationTab()
{
    delete d;
}

void ExtraInformationTab::save()
{
    if (!d->editingEnabled || !d->currentPatient)
    {
        return;
    }

    for (QMap<TrialContextInfo::Trial, QCheckBox*>::const_iterator it = d->trialCheckboxes.begin(); it != d->trialCheckboxes.end(); ++it)
    {
        TrialContextInfo info(it.key());
        if (it.value()->isChecked())
        {
            d->currentPatient->patientProperties.addProperty(PatientPropertyName::trialParticipation(),
                                                             info.id);
        }
        else
        {
            d->currentPatient->patientProperties.removeProperty(PatientPropertyName::trialParticipation(),
                                                                info.id);
        }
    }
    for (QMap<QString, QCheckBox*>::const_iterator it = d->pathologyNetworkCheckboxes.begin(); it != d->pathologyNetworkCheckboxes.end(); ++it)
    {
        if (it.value()->isChecked())
        {
            d->currentPatient->patientProperties.addProperty(PatientPropertyName::pathologyNetwork(),
                                                             it.key());
        }
        else
        {
            d->currentPatient->patientProperties.removeProperty(PatientPropertyName::pathologyNetwork(),
                                                                it.key());
        }
    }

    d->smokerWidget->save(d->currentPatient);

    PatientManager::instance()->updateData(d->currentPatient, PatientManager::ChangedPatientProperties);    
}

void ExtraInformationTab::setDisease(const Patient::Ptr& p, int)
{
    if (d->currentPatient == p)
    {
        return;
    }

    d->currentPatient = p;
    setEnabled(p);
    d->edited = false;
    if (!p)
    {
        return;
    }

    for (QMap<TrialContextInfo::Trial, QCheckBox*>::const_iterator it = d->trialCheckboxes.begin();
         it != d->trialCheckboxes.end(); ++it)
    {
        TrialContextInfo info(it.key());
        it.value()->setChecked(
                    p->patientProperties.hasProperty(PatientPropertyName::trialParticipation(),
                                                     info.id)
                    );
    }
    for (QMap<QString, QCheckBox*>::const_iterator it = d->pathologyNetworkCheckboxes.begin();
         it != d->pathologyNetworkCheckboxes.end(); ++it)
    {
        it.value()->setChecked(
                    p->patientProperties.hasProperty(PatientPropertyName::pathologyNetwork(),
                                                     it.key())
                    );
    }


    d->smokerWidget->load(d->currentPatient);
}

QString ExtraInformationTab::tabLabel() const
{
    return tr("Patient");
}

void ExtraInformationTab::setEditingEnabled(bool enabled)
{
    d->editingEnabled = enabled;
    foreach (QCheckBox* box, d->trialCheckboxes)
    {
        box->setEnabled(enabled);
    }
    foreach (QCheckBox* box, d->pathologyNetworkCheckboxes)
    {
        box->setEnabled(enabled);
    }
    d->smokerWidget->setEnabled(enabled);
}

void ExtraInformationTab::checkBoxChanged()
{
    d->edited = true;
}
