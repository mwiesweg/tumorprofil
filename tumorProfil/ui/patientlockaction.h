/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 25.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef PATIENTLOCKACTION_H
#define PATIENTLOCKACTION_H

#include <QAction>

#include "patient.h"

class PatientWriteLock;

class PatientLockAction : public QAction
{
    Q_OBJECT
public:
    explicit PatientLockAction(QObject *parent = 0);

    static void messageBox(QWidget* parent, const PatientWriteLock& lock);
    static void messageBox(QWidget* parent, const QString& lockInfo);

signals:

public slots:

    void setInformation(const PatientWriteLock* lock);
    void displayInformation();

protected:

    QString lockInfo;
};

class QToolBar;

class PatientLocking
{
public:
    PatientLocking();
    ~PatientLocking();

    void setPatient(const Patient::Ptr& p);
    void reset();
    bool patientEditable() const;
    void setReadOnly(bool readOnly);
    void addActionToToolBar(QToolBar* bar, bool withSpacer);

private:

    void acquirePatientWriteAccess();

    bool               readOnly;
    PatientLockAction* action;

    Patient::Ptr       patient;
    PatientWriteLock*  lock;
};

#endif // PATIENTLOCKACTION_H
