/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 03.08.2015
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "patientparsepage.h"

#include <QAction>
#include <QCheckBox>
#include <QDateEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QMenu>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>

#include "entityselectionwidgetv2.h"
#include "parser/parsertools.h"
#include "patiententerform.h"
#include "patientdisplay.h"
#include "patientmanager.h"
#include "pathologypropertyinfo.h"
#include "pathologypropertiestablemodel.h"
#include "pathologypropertiestableview.h"


PatientParsePage::PatientParsePage(const PatientParseResults& results)
    : results(results),
      patientEnterForm(0),
      entitySelectionWidget(0),
      initialDiagnosisEdit(0),
      nngmBox(0),
      model(0),
      filterModel(0),
      view(0)
{
    setTitle(tr("Befunde prüfen"));
    setSubTitle(tr("Prüfen Sie die folgenden Befunde und nicht erkannten Textabschnitte"));

    QVBoxLayout* layout = new QVBoxLayout;

    if (results.patient && results.patient->isValid())
    {
        // existing patient
        PatientDisplay* display = new PatientDisplay;
        display->setPatient(results.patientData);
        layout->addWidget(display);
    }
    else
    {
        // new patient, or missing data (gender)
        patientEnterForm = new PatientEnterForm;
        patientEnterForm->setValues(results.patientData);
        layout->addWidget(patientEnterForm);
    }

    entitySelectionWidget = new EntitySelectionWidgetV2;
    layout->addWidget(entitySelectionWidget);
    if (results.patient && results.patient->hasDisease() && results.patient->firstDisease().entity() != Pathology::UnknownEntity)
    {
        entitySelectionWidget->setEntity(results.patient->firstDisease().entity());
        entitySelectionWidget->setMode(EntitySelectionWidgetV2::DisplayMode);
    }
    else
    {
        entitySelectionWidget->setEntity(results.guessedEntity);
        entitySelectionWidget->setMode(EntitySelectionWidgetV2::EditMode);
    }
    connect(entitySelectionWidget, &EntitySelectionWidgetV2::entityChanged, this, &PatientParsePage::completeChanged);

    QHBoxLayout* idLayout = new QHBoxLayout;
    if (!results.patient)
    {
        idLayout->addWidget(new QLabel(tr("Erstdiagnose:")));
        initialDiagnosisEdit = new QDateEdit;
        idLayout->addWidget(initialDiagnosisEdit);
        initialDiagnosisEdit->setDate(ParserTools::initialDxFromResultDate(results.resultsDate));
    }
    nngmBox = new QCheckBox(tr("nNGM"));
    idLayout->addWidget(nngmBox);
    nngmBox->setChecked(results.nngm);
    idLayout->addStretch(1);
    layout->addLayout(idLayout);

    model = new PathologyPropertiesTableModel(this);
    model->setEditingEnabled(true);
    filterModel = new PathologyPropertiesTableFilterModel(this);
    view  = new PathologyPropertiesTableView;
    view->setModels(model, filterModel);
    layout->addWidget(view, 2);

    Pathology path;
    Pathology* existingPath = 0;
    if (results.patient)
    {
        // For better overview, we merge with existing properties. The calls here are the same as used by saveData.
        existingPath = ParserTools::findSuitablePathology(results.patient, results.resultsDate);
    }
    if (existingPath)
    {
        // Copy by value!
        qDebug() << "existing path" << existingPath << existingPath->properties.size() << existingPath->date;
        path = *existingPath;
        ParserTools::adjustSuitablePathology(&path, results.resultsDate);
        ParserTools::mergeNewProperties(&path, results.properties, ParserTools::ImportTool, *results.patient);
        qDebug() << "merged" << results.properties.size() << path.properties.size();
    }
    else
    {
        path.properties = results.properties;
        path.date       = results.resultsDate;
    }
    model->setPathology(path);

    QHBoxLayout* lineBelowTableView = new QHBoxLayout;

    QLabel* unknownTextLabel = new QLabel(tr("Nicht erkannte Passagen aus den Befunden:"));
    lineBelowTableView->addWidget(unknownTextLabel);

    lineBelowTableView->addStretch(1);
    QPushButton* addNewFindingButton = view->createAddEntryButton();
    lineBelowTableView->addWidget(addNewFindingButton);

    layout->addLayout(lineBelowTableView);

    QTextEdit* textEdit = new QTextEdit;
    textEdit->setReadOnly(true);
    textEdit->setPlainText(results.unrecognizedText);
    layout->addWidget(textEdit, 1);

    QPushButton* fullTextButton = new QPushButton(tr("Zeige Befundtext"));
    connect(fullTextButton, &QPushButton::clicked, this, &PatientParsePage::showFullText);
    layout->addWidget(fullTextButton);

    setLayout(layout);
}

void PatientParsePage::saveData()
{
    Patient patientData;
    if (!results.patient)
    {
        patientData = patientEnterForm->currentPatient();
    }
    QDate initialDx;
    if (initialDiagnosisEdit)
    {
        initialDx = initialDiagnosisEdit->date();
    }
    QList<Pathology> paths = model->pathologiesConsolidated();
    // paths size is 1 in our case
    PropertyList patientProps;
    if (nngmBox->isChecked())
    {
        patientProps << Property(PatientPropertyName::pathologyNetwork(), PatientPropertyValue::nngm());
    }
    results.patient = ParserTools::saveData(results.patient, patientData, initialDx, entitySelectionWidget->entity(),
                                            results.resultsDate,
                                            paths.isEmpty() ? PropertyList() : paths.first().properties,
                                            results.textPassages,
                                            ParserTools::ImportTool,
                                            patientProps);
}

bool PatientParsePage::isComplete() const
{
    if (!results.patient)
    {
        return entitySelectionWidget->entity() != Pathology::UnknownEntity && patientEnterForm->currentPatient().isValid();
    }
    return true;
}

void PatientParsePage::showFullText()
{
    QTextEdit* fullTextEdit = new QTextEdit;
    fullTextEdit->setReadOnly(true);
    fullTextEdit->setPlainText(results.text);
    fullTextEdit->show();
}
