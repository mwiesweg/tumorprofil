/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 30.08.2019
 *
 * Copyright (C) 2019 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "resectabilitystatusbox.h"

#include "databaseconstants.h"

ResectabilityStatusBox::ResectabilityStatusBox(QWidget *parent) :
    QComboBox(parent),
    m_initialIndex(-1)
{

}

bool ResectabilityStatusBox::changed() const
{
    return m_initialIndex != currentIndex();
}

void ResectabilityStatusBox::save(Disease &disease) const
{
    if (currentIndex() > 0)
    {
        disease.diseaseProperties.setProperty(DiseasePropertyName::resectabilityStatus(), currentText());
    }
    else
    {
        // no items, or item 0 which is a pseudo entry
        disease.diseaseProperties.removeProperty(DiseasePropertyName::resectabilityStatus());
    }
}

void ResectabilityStatusBox::load(const Disease &disease)
{
    clear();
    switch (disease.entity())
    {
    case Pathology::PancreaticCarcinoma:
        addItems(QStringList()
                 << "resectable"
                 << "borderline"
                 << "locally advanced");
    default:
        break;
    }

    if (count())
    {
        show();
        emit visibilityChanged(true);
        QString loc = disease.diseaseProperties.property(DiseasePropertyName::resectabilityStatus()).value;
        int index = findText(loc);
        if (index != -1)
        {
            setCurrentIndex(index);
        }
        else
        {
            setCurrentIndex(0);
        }
    }
    else
    {
        hide();
        emit visibilityChanged(false);
    }
    m_initialIndex = currentIndex();
}

void ResectabilityStatusBox::reset()
{
    clear();
    hide();
    emit visibilityChanged(false);
}
