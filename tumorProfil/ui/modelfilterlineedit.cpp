/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 21.06.2015
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "modelfilterlineedit.h"

#include <QAbstractItemView>
#include <QKeyEvent>
#include <QSortFilterProxyModel>

ModelFilterLineEdit::ModelFilterLineEdit(QAbstractItemView* view)
    : view(view)
{
    connect(this, SIGNAL(textChanged(QString)),
            view->model(), SLOT(setFilterFixedString(QString)));
    setPlaceholderText(tr("Suche Patient"));
}

void ModelFilterLineEdit::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        clear();
        return;
    }
    if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    {
        QModelIndex index = view->currentIndex();
        if (!index.isValid())
        {
            index = view->model()->index(0,0);
            view->setCurrentIndex(index);
        }
        emit selected(index);
        clear();
        view->scrollTo(index);
        return;
    }
    view->setCurrentIndex(QModelIndex());
    QLineEdit::keyPressEvent(e);

    QAbstractItemModel* model = view->model();
    int col = 0;
    QSortFilterProxyModel* filterModel = qobject_cast<QSortFilterProxyModel*>(model);
    if (filterModel)
    {
        col = filterModel->sortColumn();
    }
    for (int row=0; row<model->rowCount(); row++)
    {
        QModelIndex index = model->index(row, col);
        if (index.data().toString().startsWith(text()))
        {
            view->setCurrentIndex(index);
            return;
        }
    }
    view->setCurrentIndex(model->index(0,0));
}
