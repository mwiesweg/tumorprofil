/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 31.01.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "smokerwidget.h"

#include <QButtonGroup>
#include <QSpinBox>
#include <QRadioButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

static void create_button(const QString& label, PatientPropertyValue::SmokingStatus value,
                          QButtonGroup* group, QLayout* layout)
{
    QRadioButton* button = new QRadioButton(label);
    group->addButton(button, value);
    layout->addWidget(button);
}

SmokerWidget::SmokerWidget(QWidget *parent) :
    QWidget(parent),
    buttons(0)
{
    buttons = new QButtonGroup;
    QVBoxLayout* layout = new QVBoxLayout;

    QHBoxLayout* firstRow = new QHBoxLayout;

    create_button(tr("Unbekannt"), PatientPropertyValue::SmokingStatusUnknown, buttons, firstRow);
    create_button(tr("Nie"), PatientPropertyValue::NeverSmoker, buttons, firstRow);
    create_button(tr("Früher"), PatientPropertyValue::FormerSmoker, buttons, firstRow);
    create_button(tr("Aktiv"), PatientPropertyValue::CurrentSmoker, buttons, firstRow);

    packYears = new QSpinBox;
    packYears->setSuffix("PY");
    packYears->setSingleStep(5);
    packYears->setValue(0);
    packYears->setMaximum(999);
    packYears->setEnabled(false);
    firstRow->addWidget(packYears);

    layout->addLayout(firstRow);

    QHBoxLayout* secondRow = new QHBoxLayout;
    create_button(tr("Unbekannt - wahrscheinlich Nieraucher"), PatientPropertyValue::PresumedNeverSmoker, buttons, secondRow);
    create_button(tr("Unbekannt - wahrscheinlich Raucher"), PatientPropertyValue::PresumedSmoker, buttons, secondRow);
    connect(buttons, static_cast<void(QButtonGroup::*)(int)>(&QButtonGroup::idClicked),
            this, &SmokerWidget::buttonClicked);
    layout->addLayout(secondRow);


    setLayout(layout);
}

PatientPropertyValue::SmokingStatus SmokerWidget::smokingStatus() const
{
    return static_cast<PatientPropertyValue::SmokingStatus>(buttons->checkedId());
}

void SmokerWidget::setSmokingStatus(PatientPropertyValue::SmokingStatus status)
{
    buttons->button(status)->setChecked(true);
    packYears->setEnabled(status == PatientPropertyValue::FormerSmoker || status == PatientPropertyValue::CurrentSmoker);
    emit smokingStatusChanged(status);
}

void SmokerWidget::save(Patient::Ptr patient) const
{
    patient->setSmokingStatus(smokingStatus(), packYears->value());
}

void SmokerWidget::load(const Patient::Ptr& patient)
{
    setSmokingStatus(patient->smokingStatus());
    packYears->setValue(patient->packYears());
}

void SmokerWidget::buttonClicked(int id)
{
    setSmokingStatus(static_cast<PatientPropertyValue::SmokingStatus>(id));
    if (packYears->isEnabled())
    {
        packYears->selectAll();
        packYears->setFocus();
    }
}
