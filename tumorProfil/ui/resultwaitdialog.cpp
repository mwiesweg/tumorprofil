/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 25.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "resultwaitdialog.h"

ResultWaitDialog::ResultWaitDialog(QWidget* parent,
                                   const QString &actionTitle,
                                   bool allowCancel,
                                   Qt::WindowModality windowModality)
    : QProgressDialog(parent)
{
    setWindowModality(windowModality);
    setLabelText(actionTitle);
    if (!allowCancel)
    {
        setCancelButton(0);
    }
    setResult(0);
}

bool ResultWaitDialog::waitForResult(QFutureWatcherBase *watcher)
{
    // already finished?
    if (watcher->isFinished())
    {
        return true;
    }
    setResult(0);
    connect(watcher, &QFutureWatcherBase::progressTextChanged, this, &QProgressDialog::setLabelText);
    connect(watcher, &QFutureWatcherBase::progressRangeChanged, this, &QProgressDialog::setRange);
    connect(watcher, &QFutureWatcherBase::progressValueChanged, this, &QProgressDialog::setValue);
    connect(watcher, &QFutureWatcherBase::finished, this, &QProgressDialog::accept);
    return exec() == QDialog::Accepted;
}

void ResultWaitDialog::connectTo(QObject *obj)
{
    setResult(0);
    connect(obj, SIGNAL(progressTextChanged(QString)), this, SLOT(setLabelText(QString)));
    connect(obj, SIGNAL(progressRangeChanged(int,int)), this, SLOT(setRange(int,int)));
    connect(obj, SIGNAL(progressValueChanged(int)), this, SLOT(setValue(int)));
    connect(obj, SIGNAL(finished()), this, SLOT(accept()));
    const QMetaObject* meta = obj->metaObject();
    if (meta->indexOfMethod("cancel()") != -1)
    {
        connect(this, SIGNAL(canceled()), obj, SLOT(cancel()));
    }
}

bool ResultWaitDialog::waitForResult()
{
    // already finished?
    if (result() == QDialog::Accepted)
    {
        return true;
    }
    return exec() == QDialog::Accepted;
}

