/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 21.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labtablemodel.h"

#include <QDebug>
#include <QPair>

class LabTableModel::LabTableModelPriv
{
public:

    LabTableModelPriv()
        : wasEdited(false)
    {
    }

    LabResults results;
    QStringList mask;
    bool wasEdited;
    QMap<QString, QStringList> masks;
    QMap<QString, QVariant::Type> types;
    QStringList   maskNames;

    void loadMasks()
    {
        QPair<QStringList, QMap<QString, QStringList> > maskPair = LabResults::masks();
        maskNames = maskPair.first;
        masks = maskPair.second;
        types = LabResults::types();
    }
};

LabTableModel::LabTableModel()
    : d(new LabTableModelPriv)
{
    d->loadMasks();
}

LabTableModel::~LabTableModel()
{
    delete d;
}

void LabTableModel::setResults(const LabResults &results)
{
    beginResetModel();
    d->results = results;
    endResetModel();
    if (d->mask.isEmpty())
    {
        setMaskFromResults();
    }
}

const LabResults &LabTableModel::results() const
{
    return d->results;
}

const LabSeries &LabTableModel::series(int index) const
{
    return d->results.series[index];
}

const LabSeries &LabTableModel::series(const QModelIndex &index) const
{
    return series(index.column());
}

bool LabTableModel::changed() const
{
    return d->wasEdited;
}

void LabTableModel::setMask(const QString &maskName)
{
    setMask(d->masks.value(maskName));
}

void LabTableModel::setMask(const QStringList &mask)
{
    if (mask != d->mask)
    {
        beginResetModel();
        d->mask = mask;
        endResetModel();
    }
}

void LabTableModel::setMaskFromResults()
{
    setMask(d->results.series.names().values());
}

QStringList LabTableModel::maskNames() const
{
    return d->maskNames;
}

QStringList LabTableModel::maskEntries(const QString &maskName) const
{
    return d->masks.value(maskName);
}

int LabTableModel::addSeries(const LabSeries &s)
{
    QPair<int, bool> position = d->results.series.simulateAdd(s);
    int index = position.first;
    int willAdd = position.second;
    if (willAdd)
    {
        beginInsertColumns(QModelIndex(), index, index);
        d->results.series.add(s);
        d->wasEdited = true;
        endInsertColumns();
    }
    else
    {
        // merge, have series with same date and type already
        // do nothing, the current design does not add rows to the model
    }
    return index;
}

LabSeries LabTableModel::takeSeries(int index)
{
    beginRemoveColumns(QModelIndex(), index, index);
    LabSeries s = d->results.series.takeAt(index);
    d->wasEdited = true;
    endRemoveColumns();
    return s;
}

void LabTableModel::setSeriesDateTime(int section, const QDateTime &dateTime)
{
    setHeaderData(section, Qt::Horizontal, dateTime, Qt::EditRole);
}

int LabTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return d->mask.size();
}

int LabTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return d->results.series.size();
}

QVariant LabTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }
    QString name = d->mask[index.row()];
    QVariant value = d->results.series[index.column()].values.valueForName(name);

    switch (role)
    {
    case Qt::DisplayRole:
        return value;
    case Qt::EditRole:
        if (value.isValid())
        {
            return value;
        }
        else
        {
            return QVariant(d->types.value(name, QVariant::Double));
        }
    default:
        break;
    }
    return QVariant();
}

QVariant LabTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }
    if (orientation == Qt::Horizontal)
    {
        return d->results.series[section].dateLabel();
    }
    else
    {
        return d->mask[section];
    }
}

bool LabTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole || !index.isValid())
    {
        return false;
    }

    QString name = d->mask[index.row()];
    d->results.series[index.column()].values.edit(name, value);
    d->wasEdited = true;
    emit dataChanged(index, index);
    return true;
}

bool LabTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (orientation == Qt::Vertical || role != Qt::EditRole || !value.canConvert(QMetaType::QDateTime))
    {
        return false;
    }

    QDateTime newDateTime = value.toDateTime();
    if (d->results.series[section].dateTime == newDateTime)
    {
        return true;
    }
    if (d->results.series.find(newDateTime, d->results.series[section].type) != -1)
    {
        qDebug() << "Attempt to change date of LabSeries from" << d->results.series[section].dateTime
                 << "to" << newDateTime << ", but there is already a series with this date and time. Refusing; please catch in UI.";
        return false;
    }

    LabSeries s = takeSeries(section);
    s.dateTime = value.toDateTime();
    addSeries(s);
    return true;
}

Qt::ItemFlags LabTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return Qt::ItemFlags();
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}
