/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labtableview.h"

#include <QKeyEvent>

LabTableView::LabTableView()
{
    setModel(new LabTableModel);

    setAlternatingRowColors(true);
    setEditTriggers(NoEditTriggers);
    setSelectionMode(SingleSelection);
}

LabTableModel *LabTableView::model() const
{
    return static_cast<LabTableModel*>(QTableView::model());
}

LabSeries LabTableView::currentSeries() const
{
    if (!currentIndex().isValid())
    {
        return LabSeries();
    }
    return model()->series(currentIndex());
}

void LabTableView::removeCurrentSeries()
{
    if (!currentIndex().isValid())
    {
        return;
    }
    model()->takeSeries(currentIndex().column());
}

void LabTableView::setEditingEnabled(bool enabled)
{
    setEditTriggers(enabled ? AnyKeyPressed : NoEditTriggers);
}

void LabTableView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    if (current.isValid() != previous.isValid())
    {
        emit hasCurrentIndexChanged(current.isValid());
    }
}

bool LabTableView::edit(const QModelIndex &index, QAbstractItemView::EditTrigger trigger, QEvent *event)
{
    bool r = QTableView::edit(index, trigger, event);
    return r;
}

QModelIndex LabTableView::moveCursor(QAbstractItemView::CursorAction cursorAction, Qt::KeyboardModifiers modifiers)
{
    if (cursorAction == MoveNext)
    {
        cursorAction = MoveDown;
    }
    else if (cursorAction == MovePrevious)
    {
        cursorAction = MoveUp;
    }
    return QTableView::moveCursor(cursorAction, modifiers);
}

void LabTableView::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Delete)
    {
        QModelIndex current = currentIndex();
        if (current.isValid())
        {
            model()->setData(current, QVariant(), Qt::EditRole);
            event->accept();
            return;
        }
    }
    QTableView::keyPressEvent(event);
}

