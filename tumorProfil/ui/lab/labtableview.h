/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABTABLEVIEW_H
#define LABTABLEVIEW_H

#include <QTableView>

#include "labtablemodel.h"

class LabTableView : public QTableView
{
    Q_OBJECT
public:
    LabTableView();

    LabTableModel* model() const;

    LabSeries currentSeries() const;

public slots:

    void removeCurrentSeries();
    void setEditingEnabled(bool enabled);

signals:

    void hasCurrentIndexChanged(bool hasCurrentIndex);

protected:

    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
    bool edit(const QModelIndex &index, EditTrigger trigger, QEvent *event);
    QModelIndex moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers);
    void keyPressEvent(QKeyEvent* event);
};

#endif // LABTABLEVIEW_H
