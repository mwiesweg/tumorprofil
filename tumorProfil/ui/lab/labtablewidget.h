/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABTABLEWIDGET_H
#define LABTABLEWIDGET_H

#include "labtableview.h"
#include "patient.h"

class QPushButton;

class LabTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LabTableWidget(QWidget *parent = 0, const QString& configName = "LabTableWidget");
    ~LabTableWidget();

    void setPatient(const Patient::Ptr& p);
    void save();

signals:

public slots:

    void addSeries();
    void removeCurrentSeries();
    void editSeriesDate(int section);
    void setEditingEnabled(bool enabled);
    void setMask(const QString& maskName);

protected slots:

    void hasCurrentIndexChanged(bool has);

private:

    class LabTableWidgetPriv;
    LabTableWidgetPriv* const d;
};

#endif // LABTABLEWIDGET_H
