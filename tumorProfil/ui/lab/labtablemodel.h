/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 21.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef LABTABLEMODEL_H
#define LABTABLEMODEL_H

#include <QAbstractTableModel>
#include "labresults.h"


class LabTableModel : public QAbstractTableModel
{
public:
    LabTableModel();
    ~LabTableModel();

    void setResults(const LabResults& results);
    const LabResults &results() const;
    const LabSeries &series(int index) const;
    const LabSeries &series(const QModelIndex &index) const;
    bool changed() const;

    void setMask(const QString& maskName);
    void setMask(const QStringList& names);
    void setMaskFromResults();
    QStringList maskNames() const;
    QStringList maskEntries(const QString& maskName) const;

    int addSeries(const LabSeries& s);
    LabSeries takeSeries(int index);

    void setSeriesDateTime(int section, const QDateTime& dateTime);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

protected:

    class LabTableModelPriv;
    LabTableModelPriv* const d;
};

#endif // LABTABLEMODEL_H
