/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "labtablewidget.h"

#include <QComboBox>
#include <QDateEdit>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFile>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QMessageBox>
#include <QPair>
#include <QPointer>
#include <QPushButton>
#include <QSettings>
#include <QVBoxLayout>

#include "constants.h"
#include "patientmanager.h"

class LabTableWidget::LabTableWidgetPriv
{
public:

    LabTableWidgetPriv()
        : view(0),
          addButton(0),
          clearButton(0),
          maskBox(0),
          lastEnteredDate(QDate::currentDate()),
          editingEnabled(false)
    {
    }

    Patient::Ptr  patient;
    LabTableView* view;
    QPushButton*  addButton;
    QPushButton*  clearButton;
    QComboBox*    maskBox;
    QDate         lastEnteredDate;
    bool          editingEnabled;
    QString       configName;
};

namespace
{
    const char* configLastLabMask = "Last Lab Mask";
}

LabTableWidget::LabTableWidget(QWidget *parent, const QString &configName)
    : QWidget(parent),
      d(new LabTableWidgetPriv)
{
    d->configName = configName;
    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(d->configName);

    d->view = new LabTableView;
    QFont f = d->view->font();
    f.setPointSize(std::min(9, f.pointSize()));
    d->view->setFont(f);
    d->view->verticalHeader()->setDefaultSectionSize(d->view->verticalHeader()->minimumSectionSize());
    connect(d->view->horizontalHeader(), &QHeaderView::sectionDoubleClicked,
            this, &LabTableWidget::editSeriesDate);
    connect(d->view, &LabTableView::hasCurrentIndexChanged,
            this, &LabTableWidget::hasCurrentIndexChanged);

    QHBoxLayout* buttonLayout = new QHBoxLayout;

    d->addButton = new QPushButton(QIcon::fromTheme("add"), tr("Weiteres Datum hinzufügen"));
    buttonLayout->addWidget(d->addButton);
    connect(d->addButton, &QPushButton::clicked,
            this, &LabTableWidget::addSeries);

    buttonLayout->addStretch();

    d->clearButton = new QPushButton(QIcon::fromTheme("delete"), tr("Ausgewähltes Datum löschen"));
    buttonLayout->addWidget(d->clearButton);
    d->clearButton->setEnabled(false);
    connect(d->clearButton, &QPushButton::clicked,
            this, &LabTableWidget::removeCurrentSeries);

    d->maskBox = new QComboBox;
    d->maskBox->addItems(d->view->model()->maskNames());
    d->maskBox->setEditable(false);
    d->maskBox->setCurrentIndex(0);
    connect(d->maskBox, &QComboBox::currentTextChanged,
            this, &LabTableWidget::setMask);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(buttonLayout);
    layout->addWidget(d->view, 1);
    layout->addWidget(d->maskBox, 0, Qt::AlignLeft);
    setLayout(layout);

    setEditingEnabled(false);

    int lastMaskIndex = d->maskBox->findText(settings.value(configLastLabMask).toString());
    if (lastMaskIndex != -1)
    {
        d->maskBox->setCurrentIndex(lastMaskIndex);
    }
    setMask(d->maskBox->currentText());
}

LabTableWidget::~LabTableWidget()
{
    delete d;
}

void LabTableWidget::setPatient(const Patient::Ptr &p)
{
    d->patient = p;
    if (p)
    {
        LabResults results = d->patient->labResults();
        d->view->model()->setResults(results);
    }
    else
    {
        d->view->model()->setResults(LabResults());
    }
}

void LabTableWidget::save()
{
    if (!d->patient || !d->view->model()->changed() || !d->editingEnabled)
    {
        return;
    }
    PatientManager::instance()->setLabResults(d->patient, d->view->model()->results());
}

static QDateTime dateTimeDialog(QWidget* parent,
                                const QDateTime& defaultDateTime,
                                const QString& windowTitle)
{
    bool withTime = defaultDateTime.time() != QTime(0,0,0,0);

    QPointer<QDialog> dialog = new QDialog(parent);
    dialog->setWindowTitle(windowTitle);
    QLabel* label = new QLabel(parent->tr("Datum:"));
    QDateTimeEdit* edit = withTime ? new QDateTimeEdit : new QDateEdit;
    edit->setDateTime(defaultDateTime);

    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(edit);
    layout->addWidget(buttons);
    dialog->setLayout(layout);
    QObject::connect(buttons, &QDialogButtonBox::accepted, dialog.data(), &QDialog::accept);
    QObject::connect(buttons, &QDialogButtonBox::rejected, dialog.data(), &QDialog::reject);

    if (dialog->exec() == QDialog::Accepted)
    {
        return edit->dateTime();
    }
    return QDateTime();
}

void LabTableWidget::addSeries()
{
    QDateTime dateTime = dateTimeDialog(this, d->lastEnteredDate.startOfDay(), tr("Neue Laborwerte"));
    if (dateTime.isValid())
    {
        LabSeries s(dateTime, LabSeries::clinicalType());
        int index = d->view->model()->addSeries(s);
        d->lastEnteredDate = dateTime.date();
        d->view->setCurrentIndex(d->view->model()->index(0, index));
    }
}

void LabTableWidget::removeCurrentSeries()
{
    QString dateLabel = d->view->currentSeries().dateLabel();
    if (QMessageBox::question(this,
                              tr("Laborwerte löschen"),
                              tr("Möchten Sie die Laborwerte vom %1 löschen?").arg(dateLabel))
            == QMessageBox::Yes)
    {
        d->view->removeCurrentSeries();
    }
}

void LabTableWidget::editSeriesDate(int section)
{
    if (!d->editingEnabled)
    {
        return;
    }
    QDateTime previous = d->view->model()->series(section).dateTime;
    QDateTime dateTime = dateTimeDialog(this, previous, tr("Datum ändern"));
    if (dateTime.isValid())
    {
        if (d->view->model()->results().series.find(dateTime, LabSeries::clinicalType()) != -1)
        {
            QMessageBox::warning(this, tr("Fehler beim Ändern des Datums"),
                                 tr("Es gibt bereits Laborwerte mit diesem Datum!"));
            return;
        }
        d->view->model()->setSeriesDateTime(section, dateTime);
    }
}

void LabTableWidget::setEditingEnabled(bool enabled)
{
    d->editingEnabled = enabled;
    d->view->setEditingEnabled(enabled);
    d->addButton->setEnabled(enabled);
    hasCurrentIndexChanged(d->view->currentIndex().isValid());
}

void LabTableWidget::setMask(const QString &maskName)
{
    d->view->model()->setMask(maskName);

    QSettings settings(ORGANIZATION, APPLICATION);
    settings.beginGroup(d->configName);
    settings.setValue(configLastLabMask, maskName);
}

void LabTableWidget::hasCurrentIndexChanged(bool has)
{
    d->clearButton->setEnabled(has && d->editingEnabled);
}
