/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 31.01.2012
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef SMOKERWIDGET_H
#define SMOKERWIDGET_H

#include <QWidget>

#include "databaseconstants.h"
#include "patient.h"

class QButtonGroup;
class QSpinBox;

class SmokerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SmokerWidget(QWidget *parent = 0);

    PatientPropertyValue::SmokingStatus smokingStatus() const;
    
signals:

    void smokingStatusChanged(PatientPropertyValue::SmokingStatus);
    
public slots:

    void setSmokingStatus(PatientPropertyValue::SmokingStatus status);
    void save(Patient::Ptr patient) const;
    void load(const Patient::Ptr& patient);

private slots:

    void buttonClicked(int id);

protected:

    QButtonGroup *buttons;
    QSpinBox     *packYears;
};

#endif // SMOKERWIDGET_H
