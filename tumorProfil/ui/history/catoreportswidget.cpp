/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#include "catoreportswidget.h"

#include <QDebug>
#include <QHeaderView>
#include <QLabel>
#include <QListWidget>
#include <QTableWidget>
#include <QVBoxLayout>

#include "databaseaccess.h"
#include "patientdb.h"
#include "systemictherapy.h"

class CatoReportsWidget::CatoReportsWidgetPriv
{
public:
    CatoReportsWidgetPriv()
        : patientList(0),
          medicationTable(0)
    {

    }

    QListWidget*    patientList;
    QTableWidget*   medicationTable;
};

class CatoReportsDateTableItem : public QTableWidgetItem
{
public:
    CatoReportsDateTableItem(const QDate& date)
        : QTableWidgetItem(date.toString("dd.MM.yyyy"))
    {
        setData(Qt::UserRole, date);
        setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    }

    bool operator<(const QTableWidgetItem& other) const
    {
        return data(Qt::UserRole).toDate() < other.data(Qt::UserRole).toDate();
    }
};

namespace
{
enum
{
    ColumnDate = 0,
    ColumnMedication = 1
};
}

CatoReportsWidget::CatoReportsWidget()
    : d(new CatoReportsWidgetPriv)
{
    QVBoxLayout* layout = new QVBoxLayout;

    QLabel* patientLabel = new QLabel(tr("Aus CATO:"));
    layout->addWidget(patientLabel);
    d->patientList = new QListWidget;
    layout->addWidget(d->patientList, 1);
    connect(d->patientList, &QListWidget::currentItemChanged, this, &CatoReportsWidget::currentPatientChanged);

    //QLabel* medicationLabel = new QLabel(tr("Medikationen aus CATO:"));
    //layout->addWidget(medicationLabel);
    d->medicationTable = new QTableWidget();
    d->medicationTable->setColumnCount(2);
    d->medicationTable->horizontalHeader()->setStretchLastSection(true);
    d->medicationTable->horizontalHeader()->setVisible(false);
    d->medicationTable->verticalHeader()->setVisible(false);
    d->medicationTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    //d->medicationTable->setHorizontalHeaderLabels(QStringList() << tr("Datum") << tr("Medikation"));
    layout->addWidget(d->medicationTable, 4);
    connect(d->medicationTable, &QTableWidget::itemActivated, this, &CatoReportsWidget::medicationActivated);

    setLayout(layout);
}

CatoReportsWidget::~CatoReportsWidget()
{
    delete d;
}

void CatoReportsWidget::setPatient(const Patient &patientData)
{
    d->patientList->clear();
    if (patientData.isNull())
    {
        return;
    }
    QHash<int, QString> ids = DatabaseAccess().db()->possiblePatientTextIdentifiers(patientData);
    for (QHash<int, QString>::const_iterator it = ids.begin(); it != ids.end(); ++it)
    {
        QListWidgetItem* item = new QListWidgetItem(it.value());
        item->setData(Qt::UserRole, it.key());
        d->patientList->addItem(item);
    }
    d->patientList->setCurrentRow(0);
}

void CatoReportsWidget::currentPatientChanged(QListWidgetItem *item)
{
    d->medicationTable->clear();
    d->medicationTable->setSortingEnabled(false);
    if (!item)
    {
        return;
    }
    QList<QVariant> values = DatabaseAccess().db()->findValuesForIdentifier(PatientDB::CATOReports, item->data(Qt::UserRole).toInt());
    d->medicationTable->setRowCount(values.size() / 2);
    for (int i=0; i+1<values.size(); i += 2)
    {
        const int row = i/2;

        QTableWidgetItem* dateItem = new CatoReportsDateTableItem(values[i+1].toDate());

        QString medication = values[i].toString();
        QString substanceName, tradeName;
        substanceName = SystemicTherapy::toCanonicalSubstance(medication, &tradeName);
        if (!tradeName.isNull())
        {
            medication.replace(tradeName, substanceName);
        }
        QTableWidgetItem* medicationItem = new QTableWidgetItem(medication);
        medicationItem->setData(Qt::UserRole, values[i]);
        medicationItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

        d->medicationTable->setItem(row, ColumnMedication, medicationItem);
        d->medicationTable->setItem(row, ColumnDate, dateItem);
    }
    d->medicationTable->sortItems(ColumnDate);
}

void CatoReportsWidget::medicationActivated(QTableWidgetItem *item)
{
    if (!item)
    {
        return;
    }
    QTableWidgetItem* dateItem = d->medicationTable->item(item->row(), ColumnDate);
    QTableWidgetItem* medicationItem = d->medicationTable->item(item->row(), ColumnMedication);
    emit activated(dateItem->data(Qt::UserRole).toDate(), medicationItem->data(Qt::UserRole).toString());
}
