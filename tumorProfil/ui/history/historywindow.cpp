/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 28.02.2013
 *
 * Copyright (C) 2012 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "historywindow.h"

// Qt includes

#include <QAction>
#include <QCheckBox>
#include <QDateEdit>
#include <QLineEdit>
#include <QDebug>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QMenu>
#include <QPushButton>
#include <QScrollArea>
#include <QToolBar>
#include <QToolButton>
#include <QSortFilterProxyModel>
#include <QSplitter>
#include <QListWidget>
#include <QTreeView>
#include <QVBoxLayout>

// Local includes

#include "historypatientlistview.h"
#include "patientpropertymodelviewadapter.h"
#include "diseasehistorymodel.h"
#include "historyelementeditwidget.h"
#include "history/catoreportswidget.h"
#include "history/historyiterator.h"
#include "modelfilterlineedit.h"
#include "patientdisplay.h"
#include "patientpropertyfiltermodel.h"
#include "patientlockaction.h"
#include "patientmanager.h"
#include "patientpropertymodel.h"
#include "primarytumorlocationbox.h"
#include "resectabilitystatusbox.h"
#include "visualhistorywidget.h"

class HistoryWindowProofReadOutput : public HistoryProofreader
{
public:
    HistoryWindowProofReadOutput(QListWidget* lw) : list(lw)
    {
    }

    void problem(const HistoryElement *element, const QString &problem)
    {
        QListWidgetItem* item = new QListWidgetItem(problem);
        item->setData(Qt::UserRole, QVariant::fromValue<HistoryElement*>((HistoryElement*)element));
        list->addItem(item);
    }

    void reset()
    {
        list->clear();
    }

    void elementWasRemoved(const HistoryElement *element)
    {
        for (int i=0; i<list->count(); )
        {
            if (list->item(i)->data(Qt::UserRole).value<HistoryElement*>() == element)
            {
                delete list->takeItem(i);
            }
            else
            {
                i++;
            }
        }
    }

protected:

    QListWidget* list;
};

class HistoryElementSortModel : public QSortFilterProxyModel
{
public:

    HistoryElementSortModel(QObject* parent)
        : QSortFilterProxyModel(parent)
    {
        setDynamicSortFilter(true);
    }

    virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const
    {
        HistoryElement* l = DiseaseHistoryModel::retrieveElement(left);
        HistoryElement* r = DiseaseHistoryModel::retrieveElement(right);
        if (!l || !r)
        {
            return false;
        }
        if (l->date.isValid() && r->date.isValid())
        {
            if (l->date == r->date)
            {
                if (l->is<Therapy>() || r->is<Therapy>())
                {
                    if (l->is<Therapy>() != r->is<Therapy>())
                    {
                        // only one is Therapy
                        return !l->is<Therapy>();
                    }
                    else
                    {
                        // both are therapies
                        return l->as<Therapy>()->end < r->as<Therapy>()->end;
                    }
                }
            }
            return l->date < r->date;
        }
        else
        {
            // only one is valid
            if (l->date.isValid() || r->date.isValid())
            {
                // valid date is "less than" invalid
                return l->date.isValid();
            }
            return false;
        }
    }
};

class HistoryColorProvider : public RoleDataProvider
{
public:
    virtual QVariant data(const PatientModel*, const QModelIndex&, const Patient::Ptr& p)
    {
        QHash<int,QVariant>::const_iterator it = hash.find(p->id);
        if (it != hash.end())
        {
            return it.value();
        }
        if (!p->hasDisease())
        {
            return QVariant();
        }
        const Disease& disease = p->firstDisease();
        CurrentStateIterator cs(disease.history);
        QColor c = VisualHistoryWidget::colorForState(cs.effectiveState());
        if (c.isValid())
        {
            if (c == QColor(Qt::white))
            {
                c = Qt::lightGray;
            }
            c.setAlpha(100);
            hash[p->id] = c;
            return c;
        }
        else
        {
            hash[p->id] = QVariant();
            return QVariant();
        }
    }
    QHash<int,QVariant> hash;
};

class HistoryWindow::Private
{
public:
    Private()
        : viewSplitter(0),
          patientView(0),
          historyModel(0),
          heading(0),
          clearButton(0),
          mainPanelLayout(0),
          editWidgetLayout(0),
          visualHistoryWidget(0),
          patientDisplay(0),
          sortModel(0),
          historyView(0),
          catoWidget(0),
          addBar(0),
          initialDiagnosisEdit(0),
          tnmEdit(0),
          primaryTumorLocationBox(0),
          resectabilityStatusBox(0),
          lastDocumentation(0),
          lastDocumentationDate(0),
          lastValidation(0),
          lastValidationDate(0),
          proofOutput(0),
          colorProvider(0),
          proofReader(0),
          currentElement(0),
          currentWidget(0),
          readOnly(false)
    {
    }

    QSplitter              *viewSplitter;
    HistoryPatientListView *patientView;
    DiseaseHistoryModel    *historyModel;
    QLabel                 *heading;
    QPushButton            *clearButton;
    QVBoxLayout            *mainPanelLayout;
    QVBoxLayout            *editWidgetLayout;
    VisualHistoryWidget    *visualHistoryWidget;
    PatientDisplay         *patientDisplay;
    HistoryElementSortModel*sortModel;
    QTreeView              *historyView;
    CatoReportsWidget      *catoWidget;
    QToolBar               *addBar;
    QDateEdit              *initialDiagnosisEdit;
    QLineEdit              *tnmEdit;
    PrimaryTumorLocationBox*primaryTumorLocationBox;
    ResectabilityStatusBox* resectabilityStatusBox;
    QCheckBox              *lastDocumentation;
    QDateEdit              *lastDocumentationDate;
    QCheckBox              *lastValidation;
    QDateEdit              *lastValidationDate;
    QListWidget            *proofOutput;
    HistoryColorProvider   *colorProvider;
    HistoryWindowProofReadOutput* proofReader;

    Patient::Ptr              currentPatient;
    HistoryElement           *currentElement;
    HistoryElementEditWidget *currentWidget;
    PatientLocking            lock;
    bool                      readOnly;
};

Q_GLOBAL_STATIC(QList<HistoryWindow*>, currentWindows)

HistoryWindow::HistoryWindow(QWidget *parent)
    : FilterMainWindow(parent),
      d(new Private)
{
    currentWindows->append(this);

    setWindowTitle(tr("Krankheitsverlauf"));
    setWindowIcon(QIcon::fromTheme("calendar"));
    setupToolbar();
    setupView();
    setCurrentPatient(Patient::Ptr());
    setCurrentElement(0);

    QAction* toggleCatoAction = toolBar()->addAction(QIcon::fromTheme("server_chart"), tr("Zeige CATO"));
    toggleCatoAction->setCheckable(true);
    connect(toggleCatoAction, &QAction::toggled, this, &HistoryWindow::toggleShowCato);

    d->lock.addActionToToolBar(toolBar(), true);

    //QAction* requestMainWindowAction = toolBar()->addAction(QIcon::fromTheme("folder_table"), tr("Öffne Befundeingabe"));
    //connect(requestMainWindowAction, &QAction::toggled, this, &HistoryWindow::requestMainWindowActivated);
}

HistoryWindow::~HistoryWindow()
{
    currentWindows->removeOne(this);
    setCurrentPatient(Patient::Ptr());
    delete d->colorProvider;
    delete d;
}

void HistoryWindow::closeEvent(QCloseEvent *e)
{
    applyData();
    FilterMainWindow::closeEvent(e);
}

Patient::Ptr HistoryWindow::currentPatient() const
{
    return d->currentPatient;
}

HistoryElement* HistoryWindow::currentElement() const
{
    return d->currentElement;
}

DiseaseHistory HistoryWindow::currentHistory() const
{
    if (!d->currentPatient)
    {
        return DiseaseHistory();
    }
    return d->historyModel->history();
}

bool HistoryWindow::readOnly() const
{
    return d->readOnly;
}

HistoryWindow *HistoryWindow::currentEditableWindow()
{
    foreach (HistoryWindow* w, *currentWindows)
    {
        if (w->d->readOnly)
        {
            return w;
        }
    }
    return 0;
}

void HistoryWindow::setReadOnly(bool readOnly)
{
    d->readOnly = readOnly;
    d->lock.setReadOnly(d->readOnly);
    applyReadOnlyStatus();
}

void HistoryWindow::toggleShowCato(bool show)
{
    d->catoWidget->setVisible(show);
    if (show)
    {
        d->catoWidget->setPatient(d->currentPatient ? *d->currentPatient : Patient());
    }
}

void HistoryWindow::applyData()
{
    if (d->readOnly || !d->lock.patientEditable())
    {
        return;
    }
    if (d->currentPatient)
    {
        if (d->currentWidget)
        {
            d->currentWidget->applyToElement();
        }
        bool changed = false;

        DiseaseHistory history = d->historyModel->history();
        if (!d->currentPatient->hasDisease())
        {
            if (history.isEmpty())
            {
                return;
            }
            d->currentPatient->diseases << Disease();
        }

        Disease& disease = d->currentPatient->firstDisease();
        DiseaseHistory previousHistory = disease.history;
        history.sort();

        // update history properties
        QDate lastDoc = history.lastDocumentation();
        if (d->lastDocumentation->isChecked())
        {
            if (lastDoc != d->lastDocumentationDate->date())
            {
                changed = true;
                history.setLastDocumentation(d->lastDocumentationDate->date());
            }
        }
        else
        {
            if (lastDoc.isValid())
            {
                changed = true;
                history.setLastDocumentation(QDate());
            }
        }

        QDate lastValid = history.lastValidation();
        if (d->lastValidation->isChecked())
        {
            if (lastValid != d->lastValidationDate->date())
            {
                changed = true;
                history.setLastValidation(d->lastValidationDate->date());
            }
        }
        else
        {
            if (lastValid.isValid())
            {
                changed = true;
                history.setLastValidation(QDate());
            }
        }

        if (changed || history != previousHistory)
        {
            /*qDebug() << "history changed from";
            qDebug() << previousHistory.toXml();
            qDebug() << "to new history";
            qDebug() << history.toXml();*/
            disease.history = history;
            changed = true;
        }
        //qDebug() << "History of patient" << d->currentPatient->surname;
        //qDebug() << d->currentPatient->firstDisease().history().toXml();
        if (disease.initialDiagnosis != d->initialDiagnosisEdit->date())
        {
            disease.initialDiagnosis = d->initialDiagnosisEdit->date();
            changed = true;
        }
        if (disease.initialTNM.toText() != d->tnmEdit->text())
        {
            disease.initialTNM.setTNM(d->tnmEdit->text());
            changed = true;
        }

        if (d->primaryTumorLocationBox->changed())
        {
            d->primaryTumorLocationBox->save(disease);
            changed = true;
        }

        if (d->resectabilityStatusBox->changed())
        {
            d->resectabilityStatusBox->save(disease);
            changed = true;
        }

        if (changed)
        {
            PatientManager::instance()->updateData(d->currentPatient,
                                                   PatientManager::ChangedDiseaseHistory |
                                                   PatientManager::ChangedDiseaseMetadata |
                                                   PatientManager::ChangedDiseaseProperties);
            //PatientManager::instance()->historySecurityCopy(d->currentPatient, "history", d->currentPatient->firstDisease().history.toXml());

            d->colorProvider->hash.remove(d->currentPatient->id);
        }
        else
        {
            //qDebug() << "Data unchanged, skipping DB write operation";
        }
    }
}

void HistoryWindow::setCurrentPatient(const Patient::Ptr &p)
{
    applyData();
    setCurrentElement(0);
    d->lock.reset();
    d->currentPatient = p;
    d->initialDiagnosisEdit->setDate(QDate::currentDate());
    d->proofReader->reset();

    if (d->currentPatient && d->currentPatient->hasDisease())
    {
        PatientManager::instance()->checkIsUpToDate(p);
        d->lock.setPatient(d->currentPatient);
        const Disease& disease = d->currentPatient->firstDisease();
        DiseaseHistory history = disease.history;
        history.detach();
        d->historyModel->setHistory(history);
        d->initialDiagnosisEdit->setDate(disease.initialDiagnosis);
        d->tnmEdit->setText(disease.initialTNM.toText());
        d->primaryTumorLocationBox->load(disease);
        d->resectabilityStatusBox->load(disease);
        d->visualHistoryWidget->setHistory(history);
        QDate lastDoc = history.lastDocumentation();
        d->lastDocumentation->setChecked(lastDoc.isValid());
        d->lastDocumentationDate->setDate(lastDoc.isValid() ? lastDoc : QDate::currentDate());
        QDate lastValid = history.lastValidation();
        d->lastValidation->setChecked(lastValid.isValid());
        d->lastValidationDate->setDate(lastValid.isValid() ? lastValid : (lastDoc.isValid() ? lastDoc : (history.end().isValid() ? history.end() : QDate::currentDate())));
    }
    else
    {
        DiseaseHistory history;
        d->historyModel->setHistory(history);
        d->visualHistoryWidget->setHistory(history);
        d->tnmEdit->setText(QString());
        d->primaryTumorLocationBox->reset();
        d->resectabilityStatusBox->reset();
        d->initialDiagnosisEdit->setDate(QDate::currentDate());
        d->lastDocumentation->setChecked(false);
        d->lastDocumentationDate->setDate(QDate::currentDate());
        d->lastValidation->setChecked(false);
        d->lastDocumentationDate->setDate(QDate::currentDate());
    }
    d->patientDisplay->setPatient(d->currentPatient);
    if (d->catoWidget->isVisible())
    {
        d->catoWidget->setPatient(d->currentPatient ? *d->currentPatient : Patient());
    }
    // patientEditable() may have changed
    applyReadOnlyStatus();
}

void HistoryWindow::applyReadOnlyStatus()
{
    if (d->currentWidget)
    {
        d->currentWidget->setReadOnly(d->readOnly || !d->lock.patientEditable());
    }
    d->initialDiagnosisEdit->setReadOnly(d->readOnly || !d->lock.patientEditable());
    d->tnmEdit->setReadOnly(d->readOnly || !d->lock.patientEditable());
    d->primaryTumorLocationBox->setEnabled(!d->readOnly && d->lock.patientEditable());
    d->resectabilityStatusBox->setEnabled(!d->readOnly && d->lock.patientEditable());
    d->addBar->setEnabled(d->currentPatient && !d->readOnly && d->lock.patientEditable());
    d->lastDocumentation->setEnabled(!d->readOnly && d->lock.patientEditable());
    d->lastValidation->setEnabled(!d->readOnly && d->lock.patientEditable());
    d->lastDocumentationDate->setReadOnly(d->readOnly || !d->lock.patientEditable());
    d->lastValidationDate->setReadOnly(d->readOnly || !d->lock.patientEditable());
    d->clearButton->setEnabled(d->currentElement && !d->readOnly && d->lock.patientEditable());
}

void HistoryWindow::historyElementActivated(const QModelIndex& index)
{
    HistoryElement* elem = DiseaseHistoryModel::retrieveElement(index);
    while (elem && elem->parent())
    {
        elem = elem->parent();
    }
    //qDebug() << "history element activated" << elem;
    setCurrentElement(elem);
}

void HistoryWindow::setCurrentElement(HistoryElement* e)
{
    if (d->currentElement && d->currentWidget)
    {
         d->currentWidget->applyToElement();
         delete d->currentWidget;
         d->currentWidget = 0;
    }

    d->currentElement = e;
    if (e)
    {
        d->currentWidget = HistoryElementEditWidget::create(e);
        if (d->currentWidget)
        {
            d->editWidgetLayout->addWidget(d->currentWidget);
            connect(d->currentWidget, SIGNAL(changed()), this, SLOT(currentElementChanged()));
            if (d->currentElement->is<Therapy>())
            {
                connect(d->currentWidget, SIGNAL(addTherapyElement(TherapyElement*)),
                        this, SLOT(currentElementAddTherapyElement(TherapyElement*)));
                connect(d->currentWidget, SIGNAL(therapyElementChanged(TherapyElement*)),
                        this, SLOT(currentElementTherapyElementChanged(TherapyElement*)));
                connect(d->currentWidget, SIGNAL(therapyElementRemove(TherapyElement*)),
                        this, SLOT(currentElementTherapyElementRemove(TherapyElement*)));
            }
            d->currentWidget->setFocus();
            d->currentWidget->setReadOnly(d->readOnly || !d->lock.patientEditable());
        }
    }
    d->clearButton->setEnabled(d->currentElement && !d->readOnly && d->lock.patientEditable());
    d->heading->setText(d->currentWidget ? d->currentWidget->heading() : QString());
    if (d->currentElement)
    {
        QModelIndex index = d->sortModel->mapFromSource(d->historyModel->index(d->currentElement));
        //d->historyView->expand(index);
        //d->historyView->selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect);
        d->historyView->setCurrentIndex(index);
        d->visualHistoryWidget->setCursor(d->currentElement->date);
    }
    else
    {
        d->historyView->setCurrentIndex(QModelIndex());
        d->visualHistoryWidget->setCursor(QDate());
    }
}

void HistoryWindow::clearCurrentElement()
{
    if (d->currentElement)
    {
        d->historyModel->takeElement(d->currentElement);
        d->proofReader->elementWasRemoved(d->currentElement);
        delete d->currentElement;
        d->currentElement = 0;
        delete d->currentWidget;
        d->currentWidget = 0;
        setCurrentElement(DiseaseHistoryModel::retrieveElement(d->historyView->currentIndex()));
    }
}

static void addToolbarMenuAction(QMenu* menu, const QIcon& icon, const QString& text, const QVariant& userData)
{
    QAction* action = menu->addAction(icon, text);
    action->setData(userData);
}

void HistoryWindow::setupView()
{
    d->viewSplitter = new QSplitter(Qt::Horizontal, this);

    // Patient list view
    QWidget* firstWidget = new QWidget;
    QVBoxLayout* firstWidgetLayout = new QVBoxLayout;
    d->patientView = new HistoryPatientListView;
    d->patientView->setAdapter(adapter());
    d->colorProvider = new HistoryColorProvider;
    adapter()->model()->installRoleDataProvider(Qt::BackgroundRole, d->colorProvider);
    adapter()->filterModel()->setFilterCaseSensitivity(Qt::CaseInsensitive);
    ModelFilterLineEdit* searchBar = new ModelFilterLineEdit(d->patientView);
    connect(d->patientView, SIGNAL(currentChanged(Patient::Ptr)), this, SLOT(setCurrentPatient(Patient::Ptr)));
    connect(d->patientView, SIGNAL(currentChanged(Patient::Ptr)), this, SIGNAL(activated(Patient::Ptr)));
    firstWidgetLayout->addWidget(d->patientView, 1);
    firstWidgetLayout->addWidget(searchBar);
    firstWidget->setLayout(firstWidgetLayout);

    // History Tree View
    QWidget* secondWidget = new QWidget;
    QVBoxLayout* secondWidgetLayout = new QVBoxLayout;
    d->historyModel = new DiseaseHistoryModel(this);
    d->historyView  = new QTreeView;
    d->sortModel    = new HistoryElementSortModel(this);
    d->sortModel->setSourceModel(d->historyModel);
    d->historyView->setModel(d->sortModel);
    d->sortModel->sort(0);
    connect(d->historyView, SIGNAL(clicked(QModelIndex)), this, SLOT(historyElementActivated(QModelIndex)));
    connect(d->historyModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(slotHistoryChanged()));
    connect(d->historyModel, SIGNAL(rowsAboutToBeRemoved(QModelIndex,int,int)), this, SLOT(slotHistoryAboutToChange()));
    connect(d->historyModel, SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SLOT(slotHistoryChanged()));
    connect(d->historyModel, SIGNAL(rowsAboutToBeInserted(QModelIndex,int,int)), this, SLOT(slotHistoryAboutToChange()));
    connect(d->historyModel, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(slotHistoryChanged()));
    secondWidgetLayout->addWidget(d->historyView, 2);
    // CATO widget
    d->catoWidget = new CatoReportsWidget;
    connect(d->catoWidget, &CatoReportsWidget::activated, this, &HistoryWindow::catoMedicationActivated);
    secondWidgetLayout->addWidget(d->catoWidget, 1);
    secondWidget->setLayout(secondWidgetLayout);

    d->proofOutput = new QListWidget;
    d->proofReader = new HistoryWindowProofReadOutput(d->proofOutput);
    connect(d->proofOutput, SIGNAL(itemClicked(QListWidgetItem*)),
            this, SLOT(proofItemClicked(QListWidgetItem*)));

    // Main (editing) panel
    QWidget* mainPanel = new QWidget;
    d->mainPanelLayout = new QVBoxLayout;
    d->mainPanelLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);

    QScrollArea* visualHistoryWidgetScrollArea = new QScrollArea;
    d->visualHistoryWidget = new VisualHistoryWidget;
    d->visualHistoryWidget->setProofReader(d->proofReader);
    visualHistoryWidgetScrollArea->setWidget(d->visualHistoryWidget);
    visualHistoryWidgetScrollArea->setWidgetResizable(true);
    visualHistoryWidgetScrollArea->setFrameStyle(QFrame::NoFrame);
    d->mainPanelLayout->addWidget(visualHistoryWidgetScrollArea);
    connect(d->visualHistoryWidget, (void (VisualHistoryWidget::*)(HistoryElement*))(&VisualHistoryWidget::clicked), this, &HistoryWindow::visualHistoryClicked);

    d->patientDisplay = new PatientDisplay;
    d->patientDisplay->setShowGender(false);
    d->mainPanelLayout->addWidget(d->patientDisplay);

    QFormLayout* idAndTnmLayout = new QFormLayout;
    idAndTnmLayout->setLabelAlignment(Qt::AlignLeft);
    d->initialDiagnosisEdit = new QDateEdit;
    idAndTnmLayout->addRow(tr("Erstdiagnose:"), d->initialDiagnosisEdit);
    d->tnmEdit = new QLineEdit;
    idAndTnmLayout->addRow(tr("TNM (initial):"), d->tnmEdit);

    d->primaryTumorLocationBox = new PrimaryTumorLocationBox;
    idAndTnmLayout->addRow(tr("Primärtumorlokalisation"), d->primaryTumorLocationBox);
    connect(d->primaryTumorLocationBox, &PrimaryTumorLocationBox::visibilityChanged,
            idAndTnmLayout->labelForField(d->primaryTumorLocationBox), &QWidget::setVisible);

    d->resectabilityStatusBox = new ResectabilityStatusBox;
    idAndTnmLayout->addRow(tr("Resektabilität:"), d->resectabilityStatusBox);
    connect(d->resectabilityStatusBox, &ResectabilityStatusBox::visibilityChanged,
            idAndTnmLayout->labelForField(d->resectabilityStatusBox), &QWidget::setVisible);

    d->lastDocumentation = new QCheckBox(tr("Letzte Doku"));
    d->lastDocumentationDate = new QDateEdit;
    d->lastDocumentationDate->setEnabled(false);
    QPushButton* lastDocumentationTodayButton = new QPushButton(tr("Heute"));
    connect(d->lastDocumentation, SIGNAL(toggled(bool)), d->lastDocumentationDate, SLOT(setEnabled(bool)));
    connect(d->lastDocumentation, SIGNAL(toggled(bool)), lastDocumentationTodayButton, SLOT(setEnabled(bool)));
    connect(d->lastDocumentation, SIGNAL(toggled(bool)), this, SLOT(slotLastDocumentationDateChanged()));
    connect(d->lastDocumentationDate, SIGNAL(dateChanged(QDate)), this, SLOT(slotLastDocumentationDateChanged()));
    connect(lastDocumentationTodayButton, &QPushButton::clicked, this, &HistoryWindow::slotLastDocumentationSetToCurrentDate);
    QHBoxLayout* lastDocHBox = new QHBoxLayout;
    lastDocHBox->addWidget(d->lastDocumentationDate, 1);
    lastDocHBox->addWidget(lastDocumentationTodayButton);
    idAndTnmLayout->addRow(d->lastDocumentation, lastDocHBox);

    d->lastValidation = new QCheckBox(tr("Validiert"));
    d->lastValidationDate = new QDateEdit;
    d->lastValidationDate->setEnabled(false);
    QPushButton* lastValidationTodayButton = new QPushButton(tr("Heute"));
    connect(d->lastValidation, SIGNAL(toggled(bool)), d->lastValidationDate, SLOT(setEnabled(bool)));
    connect(lastValidationTodayButton, &QPushButton::clicked, this, &HistoryWindow::slotLastValidationSetToCurrentDate);
    QHBoxLayout* lastValHBox = new QHBoxLayout;
    lastValHBox->addWidget(d->lastValidationDate, 1);
    lastValHBox->addWidget(lastValidationTodayButton);
    idAndTnmLayout->addRow(d->lastValidation, lastValHBox);

    d->mainPanelLayout->addLayout(idAndTnmLayout);

    // Item Tool bar
    d->addBar = new QToolBar;
    //d->addBar->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    //d->addBar->setOrientation(Qt::Vertical);
    d->addBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    d->addBar->addAction(QIcon::fromTheme("pill"), tr("Chemo"), this, SLOT(addChemotherapy()));
    QAction* therapyAction = d->addBar->addAction(QIcon::fromTheme("basket"), tr("Therapie..."));
    d->addBar->addAction(QIcon::fromTheme("heart_add"), tr("Befund"), this, SLOT(addFinding()));
    d->addBar->addAction(QIcon::fromTheme("chart_bar"), tr("Zustand"), this, SLOT(addDiseaseState()));

    QToolButton* therapyButton = static_cast<QToolButton*>(d->addBar->widgetForAction(therapyAction));
    therapyButton->setPopupMode(QToolButton::InstantPopup);
    QMenu* therapyMenu = new QMenu;
    addToolbarMenuAction(therapyMenu, QIcon::fromTheme("transmit"), tr("Radiotherapie"), Therapy::RTx);
    addToolbarMenuAction(therapyMenu, QIcon::fromTheme("transmit_add"), tr("Radiochemotherapie"), Therapy::RCTx);
    addToolbarMenuAction(therapyMenu, QIcon::fromTheme("pencil"), tr("Operation"), Therapy::Surgery);
    addToolbarMenuAction(therapyMenu, QIcon::fromTheme("wrench"), tr("Intervention"), Therapy::Intervention);
    connect(therapyMenu, SIGNAL(triggered(QAction*)), this, SLOT(addTherapy(QAction*)));
    therapyButton->setMenu(therapyMenu);
    d->mainPanelLayout->addWidget(d->addBar);

    QHBoxLayout* headingLayout = new QHBoxLayout;
    d->heading = new QLabel;
    QFont f = font();
    f.setBold(true);
    f.setPixelSize(20);
    d->heading->setFont(f);
    d->clearButton = new QPushButton(QIcon::fromTheme("delete"), tr("Löschen"));
    connect(d->clearButton, SIGNAL(clicked()), this, SLOT(clearCurrentElement()));
    headingLayout->addWidget(d->heading);
    headingLayout->addStretch(1);
    headingLayout->addWidget(d->clearButton);
    d->mainPanelLayout->addLayout(headingLayout);

    d->editWidgetLayout = new QVBoxLayout;
    d->mainPanelLayout->addLayout(d->editWidgetLayout);
    d->mainPanelLayout->addStretch(1);

    d->mainPanelLayout->addWidget(d->proofOutput);

    mainPanel->setLayout(d->mainPanelLayout);
    QScrollArea* scrollArea = new QScrollArea;
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(mainPanel);

    d->viewSplitter->addWidget(firstWidget);
    d->viewSplitter->addWidget(secondWidget);
    d->viewSplitter->addWidget(scrollArea);
    //d->viewSplitter->addWidget(mainPanel);
    setCentralWidget(d->viewSplitter);

    d->viewSplitter->setSizes(QList<int>() << 250 << 250 << 500);
}

void HistoryWindow::addChemotherapy()
{
    addTherapy(Therapy::CTx);
}

void HistoryWindow::addFinding()
{
    QDate date = dateForNewElement();
    Finding* f = new Finding;
    f->date = date;
    d->historyModel->addElement(f);
    setCurrentElement(f);
}

void HistoryWindow::addDiseaseState()
{
    QDate date = dateForNewElement();
    DiseaseState* s = new DiseaseState;
    s->date = date;
    d->historyModel->addElement(s);
    setCurrentElement(s);
}

void HistoryWindow::addTherapy(QAction* action)
{
    addTherapy((Therapy::Type)action->data().toInt());
}

QDate HistoryWindow::dateForNewElement() const
{
    QDate date;
    if (d->currentElement)
    {
        date = d->currentElement->date;
        if (d->currentElement->is<Therapy>())
        {
            QDate end = d->currentElement->as<Therapy>()->end;
            if (end.isValid())
            {
                date = end;
            }
        }
    }
    if (!date.isValid())
    {
        date = currentHistory().latestDate();
    }
    if (!date.isValid())
    {
        date = QDate::currentDate();
    }
    return date;
}

void HistoryWindow::addTherapy(Therapy::Type type)
{
    QDate date = dateForNewElement();

    Therapy* t = new Therapy;
    t->type = type;
    t->date = date;

    TherapyElement* elem = 0;

    switch (type)
    {
    case Therapy::CTx:
        elem = new Chemotherapy;
        break;
    case Therapy::RTx:
        elem = new Radiotherapy;
        break;
    case Therapy::RCTx:
        elem = new Radiotherapy;
        break;
    case Therapy::Surgery:
    case Therapy::Intervention:
        break;
    }

    if (elem)
    {
        t->elements << elem;
    }
    d->historyModel->addElement(t);
    setCurrentElement(t);
}

void HistoryWindow::currentElementChanged()
{
    //qDebug() << "Current element changed";
    if (!d->currentWidget || !d->currentElement)
    {
        return;
    }
    d->currentWidget->applyToElement();
    d->historyModel->elementChanged(d->currentElement);
}

void HistoryWindow::currentElementAddTherapyElement(TherapyElement* te)
{
    d->historyModel->addElement(d->currentElement, te);
}

void HistoryWindow::currentElementTherapyElementChanged(TherapyElement* te)
{
    //qDebug() << "Current therapy element changed";
    d->historyModel->elementChanged(te);
}

void HistoryWindow::currentElementTherapyElementRemove(TherapyElement* te)
{
    TherapyEditWidget* tew = qobject_cast<TherapyEditWidget*>(d->currentWidget);
    if (!tew)
    {
        qDebug() << "Bug! TEW is not a TEW";
        return;
    }
    d->historyModel->takeElement(te);
    tew->removeElementUI(te);
    d->proofReader->elementWasRemoved(te);
    delete te;
}

void HistoryWindow::proofItemClicked(QListWidgetItem *item)
{
    HistoryElement* e = item->data(Qt::UserRole).value<HistoryElement*>();
    if (!e)
    {
        return;
    }
    QModelIndex index = d->sortModel->mapFromSource(d->historyModel->index(e));
    d->patientView->setCurrentIndex(index);
    historyElementActivated(index);
}

void HistoryWindow::slotHistoryAboutToChange()
{
    d->visualHistoryWidget->setHistory(DiseaseHistory());
}

void HistoryWindow::slotHistoryChanged()
{
    d->visualHistoryWidget->setHistory(d->historyModel->history());
    if (d->currentElement)
    {
        d->visualHistoryWidget->setCursor(d->currentElement->date);
    }
}

void HistoryWindow::slotLastDocumentationDateChanged()
{
    d->visualHistoryWidget->updateLastDocumentation(
                d->lastDocumentation->isChecked() ? d->lastDocumentationDate->date() : QDate());
    if (d->lastDocumentation->isChecked() && !d->lastValidation->isChecked())
    {
        d->lastValidationDate->setDate(d->lastDocumentationDate->date());
    }
}

void HistoryWindow::slotLastDocumentationSetToCurrentDate()
{
    d->lastDocumentationDate->setDate(QDate::currentDate());
}

void HistoryWindow::slotLastValidationSetToCurrentDate()
{
    d->lastValidationDate->setDate(QDate::currentDate());
}

void HistoryWindow::catoMedicationActivated(const QDate &date, const QString &medication)
{
    if (d->currentWidget)
    {
        TherapyEditWidget* ew = qobject_cast<TherapyEditWidget*>(d->currentWidget);
        if (ew)
        {
            ew->catoMedicationActivated(date, medication);
        }
    }
}

void HistoryWindow::requestMainWindowActivated()
{
    emit mainWindowRequested(d->currentPatient);
}

void HistoryWindow::visualHistoryClicked(HistoryElement* e)
{
    if (e)
    {
        setCurrentElement(e);
    }
}

void HistoryWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_F10)
    {
        QModelIndex next = d->patientView->indexBelow(d->patientView->currentIndex());
        if (next.isValid())
        {
            d->patientView->setCurrentIndex(next);
        }
    }
    if (e->key() == Qt::Key_F9)
    {
        QModelIndex prev = d->patientView->indexAbove(d->patientView->currentIndex());
        if (prev.isValid())
        {
            d->patientView->setCurrentIndex(prev);
        }
    }
}
