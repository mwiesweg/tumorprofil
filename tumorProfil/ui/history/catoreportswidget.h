/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 22.05.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef CATOREPORTSWIDGET_H
#define CATOREPORTSWIDGET_H

#include <QWidget>

#include "patient.h"

class QListWidgetItem;
class QTableWidgetItem;

class CatoReportsWidget : public QWidget
{
    Q_OBJECT
public:
    CatoReportsWidget();
    ~CatoReportsWidget();

public slots:

    void setPatient(const Patient& patientData);

signals:

    void activated(const QDate& date, const QString& medication);

protected slots:

    void currentPatientChanged(QListWidgetItem*);
    void medicationActivated(QTableWidgetItem*);

private:

    class CatoReportsWidgetPriv;
    CatoReportsWidgetPriv* const d;
};

#endif // CATOREPORTSWIDGET_H
