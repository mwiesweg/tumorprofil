/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 25.01.2018
 *
 * Copyright (C) 2018 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef RESULTWAITDIALOG_H
#define RESULTWAITDIALOG_H

#include <QFutureWatcher>
#include <QProgressDialog>

class ResultWaitDialog : public QProgressDialog
{
public:    
    ResultWaitDialog(QWidget* parent, const QString &actionTitle, bool allowCancel, Qt::WindowModality windowModality = Qt::ApplicationModal);

    bool waitForResult(QFutureWatcherBase *watcher);

    // assumes the same signal signature as a QFutureWatcher. But you must first call connectTo, then start the computation, then waitForResult().
    void connectTo(QObject *obj);
    bool waitForResult();

    template <typename T>
    bool waitForResult(const QFuture<T>& future)
    {
        QFutureWatcher<T> watcher;
        watcher.setFuture(future);
        return waitForResult(&watcher);
    }
};

#endif // RESULTWAITDIALOG_H
