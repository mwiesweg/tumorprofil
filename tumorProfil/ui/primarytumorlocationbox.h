/* ============================================================
 *
 * This file is a part of Tumorprofil
 *
 * Date        : 08.11.2017
 *
 * Copyright (C) 2017 by Marcel Wiesweg <marcel dot wiesweg at uk-essen dot de>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */
#ifndef PRIMARYTUMORLOCATIONBOX_H
#define PRIMARYTUMORLOCATIONBOX_H

#include <QComboBox>

#include "disease.h"
#include "property.h"

class PrimaryTumorLocationBox : public QComboBox
{
    Q_OBJECT
public:

    PrimaryTumorLocationBox(QWidget* parent = 0);

    bool changed() const;

public slots:

    void save(Disease& disease) const;
    void load(const Disease& disease);
    void reset();

signals:

    void visibilityChanged(bool visible);

protected:

    int m_initialIndex;
};

#endif // PRIMARYTUMORLOCATIONBOX_H
