TEMPLATE = subdirs

tumorProfil.subdir = tumorProfil
tumorUsers.subdir = tumorProfil/TumorUsers
SUBDIRS = tumorProfil \
          tumorUsers

win32|!exists(/usr/include/libcryptopp/aes.h)
{
    tumorProfil.depends = cryptopp563
    tumorUsers.depends = cryptopp563
    SUBDIRS += cryptopp563
}
